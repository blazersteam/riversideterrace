<?php get_header(); ?>
<?php 
	$erro_page_bg_color = houserent_theme_get_customizer_field('404_page_bg_color','');
	if( $erro_page_bg_color ){
		$bg_gradiant = "background: ".esc_attr( $erro_page_bg_color['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $erro_page_bg_color['primary'] )." 0%, ".esc_attr( $erro_page_bg_color['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $erro_page_bg_color['primary'] )." 0%, ".esc_attr( $erro_page_bg_color['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $erro_page_bg_color['primary'] )." 0%, ".esc_attr( $erro_page_bg_color['secondary'] )." 100%);";
	} else {
		$bg_gradiant = '';
	}
?>
	<div class="error-page-area">
		<div class="container-fluid pd-zero">
			<div class="default-template-gradient default-pd-center" style="<?php echo esc_attr( $bg_gradiant ); ?>">
				<div class="row">
					<div class="col-md-12">
						<div class="error-text-content">
							<h2 class="error-title"><?php esc_html_e( '404', 'houserent' ); ?></h2>
							<p class="error-description"><?php esc_html_e( 'Page not pound', 'houserent' ); ?></p>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="button nevy-blue-bg">
							    <?php esc_html_e( 'go to home', 'houserent' ) ?>
							</a>
						</div><!-- /.text-content -->
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.main-slider-->
		</div><!-- /.container-fluid -->           
	</div><!-- /.slider-area container-fluid -->
<?php get_footer(); ?>