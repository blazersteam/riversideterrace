
<?php get_header(); ?>

<div class="page-header default-template-gradient">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                
                <h2 class="page-title">
                    <?php esc_html_e( 'Browsing Category', 'houserent' ); ?>
                </h2>
                <p class="page-description">
                    <?php echo single_cat_title( '', false ); ?>
                </p>        
            </div><!-- /.col-md-12 -->
        </div><!-- /.row-->
    </div><!-- /.container-fluid -->           
</div>

<?php if( function_exists( 'fw_ext_get_breadcrumbs' )): ?>
    <div class="breadcrumbs-area">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                   <?php echo fw_ext_get_breadcrumbs( '>' ); ?>
               </div><!-- /.col-md-12 -->
           </div><!-- /.row -->
       </div><!-- /.container -->
   </div>
<?php endif; ?>
<!-- ======Apartments-area======== --> 
<div class="apartments-area four bg-gray-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="apartment-tab-area">
                    <div class="row">
                        <?php 
                            if ( have_posts() ){
                                $i = 1; 
                                while ( have_posts() ) : the_post();
                                    get_template_part( 'template-parts/content', 'rental' );
                                    if( $i % 3 == 0 ):
                                        echo '<div class="clearfix"></div>';
                                    endif; 
                                    $i++;
                                endwhile;
                            } else {
                                get_template_part( 'template-parts/content', 'none-rental' );
                            }
                        ?>
                    </div><!-- /.row -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            <?php houserent_theme_posts_pagination_nav(); ?>
                        </div> <!-- /.col-md-12 -->
                    </div>  <!-- /.row -->

                </div><!-- /.apartment-tab-area -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.Apartments-area-->
<?php get_footer(); ?>