
<!-- ======footer-area================================== --> 
<?php
   // $default_image =  get_template_directory_uri().'/assets/images/backend/footer/footer-bg.png';
    $footer_bg_image = houserent_theme_get_customizer_field('footer_bg_image', '' );
    $footer_bg_image = ( $footer_bg_image ) ? $footer_bg_image['url'] : $default_image ;
    $footer_bg_color = houserent_theme_get_customizer_field('footer_bg_color', '#ffffff' );

?>
<footer class="footer-area" style="background-image:url(<?php echo esc_url( $footer_bg_image );?> ); background-color: <?php echo esc_attr($footer_bg_color); ?>">
    <div class="container">
        <div class="row">
            <?php
            // show footer widget with condition
            $columns = intval( houserent_theme_footer_widgets_columns() );
            $col_class = 12 / max( 1, $columns );
            $col_class_sm = 12 / max( 1, $columns );
            if ( $columns == 4 ) {
                $col_class_sm = 6;
            } 
            $col_class = "col-sm-$col_class_sm col-md-$col_class";
               for ( $i = 1; $i <= $columns ; $i++ ) {
                if ( $columns == 3 ) :
                    if ( $i == 3 ) {
                        $col_class = "col-sm-12 col-md-$col_class";
                    } else {
                        $col_class = "col-sm-6 col-md-$col_class";
                    } 
                endif; ?>
                <div class="widget-area sidebar-footer-<?php echo esc_attr($i) ?> <?php echo esc_attr( $col_class ) ?>">
                    <?php if ( is_active_sidebar( 'sidebar-footer-'.$i ) ) { ?>
                    <?php dynamic_sidebar( 'sidebar-footer-'.$i ); ?>
                    <?php } ?>
                </div>
                <?php } ?>

        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="bottom-content">
                    <p><?php echo houserent_theme_get_customizer_field('copyright_text', wp_kses( __('&copy; 2017 <b>HouseRent WP Theme</b> -- Made with love by <a href="#"><b>SoftHopper.net</b></a>', 'houserent'), houserent_theme_html_allow() ) ); ?></p>
                </div><!-- /.bottom-top-content -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</footer><!-- /.footer-area -->
    <?php wp_footer(); ?>
    <script>
    jQuery( document ).ready(function() {
        jQuery("#menu-primary-menu .menu-item-type-custom a").attr("download","");
    });
    </script>
</body>
</html>