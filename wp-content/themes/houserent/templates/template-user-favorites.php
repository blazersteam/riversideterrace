<?php
/**
 * Template Name: User - Favorites
 */
get_header(); ?>
<!-- ====== Page Header ====== --> 
<div class="page-header default-template-gradient">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                
                <h2 class="page-title"><?php esc_html_e('Your Favorites','houserent'); ?></h2>
            </div>
        </div><!-- /.row-->
    </div><!-- /.container-fluid -->           
</div><!-- /.page-header -->

<div class="apartments-area clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<div class="product-user-favourites">
            	    <?php 
                    if( is_user_logged_in() ) {
            	        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                        $fav_ids = unserialize( get_user_meta( get_current_user_id(), 'houserent_user_favorites', true ) );
                        if(!empty($fav_ids)) {
                            $query_args = array(
                               'post_type' => 'rental',
                               'paged'     => $paged,
                               'posts_per_page' => 6, //sh_hr_get_customizer_field('product_fav_per_page')
                               'post__in' => $fav_ids, 
                            );
                            $i = 1;
                            $loop = new WP_Query($query_args);
                	        while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                <?php
                                    $rental_price_conditions = get_post_meta( get_the_ID(),'rental_price_conditions')[0];
                                    $rental_bedrooms = get_post_meta( get_the_ID(),'rental_bedrooms')[0];
                                    $rental_baths = get_post_meta( get_the_ID(),'rental_baths')[0];
                                    $rental_location = get_post_meta( get_the_ID(),'rental_location')[0];
                                    $rental_gallery_item_images = houserent_theme_uny_post_meta('rental_gallery_item_images','');
                                    $views_count = get_post_meta( get_the_ID(),'houserent_theme_post_views_count')[0];
                                    $rental_cat = get_the_terms( get_the_ID(), 'rental_cat' );
                                ?>
   
                                <div class="col-md-4 col-sm-6">
                                    <div class="apartments-content">

                                        <?php if ( has_post_thumbnail() ): ?>
                                            <div class="image-content">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php  
                                                        echo houserent_theme_get_featured_img(360, 270, false, 'true');
                                                    ?>
                                                </a>
                                            </div><!-- /.image-content -->
                                        <?php endif; ?>
                                        
                                        <div class="text-content">
                                            <div class="top-content">
                                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                <?php if( $rental_location ): ?>
                                                <span>
                                                    <i class="fa fa-map-marker"></i> 
                                                    <?php echo esc_html( $rental_location ); ?>
                                                </span> 
                                                <?php endif; ?>

                                                <?php if( $rental_cat ): ?>
                                                <span>
                                                    <i class="fa fa-home"></i> 
                                                    <?php 
                                                        if ( $rental_cat && ! is_wp_error( $rental_cat ) ) : 
                                                            foreach ( $rental_cat as $term ) {
                                                                $term_link = get_term_link( $term );
                                                                ?>
                                                                    <a href="<?php echo esc_url( $term_link ); ?>"><?php echo esc_html( $term->name ); ?></a>
                                                                <?php
                                                            }
                                                        endif; 
                                                    ?>
                                                </span> 
                                                <?php endif; ?>
                                            </div><!-- /.top-content -->

                                            <div class="bottom-content clearfix">
                                            <?php if( $rental_bedrooms ): ?>
                                                <div class="meta-bed-room">
                                                    <i class="fa fa-bed"></i>
                                                    <?php 
                                                        echo esc_html( $rental_bedrooms );
                                                        $room_singular = ( $rental_bedrooms <= 1 ) ? esc_html__( ' Bedroom', 'houserent' ) : esc_html__( ' Bedrooms', 'houserent' ) ;
                                                        echo esc_html( $room_singular );
                                                    ?>
                                                </div>
                                            <?php endif;
                                                if( $rental_baths ):
                                            ?>
                                                <div class="meta-bath-room">
                                                    <i class="fa fa-bath"></i>
                                                    <?php 
                                                        echo esc_html( $rental_baths );
                                                        $room_baths = ( $rental_baths <= 1 ) ? esc_html__( ' Bathroom', 'houserent' ) : esc_html__( ' Bathrooms', 'houserent' ) ;
                                                        echo esc_html( $room_baths );
                                                    ?>
                                                </div>
                                            <?php endif; ?>
                                                <span class="clearfix"></span>
                                            <?php if( $rental_price_conditions ): ?>
                                                <div class="rent-price pull-left">
                                                    <?php 
                                                        if( $rental_price_conditions ):
                                                        echo esc_html( houserent_theme_currency() .''. ucfirst( $rental_price_conditions) );
                                                        ?>
                                                        <span><?php echo ( isset( get_post_meta( get_the_ID(),'rental_price_duration')[0] ) ) ? get_post_meta( get_the_ID(),'rental_price_duration')[0] : ' '; ?></span>
                                                        <?php endif; 
                                                    ?>
                                                </div>
                                            <?php endif; ?>
                                                <div class="share-meta dropup pull-right">
                                                    <?php houserent_theme_rental_share(); ?>
                                                </div>
                                            </div><!-- /.bottom-content -->
                                        </div><!-- /.text-content -->
                                    </div><!-- /.partments-content -->
                                </div><!-- /.col-md-4 -->

                    	        <?php if( $i % 3 == 0 ): ?>
                    	           <div class="clearfix"></div>
                    	        <?php endif; $i++;
                	        endwhile; 
                	        wp_reset_postdata(); ?>
                            <div class="col-md-12">
                                <?php houserent_theme_posts_pagination_nav($loop); ?>
                            </div> <!-- /.col-md-12 -->
                            <?php 
                        } else {
                            echo "<div class='alert alert-info'>".esc_html__('You have not added any favorite product yet', 'houserent')."</div>";
                        }
            	    ?>
                    <?php } else { ?> 
                        <p class="alert alert-info"><?php esc_html_e( 'You have to be logged-in to see this page content.', 'houserent' ); ?></p> 
                    <?php } ?>
            	</div><!-- /.row -->
            </div><!-- /.col-md-12 --> 
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <?php houserent_theme_posts_pagination_nav(); ?>
            </div> <!-- /.col-md-12 -->
        </div>  <!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.favorite-area-->
<?php get_footer(); ?>