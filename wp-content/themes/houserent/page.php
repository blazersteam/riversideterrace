<?php get_header(); ?>
	<div class="page-content">
		<?php
		    $page_builder = false;

		    if ( function_exists( 'fw_ext_page_builder_is_builder_post' ) ) {
		        $page_builder = ( fw_ext_page_builder_is_builder_post( $post->ID ) == true ) ? true : false;
		    }
		    
		    if ( $page_builder == 1 ) {
	            if ( have_posts() ) : 
	                while ( have_posts() ) : the_post(); 
	                echo '<div class="page-builder-content">';
	                    the_content();
	                echo '</div>';
	                endwhile; 
	            endif;
		    } else {
		?>
			<?php 

				$page_breadcrumbs = houserent_theme_get_customizer_field('page_breadcrumbs','hide');
				$show_page_title = houserent_theme_get_customizer_field('show_page_title','show');
				$page_social_share = houserent_theme_get_customizer_field('page_social_share','hide');

				$layout_page_url = ( isset($_GET["layout-page"]) ) ? $_GET["layout-page"]  : "";
				$genarel_page_meta = houserent_theme_uny_post_meta('meta_page_layout','default');
				$theme_settings_panel = houserent_theme_get_customizer_field('genarel_post_page_layout','full_width');
				
				if ( $layout_page_url ) {
				    $genarel_page_sidebar_condition = $layout_page_url;
				} elseif ( $genarel_page_meta != 'default' ) {
				    $genarel_page_sidebar_condition = $genarel_page_meta;
				} elseif ( $theme_settings_panel ) {
				    $genarel_page_sidebar_condition = $theme_settings_panel;
				} else {
				    $genarel_page_sidebar_condition = 'full_width';
				}

				switch ( $genarel_page_sidebar_condition ) {
				    case 'sidebar_left':
				        $page_content_class = 'col-md-8 col-md-push-4';
				        $page_class = 'left-sidebar';
				        break;

				    case 'sidebar_right':
				        $page_content_class = 'col-md-8';
				        $page_class = 'right-sidebar';
				        break;
				    
				    default:
				        $theme_settings_column_width = houserent_theme_get_customizer_field('genarel_post_page_column_width','12');
				        $page_content_class = 'col-md-'.$theme_settings_column_width.' full-width-content';
				        $page_class = 'fullwidth';
				        break;
				}
			?>
			<?php if( function_exists( 'fw_ext_get_breadcrumbs' ) && $page_breadcrumbs == 'show' ): ?>
				<div class="breadcrumbs-area">
			       <div class="container">
			           <div class="row">
			               <div class="col-md-12">
			                   <?php echo fw_ext_get_breadcrumbs( '>' ); ?>
			               </div><!-- /.col-md-12 -->
			           </div><!-- /.row -->
			       </div><!-- /.container -->
			   </div>
			<?php endif; ?>


			<div class="single-content bg-gray-color">
				<div class="container">
					<div class="row">
						<div class="<?php echo esc_attr( $page_content_class ); ?>">
							<div class="single-main-content">
								<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
									<article class="post">
										<?php if( $show_page_title == "show" ): ?>
											<div class="entry-header">
												<h2 class="entry-title"><?php the_title(); ?></h2>
											</div><!-- /.entry-header -->
										<?php endif; ?>
										
										<?php if ( has_post_thumbnail() ): ?>
										    <figure class="post-thumb">
										        <?php
									                echo houserent_theme_get_featured_img(1140, 450, false, 'true');
									            ?>
									    	</figure><!-- /.post-thumb -->
										<?php endif; ?>

									    <div class="entry-content">
									    	<?php the_content(); ?>
									    </div><!-- /.entry-content -->

									    <?php 
									    	( $page_social_share == 'show' ) ? houserent_theme_social_share_link() : '';
									    ?>
									</article><!-- /.post -->
								<?php endwhile; endif; ?>
							</div><!-- /.single-main-content -->
						</div><!-- /.col-md-12 -->
						<?php get_sidebar(); ?>
					</div><!-- /.row -->
				</div><!-- /.container -->
			</div><!-- /.single-content -->
			<!-- Comment-Area-->
			<?php 
			    // If comments are open or we have at least one comment, load up the comment template.
			    if ( comments_open() || get_comments_number() ) {
			        comments_template();
			    }
			?>
			<!--/.comment-respond-->
	  	<?php } ?>
  	</div>
<?php get_footer(); ?>