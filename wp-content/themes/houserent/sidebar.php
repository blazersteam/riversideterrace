<?php

    if ( is_home() || is_archive() || is_search() ) {

        $blog_layout_url = (isset( $_GET['blog-layout'] ) ) ? esc_html( $_GET['blog-layout'] ) : '';
        $blog_layout = ( $blog_layout_url ) ? $blog_layout_url :  $blog_layout_page_layout_sec = houserent_theme_get_customizer_field('blog_layout_page_layout_sec','blog_layout_page_layout','sidebar_right') ;

    } elseif( is_single() ) {

        $single_layout = ( isset($_GET["single-layout"]) ) ? $_GET["single-layout"]  : "";
        $single_page_meta = houserent_theme_uny_post_meta('meta_post_layout','default');
        $theme_settings_panel = houserent_theme_get_customizer_field('single_post_page_layout','full_width');
        
        if( $single_layout ){
            $blog_layout = $single_layout;
        } elseif ( $single_page_meta != 'default' ) {
            $blog_layout = $single_page_meta;
        } elseif ( $theme_settings_panel ) {
            $blog_layout = $theme_settings_panel;
        }

    } elseif( is_page() ) {

        $layout_page_url = ( isset($_GET["layout-page"]) ) ? $_GET["layout-page"]  : "";
        $genarel_page_meta = houserent_theme_uny_post_meta('meta_page_layout','default');
        $theme_settings_panel = houserent_theme_get_customizer_field('genarel_post_page_layout','full_width');


        if( $layout_page_url ){
            $blog_layout = $layout_page_url;
        } elseif ( $genarel_page_meta != 'default' ) {
            $blog_layout = $genarel_page_meta;
        } elseif ( $theme_settings_panel ) {
            $blog_layout = $theme_settings_panel;
        }

    }


    if( $blog_layout ){
        switch ( $blog_layout ) {
            case 'sidebar_left':
                $class_cont = "col-md-4 col-md-pull-8";
                break;
            
            default:
                $class_cont = "col-md-4";
                break;
        }
    }

?>
<?php if( $blog_layout != 'full_width' ): ?>
<div class="<?php echo esc_attr( $class_cont ); ?>">
    <div id="secondary" class="widget-area blog-content-right">
        <?php
            $sidebar_id = ( is_home() ) ? "sidebar-home-page" : "sidebar-default"; 

            if ( is_active_sidebar( $sidebar_id ) ) { 
                if ( ! dynamic_sidebar( $sidebar_id ) ) :  
                endif; // end sidebar widget area
            } elseif ( is_archive() ) {

                the_widget('WP_Widget_Categories', '', 'before_widget=<div class="widget widget_categories">&before_title=<div class="widget-title-area"><h3 class="widget-title" >&after_title=</h3></div>&after_widget=</div>'); 

                the_widget('WP_Widget_Archives', '', 'before_widget=<div class="widget widget_archive">&before_title=<div class="widget-title-area"><h3 class="widget-title" >&after_title=</h3></div>&after_widget=</div>'); 

                the_widget('WP_Widget_Tag_Cloud', '', 'before_widget=<div class="widget widget_tag_cloud">&before_title=<div class="widget-title-area"><h3 class="widget-title" >&after_title=</h3></div>&after_widget=</div>'); 
            }

        ?>
    </div>
</div>
<?php endif; ?>