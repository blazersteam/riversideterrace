<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}
$houserent_plugin = function_exists( 'houserent_theme_custom_posts' );

$options = array(
    'general' => array(
        'title'   => esc_html__( 'General', 'houserent' ),
        'type'    => 'tab',
        'options' => array(
            'favicon_tab' => array(
                'title'   => esc_html__( 'Favicon', 'houserent' ),
                'type'    => 'tab',
                'options' => array( 
                    'favicon'   => array(
                        'type'  => 'upload', 
                        'label' => esc_html__('Favicon Main', 'houserent'), 
                        'desc' => esc_html__('Favicon for all (16px X 1px OR 32px X 32px)', 'houserent'),
                    ),
                    'icon_iphone'   => array(
                        'type'  => 'upload', 
                        'label' => esc_html__('Apple iPhone Icon', 'houserent'),
                        'desc' => esc_html__('Icon for Apple iPhone (57px X 57px)', 'houserent'),
                    ),
                    'icon_iphone_retina'   => array(
                        'type'  => 'upload', 
                        'label' => esc_html__('Apple iPhone Retina Icon', 'houserent'),
                        'desc' => esc_html__('Icon for Apple iPhone Retina (114px X 114px)', 'houserent'),
                    ),
                    'icon_ipad'   => array(
                        'type'  => 'upload', 
                        'label' => esc_html__('Apple iPad Icon', 'houserent'),
                        'desc' => esc_html__('Icon for Apple iPad (72px X 72px)', 'houserent'),
                    ),
                    'icon_ipad_retina'   => array(
                        'type'  => 'upload', 
                        'label' => esc_html__('Apple iPad Retina Icon', 'houserent'),
                        'desc' => esc_html__('Icon for Apple iPad Retina (144px X 144px)', 'houserent'),
                    ),
                ),
            ),
            'page_settings_tab' => array(
                'title'   => esc_html__( 'Page Settings', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                    'genarel_post_page_layout'  => array(
                        'label'   => esc_html__( 'General Page Layout', 'houserent' ),
                        'type'    => 'image-picker',
                        'value'   => 'full_width',
                        'desc'    => esc_html__( 'Hare You can set General page layout (Ex. Sidebar Left, Sidebar Right or Full width).',
                            'houserent' ),
                        'choices' => array(
                            'sidebar_left' => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/left.png'
                                ),
                                'large' => array(
                                    'height' => 214,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/left-big.png'
                                ),
                            ),
                            'sidebar_right' => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/right.png'
                                ),
                                'large' => array(
                                    'height' => 214,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/right-big.png'
                                ),
                            ),
                            'full_width' => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/full.png'
                                ),
                                'large' => array(
                                    'height' => 214,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/full-big.png'
                                ),
                            ),
                        ),
                    ),
                    'genarel_post_page_column_width' => array(
                        'label' => esc_html__( 'Select Full Width Column', 'houserent' ),
                        'type'  => 'slider',
                        'value' => 12,                                        
                        'properties' => array(
                            'min' => 8,
                            'max' => 12,
                            'step' => 1,
                        ),
                        'desc'  => esc_html__( 'Choose column width for full width layout', 'houserent' ),
                    ),
                   'page_breadcrumbs'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Page Breadcrumbs', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'hide',
                        'desc'         => esc_html__( 'You can show or hide page breadcrumbs from here',
                            'houserent' ),
                    ),
                   'show_page_title'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Page Title', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide page title from here',
                            'houserent' ),
                    ),
                   'page_social_share'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Social Share Link', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'hide',
                        'desc'         => esc_html__( 'You can show or hide social share link from here',
                            'houserent' ),
                    ),
                ),
            ),
            'single_page_settings_tab' => array(
                'title'   => esc_html__( 'Single Post Page', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                    'single_post_page_layout'  => array(
                        'label'   => esc_html__( 'Single Post Page Layout', 'houserent' ),
                        'type'    => 'image-picker',
                        'value'   => 'full_width',
                        'desc'    => esc_html__( 'Hare you can set single post page layout (Ex. Sidebar Left, Sidebar Right or Full width).',
                            'houserent' ),
                        'choices' => array(
                            'sidebar_left' => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/left.png'
                                ),
                                'large' => array(
                                    'height' => 214,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/left-big.png'
                                ),
                            ),
                            'sidebar_right' => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/right.png'
                                ),
                                'large' => array(
                                    'height' => 214,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/right-big.png'
                                ),
                            ),
                            'full_width' => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/full.png'
                                ),
                                'large' => array(
                                    'height' => 214,
                                    'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/full-big.png'
                                ),
                            ),
                        ),
                    ),
                    'single_post_page_column_width' => array(
                        'label' => esc_html__( 'Select Full Width Column', 'houserent' ),
                        'type'  => 'slider',
                        'value' => 12,                                        
                        'properties' => array(
                            'min' => 8,
                            'max' => 12,
                            'step' => 1,
                        ),
                        'desc'  => esc_html__( 'Choose column width for full width layout', 'houserent' ),
                    ),
                   'breadcrumbs_single'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Breadcrumbs Section', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide breadcrumbs section in single post page from here', 'houserent' ),
                    ),
                   'date_single'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Date Section', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide date section in single post page from here', 'houserent' ),
                    ),
                   'category_single'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Category Section', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide category section in single post page from here',
                            'houserent' ),
                    ),
                   'tags_single'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Tags Section', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide tags section from here', 'houserent' ),
                    ),
                   'view_single'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'View Section', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide post view counter section from here', 'houserent' ),
                    ),
                   'like_single'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Like Section', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide like section from here', 'houserent' ),
                    ),
                   'social_single'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Social Share Link', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide social share link from here',
                            'houserent' ),
                    ),
                   'single_page_related_post'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Related Posts', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide related posts from here',
                            'houserent' ),
                    ),
                   'single_page_related_post_query'   => array(
                        'label'   => esc_html__( 'Show Related Post From', 'houserent' ),
                        'type'    => 'radio',
                        'value'   => 'author',
                        'desc'    => esc_html__( 'Show related by this query type','houserent'),
                        'choices' => array(
                            'tag' => esc_html__( 'Tag', 'houserent' ),
                            'category' => esc_html__( 'Category', 'houserent' ),
                            'author' => esc_html__( 'Author', 'houserent' ),
                        ),
                    ),
                ),
            ),
            '404_page_settings_tab' => array(
                'title'   => esc_html__( '404 Page', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                    '404_page_bg_color' => array(
                        'label' => esc_html__( 'Background Gradient Color', 'houserent' ),
                        'type'  => 'gradient',
                        'value' => array(
                                'primary'   => '#21b75f',
                                'secondary' => '#31386e',
                        ),
                        'desc'  => esc_html__( 'Here you can change background gradient color', 'houserent' ),
                    ),
                ),
            ),
            'other_settings_tab' => array(
                'title'   => esc_html__( 'Other Settings', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                    'other_settings_color_type' => array(
                        'type'         => 'multi-picker',
                        'label'        => false,
                        'desc'         => false,
                        'picker'       => array(
                            'other_settings_color' => array(
                                'type'  => 'switch',
                                'label' => esc_html__( 'Widget Title Color Type', 'houserent' ),
                                'right-choice' => array(
                                    'value' => 'color',
                                    'label' => esc_html__( 'Color', 'houserent' )
                                ),
                                'left-choice'  => array(
                                    'value' => 'gradient',
                                    'label' => esc_html__( 'Gradient', 'houserent' )
                                ),
                                'value'     => 'gradient',
                                'desc'      => esc_html__( 'You can change sidebar widget title color from here','houserent' ),
                            ),
                        ),
                        'choices'  => array(
                            'color'  => array(
                                'other_settings_color_c'   => array(
                                     'type'  => 'color-picker',
                                     'label' => esc_html__( 'Solid Color', 'houserent' ),
                                     'value' => '#21b75f',
                                     'desc'  => esc_html__( 'You can change sidebar widget title color from here', 'houserent' ),
                                 ),
                            ),
                            'gradient'  => array(
                                'other_settings_gradient_c'   => array(
                                    'label' => esc_html__( 'Gradient', 'houserent' ),
                                    'type'  => 'gradient',
                                    'value' => array(
                                            'primary'   => '#21b75f',
                                            'secondary' => '#31386e',
                                    ),
                                    'desc'  => esc_html__( 'Here you can change sidebar widget title gradient color', 'houserent' ),
                                 ),
                            ),
                        ),
                        'show_borders' => false,
                    ),
                   
                   'custom_css'   => array(
                        'type'  => 'textarea', 
                        'label' =>  esc_html__( 'Custom CSS Code', 'houserent' ),
                        'desc' => esc_html__( 'Paste your CSS code here without <style></style> tag', 'houserent' ),
                   ),
                   'custom_js'   => array(
                        'type'  => 'textarea', 
                        'label' =>  esc_html__( 'Custom JS Code', 'houserent' ),
                        'desc' => esc_html__( 'Paste your JS code here without <script></script> tag', 'houserent' ),
                   ),
                ),
            ),
        ),
    ),
    'typography_settings' => array(
        'title' => esc_html__( 'Typography Settings', 'houserent' ),
        'type' => 'tab',
        'options' => array(
            'global_body_fonts'  => array(
                'label' => esc_html__( 'Body Font', 'houserent' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Poppins',
                    'subset'    => 'latin-ext',
                    'variation' => 'regular',
                    'size'      => '16px',
                    'line-height' => '1.45em',
                    'letter-spacing' => '',
                    'color'     => '#4b4b4b'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the body font properties.','houserent' ),
            ),
            'block_quote_fonts'  => array(
                'label' => esc_html__( 'Block Quote Content', 'houserent' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Playfair Display',
                    'subset'    => 'latin-ext',
                    'variation' => 'italic',
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => false,
                    'line-height'    => false,
                    'letter-spacing' => false,
                    'color'          => false
                ),
                'desc'  => esc_html__( 'You can specify the block quote content font properties.','houserent' ),
            ),
            'hading_one_font'  => array(
                'label' => esc_html__( 'Heading h1 Font', 'houserent' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Playfair Display',
                    'subset'    => 'latin-ext',
                    'variation' => 'regular',
                    'size'      => '2.441em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#2b2b2b'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h1 Font font properties.','houserent' ),
            ),
            'hading_two_font'  => array(
                'label' => esc_html__( 'Heading h2 Font', 'houserent' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Playfair Display',
                    'subset'    => 'latin-ext',
                    'variation' => 'regular',
                    'size'      => '1.953em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#2b2b2b'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h2 font font properties.','houserent' ),
            ),
            'hading_three_font'  => array(
                'label' => esc_html__( 'Heading h3 Font', 'houserent' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Playfair Display',
                    'subset'    => 'latin-ext',
                    'variation' => 'regular',
                    'size'      => '1.563em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#2b2b2b'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h3 font font properties.','houserent' ),
            ),
            'hading_four_font'  => array(
                'label' => esc_html__( 'Heading h4 Font', 'houserent' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Playfair Display',
                    'subset'    => 'latin-ext',
                    'variation' => 'regular',
                    'size'      => '1.25em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#2b2b2b'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h4 font font properties.','houserent' ),
            ),
            'hading_five_font'  => array(
                'label' => esc_html__( 'Heading h5 Font', 'houserent' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Playfair Display',
                    'subset'    => 'latin-ext',
                    'variation' => 'regular',
                    'size'      => '1em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#2b2b2b'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h5 font font properties.','houserent' ),
            ),
            'hading_six_font'  => array(
                'label' => esc_html__( 'Heading h6 Font', 'houserent' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Playfair Display',
                    'subset'    => 'latin-ext',
                    'variation' => 'regular',
                    'size'      => '0.8em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#2b2b2b'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h6 Font font properties.','houserent' ),
            ),


        ),
    ),
    'header_settings' => array(
        'title'   => esc_html__( 'Header Settings', 'houserent' ),
        'type'    => 'tab',
        'options' => array(
            'header_tab' => array(
                'title'   => esc_html__( 'Header General Settings', 'houserent' ),
                'type'    => 'tab',
                'options' => array( 
                    'header_logo' => array(
                        'type'    => 'upload',
                        'label'   => esc_html__( 'Upload Main Logo', 'houserent' ),
                        'desc' => esc_html__('Upload site logo here (16px X 1px OR 32px X 32px)', 'houserent'),
                    ),
                    'header_top_right_menu'  => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Header Top Menus', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can hide or show header top right menu section from here (short menu, search, popup menu)', 'houserent' ),
                    ),
                    'header_menu_animation' => array(
                        'label'   => esc_html__( 'Menu Animation', 'houserent' ),
                        'desc' => esc_html__('Select one animation for menu', 'houserent'),
                        'type'    => 'select',
                        'choices' => array(
                             'slide' => esc_html__( 'Slide', 'houserent' ),
                             'fade' => esc_html__( 'Fade', 'houserent' ),
                             'fadeInUp' => esc_html__( 'FadeInUp', 'houserent' ),
                             'skew' => esc_html__( 'Skew', 'houserent' ),
                             'flipIn' => esc_html__( 'FlipIn', 'houserent' ),
                         ),
                    ),                    
                    'header_social_profile' => array(
                        'label'         => esc_html__( 'Link Social Profile', 'houserent' ),
                        'type'          => 'addable-popup',
                        'desc'          => esc_html__( 'This section is not working on header variation one', 'houserent' ),
                        'template'      => '{{- social_profile_url }}',                                    
                        'add-button-text' => esc_html__('Add social', 'houserent'),
                        'popup-options' => array(
                            'social_icons' => array(
                                'type'  => 'icon-v2',
                                'preview_size' => 'medium',
                                'modal_size' => 'medium',
                                'label' => esc_html__( 'Select Icon', 'houserent' ),
                                'desc'  => esc_html__( 'Please select your social profile icon from here', 'houserent' ),
                            ),
                            'social_profile_url' => array(
                                'label' => esc_html__( 'Social Link', 'houserent' ),
                                'type'  => 'text',
                                'desc'  => esc_html__( 'Please input your social profile URL.', 'houserent' ),
                            ),
                        ),
                    ),
                    'header_special_nav' => array(
                        'label'         => esc_html__( 'Header Special Menu', 'houserent' ),
                        'type'          => 'addable-popup',
                        'desc'          => esc_html__( 'This section is for header top special navigation menu', 'houserent' ),
                        'template'      => '{{- special_nav_label }}',                                    
                        'add-button-text' => esc_html__('Add menu', 'houserent'),
                        'popup-options' => array(
                            'special_nav_icons' => array(
                                'type'  => 'icon-v2',
                                'preview_size' => 'medium',
                                'modal_size' => 'medium',
                                'label' => esc_html__( 'Select Icon', 'houserent' ),
                                'desc'  => esc_html__( 'Please select icon from here', 'houserent' ),
                            ),
                            'special_nav_label' => array(
                                'label' => esc_html__( 'Item Label', 'houserent' ),
                                'type'  => 'text',
                                'desc'  => esc_html__( 'Put item name here.', 'houserent' ),
                            ),
                            'special_nav_url' => array(
                                'label' => esc_html__( 'Link URL', 'houserent' ),
                                'type'  => 'text',
                                'desc'  => esc_html__( 'Put item linked URL.', 'houserent' ),
                            ),
                        ),
                    ),
                    'special_button_label' => array(
                        'label' => esc_html__( 'Button Label', 'houserent' ),
                        'type'  => 'text',
                        'value'  => 'Booking',
                        'desc'  => esc_html__( 'Put button name here. (Location right side of menu in header)', 'houserent' ),
                    ),
                    'special_button_link' => array(
                        'label' => esc_html__( 'Button Link', 'houserent' ),
                        'type'  => 'text',
                        'value'  => '#',
                        'desc'  => esc_html__( 'Put button link here. (Location right side of menu in header)', 'houserent' ),
                    ),
                ),
            ),
            'header_variation_tab' => array(
                'title'   => esc_html__( 'Header Variation Settings', 'houserent' ),
                'type'    => 'tab',
                'options' => array(                    
                    'header_layout' => array(
                        'type'         => 'multi-picker',
                        'label'        => false,
                        'desc'         => false,
                        'picker'       => array(
                            'header_variation' => array(
                                'label'   => esc_html__( 'Header Style', 'houserent' ),
                                'type'    => 'image-picker',                                
                                'value'    => 'one',
                                'choices' => array(
                                    'one'  => array(
                                        'small' => array(
                                            'height' => 70,
                                            'src'    => get_template_directory_uri() . '/assets/images/backend/header-variation/header-one-small.jpg'
                                        ),
                                        'large' => array(
                                            'height' => 94,
                                            'src'    => get_template_directory_uri() . '/assets/images/backend/header-variation/header-one-large.jpg'
                                        ),
                                    ),
                                    'two'  => array(
                                        'small' => array(
                                            'height' => 70,
                                            'src'    => get_template_directory_uri() . '/assets/images/backend/header-variation/header-two-small.jpg'
                                        ),
                                        'large' => array(
                                            'height' => 94,
                                            'src'    => get_template_directory_uri() . '/assets/images/backend/header-variation/header-two-large.jpg'
                                        ),
                                    ),
                                    'three'  => array(
                                        'small' => array(
                                            'height' => 70,
                                            'src'    => get_template_directory_uri() . '/assets/images/backend/header-variation/header-three-small.jpg'
                                        ),
                                        'large' => array(
                                            'height' => 94,
                                            'src'    => get_template_directory_uri() . '/assets/images/backend/header-variation/header-three-large.jpg'
                                        ),
                                    ),
                                ),
                                'desc'    => esc_html__( 'Select your header layout',
                                    'houserent' )
                            ),
                        ),
                        'choices'      => array(
                            'one'  => array(
                                'header_bottom_display'  => array(
                                    'type'  => 'switch',
                                    'label' => esc_html__( 'Header Bottom', 'houserent' ),
                                    'right-choice' => array(
                                        'value' => 'show',
                                        'label' => esc_html__( 'Show', 'houserent' )
                                    ),
                                    'left-choice'  => array(
                                        'value' => 'hide',
                                        'label' => esc_html__( 'Hide', 'houserent' )
                                    ),
                                    'value'        => 'show',
                                    'desc'         => esc_html__( 'You can hide or show header bottom from here',
                                        'houserent' ),
                                ),
                                'header_bottom_bg_color' => array(
                                    'type'         => 'multi-picker',
                                    'label'        => false,
                                    'desc'         => false,
                                    'picker'       => array(
                                        'header_bottom_background' => array(
                                            'type'  => 'switch',
                                            'label' => esc_html__( 'Header Background Type', 'houserent' ),
                                            'right-choice' => array(
                                                'value' => 'color',
                                                'label' => esc_html__( 'Color', 'houserent' )
                                            ),
                                            'left-choice'  => array(
                                                'value' => 'gradient',
                                                'label' => esc_html__( 'Gradient', 'houserent' )
                                            ),
                                            'value'     => 'color',
                                            'desc'      => esc_html__( 'You can choose section background color from here','houserent' ),
                                        ),
                                    ),
                                    'choices'  => array(
                                        'color'  => array(
                                            'header_one_bg_color'   => array(
                                                 'type'  => 'color-picker',
                                                 'label' => esc_html__( 'Solid Color', 'houserent' ),
                                                 'value' => '#1C1C1C',
                                                 'desc'  => esc_html__( 'You can change background color from here', 'houserent' ),
                                             ),
                                        ),
                                        'gradient'  => array(
                                            'header_one_bg_gradient'   => array(
                                                'label' => esc_html__( 'Gradient', 'houserent' ),
                                                'type'  => 'gradient',
                                                'value' => array(
                                                    'primary'   => '#21b75f',
                                                    'secondary' => '#31386e',
                                                ),
                                                'desc'  => esc_html__( 'Here you can change overlay gradient color', 'houserent' ),
                                             ),
                                        ),
                                    ),
                                    'show_borders' => false,
                                ),
                            ),
                            'two' => array(
                                'header_bottom_display'  => array(
                                    'type'  => 'switch',
                                    'label' => esc_html__( 'Header Bottom', 'houserent' ),
                                    'right-choice' => array(
                                        'value' => 'show',
                                        'label' => esc_html__( 'Show', 'houserent' )
                                    ),
                                    'left-choice'  => array(
                                        'value' => 'hide',
                                        'label' => esc_html__( 'Hide', 'houserent' )
                                    ),
                                    'value'        => 'show',
                                    'desc'         => esc_html__( 'You can hide or show header bottom from here',
                                        'houserent' ),
                                ),
                                'header_bottom_bg_color' => array(
                                    'type'         => 'multi-picker',
                                    'label'        => false,
                                    'desc'         => false,
                                    'picker'       => array(
                                        'header_bottom_background' => array(
                                            'type'  => 'switch',
                                            'label' => esc_html__( 'Header Bottom Background Type', 'houserent' ),
                                            'right-choice' => array(
                                                'value' => 'color',
                                                'label' => esc_html__( 'Color', 'houserent' )
                                            ),
                                            'left-choice'  => array(
                                                'value' => 'gradient',
                                                'label' => esc_html__( 'Gradient', 'houserent' )
                                            ),
                                            'value'     => 'color',
                                            'desc'      => esc_html__( 'You can choose section background color from here','houserent' ),
                                        ),
                                    ),
                                    'choices'  => array(
                                        'color'  => array(
                                            'header_two_bg_color'   => array(
                                                 'type'  => 'color-picker',
                                                 'label' => esc_html__( 'Solid Color', 'houserent' ),
                                                 'value' => '#ffffff',
                                                 'desc'  => esc_html__( 'You can change background color from here', 'houserent' ),
                                             ),
                                        ),
                                        'gradient'  => array(
                                            'header_two_bg_gradient'   => array(
                                                'label' => esc_html__( 'Gradient', 'houserent' ),
                                                'type'  => 'gradient',
                                                'value' => array(
                                                        'primary'   => '#21b75f',
                                                        'secondary' => '#31386e',
                                                ),
                                                'desc'  => esc_html__( 'Here you can change overlay gradient color', 'houserent' ),
                                             ),
                                        ),
                                    ),
                                    'show_borders' => false,
                                ),
                            ),
                            'three' => array(
                                'header_bottom_display'  => array(
                                    'type'  => 'switch',
                                    'label' => esc_html__( 'Header Bottom', 'houserent' ),
                                    'right-choice' => array(
                                        'value' => 'show',
                                        'label' => esc_html__( 'Show', 'houserent' )
                                    ),
                                    'left-choice'  => array(
                                        'value' => 'hide',
                                        'label' => esc_html__( 'Hide', 'houserent' )
                                    ),
                                    'value'        => 'show',
                                    'desc'         => esc_html__( 'You can hide or show header bottom from here',
                                        'houserent' ),
                                ),
                                'header_bottom_bg_color' => array(
                                    'type'         => 'multi-picker',
                                    'label'        => false,
                                    'desc'         => false,
                                    'picker'       => array(
                                        'header_bottom_background' => array(
                                            'type'  => 'switch',
                                            'label' => esc_html__( 'Header Bottom Background Type', 'houserent' ),
                                            'right-choice' => array(
                                                'value' => 'color',
                                                'label' => esc_html__( 'Color', 'houserent' )
                                            ),
                                            'left-choice'  => array(
                                                'value' => 'gradient',
                                                'label' => esc_html__( 'Gradient', 'houserent' )
                                            ),
                                            'value'     => 'gradient',
                                            'desc'      => esc_html__( 'You can choose section background color from here','houserent' ),
                                        ),
                                    ),
                                    'choices'  => array(
                                        'color'  => array(
                                            'header_three_bg_color'   => array(
                                                 'type'  => 'color-picker',
                                                 'label' => esc_html__( 'Solid Color', 'houserent' ),
                                                 'value' => '#000000',
                                                 'desc'  => esc_html__( 'You can change background color from here', 'houserent' ),
                                             ),
                                        ),
                                        'gradient'  => array(
                                            'header_three_bg_gradient'   => array(
                                                'label' => esc_html__( 'Gradient', 'houserent' ),
                                                'type'  => 'gradient',
                                                'value' => array(
                                                        'primary'   => '#21b75f',
                                                        'secondary' => '#31386e',
                                                ),
                                                'desc'  => esc_html__( 'Here you can change overlay gradient color', 'houserent' ),
                                             ),
                                        ),
                                    ),
                                    'show_borders' => false,
                                ),
                            ),
                        ),
                        'show_borders' => false,
                    ),
                    'header_one_color'   => array(
                        'type'  => 'color-picker',
                        'label' => esc_html__( 'Text Color', 'houserent' ),
                        'value' => '#ffffff',
                        'desc'  => esc_html__( 'You can change header bottom text color from here', 'houserent' ),
                    ),
                    'registration_terms_conditions' => array(
                        'label' => esc_html__( 'Terms Conditions Page', 'houserent' ),
                        'type'  => 'text',
                        'value'  => '#',
                        'desc'  => esc_html__( 'Put header registration form terms and conditions page link here', 'houserent' ),
                    ),
                ),
            ),
        ),
    ),
    'color_settings' => array(
        'title'   => esc_html__( 'Color Settings', 'houserent' ),
        'type'    => 'tab',
        'options' => array(            
            'color_scheme_primary' => array(
                'type'         => 'multi-picker',
                'label'        => false,
                'desc'         => false,
                'picker'       => array(
                    'color_variation_primary' => array(
                        'label'   => esc_html__( 'Primary Theme Color', 'houserent' ),
                        'type'    => 'image-picker',                        
                        'value'    => 'two',
                        'choices' => array(
                            'one'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-one.png'
                                    ),
                                ),
                            'two'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-two.png'
                                    ),
                                ),
                            'three'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-three.png'
                                    ),
                                ),
                            'four'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-four.png'
                                    ),
                                ),
                            'five'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-five.png'
                                    ),
                                ),
                            'six'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-six.png'
                                    ),
                                ),
                            'seven'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-seven.png'
                                    ),
                                ),
                            'custom'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-scheme.png'
                                    ),
                                ),
                        ),
                        'desc'    => esc_html__( 'Select primary color for theme', 'houserent' ),
                        'value' => "",
                    )
                ),
                'choices'      => array(
                    'custom' => array(
                           'custom_color'  => array(
                            'type'  => 'color-picker',
                            'value' => '#0c15cf',
                            'label' => esc_html__( 'Select Custom Color', 'houserent' ),
                            'desc'         => esc_html__( 'You can select custom color from here',
                                'houserent' ),
                        ),
                    ),
                ),
                'show_borders' => false,
            ),            
            'color_scheme_secondary' => array(
                'type'         => 'multi-picker',
                'label'        => false,
                'desc'         => false,
                'picker'       => array(
                    'color_variation_secoendry' => array(
                        'label'   => esc_html__( 'Secondary Theme Color', 'houserent' ),
                        'type'    => 'image-picker',                        
                        'value'    => 'two',
                        'choices' => array(
                            'one'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-one-seceondary.png'
                                    ),
                                ),
                            'two'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-two.png'
                                    ),
                                ),
                            'three'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-three.png'
                                    ),
                                ),
                            'four'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-four.png'
                                    ),
                                ),
                            'five'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-five.png'
                                    ),
                                ),
                            'six'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-six.png'
                                    ),
                                ),
                            'seven'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-seven.png'
                                    ),
                                ),
                            'custom'  => array(
                                    'small' => array(
                                        'height' => 60,
                                        'src'    => get_template_directory_uri() . '/assets/images/backend/color/color-scheme.png'
                                    ),
                                ),
                        ),
                        'desc'    => esc_html__( 'Select theme secondary color for theme', 'houserent' ),
                        'value' => "",
                    )
                ),
                'choices'      => array(
                    'custom' => array(
                           'custom_color'  => array(
                            'type'  => 'color-picker',
                            'value' => '#0c15cf',
                            'label' => esc_html__( 'Select Custom Color', 'houserent' ),
                            'desc'         => esc_html__( 'You can select custom color from here',
                                'houserent' ),
                        ),
                    ),
                ),
                'show_borders' => false,
            ),
            'color_scheme_gradient' => array(
                'label' => esc_html__( 'Select Default Gradient', 'houserent' ),
                'type'  => 'gradient',
                'value' => array(
                        'primary'   => '#21b75f',
                        'secondary' => '#31386e',
                ),
                'desc'  => esc_html__( 'Change theme default gradient colors ', 'houserent' ),
            ),
        ),
    ),
    'layout_settings' => array(
        'title' => esc_html__( 'Blog Settings', 'houserent' ),
        'type' => 'tab',
        'options' => array(
            'blog_layouts' => array(
                'title'   => esc_html__( 'Home Layout', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                    'blog_layout_page_layout_sec' => array(
                        'type'         => 'multi-picker',
                        'label'        => false,
                        'desc'         => false,
                        'picker'       => array(
                            'blog_layout_page_layout'  => array(
                                'label'   => esc_html__( 'Blog Layout', 'houserent' ),
                                'type'    => 'image-picker',
                                'value'   => 'full_width',
                                'desc'    => esc_html__( 'Hare you can change page Layout blog page style one. (Ex. Sidebar Left, Sidebar Right or Full width).',
                                    'houserent' ),
                                'choices' => array(
                                    'sidebar_left' => array(
                                        'small' => array(
                                            'height' => 70,
                                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/left.png'
                                        ),
                                        'large' => array(
                                            'height' => 214,
                                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/left-big.png'
                                        ),
                                    ),
                                    'sidebar_right' => array(
                                        'small' => array(
                                            'height' => 70,
                                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/right.png'
                                        ),
                                        'large' => array(
                                            'height' => 214,
                                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/right-big.png'
                                        ),
                                    ),
                                ),
                            ),
                        ),
                        'choices'      => array(
                        ),
                        'show_borders' => false,
                    ),  
                ),      
            ),
        ),
    ),
    ( $houserent_plugin ) ? array( 
        'rental_settings' => array(
            'title'   => esc_html__( 'Rental Settings', 'houserent' ),
            'type'    => 'tab',
            'options' => array(
            'rental_general' => array(
                'title'   => esc_html__( 'General', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                    'rental_currency_symbol'  => array(
                        'type'  => 'text',
                        'label' => esc_html__('Currency Symbol', 'houserent'),
                        'value' => esc_html__('$', 'houserent'),
                         'desc' => esc_html__( 'Here you can change global currency symbol', 'houserent' ),
                    ),
                    'rental_post_per' => array(
                        'label' => esc_html__( 'Posts Per Page', 'houserent' ),
                        'type'  => 'slider',
                        'value' => 10,                                        
                        'properties' => array(
                            'min' => 1,
                            'max' => 100,
                            'step' => 1,
                        ),
                        'desc'  => esc_html__( 'Select post per page for rental items', 'houserent' ),
                    ),
                ),
            ),
            'rental_template' => array(
                'title'   => esc_html__( 'Rental Template', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                    'rental_page_title_sec' => array(
                        'type'         => 'multi-picker',
                        'label'        => false,
                        'desc'         => false,
                        'picker'       => array(
                            'rental_page_title'   => array(
                                 'type'  => 'switch',
                                 'label' => esc_html__( 'Page Header', 'houserent' ),
                                 'right-choice' => array(
                                     'value' => 'show',
                                     'label' => esc_html__( 'Show', 'houserent' )
                                 ),
                                 'left-choice'  => array(
                                     'value' => 'hide',
                                     'label' => esc_html__( 'Hide', 'houserent' )
                                 ),
                                 'value'        => 'show',
                                 'desc'         => esc_html__( 'You can show or hide page header from here', 'houserent' ),
                             ),
                        ),
                        'choices'      => array(
                            'show'  => array(
                                'rental_page_title_sec_bg' => array(
                                    'label' => esc_html__( 'Title Background', 'houserent' ),
                                    'type'  => 'gradient',
                                    'value' => array(
                                            'primary'   => '#21b75f',
                                            'secondary' => '#31386e',
                                    ),
                                    'desc'  => esc_html__( 'Here you can change background gradient color of title section background', 'houserent' ),
                                ),
                                'rental_page_subtitle'  => array(
                                    'type'  => 'text',
                                    'label' => esc_html__('Page Subtitle', 'houserent'),
                                    'value' => esc_html__('All rent services', 'houserent'),
                                     'desc' => esc_html__( 'Here you can change page subtitle from here', 'houserent' ),
                                ),
                            ),
                        ),
                        'show_borders' => false,
                    ),
                    'rental_breadcrumbs'   => array(
                         'type'  => 'switch',
                         'label' => esc_html__( 'Page Breadcrumbs', 'houserent' ),
                         'right-choice' => array(
                             'value' => 'show',
                             'label' => esc_html__( 'Show', 'houserent' )
                         ),
                         'left-choice'  => array(
                             'value' => 'hide',
                             'label' => esc_html__( 'Hide', 'houserent' )
                         ),
                         'value'        => 'show',
                         'desc'         => esc_html__( 'You can show or hide page breadcrumbs from here', 'houserent' ),
                     ),
                    'rental_post_per_pages' => array(
                        'label' => esc_html__( 'Post Per Page', 'houserent' ),
                        'type'  => 'slider',
                        'value' =>  9,                                        
                        'properties' => array(
                            'min' => 3,
                            'max' => 15,
                            'step' => 1,
                        ),
                        'desc'  => esc_html__( 'You can set how many post want to show per page', 'houserent' ),
                    ),
                    'rental_page_loadmore_text'  => array(
                        'type'  => 'text',
                        'label' => esc_html__('Load More Button ', 'houserent'),
                        'value' => esc_html__('More Posts', 'houserent'),
                         'desc' => esc_html__( 'Here you can change load more button text from here', 'houserent' ),
                    ),
                    'rental_page_loading_text'  => array(
                        'type'  => 'text',
                        'label' => esc_html__('Loading Button Text ', 'houserent'),
                        'value' => esc_html__('Loading...', 'houserent'),
                         'desc' => esc_html__( 'Here you can change button text when post is loading from here', 'houserent' ),
                    ),
                    'rental_page_loadmore_empty'  => array(
                        'type'  => 'text',
                        'label' => esc_html__('No More Post Button ', 'houserent'),
                        'value' => esc_html__('No More Posts', 'houserent'),
                         'desc' => esc_html__( 'Here you can change no more posts button text from here', 'houserent' ),
                    ),
                ),
            ),
            'rental_single_settings' => array(
                'title'   => esc_html__( 'Single Page Settings', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                   'rental_single_page_header_sec' => array(
                       'type'         => 'multi-picker',
                       'label'        => false,
                       'desc'         => false,
                       'picker'       => array(
                           'rental_single_page_header'   => array(
                                'type'  => 'switch',
                                'label' => esc_html__( 'Page Header', 'houserent' ),
                                'right-choice' => array(
                                    'value' => 'show',
                                    'label' => esc_html__( 'Show', 'houserent' )
                                ),
                                'left-choice'  => array(
                                    'value' => 'hide',
                                    'label' => esc_html__( 'Hide', 'houserent' )
                                ),
                                'value'        => 'show',
                                'desc'         => esc_html__( 'You can show or hide page header from here', 'houserent' ),
                            ),
                       ),
                       'choices'      => array(
                           'show'  => array(
                               'rental_single_page_header_gradient'  => array(
                                   'type'  => 'gradient',
                                   'value' => array(
                                       'primary'   => '#21b75f',
                                       'secondary' => '#31386e',
                                   ),
                                   'label' => esc_html__('Title Background', 'houserent'),
                                   'desc'  => esc_html__('Here you can change background gradient', 'houserent'),
                               ),
                               'rental_single_page_header_title'  => array(
                                   'type'  => 'text',
                                   'label' => esc_html__('Header Title', 'houserent'),
                                   'value' => esc_html__('Apartment', 'houserent'),
                                    'desc' => esc_html__( 'Here you can change page header section title from here', 'houserent' ),
                               ),
                               'rental_single_page_header_subtitle'  => array(
                                   'type'  => 'text',
                                   'label' => esc_html__('Header Subtitle', 'houserent'),
                                   'value' => esc_html__('Details about this apartment', 'houserent'),
                                    'desc' => esc_html__( 'Here you can change page header section subtitle from here', 'houserent' ),
                               ),
                           ),
                       ),
                       'show_borders' => false,
                   ),
                   'rental_single_bc'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Page Breadcrumbs', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide page breadcrumbs from here', 'houserent' ),
                    ),
                   'rental_single_title'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Post Title', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide rental item title from here', 'houserent' ),
                    ),
                   'rental_single_price'   => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Rent/Month Section', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__( 'Show', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'hide',
                            'label' => esc_html__( 'Hide', 'houserent' )
                        ),
                        'value'        => 'show',
                        'desc'         => esc_html__( 'You can show or hide rent/month section from here', 'houserent' ),
                    ),
                   'rental_single_related_sec' => array(
                       'type'         => 'multi-picker',
                       'label'        => false,
                       'desc'         => false,
                       'picker'       => array(
                           'rental_single_related'   => array(
                                'type'  => 'switch',
                                'label' => esc_html__( 'Related Items', 'houserent' ),
                                'right-choice' => array(
                                    'value' => 'show',
                                    'label' => esc_html__( 'Show', 'houserent' )
                                ),
                                'left-choice'  => array(
                                    'value' => 'hide',
                                    'label' => esc_html__( 'Hide', 'houserent' )
                                ),
                                'value'        => 'show',
                                'desc'         => esc_html__( 'You can show or hide related items from here', 'houserent' ),
                            ),
                       ),
                       'choices'      => array(
                           'show'  => array(
                               'rental_single_rel_title'  => array(
                                   'type'  => 'text',
                                   'label' => esc_html__('Related Section Title', 'houserent'),
                                   'value' => esc_html__('Related Apartments', 'houserent'),
                                    'desc' => esc_html__( 'Here you can change related section title from here', 'houserent' ),
                               ),
                           ),
                       ),
                       'show_borders' => false,
                   ),
                   'rental_single_calltoaction_sec' => array(
                       'type'         => 'multi-picker',
                       'label'        => false,
                       'desc'         => false,
                       'picker'       => array(
                           'rental_single_calltoaction'   => array(
                                'type'  => 'switch',
                                'label' => esc_html__( 'Call To Action', 'houserent' ),
                                'right-choice' => array(
                                    'value' => 'show',
                                    'label' => esc_html__( 'Show', 'houserent' )
                                ),
                                'left-choice'  => array(
                                    'value' => 'hide',
                                    'label' => esc_html__( 'Hide', 'houserent' )
                                ),
                                'value'        => 'hide',
                                'desc'         => esc_html__( 'You can show or hide call to action section from here', 'houserent' ),
                            ),
                       ),
                       'choices'      => array(
                           'show'  => array(
                               'call_to_action_gradient'  => array(
                                   'type'  => 'gradient',
                                   'value' => array(
                                       'primary'   => '#21b75f',
                                       'secondary' => '#31386e',
                                   ),
                                   'label' => esc_html__('Background Color', 'houserent'),
                                   'desc'  => esc_html__('Here you can choose background color', 'houserent'),
                               ),
                               'call_to_action_section_title' => array(
                                   'label' => esc_html__( 'Section Title', 'houserent' ),
                                   'type'  => 'text',
                                   'value' =>  esc_html__( 'Do you want to Rent your House', 'houserent' ),
                                   'desc'  => esc_html__( 'Here you can change call to action section title', 'houserent' ),
                               ),
                               'call_to_action_section_sub_title' => array(
                                   'label' => esc_html__( 'Section Subtitle', 'houserent' ),
                                   'type'  => 'text',
                                   'value' => esc_html__( 'Call us and list your property here', 'houserent' ),
                                   'desc'  => esc_html__( 'You change call to action section subtitle', 'houserent' ),
                               ),
                               'call_to_action_section_mobile' => array(
                                   'label' => esc_html__( 'Mobile Number', 'houserent' ),
                                   'type'  => 'text',
                                   'desc'  => esc_html__( 'You can set contact mobile number by this', 'houserent' ),
                               ),
                               'call_to_action_section_email' => array(
                                   'label' => esc_html__( 'Email', 'houserent' ),
                                   'type'  => 'text',
                                   'desc'  => esc_html__( 'You can set contact email by this', 'houserent' ),
                               ),
                               'call_to_action_section_contact_button_text' => array(
                                   'label' => esc_html__( 'Button Text', 'houserent' ),
                                   'type'  => 'text',
                                   'value' => esc_html__( 'Contact Us', 'houserent' ),
                                   'desc'  => esc_html__( 'You can change contact button title by this', 'houserent' ),
                               ),
                               'call_to_action_section_contact_button_url' => array(
                                   'label' => esc_html__( 'Button URL', 'houserent' ),
                                   'type'  => 'text',
                                   'value' => esc_html__( '#', 'houserent' ),
                                   'desc'  => esc_html__( 'You can change contact button URL by this', 'houserent' ),
                               ),
                           ),
                       ),
                       'show_borders' => false,
                   ),
                   'rental_single_info_sec' => array(
                       'type'         => 'multi-picker',
                       'label'        => false,
                       'desc'         => false,
                       'picker'       => array(
                           'rental_single_info'   => array(
                                'type'  => 'switch',
                                'label' => esc_html__( 'Contact Info', 'houserent' ),
                                'right-choice' => array(
                                    'value' => 'show',
                                    'label' => esc_html__( 'Show', 'houserent' )
                                ),
                                'left-choice'  => array(
                                    'value' => 'hide',
                                    'label' => esc_html__( 'Hide', 'houserent' )
                                ),
                                'value'        => 'hide',
                                'desc'         => esc_html__( 'You can show or hide contact info section from here', 'houserent' ),
                            ),
                       ),
                       'choices'      => array(
                           'show'  => array(
                                'support_info_section_title' => array(
                                    'label' => esc_html__( 'Section Title', 'houserent' ),
                                    'type'  => 'text',
                                    'value' =>  esc_html__( 'We Are Available For You 24/7', 'houserent' ),
                                    'desc'  => esc_html__( 'Here you can change section title (support HTML tag - br, em)', 'houserent' ),
                                ),
                                'support_info_section_sub_title' => array(
                                    'label' => esc_html__( 'Section Subtitle', 'houserent' ),
                                    'type'  => 'text',
                                    'value' => esc_html__( 'Our online support service is always 24 Hours', 'houserent' ),
                                    'desc'  => esc_html__( 'You can change section subtitle (support HTML tag - br, em)', 'houserent' ),
                                ),
                                'support_info_map' => array(
                                    'label' => esc_html__( 'Map Location', 'houserent' ),
                                    'type'  => 'map',
                                    'desc'  => esc_html__( 'Type and select your location from here', 'houserent' ),
                                ),
                                'support_info_section_map_api' => array(
                                    'label' => esc_html__( 'Google Map API', 'houserent' ),
                                    'type'  => 'text',
                                    'desc'  => esc_html__( 'Put your google map API here', 'houserent' ),
                                ),
                                'support_info_items' => array(
                                    'label'         => esc_html__( 'Add items', 'houserent' ),
                                    'type'          => 'addable-popup',
                                    'desc'          => esc_html__( 'Here you add support info item from here', 'houserent' ),
                                    'template'      => '{{- support_info_item_title }}',                                    
                                    'add-button-text' => esc_html__('Add Item', 'houserent'),
                                    'popup-options' => array(
                                        'support_info_item_icon' => array(
                                            'label' => esc_html__( 'Icon', 'houserent' ),
                                            'type'  => 'icon-v2',
                                            'desc'  => esc_html__( 'Select icon from here', 'houserent' ),
                                        ),
                                        'support_info_item_title' => array(
                                            'label' => esc_html__( 'Title', 'houserent' ),
                                            'type'  => 'text',
                                            'desc'  => esc_html__( 'Type title here (support HTML tag - br, em)', 'houserent' ),
                                        ),
                                        'support_info_item_description' => array(
                                            'label' => esc_html__( 'Description', 'houserent' ),
                                            'type'  => 'textarea',
                                            'desc'  => esc_html__( 'Type text description here (support HTML tag - a, i, br, em, strong, h2, h3, h4, ul, ol, li)', 'houserent' ), 
                                        ),
                                    ),
                                ),
                           ),
                       ),
                       'show_borders' => false,
                   ),
                ),
            ),
            'rental_search_page' => array(
                'title'   => esc_html__( 'Search Page', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                    'rental_page_title_sec' => array(
                        'type'         => 'multi-picker',
                        'label'        => false,
                        'desc'         => false,
                        'picker'       => array(
                            'rental_search_top_search'   => array(
                                 'type'  => 'switch',
                                 'label' => esc_html__( 'Top Search', 'houserent' ),
                                 'right-choice' => array(
                                     'value' => 'show',
                                     'label' => esc_html__( 'Show', 'houserent' )
                                 ),
                                 'left-choice'  => array(
                                     'value' => 'hide',
                                     'label' => esc_html__( 'Hide', 'houserent' )
                                 ),
                                 'value'        => 'show',
                                 'desc'         => esc_html__( 'You can show or hide top search form here', 'houserent' ),
                             ),
                        ),
                        'choices'      => array(
                            'show'  => array(
                                'rental_form_title'  => array(
                                    'type'  => 'text',
                                    'label' => esc_html__('Form Title', 'houserent'),
                                    'value' => esc_html__('For rates & Availability', 'houserent'),
                                     'desc' => esc_html__( 'Here you can change search form title in search page', 'houserent' ),
                                ),
                                'rental_form_subtitle'  => array(
                                    'type'  => 'text',
                                    'label' => esc_html__('Form Subtitle', 'houserent'),
                                    'value' => esc_html__('Search your needs', 'houserent'),
                                     'desc' => esc_html__( 'Here you can change search form subtitle in search page', 'houserent' ),
                                ),
                            ),
                        ),
                        'show_borders' => false,
                    ),
                ),
            ),
            'rental_search_form' => array(
                'title'   => esc_html__( 'Search Form', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                    'rental_form_living_area'  => array(
                        'type'  => 'checkbox',
                        'label' => esc_html__('Living Area', 'houserent'),
                        'value' => true,
                        'text'  => esc_html__('Enable', 'houserent'),
                        'desc' => esc_html__( 'Here you can enable or disable Living Area field at search form', 'houserent' ),
                    ),
                    'rental_form_category'  => array(
                        'type'  => 'checkbox',
                        'label' => esc_html__('Type', 'houserent'),
                        'value' => true,
                        'text'  => esc_html__('Enable', 'houserent'),
                        'desc' => esc_html__( 'Here you can enable or disable Rental Type field at search form', 'houserent' ),
                    ),
                    'rental_form_check_in_date'  => array(
                        'type'  => 'checkbox',
                        'label' => esc_html__('Check In Date', 'houserent'),
                        'value' => true,
                        'text'  => esc_html__('Enable', 'houserent'),
                        'desc' => esc_html__( 'Here you can enable or disable Rental Check In Date field at search form', 'houserent' ),
                    ),
                    'rental_form_check_out_date'  => array(
                        'type'  => 'checkbox',
                        'label' => esc_html__('Check Out Date', 'houserent'),
                        'value' => true,
                        'text'  => esc_html__('Enable', 'houserent'),
                        'desc' => esc_html__( 'Here you can enable or disable Rental Check Out Date field at search form', 'houserent' ),
                    ),
                    'rental_form_check_min_price'  => array(
                        'type'  => 'checkbox',
                        'label' => esc_html__('Minimum Price', 'houserent'),
                        'value' => true,
                        'text'  => esc_html__('Enable', 'houserent'),
                        'desc' => esc_html__( 'Here you can enable or disable Minimum Price Date field at search form', 'houserent' ),
                    ),
                    'rental_form_check_max_price'  => array(
                        'type'  => 'checkbox',
                        'label' => esc_html__('Maximum Price', 'houserent'),
                        'value' => true,
                        'text'  => esc_html__('Enable', 'houserent'),
                        'desc' => esc_html__( 'Here you can enable or disable Max Price Date field at search form', 'houserent' ),
                    ),                    
                    'rental_form_check_total_rooms'  => array(
                        'type'  => 'checkbox',
                        'label' => esc_html__('Total Rooms', 'houserent'),
                        'value' => true,
                        'text'  => esc_html__('Enable', 'houserent'),
                        'desc' => esc_html__( 'Here you can enable or disable Total Rooms field at search form', 'houserent' ),
                    ),
                ),
            ),
            'rental_contact_form' => array(
                'title'   => esc_html__( 'Contact Form', 'houserent' ),
                'type'    => 'tab',
                'options' => array(
                    'rental_page_contact_type' => array(
                        'type'         => 'multi-picker',
                        'label'        => false,
                        'desc'         => false,
                        'picker'       => array(
                            'rental_search_top_search'   => array(
                                'type'  => 'switch',
                                'label' => esc_html__( 'Contact Form Type', 'houserent' ),
                                'right-choice' => array(
                                    'value' => 'default',
                                    'label' => esc_html__( 'Default Form', 'houserent' )
                                ),
                                'left-choice'  => array(
                                    'value' => 'contact_form_7',
                                    'label' => esc_html__( 'Contact Form7', 'houserent' )
                                ),
                                'desc'         => esc_html__( 'You can show or hide top search form here', 'houserent' ),
                                'value'        => 'default',
                             ),
                        ),
                        'choices'      => array(
                            'default'  => array(
                                'rental_order_reci_mail'  => array(
                                    'type'  => 'text',
                                    'value'  => esc_html__('site_manager@email.com', 'houserent'),
                                    'label' => esc_html__('Order Receiver Email', 'houserent'),
                                     'desc' => esc_html__( 'Put order receiving email address here.', 'houserent' ),
                                ),
                                'rental_form_title'  => array(
                                    'type'  => 'text',
                                    'value' => 'Book This Apartment',
                                    'label' => esc_html__('Contact Form Title', 'houserent'),
                                    'desc' => esc_html__( 'Added Form Title From here', 'houserent' ),
                                ),
                                'rental_form_fullname'  => array(
                                    'type'  => 'checkbox',
                                    'label' => esc_html__('Full Name', 'houserent'),
                                    'value' => true,
                                    'text'  => esc_html__('Enable', 'houserent'),
                                    'desc' => esc_html__( 'Here you can enable or disable full name field at here', 'houserent' ),
                                ),
                                'rental_form_phone'  => array(
                                    'type'  => 'checkbox',
                                    'label' => esc_html__('Phone Number', 'houserent'),
                                    'value' => true,
                                    'text'  => esc_html__('Enable', 'houserent'),
                                    'desc' => esc_html__( 'Here you can enable or disable full name field at here', 'houserent' ),
                                ),
                                'rental_form_email'  => array(
                                    'type'  => 'checkbox',
                                    'label' => esc_html__('Email Address', 'houserent'),
                                    'value' => true,
                                    'text'  => esc_html__('Enable', 'houserent'),
                                    'desc' => esc_html__( 'Here you can enable or disable full name field at here', 'houserent' ),
                                ),
                                'rental_form_checkin_date'  => array(
                                    'type'  => 'checkbox',
                                    'label' => esc_html__('Check In Date', 'houserent'),
                                    'value' => true,
                                    'text'  => esc_html__('Enable', 'houserent'),
                                    'desc' => esc_html__( 'Here you can enable or disable check in date field at here', 'houserent' ),
                                ),
                                'rental_form_checkout_date'  => array(
                                    'type'  => 'checkbox',
                                    'label' => esc_html__('Checkout Date', 'houserent'),
                                    'value' => true,
                                    'text'  => esc_html__('Enable', 'houserent'),
                                    'desc' => esc_html__( 'Here you can enable or disable check out date field at here', 'houserent' ),
                                ),
                                'rental_form_family'  => array(
                                    'type'  => 'checkbox',
                                    'label' => esc_html__('Family Member', 'houserent'),
                                    'value' => true,
                                    'text'  => esc_html__('Enable', 'houserent'),
                                    'desc' => esc_html__( 'Here you can enable or disable full name field at here', 'houserent' ),
                                ),
                                'rental_form_children'  => array(
                                    'type'  => 'checkbox',
                                    'label' => esc_html__('Children', 'houserent'),
                                    'value' => true,
                                    'text'  => esc_html__('Enable', 'houserent'),
                                    'desc' => esc_html__( 'Here you can enable or disable full name field at here', 'houserent' ),
                                ),
                                'rental_form_messege'  => array(
                                    'type'  => 'checkbox',
                                    'label' => esc_html__('Your Message', 'houserent'),
                                    'value' => true,
                                    'text'  => esc_html__('Enable', 'houserent'),
                                    'desc' => esc_html__( 'Here you can enable or disable full name field at here', 'houserent' ),
                                ),
                            ),
                            'contact_form_7' => array(
                                'rental_form_cr7_shortcode'  => array(
                                    'type'  => 'text',
                                    'label' => esc_html__('Contact Form 7 Shortcode', 'houserent'),
                                    'desc' => esc_html__( 'You can paste contact form 7 shortcode from here', 'houserent' ),
                                ),
                            ),

                        ),
                        'show_borders' => false,
                    ),
                ),
            ),
        ),
    )) : array(),
    'google_map_settings' => array(
        'title' => esc_html__( 'Google Map Settings', 'houserent' ),
        'type' => 'tab',
        'options' => array(
            'map_api_key' => array(
                'type'  => 'gmap-key',
                'label' => esc_html__( 'Set Up Your Google Map API', 'houserent' ),
                'desc'  => esc_html__( 'You can add google map api from here','houserent' ),
            ),
            'support_info_map' => array(
                'label' => esc_html__( 'Map Location', 'houserent' ),
                'type'  => 'map',
                'desc'  => esc_html__( 'Type and select your location from here', 'houserent' ),
            ),
        ),
    ),
    'footer_section' => array(
        'title' => esc_html__( 'Footer Section', 'houserent' ),
        'type' => 'tab',
        'options' => array(
            'footer_title_color_type' => array(
                'type'         => 'multi-picker',
                'label'        => false,
                'desc'         => false,
                'picker'       => array(
                    'footer_title_color' => array(
                        'type'  => 'switch',
                        'label' => esc_html__( 'Widget Title Color Type', 'houserent' ),
                        'right-choice' => array(
                            'value' => 'color',
                            'label' => esc_html__( 'Color', 'houserent' )
                        ),
                        'left-choice'  => array(
                            'value' => 'gradient',
                            'label' => esc_html__( 'Gradient', 'houserent' )
                        ),
                        'value'     => 'color',
                        'desc'      => esc_html__( 'You can change footer widget title color from here','houserent' ),
                    ),
                ),
                'choices'  => array(
                    'color'  => array(
                        'footer_title_color_c'   => array(
                            'type'  => 'color-picker',
                            'label' => esc_html__( 'Solid Color', 'houserent' ),
                            'value' => '#FCA22A',
                            'desc'  => esc_html__( 'You can change background color from here', 'houserent' ),
                         ),
                    ),
                    'gradient'  => array(
                        'footer_title_gradient_c'   => array(
                            'label' => esc_html__( 'Gradient', 'houserent' ),
                            'type'  => 'gradient',
                            'value' => array(
                                    'primary'   => '#21b75f',
                                    'secondary' => '#31386e',
                            ),
                            'desc'  => esc_html__( 'Here you can change widget title gradient color', 'houserent' ),
                         ),
                    ),
                ),
                'show_borders' => false,
            ),
            'footer_bg_image'  => array(
                'type'  => 'upload',
                'label' => esc_html__( 'Footer Background', 'houserent' ),
                'desc'         => esc_html__( 'Upload footer background image from here', 'houserent' ),
            ),            
            'footer_bg_color'  => array(
                'type'  => 'color-picker',
                'label' => esc_html__( 'Footer Background Color', 'houserent' ),
                'value' => '#ffffff',
                'desc'         => esc_html__( 'Choose your footer background color here', 'houserent' ),
            ),
            'footer_widgets_columns'   => array(
                'label'   => esc_html__( 'Footer Widget Column', 'houserent' ),
                'type'    => 'image-picker',
                'value'   => '3',
                'choices' => array(
                    '1' => array(
                        'small' => array(
                            'height' => 70,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/footer/one-column.png'
                        ),
                    ),
                    '2' => array(
                        'small' => array(
                            'height' => 70,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/footer/two-columns.png'
                        ),
                    ),
                    '3' => array(
                        'small' => array(
                            'height' => 70,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/footer/three-columns.png'
                        ),
                    ),
                    '4' => array(
                        'small' => array(
                            'height' => 70,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/footer/four-columns.png'
                        ),
                    ),
                ),
            ),
            'copyright_text'  => array(
                'label' => esc_html__('Copyright Info', 'houserent'),
                'value' => wp_kses( __('&copy; 2017 <b>Houserent</b> -- Made with love by <a href="#"><b>SoftHopper</b></a>', 'houserent'), houserent_theme_html_allow() ),
                'type'  => 'text',
            ),
        ),
    ),
);