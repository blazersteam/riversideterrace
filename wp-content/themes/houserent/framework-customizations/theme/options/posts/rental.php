<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$options = array(
    'rental_post_type' => array(
        'title'   => esc_html__( 'Item details', 'houserent' ),
        'type'    => 'box',
        'options' => array(
            'rental_item_booked' => array(
                'type'    => 'checkbox',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_item_booked',
                ),
                'label'   => esc_html__( 'Booking Status:', 'houserent' ),
                'text'  => esc_html__('Item Booked', 'houserent'),
            ),
            'rental_start_date' => array(
                'type'    => 'date-picker',
                'desc'   => esc_html__( 'Enter Date when rental book item start', 'houserent' ),
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_start_date',
                ),
                'label'   => esc_html__( 'Book Start:', 'houserent' ),
                'min-date' => null, // By default minimum date will be current day. Set a date in format d-m-Y as a start date
                'max-date' => null,
            ),
            'rental_end_date' => array(
                'type'    => 'date-picker',
                'desc'   => esc_html__( 'Enter Date when rental item book end', 'houserent' ),
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_end_date',
                ),
                'label'   => esc_html__( 'Book End:', 'houserent' ),
                'min-date' => null, // By default minimum date will be current day. Set a date in format d-m-Y as a start date
                'max-date' => null,
            ),
            'rental_short_description' => array(
                'type'    => 'textarea',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_short_description',
                ),
                'label'   => esc_html__( 'Short Description:', 'houserent' ),
            ),
            'rental_location' => array(
                'type'    => 'text',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_location',
                ),
                'label'   => esc_html__( 'Location:', 'houserent' ),
            ),
            'rental_deposit' => array(
                'type'    => 'text',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_deposit',
                ),
                'label'   => esc_html__( 'Deposit / Bond:', 'houserent' ),
            ),
            'rental_bedrooms' => array(
                'type'    => 'text',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_bedrooms',
                ),
                'label'   => esc_html__( 'Bed Rooms:', 'houserent' ),
            ),
            'rental_baths' => array(
                'type'    => 'text',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_baths',
                ),
                'label'   => esc_html__( 'Baths:', 'houserent' ),
            ),
            'rental_rooms_total' => array(
                'type'    => 'text',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_rooms_total',
                ),
                'label'   => esc_html__( 'Rooms (total):', 'houserent' ),
            ),
            'rental_total_area' => array(
                'type'    => 'text',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_total_area',
                ),
                'label'   => esc_html__( 'Total Area (sq. ft):', 'houserent' ),
            ),
            'rental_floor' => array(
                'type'    => 'text',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_floor',
                ),
                'label'   => esc_html__( 'Floor:', 'houserent' ),
            ),
            'rental_total_floor' => array(
                'type'    => 'text',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_total_floor',
                ),
                'label'   => esc_html__( 'Total Floors:', 'houserent' ),
            ),
            'rental_available_from' => array(
                'type'    => 'text',
                'desc'   => esc_html__( 'Enter Date when rental item are available', 'houserent' ),
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_available_from',
                ),
                'label'   => esc_html__( 'Available From:', 'houserent' ),
            ),
            'rental_car_parking' => array(
                'type'    => 'text',
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_car_parking',
                ),
                'label'   => esc_html__( 'Car Parking Per Space:', 'houserent' ),
            ),
            'rental_price_conditions' => array(
                'type'    => 'text',
                'desc'   => esc_html__( 'Enter your price', 'houserent' ),
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_price_conditions',
                ),
                'label'   => esc_html__( 'Price:', 'houserent' ),
            ),
            'rental_price_duration' => array(
                'type'    => 'text',
                'value'   => esc_html__( 'Rent/Month', 'houserent' ),
                'desc'   => esc_html__( 'Please input Rent/Year, Rent/Month, Rent/Week, Rent/Day etc.', 'houserent' ),
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'rental_price_duration',
                ),
                'label'   => esc_html__( 'Price Duration:', 'houserent' ),
            ),
            'rental_indoor_features_items' => array(
                'label'         => esc_html__( 'Indoor Feature: ', 'houserent' ),
                'type'          => 'addable-popup',
                'desc'          => esc_html__( 'Here you add indoor feature of this rental item', 'houserent' ),
                'template'      => '{{- rental_indoor_feature }}',                                    
                'add-button-text' => esc_html__('Add Indoor Feature', 'houserent'),
                'popup-options' => array(
                    'rental_indoor_feature' => array(
                        'label' => esc_html__( 'Feature Name', 'houserent' ),
                        'type'  => 'text',
                        'desc'  => esc_html__( 'Type feature name here', 'houserent' ),
                    ),
                ),
            ),
            'rental_outdoor_features_items' => array(
                'label'         => esc_html__( 'Outdoor Feature: ', 'houserent' ),
                'type'          => 'addable-popup',
                'desc'          => esc_html__( 'Here you add outdoor feature of this rental item', 'houserent' ),
                'template'      => '{{- rental_outdoor_feature }}',                                    
                'add-button-text' => esc_html__('Add Outdoor Feature', 'houserent'),
                'popup-options' => array(
                    'rental_outdoor_feature' => array(
                        'label' => esc_html__( 'Feature Name', 'houserent' ),
                        'type'  => 'text',
                        'desc'  => esc_html__( 'Type feature name here', 'houserent' ),
                    ),
                ),
            ),
            'rental_gallery_item_images' => array(
                'label' => esc_html__( 'Photo Gallery: ', 'houserent' ),
                'type'  => 'multi-upload',
                'images_only' => true,
                'desc' => esc_html__( 'Here you add photo galley of this rental item', 'houserent' ),
            ),
        ),
    ),
);