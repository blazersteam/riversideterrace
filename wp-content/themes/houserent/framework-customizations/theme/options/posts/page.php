<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$options = array(
    'page' => array(
        'title'   => esc_html__( 'Page template settings', 'houserent' ),
        'type'    => 'box',
        'options' => array(
            'meta_page_layout'  => array(
                'label'   => esc_html__( 'Page Layout', 'houserent' ),
                'type'    => 'image-picker',
                'value'   => 'default',
                'desc'    => esc_html__( 'Hare you can set page layout (Ex. Sidebar Left, Sidebar Right or Full width).',
                    'houserent' ),
                'choices' => array(
                    'default' => array(
                        'small' => array(
                            'height' => 70,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/default.png'
                        ),
                        'large' => array(
                            'height' => 214,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/default-big.png'
                        ),
                    ),
                    'sidebar_left' => array(
                        'small' => array(
                            'height' => 70,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/left.png'
                        ),
                        'large' => array(
                            'height' => 214,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/left-big.png'
                        ),
                    ),
                    'sidebar_right' => array(
                        'small' => array(
                            'height' => 70,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/right.png'
                        ),
                        'large' => array(
                            'height' => 214,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/right-big.png'
                        ),
                    ),
                    'full_width' => array(
                        'small' => array(
                            'height' => 70,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/full.png'
                        ),
                        'large' => array(
                            'height' => 214,
                            'src'    => get_template_directory_uri() . '/assets/images/backend/page-layout/full-big.png'
                        ),
                    ),
                ),
            ),
        ),
    ),
);
