<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['id'] = 'houserent';

$manifest['supported_extensions'] = array(
	'page-builder' => array(),
	'backups' => array(),
	'wp-shortcodes' => array(),
	//'slider' => array(),
	//'styling' => array(),
	'breadcrumbs' => array(),
	//'events' => array(),
	//'feedback' => array(),
	//'learning' => array(),
	//'megamenu' => array(),
	//'portfolio' => array(),
	'sidebars' => array(),
);