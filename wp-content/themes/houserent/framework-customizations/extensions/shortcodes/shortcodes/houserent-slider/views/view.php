<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $slider_main_style = houserent_theme_builder_field( $atts['slider_main_style'] );
    $slider_style = $slider_main_style['slider_style'];
?>
<?php if( $slider_style == 'one' ): 
    $slider_one_cont = $slider_main_style['one']['slider_items'];
?>
    <div class="slider-area">
        <div class="pogoSlider">
            <?php 
                foreach ( $slider_one_cont as $item ) { 
                    $slide_bg = $item['slider_bg_image']['url'];
                    $slide_transition = ( $item['slide_transition'] ) ? $item['slide_transition'] : 'fade' ;
                    $slide_duration = ( $item['slide_duration'] ) ? $item['slide_duration'] : 1000 ;

                    $overlay_type = ( $slider_main_style['one']['overlay_style'] ) ? $slider_main_style['one']['overlay_style'] : 'color' ;
                    if( $overlay_type == 'color' ){
                        $overlay_class = 'overlay-color';
                    } elseif( $overlay_type == 'gradient' ){
                        $overlay_class = 'overlay-gradient';
                    } else{
                        $overlay_class = 'overlay-color';
                    }
            ?>
            <div class="pogoSlider-slide <?php echo esc_attr( $overlay_class ); ?>" data-transition="<?php echo esc_attr( $slide_transition ); ?>" data-duration="<?php echo esc_attr( $slide_duration ); ?>" style="background-image:url(<?php echo esc_url( $slide_bg ) ?>);">
                <div class="container-slider one">                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="slider-text-content">
                                <?php if( $item['slide_item_title'] ): ?>
                                <h3 class="pogoSlider-slide-element" data-in="slideDown" data-out="slideUp" data-duration="500" data-delay="500"><?php echo esc_html( $item['slide_item_title'] ); ?></h3>
                                <?php 
                                    endif;
                                    if( $item['slide_item_subtitle'] ):
                                ?>
                                <h2 class="pogoSlider-slide-element" data-in="slide-left" data-out="slideUp" data-duration="500" data-delay="500"><?php echo esc_html( $item['slide_item_subtitle'] ); ?></h2>
                                <?php 
                                    endif;
                                    if( $item['slide_item_description'] ):
                                ?>
                                <p class="pogoSlider-slide-element" data-in="slideDown" data-out="slideUp" data-duration="500" data-delay="500"><?php echo esc_html( $item['slide_item_description'] ); ?></p>
                                <?php 
                                    endif;
                                    if( $item['slide_btn_text'] && $item['slide_btn_url'] ):
                                ?>
                                    <a href="<?php echo esc_attr( $item['slide_btn_url'] ); ?>" class="button"><?php echo esc_html( $item['slide_btn_text'] ); ?></a>
                                <?php endif; ?>
                            </div><!-- /.text-content -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container-slider -->
            </div>
        <?php } ?>
        </div><!-- .pogoSlider -->
    </div><!-- /.slider-area container-fluid -->

    <?php elseif( $slider_style == 'two' ): 
        $slider_two_cont = $slider_main_style['two']['slider_items'];
    ?>
    <!-- ====== Main Slider Area================================== --> 
    <div class="main-slider-two default-template-gradient">
        <div class="container-fluid pd-zero">
            <div class="pogoSlider">
            <?php 
                foreach ( $slider_two_cont as $item_two ) {                
                $attachment_id = $item_two['slider_bg_image']['attachment_id'];
                $slide_transition = ( $item_two['slide_transition'] ) ? $item_two['slide_transition'] : 'fade' ;
                $slide_duration = ( $item_two['slide_duration'] ) ? $item_two['slide_duration'] : 1000 ;
                $slide_item_title = ( $item_two['slide_item_title'] ) ? $item_two['slide_item_title'] : '' ;
                $slide_title_url = ( $item_two['slide_title_url'] ) ? $item_two['slide_title_url'] : '' ;
                $slide_item_subtitle = ( $item_two['slide_item_subtitle'] ) ? $item_two['slide_item_subtitle'] : '' ;
            ?>
                <div class="pogoSlider-slide" data-transition="<?php echo esc_attr( $slide_transition ); ?>" data-duration="<?php echo esc_attr( $slide_duration ); ?>">
                    <div class="container-slider">                    
                        <div class="row tb">
                        <?php if( $attachment_id ): ?>
                            <div class="col-md-5 tb-cell">
                                <div class="show-image-content pogoSlider-slide-element" data-in="slideDown" data-out="slideUp" data-duration="750" data-delay="500">
                                    <img src="<?php echo esc_url( houserent_theme_settings_image( $attachment_id ,400 ,500 ) ); ?>" alt="<?php echo esc_html( $slide_item_title ); ?>" />
                                </div><!-- /.show-image-content -->
                            </div><!-- /.col-md-5 -->
                        <?php endif; ?>
                            <div class="col-md-7 tb-cell">
                                <div class="show-text-content">
                                <?php if( $slide_item_subtitle ): ?>
                                    <p class="pogoSlider-slide-element" data-in="slideDown" data-out="slideUp" data-duration="750" data-delay="500">
                                        <?php 
                                            $allowed_tag = array(
                                                'br' => array(),
                                            );
                                            echo wp_kses( $slide_item_subtitle , $allowed_tag ); 
                                        ?>
                                    </p>
                                <?php 
                                    endif;
                                    if( $slide_item_title ):
                                ?>
                                    <h2 class="pogoSlider-slide-element" data-in="slideDown" data-out="slideUp" data-duration="750" data-delay="500">
                                        <?php if( $slide_title_url ): ?>
                                        <a href="<?php echo esc_url( $slide_title_url ); ?>">
                                        <?php endif; ?>
                                            <?php echo esc_html( $slide_item_title ); ?>
                                        <?php if( $slide_title_url ): ?>
                                        </a>
                                        <?php endif; ?>
                                    </h2>
                                <?php endif; ?>
                                </div><!-- /.show-text-content -->
                            </div><!-- /.col-md-7 -->
                        </div><!-- /.row -->
                    </div><!-- /.container-slider -->
                </div>
            <?php } ?>
            </div><!-- .pogoSlider -->
        </div><!-- /.container-fluid -->
    </div><!-- /.main-slider -->

    <?php elseif( $slider_style == 'three' ): 
        $slider_three_cont = $slider_main_style['three']['slider_items'];
    ?>
    <!-- ====== Main Slider Area======= --> 
    <div class="main-slide eight">
        <div class="pogoSlider">
            <?php foreach ( $slider_three_cont as $item ) { 
                $slide_bg = $item['slider_bg_image']['url'];
                $slide_transition = ( $item['slide_transition'] ) ? $item['slide_transition'] : 'fade' ;
                $slide_duration = ( $item['slide_duration'] ) ? $item['slide_duration'] : 1000 ;
            ?>
            <div class="pogoSlider-slide" data-transition="<?php echo esc_attr( $slide_transition ); ?>" data-duration="<?php echo esc_attr( $slide_duration ); ?>" style="background-image:url(<?php echo esc_url( $slide_bg ) ?>);">
                <div class="container-slider one">                    
                    <div class="row">
                        <div class="col-md-12">
                           <div class="slider-content-eight">
                            <?php if( $item['slide_item_title'] ): ?>
                            <h3 class="pogoSlider-slide-element" data-in="slideDown" data-out="slideUp" data-duration="500" data-delay="500"><?php echo esc_html( $item['slide_item_title'] ); ?></h3>
                            <?php 
                                endif;
                                if( $item['slide_item_description'] ):
                            ?>
                            <p class="pogoSlider-slide-element" data-in="slideDown" data-out="slideUp" data-duration="500" data-delay="500"><?php echo esc_html( $item['slide_item_description'] ); ?></p>
                            <?php  endif; ?>
                          </div><!-- /.slider-content -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container-slider -->
            </div>
            <?php } ?> 
        </div><!-- .pogoSlider -->
    </div><!-- /.main-slider -->
<?php endif; ?>