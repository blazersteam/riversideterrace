<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $contact_map_title = houserent_theme_builder_field( $atts['contact_map_title'] );
    $contact_map_subtitle = houserent_theme_builder_field( $atts['contact_map_subtitle'] );
    $contact_map_gradient = houserent_theme_builder_field( $atts['contact_map_gradient'] );
    $contact_map_description = houserent_theme_builder_field( $atts['contact_map_description'] );
    $contact_map_description = houserent_theme_builder_field( $atts['contact_map_description'] );
    $contact_map_location = houserent_theme_builder_field( $atts['contact_map_location'] );
    $contact_map_api = houserent_theme_builder_field( $atts['contact_map_api'] );
    $contact_map_hight = houserent_theme_builder_field( $atts['contact_map_hight'] );
    $contact_map_hight = ( $contact_map_hight ) ? $contact_map_hight : '540' ;

    $text_gradiant = "background: linear-gradient(330deg, ".esc_attr( $contact_map_gradient['secondary'] )." 0%, ".esc_attr( $contact_map_gradient['primary'] )." 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;";


    $location_ = $contact_map_location['location'];
    $venue_ = $contact_map_location['venue'];

    $location = str_replace( ' ', '+', $location_);
    $venue = str_replace( ' ', '+', $venue_);

?>
<!-- ======map-area====== --> 
<div class="map-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-content-one">
                    <?php if( $contact_map_title ): ?>
                        <h5><?php echo esc_html( $contact_map_title ); ?></h5>
                    <?php 
                        endif;
                        if( $contact_map_subtitle ):
                    ?>
                        <h2 class="title default-text-gradient" style="<?php echo esc_attr( $text_gradiant ); ?>"><?php echo esc_html( $contact_map_subtitle ); ?></h2>
                    <?php 
                        endif;
                        if( $contact_map_description ):
                    ?>
                        <p><?php echo esc_html( $contact_map_description ); ?></p>
                    <?php endif; ?>

                </div><!-- /.about-heading-content -->
                   <div class="map-content">
                    <?php if( $contact_map_api && $contact_map_location ): ?>
                       <<?php echo 'iframe';?> height="<?php echo esc_attr( $contact_map_hight ); ?>" src="https://www.google.com/maps/embed/v1/place?key=<?php echo esc_attr( $contact_map_api ); ?>&q=<?php echo esc_attr( $venue ).','.esc_attr( $location ); ?>" allowfullscreen="allowfullscreen"></<?php echo 'iframe';?>>
                    <?php endif; ?>
                   </div><!-- /.map-content -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.map-content-area -->