<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'contact_map_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Find Our location', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change section title', 'houserent' ),
    ),
    'contact_map_subtitle' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Map &amp; Directions', 'houserent' ),
        'desc'  => esc_html__( 'You can change section subtitle', 'houserent' ),
    ),
    'contact_map_gradient' => array(
        'label' => esc_html__( 'Subtitle Gradient Colors', 'houserent' ),
        'type'  => 'gradient',
        'value' => array(
                'primary'   => '#21b75f',
                'secondary' => '#31386e',
        ),
        'desc'  => esc_html__( 'Here you can change section subtitle gradient color', 'houserent' ),
    ),
    'contact_map_description' => array(
        'label' => esc_html__( 'Section description', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Find out how to find us from your current location', 'houserent' ),
        'desc'  => esc_html__( 'You can change section description', 'houserent' ),
    ),
    'contact_map_location' => array(
        'label' => esc_html__( 'Map Selector', 'houserent' ),
        'type'  => 'map',
        'desc'  => esc_html__( 'Here type and find your location from google map', 'houserent' ),
    ),
    'contact_map_api' => array(
        'label' => esc_html__( 'Map API Key', 'houserent' ),
        'type'  => 'text',
        'desc'  => esc_html__( 'Put your google map API key here', 'houserent' ),
    ),
    'contact_map_hight' => array(
        'label' => esc_html__( 'Map Hight', 'houserent' ),
        'type'  => 'text',
        'value'  => '540',
        'desc'  => esc_html__( 'Change map hight from here', 'houserent' ),
    ),
);