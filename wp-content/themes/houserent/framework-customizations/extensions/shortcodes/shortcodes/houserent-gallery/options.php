<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'gallery_section_style'   => array(
         'type'  => 'select',
         'label' => esc_html__( 'Style', 'houserent' ),
         'value' =>  'one',
         'choices' => array(
             'one' => esc_html__( 'One', 'houserent' ),
             'two' => esc_html__( 'Two', 'houserent' ),
             'three' => esc_html__( 'Three', 'houserent' ),
         ),
     ),
    'gallery_section_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Our Photo Gallery', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change section title (HTML a, br, em, strong tags are allowed)', 'houserent' ),
    ),
    'gallery_section_sub_title' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Best of our Event portfolio Photos', 'houserent' ),
        'desc'  => esc_html__( 'You change about us section subtitle (HTML a, br, em, strong tags are allowed)', 'houserent' ),
    ),
    'gallery_section_description' => array(
        'label' => esc_html__( 'Description', 'houserent' ),
        'type'  => 'textarea',
        'desc'  => esc_html__( 'Type text description here (HTML a, br, em, strong tags are allowed)', 'houserent' ),
    ),
    'gallery_section_button_text' => array(
        'label' => esc_html__( 'Button Text', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'All photos & videos', 'houserent' ),
        'desc'  => esc_html__( 'You change button text from here', 'houserent' ),
    ),
    'gallery_section_button_url' => array(
        'label' => esc_html__( 'Button URL', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( '#', 'houserent' ),
        'desc'  => esc_html__( 'You change button URL from here', 'houserent' ),
    ),
    'gallery_items' => array(
        'label'         => esc_html__( 'Add items: ', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you add gallery item from here', 'houserent' ),
        'template'      => '{{- gallery_item_title }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'gallery_item_title' => array(
                'label' => esc_html__( 'Title', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type title here', 'houserent' ),
            ),
            'gallery_item_image' => array(
                'label' => esc_html__( 'Image', 'houserent' ),
                'type'  => 'upload',
                'desc'  => esc_html__( 'Upload image here', 'houserent' ),
            ),
        ),
    ),
);