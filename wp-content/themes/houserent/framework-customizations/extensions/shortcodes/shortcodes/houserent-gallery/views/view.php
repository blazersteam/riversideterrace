<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $gallery_section_style = houserent_theme_builder_field( $atts['gallery_section_style'] );
    $gallery_section_title = houserent_theme_builder_field( $atts['gallery_section_title'] );
    $gallery_section_sub_title = houserent_theme_builder_field( $atts['gallery_section_sub_title'] );
    $gallery_section_description = houserent_theme_builder_field( $atts['gallery_section_description'] );
    $gallery_section_button_text = houserent_theme_builder_field( $atts['gallery_section_button_text'] );
    $gallery_section_button_url = houserent_theme_builder_field( $atts['gallery_section_button_url'] );
    $gallery_items = houserent_theme_builder_field( $atts['gallery_items'] );

    if( $gallery_section_style == 'one' ):
?>
    <!-- ======gallery-area================================== --> 
    <div class="gallery-area">
        <div class="container-fluid">
            <div class="container-large-device">
                <div class="row">
                    <div class="col-md-7">
                    <?php if( $gallery_items ): ?>
                        <div class="gallery-left-content">
                            <div class="row">
                            <?php 
                                foreach ( $gallery_items as $item ) { 
                                    $item_image = $item['gallery_item_image']['attachment_id'];
                                    if ( $item_image ) {
                                        $image_url = houserent_theme_settings_image( $item_image ,266 , 300 );
                                        $full_size_image_url = wp_get_attachment_image_src( $item_image, 'full' )[0];
                                    } else {
                                        $image_url = '';
                                        $full_size_image_url = '';
                                    }
                            ?>
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                    <a class="image-pop-up" href="<?php echo esc_url( $full_size_image_url ); ?>">
                                        <img src="<?php echo esc_url($image_url); ?>" alt="<?php echo esc_html( $item['gallery_item_title'] ); ?>" />
                                    </a>
                                </div><!-- /.col-md-4 -->
                            <?php } ?>
                            </div><!-- /.row -->
                        </div><!-- /.left-content -->
                    <?php endif; ?>
                    </div><!-- /.col-md-7 -->
                    <div class="col-md-5">
                        <div class="gallery-right-content">
                        <?php  
                            $allowed_tag = array(
                                'a' => array(
                                    'href' => array(),
                                    'title' => array()
                                ),
                                'br' => array(),
                                'em' => array(),
                                'strong' => array(),
                            );
                            if( $gallery_section_title ): 
                        ?>
                            <h2><?php echo wp_kses( $gallery_section_title, $allowed_tag ); ?></h2>
                        <?php 
                            endif;
                            if( $gallery_section_sub_title ):
                        ?>
                            <h3><?php echo wp_kses( $gallery_section_sub_title, $allowed_tag ); ?></h3>
                        <?php
                            endif;
                            if( $gallery_section_description ): 
                        ?>
                            <p><?php echo wp_kses( $gallery_section_description, $allowed_tag ); ?></p>
                        <?php 
                            endif;
                            if( $gallery_section_button_text || $gallery_section_button_url ):
                        ?>
                            <a href="<?php echo esc_url( $gallery_section_button_url ); ?>" class="button nevy-button"><?php echo esc_html( $gallery_section_button_text ); ?></a>
                        <?php endif; ?>
                        </div><!-- /.right-content -->
                    </div><!-- /.col-md-5 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.container-large-device -->
    </div><!-- /.gallery-area -->

<?php 
    elseif( $gallery_section_style == 'two' ): 
?>
    <!-- ======gallery-area================================== --> 
    <div class="gallery-area four">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="gallery-left-content">
                        <?php  
                            $allowed_tag = array(
                                'a' => array(
                                    'href' => array(),
                                    'title' => array()
                                ),
                                'br' => array(),
                                'em' => array(),
                                'strong' => array(),
                            );
                            if( $gallery_section_title ): 
                        ?>
                            <h2><?php echo wp_kses( $gallery_section_title, $allowed_tag ); ?></h2>
                        <?php 
                            endif;
                            if( $gallery_section_button_text || $gallery_section_button_url ):
                        ?>
                            <a href="<?php echo esc_url( $gallery_section_button_url ); ?>" class="button nevy-button"><?php echo esc_html( $gallery_section_button_text ); ?></a>
                        <?php endif; ?>
                    </div><!-- /.right-content -->
                </div><!-- /.col-md-4 -->
                <div class="col-md-8 col-sm-8">
                <?php if( $gallery_items ): ?>
                    <div class="gallery-slider owl-carousel owl-theme">
                    <?php 
                        foreach ( $gallery_items as $item ) { 
                            $item_image = $item['gallery_item_image']['attachment_id'];
                            if ( $item_image ) {
                                $image_url = houserent_theme_settings_image( $item_image ,230 ,259 );
                                $full_size_image_url = wp_get_attachment_image_src( $item_image, 'full' )[0];
                            } else {
                                $image_url = '';
                                $full_size_image_url = '';
                            }
                    ?>
                        <div class="item">
                            <a href="<?php echo esc_url( $full_size_image_url ); ?>"><img src="<?php echo esc_url($image_url); ?>" alt="<?php echo esc_html( $item['gallery_item_title'] ); ?>" /></a>
                        </div><!-- /.item -->
                    <?php } ?>
                    </div><!-- /.owl-demo --> 
                <?php endif; ?>
                </div><!-- /.col-md-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.gallery-area -->
<?php 
    elseif( $gallery_section_style == 'three' ): 
?>
    <!-- ====== Gallery Area ====== --> 
    <div class="gallery-area">
        <div class="container-fluid">
            <div class="container-large-device">
                <div class="row">
                    <div class="col-md-7">
                    <?php if( $gallery_items ): ?>
                        <div class="gallery-left-content">
                            <div class="row">
                            <?php 
                                $i = 1;
                                foreach ( $gallery_items as $item ) { 
                                    $item_image = $item['gallery_item_image']['attachment_id'];
                                    if ( $item_image ) {
                                        $image_url = houserent_theme_settings_image( $item_image ,227 ,256 );
                                    } else {
                                        $image_url = '';
                                    }
                            ?>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img src="<?php echo esc_url($image_url); ?>" alt="<?php echo esc_html( $item['gallery_item_title'] ); ?>" />
                                </div><!-- /.col-md-4 -->
                            
                            <?php 
                                if( $i == 3 ){ break; }
                                $i++;
                                } 
                              ?>

                            </div><!-- /.row -->
                        </div><!-- /.left-content -->
                    <?php endif; ?>
                    </div><!-- /.col-md-7 -->
                    <div class="col-md-5">
                        <div class="gallery-right-content">
                        <?php  
                            $allowed_tag = array(
                                'a' => array(
                                    'href' => array(),
                                    'title' => array()
                                ),
                                'br' => array(),
                                'em' => array(),
                                'strong' => array(),
                            );
                            if( $gallery_section_title ): 
                        ?>
                            <h2><?php echo wp_kses( $gallery_section_title, $allowed_tag ); ?></h2>
                        <?php 
                            endif;
                            if( $gallery_section_button_text || $gallery_section_button_url ):
                        ?>
                            <a href="<?php echo esc_url( $gallery_section_button_url ); ?>" class="button nevy-button"><?php echo esc_html( $gallery_section_button_text ); ?></a>
                        <?php endif; ?>
                        </div><!-- /.right-content -->
                    </div><!-- /.col-md-5 -->
                </div><!-- /.row -->
            </div><!-- /.container-large-device -->
        </div><!-- /.container -->
    </div><!-- /.gallery-area -->
<?php endif; ?>