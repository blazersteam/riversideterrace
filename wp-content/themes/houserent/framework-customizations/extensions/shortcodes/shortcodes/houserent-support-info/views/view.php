<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $contact_style = houserent_theme_builder_field( $atts['support_info_style'] );
    $support_info_section_title = houserent_theme_builder_field( $atts['support_info_section_title'] );
    $support_info_section_sub_title = houserent_theme_builder_field( $atts['support_info_section_sub_title'] );
    $support_info_map = houserent_theme_builder_field( $atts['support_info_map'] );
    $support_info_section_map_api = houserent_theme_builder_field( $atts['support_info_section_map_api'] );
    $support_info_items = houserent_theme_builder_field( $atts['support_info_items'] );
    $support_info_map_hight = houserent_theme_builder_field( $atts['support_info_map_hight'] );

    $location = $support_info_map['location'];
    $location = str_replace( ' ', '+', $location);
    $venue = $support_info_map['venue'];
    $venue = str_replace( ' ', '+', $venue);

    if( $contact_style == 'one' ){
?>
<!-- ======contact-area================================== --> 
<div class="contact-area">
    <div class="container-large-device">
        <div class="container-fluid">
        <?php if( $support_info_section_title || $support_info_section_sub_title ): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-content-two available">
                    <?php if( $support_info_section_title ): ?>
                        <h2 class="title">
                            <?php 
                                $allowed_tag = array(
                                    'br' => array(),
                                    'em' => array(),
                                );
                                echo wp_kses( $support_info_section_title, $allowed_tag ); 
                            ?>
                        </h2>
                    <?php 
                        endif;
                        if( $support_info_section_sub_title ):
                    ?>
                        <h5 class="sub-title">
                            <?php 
                                $allowed_tag = array(
                                    'br' => array(),
                                    'em' => array(),
                                );
                                echo wp_kses( $support_info_section_sub_title, $allowed_tag ); 
                            ?>
                        </h5>
                    <?php endif; ?>
                    </div><!-- /.testimonial-heading-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        <?php endif; ?>
            <div class="row">
                <div class="col-md-7">
                    <div class="map-left-content">
                        <?php if( $support_info_map && $support_info_section_map_api ): ?>
                            <<?php echo 'iframe';?> height="<?php echo esc_attr( $support_info_map_hight ); ?>" src="https://www.google.com/maps/embed/v1/place?key=<?php echo esc_attr( $support_info_section_map_api ); ?>&q=<?php echo esc_attr( $venue ).','.esc_attr( $location ); ?>" allowfullscreen="allowfullscreen"></<?php echo 'iframe';?>>
                        <?php endif; ?>
                    </div><!-- /.map-left-content -->
                </div><!-- /.col-md-7 -->
                <div class="col-md-5">
                <?php if( $support_info_items ): ?>
                    <div class="map-right-content">
                        <div class="row">
                            <?php 
                                $i = 1;
                                foreach ( $support_info_items as $item) {
                            ?>
                                <div class="col-md-6 col-sm-6">
                                    <div class="contact">
                                    <?php if( $item['support_info_item_icon'] || $item['support_info_item_title'] ): ?>
                                        <h4>
                                        <?php if( $item['support_info_item_icon'] ):  ?>
                                            <?php 
                                                if ( $item['support_info_item_icon']['type'] == 'icon-font' ) {
                                            ?>
                                                <i class="<?php echo esc_attr($item['support_info_item_icon']['icon-class']);?>"></i>
                                            <?php
                                                } else if ( $item['support_info_item_icon']['type'] == 'custom-upload' ) {
                                                        $image_url = houserent_theme_settings_image( $item['support_info_item_icon']['attachment-id'] ,30 ,30 );
                                                    ?>
                                                        <img class="icon-image" src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( $item['support_info_item_title'] ); ?>" />
                                                    <?php
                                                }
                                            ?>
                                        <?php 
                                            endif;
                                            if( $item['support_info_item_title'] ):
                                        ?>
                                            <?php echo esc_html( $item['support_info_item_title'] ); ?>
                                        <?php endif; ?>
                                        </h4>
                                    <?php 
                                        endif;
                                        if( $item['support_info_item_description'] ):
                                    ?>
                                        <p>
                                        <?php 
                                            $allowed_tag = array(
                                                'a' => array(
                                                    'href' => array(),
                                                    'title' => array()
                                                ),
                                                'i' => array(
                                                    'class' => array(),
                                                ),
                                                'br' => array(),
                                                'em' => array(),
                                                'strong' => array(),
                                                'h2' => array(),
                                                'h3' => array(),
                                                'h4' => array(),
                                                'ul' => array(),
                                                'ol' => array(),
                                                'li' => array(),
                                            );
                                            echo wp_kses( $item['support_info_item_description'], $allowed_tag ); 
                                        ?>
                                        </p>
                                    <?php endif; ?>
                                    </div><!-- /.contact -->
                                </div><!-- /.col-md-6 -->
                            <?php
                                if ( $i % 2 == 0 ) {
                                    ?><div class="clearfix"></div><!-- /.clearfix --><?php 
                                }
                                $i++; 
                                } 
                            ?>
                        </div>   
                    </div><!-- /.map-right-content -->
                <?php endif; ?>
                </div><!-- /.col-md-5 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div><!-- /.contact-area -->
<?php }elseif( $contact_style == 'two' ){ ?>
    <!-- ====== Contact Area ======= --> 
    <div class="contact-area pd-zero">
        <div class="container-fluid pd-zero">
            <div class="row">
                <div class="col-md-7">
                    <div class="map-left-content">
                       <?php if( $support_info_map && $support_info_section_map_api ): ?>
                        <<?php echo 'iframe';?> height="<?php echo esc_attr( $support_info_map_hight ); ?>" src="https://www.google.com/maps/embed/v1/place?key=<?php echo esc_attr( $support_info_section_map_api ); ?>&q=<?php echo esc_attr( $venue ).','.esc_attr( $location ); ?>" allowfullscreen="allowfullscreen"></<?php echo 'iframe';?>>
                    <?php endif; ?>
                    </div><!-- /.map-left-content -->
                </div><!-- /.col-md-7 -->
                <div class="col-md-5">
                    <?php if( $support_info_section_title || $support_info_section_sub_title ): ?>
                        <div class="heading-content-two available">
                             <h2 class="title">
                            <?php 
                                $allowed_tag = array(
                                    'br' => array(),
                                    'em' => array(),
                                );
                                echo wp_kses( $support_info_section_title, $allowed_tag ); 
                            ?>
                        </h2>
                    <?php 
                        if( $support_info_section_sub_title ):
                    ?>
                        <h5 class="sub-title">
                            <?php 
                                $allowed_tag = array(
                                    'br' => array(),
                                    'em' => array(),
                                );
                                echo wp_kses( $support_info_section_sub_title, $allowed_tag ); 
                            ?>
                        </h5>
                    <?php endif; ?>
                        </div><!-- /.testimonial-heading-content -->
                    <?php endif; ?>
                    <?php if( $support_info_items ): ?>
                    <div class="map-right-content">
                        <div class="row">
                            <?php 
                                $i = 1;
                                foreach ( $support_info_items as $item) {
                            ?>
                            <div class="col-md-6 col-sm-6">
                                <div class="contact">
                                    <?php if( $item['support_info_item_icon'] || $item['support_info_item_title'] ): ?>
                                        <h4>
                                            <?php if( $item['support_info_item_icon'] ):  ?>
                                                <?php 
                                                    if ( $item['support_info_item_icon']['type'] == 'icon-font' ) {
                                                ?>
                                                    <i class="<?php echo esc_attr($item['support_info_item_icon']['icon-class']);?>"></i>
                                                <?php } 
                                                else if ( $item['support_info_item_icon']['type'] == 'custom-upload' ) {
                                                            $image_url = houserent_theme_settings_image( $item['support_info_item_icon']['attachment-id'] ,30 ,30 );
                                                        ?>
                                                            <img class="icon-image" src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( $item['support_info_item_title'] ); ?>" />
                                                        <?php
                                                    }
                                                ?>
                                            <?php endif; 
                                            if( $item['support_info_item_title'] ):
                                            ?>
                                            <?php echo esc_html( $item['support_info_item_title'] ); 
                                            endif;
                                            ?>
                                        </h4>
                                    <?php
                                        endif;
                                        if( $item['support_info_item_description'] ):
                                    ?>
                                        <p>
                                            <?php 
                                                $allowed_tag = array(
                                                    'a' => array(
                                                        'href' => array(),
                                                        'title' => array()
                                                    ),
                                                    'i' => array(
                                                        'class' => array(),
                                                    ),
                                                    'br' => array(),
                                                    'em' => array(),
                                                    'strong' => array(),
                                                    'h2' => array(),
                                                    'h3' => array(),
                                                    'h4' => array(),
                                                    'ul' => array(),
                                                    'ol' => array(),
                                                    'li' => array(),
                                                );
                                                echo wp_kses( $item['support_info_item_description'], $allowed_tag ); 
                                            ?>
                                        </p>
                                    <?php endif; ?>
                                </div><!-- /.contact -->
                            </div><!-- /.col-md-6 -->
                            <?php
                                if ( $i % 2 == 0 ):
                            ?>
                                <div class="clearfix"></div><!-- /.clearfix -->
                            <?php 
                                endif;
                                $i++; 
                                } 
                            ?>
                        </div><!-- /.row -->
                    </div><!-- /.map-right-content -->
                    <?php endif; ?>
                </div><!-- /.col-md-5 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.contact-area -->
<?php } ?>