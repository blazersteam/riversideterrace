<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'support_info_style'   => array(
        'type'  => 'switch',
        'label' => esc_html__( 'Style', 'houserent' ),
        'right-choice' => array(
            'value' => 'one',
            'label' => esc_html__( 'One', 'houserent' )
        ),
        'left-choice'  => array(
            'value' => 'two',
            'label' => esc_html__( 'Two', 'houserent' )
        ),
        'value'        => 'one',
        'desc'         => esc_html__( 'You can change support info style from here', 'houserent' ),
    ),
    'support_info_section_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'We Are Available For You 24/7', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change section title (support HTML tag - br, em)', 'houserent' ),
    ),
    'support_info_section_sub_title' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Our online support service is always 24 Hours', 'houserent' ),
        'desc'  => esc_html__( 'You can change section subtitle (support HTML tag - br, em)', 'houserent' ),
    ),
    'support_info_section_map_api' => array(
        'label' => esc_html__( 'Google Map API', 'houserent' ),
        'type'  => 'gmap-key',
        'desc'  => esc_html__( 'Put your google map API here', 'houserent' ),
    ),
    'support_info_map' => array(
        'label' => esc_html__( 'Map Location', 'houserent' ),
        'type'  => 'map',
        'desc'  => esc_html__( 'Type and select your location from here', 'houserent' ),
    ),
    'support_info_map_hight' => array(
        'label' => esc_html__( 'Map Hight', 'houserent' ),
        'type'  => 'text',
        'value'  => '540',
        'desc'  => esc_html__( 'Change map hight from here', 'houserent' ),
    ),
    'support_info_items' => array(
        'label'         => esc_html__( 'Add items: ', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you add support info item from here', 'houserent' ),
        'template'      => '{{- support_info_item_title }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'support_info_item_icon' => array(
                'label' => esc_html__( 'Icon', 'houserent' ),
                'type'  => 'icon-v2',
                'desc'  => esc_html__( 'Select icon from here', 'houserent' ),
            ),
            'support_info_item_title' => array(
                'label' => esc_html__( 'Title', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type title here (support HTML tag - br, em)', 'houserent' ),
            ),
            'support_info_item_description' => array(
                'label' => esc_html__( 'Description', 'houserent' ),
                'type'  => 'textarea',
                'desc'  => esc_html__( 'Type text description here (support HTML tag - a, i, br, em, strong, h2, h3, h4, ul, ol, li)', 'houserent' ), ),
        ),
    ),
);