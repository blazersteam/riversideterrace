<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'testimonial_section_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Testimonial', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change testimonial section title', 'houserent' ),
    ),
	'testimonial_section_sub_title' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'SOME REVIEWS', 'houserent' ),
        'desc'  => esc_html__( 'You change testimonial section subtitle', 'houserent' ),
    ),
    'testimonial_items' => array(
        'label'         => esc_html__( 'Add items: ', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you add testimonial item from here', 'houserent' ),
        'template'      => '{{- testimonial_item_name }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'testimonial_item_image' => array(
                'label' => esc_html__( 'Image', 'houserent' ),
                'type'  => 'upload',
                'desc'  => esc_html__( 'Upload user image here', 'houserent' ),
            ),
            'testimonial_item_name' => array(
                'label' => esc_html__( 'Title', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type user title here', 'houserent' ),
            ),
            'testimonial_item_user_position' => array(
                'label' => esc_html__( 'User Position', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type user position here', 'houserent' ),
            ),
            'testimonial_item_description' => array(
                'label' => esc_html__( 'Description', 'houserent' ),
                'type'  => 'textarea',
                'desc'  => esc_html__( 'Type testimonial full text description here', 'houserent' ),
            ),
            'testimonial_item_rating'  => array(
                'label'   => esc_html__( 'Select Rating :', 'houserent' ),
                'type'  => 'slider',
                'value' => 0,                                        
                'properties' => array(
                    'min' => 0,
                    'max' => 5,
                    'step' => 1,
                ),
                'desc'  => esc_html__('Select rating is between 0 to 5.', 'houserent'),
            ),
        ),
    ),
);