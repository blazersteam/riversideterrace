<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $testimonial_section_title = houserent_theme_builder_field( $atts['testimonial_section_title'] );
    $testimonial_section_sub_title = houserent_theme_builder_field( $atts['testimonial_section_sub_title'] );
    $testimonial_items = houserent_theme_builder_field( $atts['testimonial_items'] );
?>
    <!-- ====== Testimonial Area ====== --> 
    <div class="testimonial-area">
        <div class="container">
        <?php if( $testimonial_section_title || $testimonial_section_sub_title ): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="testimonial-heading-content">
                        <?php if( $testimonial_section_title ): ?>
                            <h2 class="title"><?php echo esc_html( $testimonial_section_title ); ?></h2>
                        <?php endif; ?>
                        <i class="fa fa-quote-right"></i>
                        <?php if( $testimonial_section_sub_title ): ?>
                            <h2 class="sub-title"><?php echo esc_html( $testimonial_section_sub_title ); ?></h2>
                        <?php endif; ?>
                    </div><!-- /.testimonial-heading-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        <?php endif; ?>
        <?php if( $testimonial_items ): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="testimonial-slider owl-carousel owl-theme">

                    <?php foreach ( $testimonial_items as $single_item) { ?>
                        <div class="item">
                            <div class="client-image">
                                <?php if( $single_item['testimonial_item_image'] ): ?>
                                    <?php $attachment_id = $single_item['testimonial_item_image']['attachment_id']; ?>
                                    <img src="<?php echo esc_url( houserent_theme_settings_image( $attachment_id ,70 ,70 ) ); ?>" alt="<?php echo esc_html( $single_item['testimonial_item_name'] ); ?>" />
                                <?php endif; ?>
                            </div><!-- /.client-image -->
                            <div class="client-content"> 

                                <?php if( $single_item['testimonial_item_name'] ): ?>
                                    <h3><?php echo esc_html( $single_item['testimonial_item_name'] ); ?></h3>
                                <?php endif; ?>

                                <?php if( $single_item['testimonial_item_user_position'] ): ?>
                                    <h5><?php echo esc_html( $single_item['testimonial_item_user_position'] ); ?></h5>
                                <?php endif; ?>

                                <?php if( $single_item['testimonial_item_description'] ): ?>
                                    <p><?php echo esc_html( $single_item['testimonial_item_description'] ); ?></p>
                                <?php endif; ?>

                                <?php if( $single_item['testimonial_item_rating'] ): ?>
                                    <div class="star">
                                        <?php echo houserent_theme_number_to_stars($single_item['testimonial_item_rating']); ?>
                                    </div><!-- /.star -->
                                <?php endif; ?>

                            </div><!-- /.client-content -->
                        </div><!-- /.item -->
                    <?php } ?>

                    </div><!-- /.testimonial-slider -->           
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        <?php endif; ?>
        </div><!-- /.container -->
    </div><!-- /.testimonial-area -->