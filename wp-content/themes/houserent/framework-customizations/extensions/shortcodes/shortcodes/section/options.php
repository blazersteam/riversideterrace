<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
	'is_fullwidth' => array(
		'label'        => esc_html__('Full Width', 'houserent'),
		'type'         => 'switch',
	),
	'is_fullwidth_padding' => array(
        'type'  => 'switch',
		'label'        => esc_html__('Padding', 'houserent'),
        'right-choice' => array(
            'value' => 'yes',
            'label' => esc_html__( 'Yes', 'houserent' )
        ),
        'left-choice'  => array(
            'value' => 'no',
            'label' => esc_html__( 'No', 'houserent' )
        ),
        'value' => 'yes',
        'desc'  => esc_html__('Do you want to enable left and right padding in section ?', 'houserent'),
	),
	'background_color' => array(
		'label' => esc_html__('Background Color', 'houserent'),
		'desc'  => esc_html__('Please select the background color', 'houserent'),
		'type'  => 'color-picker',
	),
	'background_image' => array(
		'label'   => esc_html__('Background Image', 'houserent'),
		'desc'    => esc_html__('Please select the background image', 'houserent'),
		'type'    => 'background-image',
		'choices' => array(
		)
	),
	'video' => array(
		'label' => esc_html__('Background Video', 'houserent'),
		'desc'  => esc_html__('Insert Video URL to embed this video', 'houserent'),
		'type'  => 'text',
	)
);
