<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'builder_divider_margin_top' => array(
        'label' => esc_html__( 'Margin Top', 'houserent' ),
        'type'  => 'text',
        'value' =>  '20px',
        'desc'  => esc_html__( 'You can set how much space you want on the top of the divider', 'houserent' ),
    ),
	'builder_divider_margin_bottom' => array(
        'label' => esc_html__( 'Margin Bottom', 'houserent' ),
        'type'  => 'text',
        'value' =>  '20px',
        'desc'  => esc_html__( 'You can set how much space you want under the bottom of the divider', 'houserent' ),
    ),
);