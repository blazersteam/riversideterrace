<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $divider_margin_top = houserent_theme_builder_field( $atts['builder_divider_margin_top'] );
    $divider_margin_bottom = houserent_theme_builder_field( $atts['builder_divider_margin_bottom'] );

?>
<div class="container-fluid section-divider">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="border-bottom-style" style="margin-top: <?php echo esc_attr( $divider_margin_top );?>; margin-bottom: <?php echo esc_attr( $divider_margin_bottom );?>">
                <div>
                    <span></span>
                </div>
            </div>
            <!--/.border-bottom-style-->
        </div>
        <!--/.col-md-12-->
    </div>
    <!--/.row-->
</div>