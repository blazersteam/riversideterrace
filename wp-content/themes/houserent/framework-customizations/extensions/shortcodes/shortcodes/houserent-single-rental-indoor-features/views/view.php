<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php if ( is_single() && get_post_type( get_the_ID() ) == 'rental' ) { ?>
<?php 
    $section_indoor_heading = houserent_theme_builder_field( $atts['section_indoor_title'] );
    $section_outdoor_heading = houserent_theme_builder_field( $atts['section_outdoor_title'] );
?>
<?php 
    $rental_indoor_features_items = houserent_theme_uny_post_meta('rental_indoor_features_items','');
    $rental_outdoor_features_items = houserent_theme_uny_post_meta('rental_outdoor_features_items','');
    if( $rental_indoor_features_items || $rental_outdoor_features_items ): 
?>
<div class="indoor-features">
    <div class="row">
        <?php if( $rental_indoor_features_items ): ?>
            <div class="col-md-6">
                <h3 class="features-title"><?php echo esc_html($section_indoor_heading); ?></h3>
                <ul class="features-list">
                <?php foreach ( $rental_indoor_features_items as $feature ){ ?>
                    <li><?php echo esc_html( $feature['rental_indoor_feature'] );?></li>
                <?php } ?>
                </ul>
            </div><!-- /.col-md-6 -->
        <?php 
            endif;
            if( $rental_outdoor_features_items ): 
        ?>
            <div class="col-md-6">
                <h3 class="features-title"><?php echo esc_html($section_outdoor_heading); ?></h3>
                <ul class="features-list">
                <?php foreach ( $rental_outdoor_features_items as $feature ){ ?>
                    <li><?php echo esc_html( $feature['rental_outdoor_feature'] );?></li>
                <?php } ?>
                </ul>
            </div><!-- /.col-md-6 -->
        <?php endif; ?>
    </div><!-- /.row -->
</div><!-- /.indoor -->
<?php endif; ?>
<?php } ?>
