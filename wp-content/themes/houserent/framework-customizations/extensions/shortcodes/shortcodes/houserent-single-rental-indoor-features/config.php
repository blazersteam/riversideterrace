<?php if (!defined('FW')) die('Forbidden');

if ( get_post_type( get_the_ID() ) == 'rental' ) {
	$cfg = array(
		'page_builder' => array(
			'title'       => esc_html__( 'Rental Indoor Features', 'houserent' ),
			'description' => esc_html__( 'This shortcode avaiable for rental single page', 'houserent' ),
			'tab'         => esc_html__( 'Rental Single' , 'houserent' ), 
	        'popup_size'    => 'small', 
		)
	);
}


