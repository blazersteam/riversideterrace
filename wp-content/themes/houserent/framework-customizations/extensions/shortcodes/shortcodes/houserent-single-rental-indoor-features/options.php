<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'section_indoor_title' => array(
	    'label'   => esc_html__( 'Heading Indoor Title:', 'houserent' ),
	    'type'    => 'text',
	    'desc'    => esc_html__( 'Put heading title here.', 'houserent' ),
	),	
	'section_outdoor_title' => array(
	    'label'   => esc_html__( 'Heading Outdoor Title:', 'houserent' ),
	    'type'    => 'text',
	    'desc'    => esc_html__( 'Put heading title here.', 'houserent' ),
	),
);