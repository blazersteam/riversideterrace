<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'coming_soon_paralux' => array(
        'label' => esc_html__( 'Parallax Image', 'houserent' ),
        'type'  => 'upload',
        'desc'  => esc_html__( 'Upload parallax background image here', 'houserent' ),
    ),
    'coming_soon_gradient' => array(
        'label' => esc_html__( 'Coming soon section Gradient Color', 'houserent' ),
        'type'  => 'gradient',
        'value' => array(
            'primary'   => '#21b75f',
            'secondary' => '#31386e',
        ),
        'desc'  => esc_html__( 'Here you can change section background gradient color', 'houserent' ),
    ),
    'coming_soon_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Coming Soon', 'houserent' ),
        'desc'  => esc_html__( 'You can change section title here', 'houserent' ),
    ),
    'coming_soon_sub_title' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'We are working on something awesome!', 'houserent' ),
        'desc'  => esc_html__( 'You can change section subtitle', 'houserent' ),
    ),
    'coming_soon_timer' => array(
        'label' => esc_html__( 'CountDown Timer', 'houserent' ),
        'type'  => 'datetime-picker',
        'value'  => '',
        'datetime-picker' => array(
            'format'        => 'Y/m/d H:i',
            'maxDate'       => false, 
            'minDate'       => false, 
            'timepicker'    => true,  
            'datepicker'    => true,  
            'defaultTime'   => '12:00'
        ),
        'desc'  => esc_html__( 'Select date and time to start countdown timer', 'houserent' ),
    ),
    'coming_soon_social' => array(
        'label'         => esc_html__( 'Add Social Link: ', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you can add social item from here', 'houserent' ),
        'template'      => '{{- coming_soon_item_title }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'coming_soon_item_title' => array(
                'label' => esc_html__( 'Select Icon', 'houserent' ),
                'type'  => 'icon',
                'desc'  => esc_html__( 'Type a social icon from here', 'houserent' ),
            ),
            'coming_soon_item_url' => array(
                'label' => esc_html__( 'Link URL', 'houserent' ),
                'type'  => 'text',
                'value'  => '#',
                'desc'  => esc_html__( 'Put link URL here', 'houserent' ),
            ),
        ),
    ),
);