<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $coming_soon_paralux = houserent_theme_builder_field( $atts['coming_soon_paralux'] );
    $coming_soon_gradient = houserent_theme_builder_field( $atts['coming_soon_gradient'] );
    $coming_soon_title = houserent_theme_builder_field( $atts['coming_soon_title'] );
    $coming_soon_sub_title = houserent_theme_builder_field( $atts['coming_soon_sub_title'] );
    $coming_soon_timer = houserent_theme_builder_field( $atts['coming_soon_timer'] );
    $coming_soon_social = houserent_theme_builder_field( $atts['coming_soon_social'] );

    if( $coming_soon_gradient ){
        $rgba_color_one = houserent_theme_hex2rgba($coming_soon_gradient['primary'],'0.85');
        $rgba_color_two = houserent_theme_hex2rgba($coming_soon_gradient['secondary'],'0.85');

        $gradiant_bg = "background: ".esc_attr( $rgba_color_one ).";
    background: -moz-linear-gradient(left, ".esc_attr( $rgba_color_two )." 0%, ".esc_attr( $rgba_color_one )." 100%);
    background: -webkit-linear-gradient(left, ".esc_attr( $rgba_color_two )." 0%, ".esc_attr( $rgba_color_one )." 100%);
    background: linear-gradient(to right, ".esc_attr( $rgba_color_two )." 0%, ".esc_attr( $rgba_color_one )." 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='rgba( ".esc_attr( $coming_soon_gradient['primary'] ).", 0.85)', endColorstr='rgba( ".esc_attr( $coming_soon_gradient['secondary'] ).", 0.85)',GradientType=1 );";
    } else {
        $gradiant_bg = "";
    }
    $coming_soon_paralux = ( $coming_soon_paralux ) ? $coming_soon_paralux['url'] : '' ;

?>
    <!-- ======slider-area======= --> 
    <div class="commingsoon gradient-transparent" style="<?php echo esc_attr( $gradiant_bg ); ?>">
        <div class="container-fluid">
            <div class="commingsoon-content jarallax" data-jarallax='{"speed": 0.3, "imgSrc": "<?php echo esc_url( $coming_soon_paralux ); ?>" }'>
                <div class="row">
                    <div class="col-md-6 full-width-content text-center">
                    <?php if( $coming_soon_title ): ?>
                        <h2 class="comming-heading"><?php echo esc_html( $coming_soon_title ); ?></h2>
                    <?php
                        endif;
                        if( $coming_soon_sub_title ):
                    ?>
                        <h5 class="comming-description"><?php echo esc_html( $coming_soon_sub_title ); ?></h5>
                    <?php 
                        endif;
                        if( $coming_soon_timer ):
                           $time_date = explode(' ', $coming_soon_timer);
                            $date = explode( '/', $time_date[0] );
                            $time = explode( ':', $time_date[1] );
                    ?>
                        <div class="commingsoon-count" data-year="<?php echo esc_attr( $date[0] ); ?>" data-month="<?php echo esc_attr( $date[1] ); ?>" data-day="<?php echo esc_attr( $date[2] ); ?>" data-hour="<?php echo esc_attr( $time[0] ); ?>" data-minute="<?php echo esc_attr( $time[1] ); ?>" ></div><!-- /.defaultCountdown -->
                    <?php 
                        endif;
                        if( $coming_soon_social ):
                    ?>
                        <div class="social-link">
                            <?php
                                 foreach ( $coming_soon_social as $item ) {
                            ?>
                             <a href="<?php echo esc_url( $item['coming_soon_item_url'] ); ?>">
                                 <i class="<?php echo esc_attr( $item['coming_soon_item_title'] ); ?>" aria-hidden="true"></i>
                             </a>
                            <?php } ?>
                        </div>
                    <?php endif; ?>
                    </div><!-- /.text-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->           
    </div><!-- /.slider-area -->