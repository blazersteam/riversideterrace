<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $rental_cat_section_style = houserent_theme_builder_field( $atts['rental_cat_section_style'] );
    $rental_style = $rental_cat_section_style['rental_cat_style'];
    $rental_cat_items = houserent_theme_builder_field( $atts['rental_cat_items'] );

?>
<?php if( $rental_style == 'one' ): ?>
<?php 
    $rental_cat_section_color = $rental_cat_section_style['one']['rental_cat_section_color'];
    $rental_cat_section_title = $rental_cat_section_style['one']['rental_cat_section_title'];
    $rental_cat_section_btn_text = $rental_cat_section_style['one']['rental_cat_section_btn_text'];
    $rental_cat_section_btn_link = $rental_cat_section_style['one']['rental_cat_section_btn_link'];

    $gradiant_css = "background: linear-gradient(330deg, ".esc_attr( $rental_cat_section_color['secondary'] )." 0%, ".esc_attr( $rental_cat_section_color['primary'] )." 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;";
    $gradiant_btn = "background: ".esc_attr( $rental_cat_section_color['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $rental_cat_section_color['primary'] )." 0%, ".esc_attr( $rental_cat_section_color['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $rental_cat_section_color['primary'] )." 0%, ".esc_attr( $rental_cat_section_color['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $rental_cat_section_color['primary'] )." 0%, ".esc_attr( $rental_cat_section_color['secondary'] )." 100%);";
?>
<!-- ======category-area================================== --> 
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
            <?php if( $rental_cat_section_title || $rental_cat_section_btn_text ): ?>
                <div class="catagory-left-content clearfix">
                    <?php if( $rental_cat_section_title ): ?>
                       <h2 class="default-text-gradient" style="<?php echo esc_attr( $gradiant_css ); ?>"><?php echo esc_html( $rental_cat_section_title ); ?></h2>
                    <?php endif; ?>
                    <?php if( $rental_cat_section_btn_text && $rental_cat_section_btn_link ): ?>
                        <a href="<?php echo esc_url( $rental_cat_section_btn_link ); ?>" class="button nevy-button"><?php echo esc_html( $rental_cat_section_btn_text ); ?></a>
                    <?php endif; ?>
                </div><!-- /.catagory-left-content -->
            <?php endif; ?>
            </div><!-- /.col-md-4 -->
            <?php if( $rental_cat_items ): ?>
            <div class="col-md-8 col-sm-8">
                <div class="catagory-right-content row">
                    <?php 
                        foreach ( $rental_cat_items as $item ) {
                            $svg_icon = ( null != $item['rental_cat_item_svg_icon'] ) ? $item['rental_cat_item_svg_icon'] : '' ;
                    ?>
                        <div class="category-list style-two col-md-3 col-sm-4 col-xs-6">
                            <a href="<?php echo esc_url( $item['rental_cat_item_url'] ); ?>">
                            <?php if( $svg_icon ){ ?>
                                <?php 
                                    $allowed_tag = array(
                                        'svg' => array(
                                            'width' => array(),
                                            'height' => array(),
                                            'viewBox' => array()
                                        ),
                                        'path' => array(
                                            'd' => array(),
                                            'class' => array()
                                        ),
                                    );
                                        echo wp_kses( $svg_icon , $allowed_tag ); 
                                ?>
                            <?php } else { ?>
                                <?php 
                                    if ( $item['rental_cat_item_icon']['type'] == 'icon-font' ) {
                                ?>
                                    <i class="<?php echo esc_attr($item['rental_cat_item_icon']['icon-class']);?>"></i>
                                <?php
                                    } else if ( $item['rental_cat_item_icon']['type'] == 'custom-upload' ) {
                                            $image_url = houserent_theme_settings_image( $item['rental_cat_item_icon']['attachment-id'] ,45 ,45 );
                                        ?>
                                            <img class="icon-image" src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( $item['rental_cat_item_title'] ); ?>" />
                                        <?php
                                    }
                                ?>
                                <?php } ?>
                                <?php if( $item['rental_cat_item_title'] ): ?>
                                    <h4><?php echo esc_html( $item['rental_cat_item_title'] ); ?></h4>
                                <?php endif; ?>
                            </a>
                        </div><!-- /.category-list -->
                    <?php } ?>
                </div><!-- /.catagory-right-content -->
            </div><!-- /.col-md-8 -->
            <?php endif; ?>
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.category-area -->
<?php elseif( $rental_style == 'two' ): ?>
<?php 
    $rental_cat_section_title = $rental_cat_section_style['two']['rental_cat_section_title'];
    $rental_cat_section_top = ( $rental_cat_section_style['two']['rental_cat_section_top'] ) ? $rental_cat_section_style['two']['rental_cat_section_top'] : '' ;
    $margin = " margin-top: ".esc_attr( $rental_cat_section_top ).";";

?>
    <!-- ======category-area====== --> 
    <div class="category-menu margin-less-top" style="<?php echo esc_attr( $margin ); ?>">
        <div class="container-fluid pd-zero">
            <div class="row">
                <div class="col-md-12">
                    <div class="category-menu-content">
                    <?php if( $rental_cat_section_title ): ?>
                        <div class="category-title">
                            <h3><span><?php echo esc_html( $rental_cat_section_title ); ?></span></h3>
                        </div><!-- /.category-titel -->
                    <?php 
                        endif; 
                        if( $rental_cat_items ):
                    ?>
                        <div class="category-slider bg-white-smoke owl-carousel">
                            <?php 
                                foreach ( $rental_cat_items as $item ) {
                                    $svg_icon = ( null != $item['rental_cat_item_svg_icon'] ) ? $item['rental_cat_item_svg_icon'] : '' ;
                            ?>
                                <div class="item">
                                    <div class="category-list style-one">
                                        <a href="<?php echo esc_url( $item['rental_cat_item_url'] ); ?>">
                                        <?php if( $svg_icon ){ ?>
                                            <?php 
                                                $allowed_tag = array(
                                                    'svg' => array(
                                                        'width' => array(),
                                                        'height' => array(),
                                                        'viewBox' => array()
                                                    ),
                                                    'path' => array(
                                                        'd' => array(),
                                                        'class' => array()
                                                    ),
                                                );
                                                    echo wp_kses( $svg_icon , $allowed_tag ); 
                                            ?>
                                        <?php } else { ?>
                                            <?php 
                                                if ( $item['rental_cat_item_icon']['type'] == 'icon-font' ) {
                                            ?>
                                                <i class="<?php echo esc_attr($item['rental_cat_item_icon']['icon-class']);?>"></i>
                                            <?php
                                                } else if ( $item['rental_cat_item_icon']['type'] == 'custom-upload' ) {
                                                        $image_url = houserent_theme_settings_image( $item['rental_cat_item_icon']['attachment-id'] ,45 ,45 );
                                                    ?>
                                                        <img class="icon-image" src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( $item['rental_cat_item_title'] ); ?>" />
                                                    <?php
                                                }
                                            ?>
                                            <?php } ?>
                                            <h4><?php echo esc_html( $item['rental_cat_item_title'] ); ?></h4>
                                        </a>
                                    </div><!-- /.category-list -->
                                </div><!-- /.item -->
                            <?php } ?>                        
                        </div><!-- /.category-slider -->
                    <?php endif; ?>  
                    </div><!-- /.category-menu-content -->
                </div> <!-- category-menu -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.catagory-menu -->
<?php 
    elseif( $rental_style == 'three' ):
    $rental_cat_section_title = ( isset( $rental_cat_section_style['three']['rental_cat_section_title'] ) ) ? $rental_cat_section_style['three']['rental_cat_section_title'] : '';
    $rental_cat_section_color = ( isset( $rental_cat_section_style['three']['rental_cat_section_color'] ) ) ? $rental_cat_section_style['three']['rental_cat_section_color'] : '';
    if( $rental_cat_section_color ){
        $gradiant_btn = "background: ".esc_attr( $rental_cat_section_color['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $rental_cat_section_color['primary'] )." 0%, ".esc_attr( $rental_cat_section_color['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $rental_cat_section_color['primary'] )." 0%, ".esc_attr( $rental_cat_section_color['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $rental_cat_section_color['primary'] )." 0%, ".esc_attr( $rental_cat_section_color['secondary'] )." 100%);";
    } else {
        $gradiant_btn = "";
    }
?>
    <!-- ====== Category Area ====== --> 
    <div class="category-menu four default-template-gradient" style="<?php echo esc_attr( $gradiant_btn ); ?>">
        <div class="container-fluid pd-zero">
            <div class="row">
                <div class="col-md-12">
                    <div class="category-menu-content">
                    
                        <div class="category-slider bg-white-smoke owl-carousel">
                            <?php 
                                foreach ( $rental_cat_items as $item ) {
                                    $svg_icon = ( null != $item['rental_cat_item_svg_icon'] ) ? $item['rental_cat_item_svg_icon'] : '' ;
                            ?>
                                <div class="item">
                                    <div class="category-list style-one">
                                        <a href="<?php echo esc_url( $item['rental_cat_item_url'] ); ?>">
                                        <?php if( $svg_icon ){ ?>
                                            <?php 
                                                $allowed_tag = array(
                                                    'svg' => array(
                                                        'width' => array(),
                                                        'height' => array(),
                                                        'viewBox' => array()
                                                    ),
                                                    'path' => array(
                                                        'd' => array(),
                                                        'class' => array()
                                                    ),
                                                );
                                                    echo wp_kses( $svg_icon , $allowed_tag ); 
                                            ?>
                                        <?php } else { ?>
                                            <?php 
                                                if ( $item['rental_cat_item_icon']['type'] == 'icon-font' ) {
                                            ?>
                                                <i class="<?php echo esc_attr($item['rental_cat_item_icon']['icon-class']);?>"></i>
                                            <?php
                                                } else if ( $item['rental_cat_item_icon']['type'] == 'custom-upload' ) {
                                                        $image_url = houserent_theme_settings_image( $item['rental_cat_item_icon']['attachment-id'] ,45 ,45 );
                                                    ?>
                                                        <img class="icon-image" src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( $item['rental_cat_item_title'] ); ?>" />
                                                    <?php
                                                }
                                            ?>
                                            <?php } ?>
                                            <h4><?php echo esc_html( $item['rental_cat_item_title'] ); ?></h4>
                                        </a>
                                    </div><!-- /.category-list -->
                                </div><!-- /.item -->
                            <?php } ?>

                        </div><!-- /.category-slider -->  
                    </div><!-- /.category-menu-content -->
                </div> <!-- category-menu -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.catagory-menu -->
<?php 
    elseif( $rental_style == 'four' ):
    $rental_cat_section_title = $rental_cat_section_style['four']['rental_cat_section_title'];
    $rental_cat_section_color = $rental_cat_section_style['four']['rental_cat_section_color'];
    if( $rental_cat_section_color ){
        $gradiant_btn = "background: ".esc_attr( $rental_cat_section_color['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $rental_cat_section_color['primary'] )." 0%, ".esc_attr( $rental_cat_section_color['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $rental_cat_section_color['primary'] )." 0%, ".esc_attr( $rental_cat_section_color['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $rental_cat_section_color['primary'] )." 0%, ".esc_attr( $rental_cat_section_color['secondary'] )." 100%);";
    } else {
        $gradiant_btn = "";
    }
?>
   <div class="category-menu seven">
       <div class="row">
           <div class="col-md-12">
               <div class="category-menu-content">
                    <?php if( $rental_cat_section_title ): ?>
                        <div class="category-title">
                            <h3  style="<?php echo esc_attr( $gradiant_btn ); ?>"><span><?php echo esc_html( $rental_cat_section_title ); ?></span></h3>
                        </div><!-- /.category-titel -->
                    <?php endif; ?>
                   <div class="category-slider-seven bg-white-smoke owl-carousel">
                        

                        <?php 
                            foreach ( $rental_cat_items as $item ) {
                                $svg_icon = ( null != $item['rental_cat_item_svg_icon'] ) ? $item['rental_cat_item_svg_icon'] : '' ;
                        ?>
                            <div class="item">
                                <div class="category-list style-one">
                                    <a href="<?php echo esc_url( $item['rental_cat_item_url'] ); ?>">
                                    <?php if( $svg_icon ){ ?>
                                        <?php 
                                            $allowed_tag = array(
                                                'svg' => array(
                                                    'width' => array(),
                                                    'height' => array(),
                                                    'viewBox' => array()
                                                ),
                                                'path' => array(
                                                    'd' => array(),
                                                    'class' => array()
                                                ),
                                            );
                                                echo wp_kses( $svg_icon , $allowed_tag ); 
                                        ?>
                                    <?php } else { ?>
                                        <?php 
                                            if ( $item['rental_cat_item_icon']['type'] == 'icon-font' ) {
                                        ?>
                                            <i class="<?php echo esc_attr($item['rental_cat_item_icon']['icon-class']);?>"></i>
                                        <?php
                                            } else if ( $item['rental_cat_item_icon']['type'] == 'custom-upload' ) {
                                                    $image_url = houserent_theme_settings_image( $item['rental_cat_item_icon']['attachment-id'] ,45 ,45 );
                                                ?>
                                                    <img class="icon-image" src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( $item['rental_cat_item_title'] ); ?>" />
                                                <?php
                                            }
                                        ?>
                                        <?php } ?>
                                        <h4><?php echo esc_html( $item['rental_cat_item_title'] ); ?></h4>
                                    </a>
                                </div><!-- /.category-list -->
                            </div><!-- /.item -->
                        <?php } ?>

                   </div><!-- /.category-slider -->  
               </div><!-- /.category-menu-content -->
           </div> <!-- category-menu -->
       </div><!-- /.col-md-12 -->
   </div><!-- /.catagory-menu -->
<?php endif; ?>