<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'rental_cat_section_style' => array(
        'type'         => 'multi-picker',
        'label'        => false,
        'desc'         => false,
        'picker'       => array(
            'rental_cat_style' => array(
                'label'   => esc_html__( 'Category Style', 'houserent' ),
                'type'    => 'select',
                'value'    => 'one',
                'choices' => array(
                    'one' => esc_html__( 'One', 'houserent' ),
                    'two' => esc_html__( 'Two', 'houserent' ),
                    'three' => esc_html__( 'Three', 'houserent' ),
                    'four' => esc_html__( 'Four', 'houserent' ),
                ),
                'desc'    => esc_html__( 'Select Category style from here', 'houserent' ),
            )
        ),
        'choices'      => array(
            'one'  => array( 
                'rental_cat_section_color' => array(
                    'label' => esc_html__( 'Title and Button Gradient Color', 'houserent' ),
                    'type'  => 'gradient',
                    'value' => array(
                            'primary'   => '#21b75f',
                            'secondary' => '#31386e',
                    ),
                    'desc'  => esc_html__( 'Here you can change section title and button gradient color', 'houserent' ),
                ),
                'rental_cat_section_title' => array(
                    'label' => esc_html__( 'Section Title', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  esc_html__( 'Rental Category', 'houserent' ),
                    'desc'  => esc_html__( 'Here you can change section title', 'houserent' ),
                ),
                'rental_cat_section_btn_text' => array(
                    'label' => esc_html__( 'Button Text', 'houserent' ),
                    'type'  => 'text',
                    'value' => esc_html__( 'ALL Apartments', 'houserent' ),
                    'desc'  => esc_html__( 'You can change button text here', 'houserent' ),
                ),
                'rental_cat_section_btn_link' => array(
                    'label' => esc_html__( 'Link URL', 'houserent' ),
                    'type'  => 'text',
                    'value'  => '#',
                    'desc'  => esc_html__( 'Put button link URL', 'houserent' ),
                ),
            ),
            'two'  => array(
                'rental_cat_section_top' => array(
                    'label' => esc_html__( 'Margin Top', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  '-35px',
                    'desc'  => esc_html__( 'You can set how much space you want on the top', 'houserent' ),
                ),
                'rental_cat_section_title' => array(
                    'label' => esc_html__( 'Section Title', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  esc_html__( 'We Provide', 'houserent' ),
                    'desc'  => esc_html__( 'Here you can change section title', 'houserent' ),
                ),                      
            ),
            'three'  => array(
                /*'rental_cat_section_title' => array(
                    'label' => esc_html__( 'Section Title', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  esc_html__( 'We Provide', 'houserent' ),
                    'desc'  => esc_html__( 'Here you can change section title', 'houserent' ),
                ),*/
                'rental_cat_section_color' => array(
                    'label' => esc_html__( 'Title and Button Gradient Color', 'houserent' ),
                    'type'  => 'gradient',
                    'value' => array(
                            'primary'   => '#21b75f',
                            'secondary' => '#31386e',
                    ),
                    'desc'  => esc_html__( 'Here you can change section background gradient color', 'houserent' ),
                ),                   
            ),
            'four'  => array(
                'rental_cat_section_title' => array(
                    'label' => esc_html__( 'Section Title', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  esc_html__( 'We Provide', 'houserent' ),
                    'desc'  => esc_html__( 'Here you can change section title', 'houserent' ),
                ),
                'rental_cat_section_color' => array(
                    'label' => esc_html__( 'Title and Button Gradient Color', 'houserent' ),
                    'type'  => 'gradient',
                    'value' => array(
                            'primary'   => '#21b75f',
                            'secondary' => '#31386e',
                    ),
                    'desc'  => esc_html__( 'Here you can change section background gradient color', 'houserent' ),
                ),                   
            ),
        ),
        'show_borders' => false,
    ), 

    'rental_cat_items' => array(
        'label'         => esc_html__( 'Add items: ', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you add fun-fact item from here', 'houserent' ),
        'template'      => '{{- rental_cat_item_title }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'rental_cat_item_svg_icon' => array(
                'label' => esc_html__( 'SVG Code', 'houserent' ),
                'type'  => 'textarea',
                'value'  => '',
                'desc'  => esc_html__( 'Put icon SVG code here', 'houserent' ),
            ),
            'rental_cat_item_icon' => array(
                'label' => esc_html__( 'Icon', 'houserent' ),
                'type'  => 'icon-v2',
                'desc'  => esc_html__( 'Choose a icon for item', 'houserent' ),
            ),
            'rental_cat_item_title' => array(
                'label' => esc_html__( 'Title', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type title here', 'houserent' ),
            ),
            'rental_cat_item_url' => array(
                'label' => esc_html__( 'Linked URL', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type item URL here', 'houserent' ),
            ),
        ),
    ),
);