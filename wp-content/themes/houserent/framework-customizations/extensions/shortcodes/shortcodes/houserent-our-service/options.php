<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'our_service_section_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Service we our provide', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change section title', 'houserent' ),
    ),
    'our_service_section_sub_title' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'WELL COME TO OUR SERVICE', 'houserent' ),
        'desc'  => esc_html__( 'You change section subtitle', 'houserent' ),
    ),
    'our_service_items' => array(
        'label'         => esc_html__( 'Add items: ', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'You can add our service item from here', 'houserent' ),
        'template'      => '{{- our_service_item_title }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'our_service_item_image' => array(
                'label' => esc_html__( 'Image', 'houserent' ),
                'type'  => 'upload',
                'desc'  => esc_html__( 'Upload image here', 'houserent' ),
            ),
            'our_service_item_title' => array(
                'label' => esc_html__( 'Title', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type user title here', 'houserent' ),
            ),
            'our_service_item_description' => array(
                'label' => esc_html__( 'Short Description', 'houserent' ),
                'type'  => 'textarea',
                'desc'  => esc_html__( 'Type a short description here', 'houserent' ),
            ),
            'our_service_item_url' => array(
                'label' => esc_html__( 'Linked URL', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Put linked URL here', 'houserent' ),
            ),
        ),
    ),
);