<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $our_service_section_title = houserent_theme_builder_field( $atts['our_service_section_title'] );
    $our_service_section_sub_title = houserent_theme_builder_field( $atts['our_service_section_sub_title'] );
    $our_service_items = houserent_theme_builder_field( $atts['our_service_items'] );
?>
<!-- ======service-area================================== --> 
<div class="service-area">
    <div class="container">
    <?php if( $our_service_section_title || $our_service_section_sub_title ): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="heading-content-one">
                <?php if( $our_service_section_title ): ?>
                    <h2 class="title"><?php echo esc_html( $our_service_section_title ); ?></h2>
                <?php 
                    endif;
                    if( $our_service_section_sub_title ):
                ?>
                    <h5 class="sub-title"><?php echo esc_html( $our_service_section_sub_title ); ?></h5>
                <?php endif; ?>
                </div><!-- /.Apartments-heading-content -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    <?php endif; ?>

        <div class="row">
        <?php 
            if( $our_service_items ): 
            $i = 1;
            foreach ($our_service_items as $item) {
                $border_class = ( $i > 3 ) ? 'border' : '' ;
        ?>
            <div class="col-md-4 col-sm-6">
                <div class="service-content <?php echo esc_attr( $border_class ); ?>">
                <?php 
                    if( $item['our_service_item_image'] ): 
                    $attachment_id = $item['our_service_item_image']['attachment_id'];
                ?>
                    <div class="image-content">
                    <?php if( $item['our_service_item_url'] ): ?>
                        <a href="<?php echo esc_url( $item['our_service_item_url'] ); ?>">
                    <?php endif; ?>
                            <img src="<?php echo esc_url( houserent_theme_settings_image( $attachment_id ,190 ,130 ) ); ?>" alt="<?php echo esc_html( $item['our_service_item_title'] ); ?>" />
                    <?php if( $item['our_service_item_url'] ): ?>
                        </a>
                    <?php endif; ?>
                    </div><!-- /.image-content -->
                <?php endif; ?>
                 
                    <div class="text-content">
                    <?php if( $item['our_service_item_title'] ): ?>
                        <h3>
                        <?php if( $item['our_service_item_url'] ): ?>
                            <a href="<?php echo esc_url( $item['our_service_item_url'] ); ?>">
                        <?php endif; ?>
                                <?php echo esc_html( $item['our_service_item_title'] ); ?>
                        <?php if( $item['our_service_item_url'] ): ?>
                            </a>
                        <?php endif; ?>
                        </h3>
                    <?php endif; ?>
                    <?php if( $item['our_service_item_description'] ): ?>
                        <p><?php echo esc_html( $item['our_service_item_description'] ); ?></p>
                    <?php endif; ?>
                    </div><!-- /.text-content -->
                </div><!-- /.service-content -->
            </div><!-- /.col-md-4 -->

            <?php if( $i % 3 == 0 ): ?>
                <div class="clearfix visible-xs-block"></div>
            <?php endif; ?>

        <?php
            $i++;
            }
            endif; 
        ?>

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.service-area-->