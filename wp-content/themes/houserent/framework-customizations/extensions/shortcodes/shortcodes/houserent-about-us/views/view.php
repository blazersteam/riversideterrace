<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $auto_id = 'unique_'. fw_unique_increment();
    $about_us_section_title = houserent_theme_builder_field( $atts['about_us_section_title'] );
    $about_us_section_sub_title = houserent_theme_builder_field( $atts['about_us_section_sub_title'] );
    $about_us_items = houserent_theme_builder_field( $atts['about_us_items'] );
?>
<!-- ======aboutus-area================================== --> 
<div class="aboutus-area">
    <div class="container">
    <?php if( $about_us_section_title || $about_us_section_sub_title ): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="heading-content-one">
                    <?php if( $about_us_section_title ): ?>
                        <h2 class="title"><?php echo esc_html( $about_us_section_title ); ?></h2>
                    <?php endif; ?>

                    <?php if( $about_us_section_title ): ?>
                        <h5 class="sub-title"><?php echo esc_html( $about_us_section_sub_title ); ?></h5>
                    <?php endif; ?>
                </div><!-- /.heading-content -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    <?php endif; ?>
    
    <?php if( $about_us_items ): ?>
        <div class="row">
            <div class="col-md-2">
                <div class="tab-list">
                    <ul class="nav nav-tabs about-tab hidden-xs hidden-sm" role="tablist">
                    <?php 
                        $i = 1;
                        foreach ($about_us_items as $item_title) {
                        $title_active_class = ( $i == 1 ) ? 'active' : '' ; 
                        $unique_id = $auto_id.'_'.$i;
                    ?>
                        <li role="presentation" class="<?php echo esc_attr( $title_active_class ); ?>"><a href="#<?php echo esc_attr( $unique_id ); ?>" aria-controls="<?php echo esc_attr( $unique_id ); ?>" role="tab" data-toggle="tab"><?php echo esc_html( $item_title['about_us_item_name'] ); ?></a>
                        </li>
                    <?php $i++; } ?>
                    </ul>
                    <div class="hidden-md hidden-lg">
                        <select class="about-mobile">

                        <?php 
                            $i = 0;
                            foreach ($about_us_items as $item_title) {
                            $title_active_class = ( $i == 1 ) ? 'active' : '' ; 
                            $unique_id = $auto_id.'_'.$i;
                        ?>
                            <option value='<?php echo esc_attr( $i ); ?>'><?php echo esc_html( $item_title['about_us_item_name'] ); ?></option>                        
                        <?php $i++; } ?>

                        </select>
                    </div>

              </div> <!-- /.tab-list -->
            </div> <!-- /.col-md-2 -->

            <div class="col-md-10">
                <div class="tab-content">
                <?php 
                    $ii = 1;
                    foreach ($about_us_items as $item_content) {
                    $content_active_class = ( $ii == 1 ) ? 'active in' : '' ; 
                    $unique_id_content = $auto_id.'_'.$ii;
                ?>
                    <div role="tabpanel" class="tab-pane fade <?php echo esc_attr( $content_active_class ); ?>" id="<?php echo esc_attr( $unique_id_content ); ?>">
                        <div class="col-md-6">
                            <?php if( $item_content['about_us_item_description'] ): ?>
                            <div class="text-content">
                                <?php 
                                    $allowed_tag = array(
                                        'a' => array(
                                            'href' => array(),
                                            'title' => array()
                                        ),
                                        'br' => array(),
                                        'em' => array(),
                                        'strong' => array(),
                                        'h1' => array(),
                                        'h2' => array(),
                                        'h3' => array(),
                                        'ul' => array(),
                                        'ol' => array(),
                                        'li' => array(),
                                    );
                                        echo wp_kses( $item_content['about_us_item_description'], $allowed_tag ); 
                                ?>
                            </div><!-- /.text-content -->
                        <?php endif; ?>    
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6">
                            <div class="image-content">
                                <?php if( $item_content['about_us_item_image'] ): ?>
                                    <?php $attachment_id = $item_content['about_us_item_image']['attachment_id']; ?>
                                    <img src="<?php echo esc_url( houserent_theme_settings_image( $attachment_id ,443 ,334 ) ); ?>" alt="<?php echo esc_html( $item_content['about_us_item_name'] ); ?>" />
                                <?php endif; ?>
                            </div><!-- /.text-content -->
                        </div><!-- /.col-md-6 -->
                    </div> <!-- /.home -->
                <?php $ii++; } ?>

                </div> <!-- /.tab-content -->
             </div><!-- /.col-md-10 -->
        </div><!-- /.row -->
    <?php endif; ?>
    </div> <!-- /.container -->
</div> <!-- /.aboutus-area -->