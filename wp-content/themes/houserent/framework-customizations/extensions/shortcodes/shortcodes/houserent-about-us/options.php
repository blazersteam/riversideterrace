<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'about_us_section_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'About Us', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change about us section title', 'houserent' ),
    ),
    'about_us_section_sub_title' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'WELCOME TO OUR HOUSE RENT COMPANY', 'houserent' ),
        'desc'  => esc_html__( 'You change about us section subtitle', 'houserent' ),
    ),
    'about_us_items' => array(
        'label'         => esc_html__( 'Add items: ', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you add about_us item from here', 'houserent' ),
        'template'      => '{{- about_us_item_name }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'about_us_item_name' => array(
                'label' => esc_html__( 'Title', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type user title here', 'houserent' ),
            ),
            'about_us_item_description' => array(
                'label' => esc_html__( 'Description', 'houserent' ),
                'type'  => 'textarea',
                'desc'  => esc_html__( 'Type text description here', 'houserent' ),
            ),
            'about_us_item_image' => array(
                'label' => esc_html__( 'Image', 'houserent' ),
                'type'  => 'upload',
                'desc'  => esc_html__( 'Upload image here', 'houserent' ),
            ),
        ),
    ),
);