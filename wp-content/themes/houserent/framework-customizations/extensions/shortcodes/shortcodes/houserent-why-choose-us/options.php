<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'why_choose_us_section_gradient' => array(
        'label' => esc_html__( 'Section Title and Heading Gradient Color', 'houserent' ),
        'type'  => 'gradient',
        'value' => array(
                'primary'   => '#21b75f',
                'secondary' => '#31386e',
        ),
        'desc'  => esc_html__( 'Here you can change section title and heading text gradient color', 'houserent' ),
    ),
    'why_choose_us_section_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Why Choose Us', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change section title', 'houserent' ),
    ),
    'why_choose_us_section_sub_title' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Best offers from the house chef', 'houserent' ),
        'desc'  => esc_html__( 'You change section subtitle', 'houserent' ),
    ),
    'why_choose_us_section_heading' => array(
        'label' => esc_html__( 'Heading', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Best Rent Service enjoy your life', 'houserent' ),
        'desc'  => esc_html__( 'You change section left heading text', 'houserent' ),
    ),
    'why_choose_us_section_description' => array(
        'label' => esc_html__( 'Description', 'houserent' ),
        'type'  => 'textarea',
        'desc'  => esc_html__( 'You change section right description', 'houserent' ),
    ),
);