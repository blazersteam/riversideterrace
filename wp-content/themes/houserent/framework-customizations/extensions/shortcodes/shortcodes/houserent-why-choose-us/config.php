<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Why Choose Us', 'houserent' ),
		'description' => esc_html__( 'You can show why choose us sections by this shortcode', 'houserent' ),
		'tab'         => esc_html__( 'HouseRent' , 'houserent' ), 
        'popup_size'    => 'small', 
	)
);