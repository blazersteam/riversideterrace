<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $why_choose_us_section_gradient = houserent_theme_builder_field( $atts['why_choose_us_section_gradient'] );
    $why_choose_us_section_title = houserent_theme_builder_field( $atts['why_choose_us_section_title'] );
    $why_choose_us_section_sub_title = houserent_theme_builder_field( $atts['why_choose_us_section_sub_title'] );
    $why_choose_us_section_heading = houserent_theme_builder_field( $atts['why_choose_us_section_heading'] );
    $why_choose_us_section_description = houserent_theme_builder_field( $atts['why_choose_us_section_description'] );

    $gradiant_css = "background: linear-gradient(330deg, ".esc_attr( $why_choose_us_section_gradient['secondary'] )." 0%, ".esc_attr( $why_choose_us_section_gradient['primary'] )." 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;";
?>

<div class="about-main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="about-top-content">
                <?php if( $why_choose_us_section_title || $why_choose_us_section_sub_title ): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title-area text-center">
                            <?php if( $why_choose_us_section_title ): ?>
                                <h2 class="section-title default-text-gradient" style="<?php echo esc_attr( $gradiant_css ); ?>">
                                    <?php 
                                        $allowed_tag = array(
                                            'br' => array(),
                                            'em' => array(),
                                        );
                                        echo wp_kses( $why_choose_us_section_title, $allowed_tag ); 
                                    ?>
                                </h2>
                            <?php 
                                endif;
                                if( $why_choose_us_section_sub_title ):
                            ?>
                                <p class="section-description"><?php echo esc_html( $why_choose_us_section_sub_title ); ?></p>
                            <?php endif; ?>
                            </div><!-- /.about-heading-content -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                <?php endif; ?>

                    <div class="row">
                    <?php if( $why_choose_us_section_heading ): ?>
                        <div class="col-md-5">
                            <div class="about-content-left">
                                <h2 style="<?php echo esc_attr( $gradiant_css ); ?>">
                                    <?php 
                                        $allowed_tag = array(
                                            'br' => array(),
                                            'em' => array(),
                                        );
                                        echo wp_kses( $why_choose_us_section_heading, $allowed_tag ); 
                                    ?>
                                </h2>
                            </div><!-- /.about-content-left-->
                        </div><!-- /.col-md-5 -->
                    <?php 
                        endif;
                        if( $why_choose_us_section_description ):
                    ?>
                        <div class="col-md-7">
                            <div class="about-content-right">
                                <p>
                                    <?php 
                                        $allowed_tag = array(
                                            'a' => array(
                                                'href' => array(),
                                                'title' => array()
                                            ),
                                            'i' => array(
                                                'class' => array(),
                                            ),
                                            'br' => array(),
                                            'em' => array(),
                                            'strong' => array(),
                                            'h2' => array(),
                                            'h3' => array(),
                                            'h4' => array(),
                                            'ul' => array(),
                                            'ol' => array(),
                                            'li' => array(),
                                        );
                                        echo wp_kses( $why_choose_us_section_description, $allowed_tag ); 
                                    ?>
                                </p>
                            </div><!-- /.about-conten-right-->
                        </div><!-- /.col-md-7 -->
                    <?php endif; ?>
                    </div><!-- /.row -->
                </div><!-- /.top-content -->

            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.about-main-content -->