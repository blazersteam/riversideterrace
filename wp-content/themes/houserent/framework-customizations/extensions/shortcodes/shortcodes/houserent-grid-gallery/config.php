<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Grid Gallery', 'houserent' ),
		'description' => esc_html__( 'You can set Grid gallery by this shortcode', 'houserent' ),
		'tab'         => esc_html__( 'HouseRent', 'houserent' ), 
        'popup_size'    => 'small', 
	)
);