<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $grid_gallery_title = houserent_theme_builder_field( $atts['grid_gallery_title'] );
    $grid_gallery_h_color = houserent_theme_builder_field( $atts['grid_gallery_h_color'] );
    $grid_gallery_style = houserent_theme_builder_field( $atts['grid_gallery_sec']['grid_gallery_style'] );
    if( $grid_gallery_h_color ){
        $text_gradiant = "background: linear-gradient(330deg, ".esc_attr( $grid_gallery_h_color['secondary'] )." 25%, ".esc_attr( $grid_gallery_h_color['primary'] )." 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;";
    } else{
        $text_gradiant = "";
    }
?>
<!-- ====== Photo Gallery Area ====== --> 
<div class="photo-gallery-area">
    <div class="container">
        <?php if( $grid_gallery_title ): ?>
            <div class="col-md-12">
                <div class="heading-content-two text-center">
                    <h2 class="title default-text-gradient" style="<?php echo esc_attr( $text_gradiant ); ?>"><?php echo esc_html( $grid_gallery_title ); ?></h2>
                </div><!-- /.photo-heading-content -->
            </div><!-- /.col-md-12 -->
        <?php endif; ?>
        <div class="row">
            <?php
                if( $grid_gallery_style == 'photo' ):
                $photo_gallery_items = houserent_theme_builder_field( $atts['grid_gallery_sec']['photo']['photo_gallery_items'] );
                foreach ( $photo_gallery_items as $photo ) {
                    $item_image = $photo['photo_gallery_image']['attachment_id'];
                    if ( $item_image ) {
                        $image_url = houserent_theme_settings_image( $item_image ,263 , 228 );
                        $full_size_image_url = wp_get_attachment_image_src( $item_image, 'full' )[0];
                    } else {
                        $image_url = '';
                        $full_size_image_url = '';
                    }
            ?>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="gallery-image-content">
                        <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( $photo['photo_gallery_title'] ); ?>" />
                        <div class="overlay-background gradient-transparent"> 
                            <a href="<?php echo esc_url( $full_size_image_url ); ?>" class="image-pop-up">
                               <span><i class="fa fa-picture-o"></i>
                                    <?php
                                        if( $photo['photo_gallery_title'] ):
                                            echo esc_html( $photo['photo_gallery_title'] );
                                        endif;
                                    ?>
                               </span>
                            </a><!-- /.hover-content -->
                        </div><!-- /.hover-content -->
                    </div><!-- /.image-content -->
                </div><!-- /.col-md-3 -->

            <?php
                }
                elseif( $grid_gallery_style == 'video' ):
                $video_gallery_items = houserent_theme_builder_field( $atts['grid_gallery_sec']['video']['video_gallery_items'] );
                foreach ( $video_gallery_items as $video ) {
                    $item_thumb = $video['video_gallery_thumb']['attachment_id'];
                    if ( $item_thumb ) {
                        $image_url = houserent_theme_settings_image( $item_thumb ,263 , 228 );
                    } else {
                        $image_url = '';
                    }
            ?>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="gallery-image-content">
                        <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( $video['video_gallery_title'] ); ?>" />
                        <div class="overlay-background gradient-transparent"> 
                            <a href="<?php echo esc_url( $video['video_gallery_video_url'] ); ?>" class="video-pop-up">
                               <span><i class="fa fa-play"></i>
                                    <?php
                                        if( $video['video_gallery_title'] ):
                                            echo esc_html( $video['video_gallery_title'] );
                                        endif;
                                    ?>
                               </span>
                            </a><!-- /.hover-content -->
                        </div><!-- /.hover-content -->
                    </div><!-- /.image-content -->
                </div><!-- /.col-md-3 -->
            <?php } endif; ?>
        </div><!-- /.row -->   
    </div><!-- /.container -->
</div><!-- /.photo-gallery-area-->