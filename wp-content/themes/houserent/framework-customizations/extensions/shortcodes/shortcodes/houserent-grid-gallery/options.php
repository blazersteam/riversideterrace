<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'grid_gallery_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Awesome Gallery', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change section title', 'houserent' ),
    ),
    'grid_gallery_h_color' => array(
        'label' => esc_html__( 'Title Gradient', 'houserent' ),
        'type'  => 'gradient',
        'value' => array(
            'primary'   => '#21b75f',
            'secondary' => '#31386e',
        ),
        'desc'  => esc_html__( 'Here you can change title gradient color', 'houserent' ),
    ),
    'grid_gallery_sec' => array(
        'type'         => 'multi-picker',
        'label'        => false,
        'desc'         => false,
        'picker'       => array(
            'grid_gallery_style'   => array(
                 'type'  => 'select',
                 'label' => esc_html__( 'Gallery Style', 'houserent' ),
                 'value' =>  'photo',
                 'choices' => array(
                     'photo' => esc_html__( 'Photo', 'houserent' ),
                     'video' => esc_html__( 'Video', 'houserent' ),
                 ),
             ),
        ),
        'choices'      => array(
            'photo'  => array( 
                'photo_gallery_items' => array(
                        'label'         => esc_html__( 'Add Photo: ', 'houserent' ),
                        'type'          => 'addable-popup',
                        'desc'          => esc_html__( 'Here you add gallery item from here', 'houserent' ),
                        'template'      => '{{- photo_gallery_title }}',                                    
                        'add-button-text' => esc_html__('Add Item', 'houserent'),
                        'popup-options' => array(
                            'photo_gallery_title' => array(
                                'label' => esc_html__( 'Title', 'houserent' ),
                                'type'  => 'text',
                                'desc'  => esc_html__( 'Type title here', 'houserent' ),
                            ),
                            'photo_gallery_image' => array(
                                'label' => esc_html__( 'Image', 'houserent' ),
                                'type'  => 'upload',
                                'desc'  => esc_html__( 'Upload image here', 'houserent' ),
                            ),
                        ),
                    ),
            ),
            'video'  => array(
                'video_gallery_items' => array(
                        'label'         => esc_html__( 'Add Video: ', 'houserent' ),
                        'type'          => 'addable-popup',
                        'desc'          => esc_html__( 'Here you add gallery item from here', 'houserent' ),
                        'template'      => '{{- video_gallery_title }}',                                    
                        'add-button-text' => esc_html__('Add Item', 'houserent'),
                        'popup-options' => array(
                            'video_gallery_title' => array(
                                'label' => esc_html__( 'Title', 'houserent' ),
                                'type'  => 'text',
                                'desc'  => esc_html__( 'Type title here', 'houserent' ),
                            ),
                            'video_gallery_thumb' => array(
                                'label' => esc_html__( 'Video Thumbnail', 'houserent' ),
                                'type'  => 'upload',
                                'desc'  => esc_html__( 'Upload thumbnail for video here', 'houserent' ),
                            ),
                            'video_gallery_video_url' => array(
                                'label' => esc_html__( 'Video URL', 'houserent' ),
                                'type'  => 'text',
                                'desc'  => esc_html__( 'Put video URL here', 'houserent' ),
                            ),
                        ),
                    ),
            ),
        ),
        'show_borders' => false,
    ), 

);