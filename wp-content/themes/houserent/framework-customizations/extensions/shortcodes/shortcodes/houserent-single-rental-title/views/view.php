<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php if ( is_single() && get_post_type( get_the_ID() ) == 'rental' ) { ?>
<h3 class="apartment-title"><?php the_title(); ?></h3>
<?php } ?>
