<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php
    $author_info_section_gradient = houserent_theme_builder_field( $atts['author_info_section_gradient'] );
    $author_info_author_image = houserent_theme_builder_field( $atts['author_info_author_image'] );
    $author_info_name = houserent_theme_builder_field( $atts['author_info_name'] );
    $author_info_position = houserent_theme_builder_field( $atts['author_info_position'] );
    $author_info_description = houserent_theme_builder_field( $atts['author_info_description'] );
    $author_info_social = houserent_theme_builder_field( $atts['author_info_social'] );
    $author_info_author_sign = houserent_theme_builder_field( $atts['author_info_author_sign'] );

    $text_gradiant = "background: linear-gradient(330deg, ".esc_attr( $author_info_section_gradient['primary'] )." 0%, ".esc_attr( $author_info_section_gradient['secondary'] )." 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;";
    $bg_gradiant = "background: ".esc_attr( $author_info_section_gradient['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $author_info_section_gradient['primary'] )." 0%, ".esc_attr( $author_info_section_gradient['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $author_info_section_gradient['primary'] )." 0%, ".esc_attr( $author_info_section_gradient['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $author_info_section_gradient['primary'] )." 0%, ".esc_attr( $author_info_section_gradient['secondary'] )." 100%);";

?>
<div class="about-bottom-content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 full-width-content">
                <?php 
                    if( $author_info_author_image ): 
                    $attachment_id = $author_info_author_image['attachment_id'];
                ?>
                    <div class="image-content gradient-circle">
                        <div style="<?php echo esc_attr( $bg_gradiant ); ?>">
                            <span>
                                <img src="<?php echo esc_url( houserent_theme_settings_image( $attachment_id ,214 ,214 ) ); ?>" alt="<?php echo esc_html( $author_info_name ); ?>" />
                            </span>
                        </div>
                    </div><!-- /.image-content -->
                <?php
                    endif; 
                    if( $author_info_name || $author_info_position ):
                ?>
                    <div class="author-content">
                        <div class="author-content-area">                        
                            <?php if( $author_info_name ): ?>
                                <h2 class="author-name default-text-gradient" style="<?php echo esc_attr( $text_gradiant ); ?>"><?php echo esc_html( $author_info_name ); ?></h2> 
                            <?php 
                                endif;
                                if( $author_info_position ):
                            ?>
                                <p class="author-designation"><?php echo esc_html( $author_info_position ); ?></p>
                            <?php endif; ?>
                        </div><!-- /.author-content-area -->
                    </div><!-- /.author-content -->
                    <?php 
                        endif;
                        if( $author_info_description ): 
                    ?>
                   <p>
                        <?php 
                            $allowed_tag = array(
                                'a' => array(
                                    'href' => array(),
                                    'title' => array()
                                ),
                                'i' => array(
                                    'class' => array(),
                                ),
                                'br' => array(),
                                'em' => array(),
                                'strong' => array(),
                                'h2' => array(),
                                'h3' => array(),
                                'h4' => array(),
                                'ul' => array(),
                                'ol' => array(),
                                'li' => array(),
                            );
                            echo wp_kses( $author_info_description, $allowed_tag ); 
                        ?>
                   </p>
                    <?php 
                        endif; 
                        if( $author_info_social ):
                    ?>
                   <div class="social-media footer pull-left">
                       <span class="pull-left"><?php esc_html_e( 'Follow me :', 'houserent' ); ?></span>
                       <?php
                            foreach ( $author_info_social as $item ) {
                       ?>
                        <a href="<?php echo esc_url( $item['author_info_item_link'] ); ?>">
                            <i class="<?php echo esc_attr( $item['author_info_item_name'] ); ?>" aria-hidden="true" style="<?php echo esc_attr( $text_gradiant ); ?>" ></i>
                        </a>
                       <?php } ?>
                   </div><!-- /.social-media -->
                <?php 
                    endif;
                    if( $author_info_author_sign ):
                        $sign_id = $author_info_author_sign['attachment_id'];
                ?>
                    <div class="author-sign pull-right">
                       <img src="<?php echo esc_url( houserent_theme_settings_image( $sign_id ,214 ,214 ) ); ?>" alt="<?php echo esc_html( $author_info_name ); ?>" />
                    </div><!-- /.images -->
                <?php endif; ?>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.about-main-content -->