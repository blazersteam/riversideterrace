<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'author_info_section_gradient' => array(
        'label' => esc_html__( 'Section Gradient Color', 'houserent' ),
        'type'  => 'gradient',
        'value' => array(
                'primary'   => '#21b75f',
                'secondary' => '#31386e',
        ),
        'desc'  => esc_html__( 'Here you can change section gradient color', 'houserent' ),
    ),
    'author_info_author_image' => array(
        'label' => esc_html__( 'Author Image', 'houserent' ),
        'type'  => 'upload',
        'desc'  => esc_html__( 'Here you upload author image', 'houserent' ),
    ),
    'author_info_name' => array(
        'label' => esc_html__( 'Author Name', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Dr. Abdus Sabbir', 'houserent' ),
        'desc'  => esc_html__( 'Here you can set author name', 'houserent' ),
    ),
    'author_info_position' => array(
        'label' => esc_html__( 'Author Position', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Rent House Admin', 'houserent' ),
        'desc'  => esc_html__( 'You author position', 'houserent' ),
    ),
    'author_info_description' => array(
        'label' => esc_html__( 'Author Description', 'houserent' ),
        'type'  => 'textarea',
        'desc'  => esc_html__( 'You author short description', 'houserent' ),
    ),
    'author_info_social' => array(
        'label'         => esc_html__( 'Add Social Link: ', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you add author_info item from here', 'houserent' ),
        'template'      => '{{- author_info_item_name }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'author_info_item_name' => array(
                'label' => esc_html__( 'Select Icon', 'houserent' ),
                'type'  => 'icon',
                'desc'  => esc_html__( 'Type a social icon from here', 'houserent' ),
            ),
            'author_info_item_link' => array(
                'label' => esc_html__( 'Link URL', 'houserent' ),
                'type'  => 'text',
                'value'  => '#',
                'desc'  => esc_html__( 'Put link URL here', 'houserent' ),
            ),
        ),
    ),
    'author_info_author_sign' => array(
        'label' => esc_html__( 'Author Signature', 'houserent' ),
        'type'  => 'upload',
        'desc'  => esc_html__( 'Upload author signature here', 'houserent' ),
    ),
);