<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $form_style = 'two';
    $form_style = houserent_theme_builder_field( $atts['form_style'] );
    $form_margin_top = ( houserent_theme_builder_field( $atts['form_margin_top'] ) ) ? houserent_theme_builder_field( $atts['form_margin_top'] ) : '0px' ;
    $form_title = houserent_theme_builder_field( $atts['form_title'] );
    $form_subtitle = houserent_theme_builder_field( $atts['form_subtitle'] );
    $margin = " margin-top: ".esc_attr( $form_margin_top ).";";
?>

<?php 
    $terms = get_terms( 'rental_cat', array( 'hide_empty' => false, ) );
    $aria_name = (isset( $_GET['s'] ) ) ? esc_html( $_GET['s'] ) : '';
    $texonomy_slug =  (isset( $_GET['cal_slug'] ) ) ? esc_html( $_GET['cal_slug'] ) : '';
    $max_price =  (isset( $_GET['max_price'] ) ) ? esc_html( $_GET['max_price'] ) : '';
    $min_price =  (isset( $_GET['min_price'] ) ) ? esc_html( $_GET['min_price'] ) : '';
    $total_room =  (isset( $_GET['rental_rooms_total'] ) ) ? esc_html( $_GET['rental_rooms_total'] ) : '';
    $start_date =  (isset( $_GET['rental_start_date'] ) ) ? esc_html( $_GET['rental_start_date'] ) : '';
    $end_date =  (isset( $_GET['rental_end_date'] ) ) ? esc_html( $_GET['rental_end_date'] ) : '';
?>
<!-- ======Availability-area================================== --> 
<?php if( $form_style == 'one' ): ?>
<div class="availability-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-content-three">
                <?php if( $form_title ): ?>
                    <h2 class="title"><?php echo esc_html( $form_title ); ?></h2>
                <?php
                    endif;
                    if( $form_subtitle ):
                ?>
                    <h5 class="sub-title">
                        <?php 
                            $allowed_tag = array(
                                'span' => array(),
                            );
                            echo wp_kses( $form_subtitle , $allowed_tag ); 
                        ?>
                    </h5>
                <?php endif; ?>
                </div><!-- /.Availability-content -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row --> 
        <div class="row">
            <form role="search" method="get" class="advance_search_query" action="<?php echo esc_url(home_url('/')); ?>">
                <div class="col-md-12">
                    <div class="form-content">
                        <?php if( houserent_get_options('rental_form_living_area') == true ) { ?>
                        <div class="form-group living-area">
                            <label><?php esc_html_e( 'Living Aria', 'houserent' ); ?></label>
                            <input type="text" class="tags search-field" value="<?php echo esc_html( $aria_name ); ?>" name="s" placeholder="<?php esc_html_e( 'Where do you want to live?', 'houserent' ); ?>">
                            <input type="hidden" name="post_type" value="rental" />
                        </div><!-- /.form-group -->
                        <?php } ?>
                        
                        <?php if( houserent_get_options('rental_form_category') == true ) { ?>
                        <?php if( $terms ): ?>
                            <div class="form-group category-type">
                                <label><?php esc_html_e( 'Type', 'houserent' ); ?></label>
                                <select name="cal_slug">
                                    <option value=""><?php esc_html_e(  'any', 'houserent' ); ?></option>
                                    <?php 
                                        foreach ( $terms as $single_cat ) {  
                                        $selected = ( $texonomy_slug == $single_cat->slug ) ? 'selected="selected"' : '' ;
                                    ?>
                                        <option <?php echo esc_attr( $selected ); ?> value="<?php echo esc_attr( $single_cat->slug ); ?>"><?php echo esc_html( $single_cat->name ); ?></option>
                                    <?php } ?>
                                </select>
                            </div><!-- /.form-group -->
                        <?php endif; ?>
                        <?php } ?>

                        <?php if( houserent_get_options('rental_form_check_in_date') == true ) { ?>
                        <div class="form-group min-price">
                            <label><?php esc_html_e( 'Check In', 'houserent' ); ?></label>
                            <input class="datepicker check-in" type="text" value="<?php echo get_query_var( "rental_date_from", "" ); ?>" name="rental_date_from">
                        </div><!-- /.form-group -->
                        <?php } ?>
                        
                        <?php if( houserent_get_options('rental_form_check_out_date') == true ) { ?>
                        <div class="form-group max-price">
                            <label><?php esc_html_e( 'Check Out', 'houserent' ); ?></label>
                            <input class="datepicker check-out" type="text" value="<?php echo get_query_var( "rental_date_to", "" ); ?>" name="rental_date_to">
                        </div><!-- /.form-group -->
                        <?php } ?>

                        <?php if( houserent_get_options('rental_form_check_min_price') == true ) { ?>
                        <div class="form-group min-price">
                            <label><?php esc_html_e( 'Min Price', 'houserent' ); ?></label>
                            <input type="text" value="<?php echo esc_attr( $min_price ); ?>" name="min_price" placeholder="<?php esc_html_e( 'Ex - 200', 'houserent' ); ?>">
                        </div><!-- /.form-group -->
                        <?php } ?>

                        <?php if( houserent_get_options('rental_form_check_max_price') == true ) { ?>
                        <div class="form-group max-price">
                            <label><?php esc_html_e( 'Max Price', 'houserent' ); ?></label>
                            <input type="text" value="<?php echo esc_attr( $max_price ); ?>" name="max_price" placeholder="<?php esc_html_e( 'Ex - 5000', 'houserent' ); ?>">
                        </div><!-- /.form-group -->
                        <?php } ?>

                        <?php if( houserent_get_options('rental_form_check_total_rooms') == true ) { ?>
                        <div class="form-group number-rooms">
                            <label><?php esc_html_e( 'Rooms', 'houserent' ); ?></label>
                            <input type="number" value="<?php echo esc_attr( $total_room ); ?>" name="rental_rooms_total" placeholder="<?php esc_html_e( 'Total rooms', 'houserent' ); ?>">
                        </div><!-- /.form-group -->
                        <?php } ?>
                    </div><!-- /.form-content -->
                </div><!-- /.col-md-12 -->
                <div class="col-md-12">
                    <button class="button nevy-button availity-button" type="submit"><?php esc_html_e( 'Check Availability', 'houserent' ); ?></button>
                </div><!-- /.col-md-12 -->
            </form>
        </div><!-- /.row -->
    </div><!-- /.container  -->
</div><!-- /.Availability-area -->
<?php elseif( $form_style == 'two' ): ?>
<div class="form-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form role="search" method="get" class="advance_search_query" action="<?php echo esc_url(home_url('/')); ?>">
                    <div class="form-bg form-top" style="<?php echo esc_attr( $margin ); ?>">
                        <div class="form-content">
                            <?php if( houserent_get_options('rental_form_living_area') == true ) { ?>
                            <div class="form-group living-area">
                                <label><?php esc_html_e( 'Living Aria', 'houserent' ); ?></label>
                                <input type="text" class="tags search-field" value="<?php echo esc_html( $aria_name ); ?>" name="s" placeholder="<?php esc_html_e( 'Where do you want to live?', 'houserent' ); ?>">
                                <input type="hidden" name="post_type" value="rental" />
                            </div><!-- /.form-group -->
                            <?php } ?>
                            
                            <?php if( houserent_get_options('rental_form_category') == true ) { ?>
                            <?php if( $terms ): ?>
                                <div class="form-group category-type">
                                    <label><?php esc_html_e( 'Type', 'houserent' ); ?></label>
                                    <select name="cal_slug">
                                        <option value=""><?php esc_html_e(  'any', 'houserent' ); ?></option>
                                        <?php 
                                            foreach ( $terms as $single_cat ) {  
                                            $selected = ( $texonomy_slug == $single_cat->slug ) ? 'selected="selected"' : '' ;
                                        ?>
                                            <option <?php echo esc_attr( $selected ); ?> value="<?php echo esc_attr( $single_cat->slug ); ?>"><?php echo esc_html( $single_cat->name ); ?></option>
                                        <?php } ?>
                                    </select>
                                </div><!-- /.form-group -->
                            <?php endif; ?>
                            <?php } ?>

                            <?php if( houserent_get_options('rental_form_check_in_date') == true ) { ?>
                            <div class="form-group min-price">
                                <label><?php esc_html_e( 'Check In', 'houserent' ); ?></label>
                                <input class="datepicker check-in" type="text" value="<?php echo get_query_var( "rental_date_from", "" ); ?>" name="rental_date_from">
                            </div><!-- /.form-group -->
                            <?php } ?>
                            
                            <?php if( houserent_get_options('rental_form_check_out_date') == true ) { ?>
                            <div class="form-group max-price">
                                <label><?php esc_html_e( 'Check Out', 'houserent' ); ?></label>
                                <input class="datepicker check-out" type="text" value="<?php echo get_query_var( "rental_date_to", "" ); ?>" name="rental_date_to">
                            </div><!-- /.form-group -->
                            <?php } ?>

                            <?php if( houserent_get_options('rental_form_check_min_price') == true ) { ?>
                            <div class="form-group min-price">
                                <label><?php esc_html_e( 'Min Price', 'houserent' ); ?></label>
                                <input type="text" value="<?php echo esc_attr( $min_price ); ?>" name="min_price" placeholder="<?php esc_html_e( 'Ex - 200', 'houserent' ); ?>">
                            </div><!-- /.form-group -->
                            <?php } ?>

                            <?php if( houserent_get_options('rental_form_check_max_price') == true ) { ?>
                            <div class="form-group max-price">
                                <label><?php esc_html_e( 'Max Price', 'houserent' ); ?></label>
                                <input type="text" value="<?php echo esc_attr( $max_price ); ?>" name="max_price" placeholder="<?php esc_html_e( 'Ex - 5000', 'houserent' ); ?>">
                            </div><!-- /.form-group -->
                            <?php } ?>

                            <?php if( houserent_get_options('rental_form_check_total_rooms') == true ) { ?>
                            <div class="form-group number-rooms">
                                <label><?php esc_html_e( 'Rooms', 'houserent' ); ?></label>
                                <input type="number" value="<?php echo esc_attr( $total_room ); ?>" name="rental_rooms_total" placeholder="<?php esc_html_e( 'Total rooms', 'houserent' ); ?>">
                            </div><!-- /.form-group -->
                            <?php } ?>
                        </div><!-- /.form-content -->
                    </div><!-- /.form-bg -->
                </form> <!-- /.advance_search_query -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.form-area -->
<?php endif; ?>