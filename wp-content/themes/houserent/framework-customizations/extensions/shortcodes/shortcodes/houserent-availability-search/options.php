<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'form_style'   => array(
         'type'  => 'switch',
         'label' => esc_html__( 'Style', 'houserent' ),
         'right-choice' => array(
             'value' => 'one',
             'label' => esc_html__( 'One', 'houserent' )
         ),
         'left-choice'  => array(
             'value' => 'two',
             'label' => esc_html__( 'Two', 'houserent' )
         ),
         'value' => 'one',
         'desc'  => esc_html__( 'You can change form style from here', 'houserent' ),
     ),
    'form_margin_top' => array(
        'label' => esc_html__( 'Margin Top', 'houserent' ),
        'type'  => 'text',
        'value' =>  '-60px',
        'desc'  => esc_html__( 'You can set how much space you want on the top', 'houserent' ),
    ),
	'form_title' => array(
        'label' => esc_html__( 'Title', 'houserent' ),
        'type'  => 'text',
        'value'  => 'For rates & Availability',
        'desc'  => esc_html__( 'Type form section title here', 'houserent' ),
    ),
    'form_subtitle' => array(
        'label' => esc_html__( 'Subtitle', 'houserent' ),
        'type'  => 'text',
        'value'  => 'Search your Rental item',
        'desc'  => esc_html__( 'Type form section subtitle here', 'houserent' ),
    ),
);