<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'call_to_action_margin' => array(
        'label' => esc_html__( 'Margin Top', 'houserent' ),
        'type'  => 'text',
        'value' =>  '',
        'desc'  => esc_html__( 'Here you can change margin top value of this section (Note: This option is worked only section style two )', 'houserent' ),
    ),
    'call_to_action_style' => array(
        'type'  => 'switch',
        'label' => esc_html__( 'Style', 'houserent' ),
        'right-choice' => array(
            'value' => 'one',
            'label' => esc_html__( 'One', 'houserent' )
        ),
        'left-choice'  => array(
            'value' => 'two',
            'label' => esc_html__( 'Two', 'houserent' )
        ),
        'value'     => 'one',
        'desc'      => esc_html__( 'You can choose call to action section style','houserent' ),
    ),
    'call_to_action_sec' => array(
        'type'         => 'multi-picker',
        'label'        => false,
        'desc'         => false,
        'picker'       => array(
            'call_to_action_bg' => array(
                'type'  => 'switch',
                'label' => esc_html__( 'Background Type', 'houserent' ),
                'right-choice' => array(
                    'value' => 'color',
                    'label' => esc_html__( 'Color', 'houserent' )
                ),
                'left-choice'  => array(
                    'value' => 'image',
                    'label' => esc_html__( 'Image', 'houserent' )
                ),
                'value'     => 'color',
                'desc'      => esc_html__( 'You can choose call to action section background type','houserent' ),
            ),
        ),
        'choices'  => array(
            'color'  => array(
                'call_to_action_bg_color'  => array(
                    'type'  => 'rgba-color-picker',
                    'value' => 'rgba(20,10,10,0.49)',
                    'palettes' => array( '#ba4e4e', '#0ce9ed', '#941940' ),
                    'label' => esc_html__('Backgourd Color', 'houserent'),
                    'desc'  => esc_html__('Here you can choose background color (Color is not working when Gradient is selected)', 'houserent'),
                ),
                'call_to_action_bg_overlay' => array(
                    'label' => esc_html__( 'Background Gradient', 'houserent' ),
                    'type'  => 'gradient',
                    'value' => array(
                        'primary'   => '',
                        'secondary' => '',
                    ),
                    'desc'  => esc_html__( 'Here you can change background gradient', 'houserent' ),
                ),
            ),
            'image'  => array(
                'call_to_action_section_background' => array(
                    'label' => esc_html__( 'Section Background', 'houserent' ),
                    'type'  => 'upload',
                    'desc'  => esc_html__( 'Here you can upload section background image', 'houserent' ),
                ),
            ),
        ),
        'show_borders' => false,
    ),
    'call_to_action_text_color' => array(
        'label' => esc_html__( 'Text Color', 'houserent' ),
        'type'  => 'color-picker',
        'value' =>  '#FCA22A',
        'desc'  => esc_html__( 'Here you can change style one text color of heading and email, phone ', 'houserent' ),
    ),    
    'call_to_action_button_color' => array(
        'label' => esc_html__( 'Button Color', 'houserent' ),
        'type'  => 'color-picker',
        'value' =>  '#FCA22A',
        'desc'  => esc_html__( 'Here you can change style one button color', 'houserent' ),
    ),
    'call_to_action_section_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Do you want to Rent your House', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change call to action section title', 'houserent' ),
    ),
    'call_to_action_section_sub_title' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Call us and list your property here', 'houserent' ),
        'desc'  => esc_html__( 'You change call to action section subtitle', 'houserent' ),
    ),
    'call_to_action_section_mobile' => array(
        'label' => esc_html__( 'Mobile Number', 'houserent' ),
        'type'  => 'text',
        'desc'  => esc_html__( 'You can set contact mobile number by this', 'houserent' ),
    ),
    'call_to_action_section_email' => array(
        'label' => esc_html__( 'Email', 'houserent' ),
        'type'  => 'text',
        'desc'  => esc_html__( 'You can set contact email by this', 'houserent' ),
    ),
    'call_to_action_section_contact_button_text' => array(
        'label' => esc_html__( 'Button Text', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Contact Us', 'houserent' ),
        'desc'  => esc_html__( 'You can change contact button title by this', 'houserent' ),
    ),
    'call_to_action_section_contact_button_url' => array(
        'label' => esc_html__( 'Button URL', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( '#', 'houserent' ),
        'desc'  => esc_html__( 'You can change contact button URL by this', 'houserent' ),
    ),
);