<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Call To Action', 'houserent' ),
		'description' => esc_html__( 'You can set call to action by this shortcode', 'houserent' ),
		'tab'         => esc_html__( 'HouseRent', 'houserent' ), 
        'popup_size'    => 'small', 
	)
);