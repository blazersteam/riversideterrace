<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $class = "";
    $call_to_action_style = houserent_theme_builder_field( $atts['call_to_action_style'] );
    $call_to_action_margin = houserent_theme_builder_field( $atts['call_to_action_margin'] );
    

    if( $call_to_action_margin ){
        $margin = ( $call_to_action_style == 'one' ) ? ' display: block; position: relative; margin-top: '.esc_attr( $call_to_action_margin ).';' : '' ;
    } else {
        $margin = '';
    }

    $call_to_action_bg_type = houserent_theme_builder_field( $atts['call_to_action_sec'] );
    if( $call_to_action_bg_type['call_to_action_bg'] == 'color' ){

        $bgc = $call_to_action_bg_type['color']['call_to_action_bg_color'];
        $bg_gr = $call_to_action_bg_type['color']['call_to_action_bg_overlay'];
        if( $bg_gr['primary'] ){
            $bg  = "style = 'background: ".esc_attr( $bg_gr['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $bg_gr['primary'] )." 0%, ".esc_attr( $bg_gr['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $bg_gr['primary'] )." 0%, ".esc_attr( $bg_gr['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $bg_gr['primary'] )." 0%, ".esc_attr( $bg_gr['secondary'] )." 100%);'";

        } else {
            $bg = 'style="background: '.esc_attr($bgc).';'.esc_attr( $margin ).'"';
        }
        $class = "";

    } elseif( $call_to_action_bg_type['call_to_action_bg'] == 'image' ){
        $bgi = $call_to_action_bg_type['image']['call_to_action_section_background'];
        if ( $bgi ) {
            $attachment_id = $bgi['attachment_id'];
            $image_url = houserent_theme_settings_image( $attachment_id ,1368 ,275 );
            $bg = 'style="background-image:url('.esc_url($image_url).');'.esc_attr( $margin ).'"';
            $class = "overlay-bg";
        } else {
            $bg = '';
            $class = "";
        }
    }

    $call_to_action_text_color = houserent_theme_builder_field( $atts['call_to_action_text_color'] );
    $call_to_action_button_color = houserent_theme_builder_field( $atts['call_to_action_button_color'] );

    $call_to_action_section_title = houserent_theme_builder_field( $atts['call_to_action_section_title'] );
    $call_to_action_section_sub_title = houserent_theme_builder_field( $atts['call_to_action_section_sub_title'] );
    $call_to_action_section_mobile = houserent_theme_builder_field( $atts['call_to_action_section_mobile'] );
    $call_to_action_section_email = houserent_theme_builder_field( $atts['call_to_action_section_email'] );
    $call_to_action_section_contact_button_text = houserent_theme_builder_field( $atts['call_to_action_section_contact_button_text'] );
    $call_to_action_section_contact_button_url = houserent_theme_builder_field( $atts['call_to_action_section_contact_button_url'] );

?>
<?php if( $call_to_action_style == 'one' ){ ?>
<div class="call-to-action style-two transfarent-white-bg"  <?php $allowed_tag = array( 'class' => array() ); echo wp_kses( $bg, $allowed_tag ); ?> >
    <div class="container">
        <div class="row tb">
            <div class="col-md-6 tb-cell">
                <div class="contact-left-content">
                    <?php if( $call_to_action_section_title ): ?>
                        <h3 style="color: <?php echo esc_attr( $call_to_action_text_color )?>"><?php echo esc_html( $call_to_action_section_title ); ?></h3>
                    <?php 
                        endif;
                        if( $call_to_action_section_sub_title ):
                    ?>
                        <h4><?php echo esc_html( $call_to_action_section_sub_title ); ?></h4>
                    <?php endif; ?>
                </div><!-- /.left-content -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6 tb-cell">
                <div class="contact-right-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="contact-left">
                                <?php if( $call_to_action_section_mobile ): ?>
                                    <h4 style="color: <?php echo esc_attr( $call_to_action_text_color )?>"><a href="tel:<?php echo esc_attr( $call_to_action_section_mobile ); ?>"><?php echo esc_html( $call_to_action_section_mobile ); ?></a></h4>
                                <?php 
                                    endif;
                                    if( $call_to_action_section_email ):
                                ?>
                                    <h4 style="color: <?php echo esc_attr( $call_to_action_text_color )?>">
                                        <a href="mailto:<?php echo esc_attr( $call_to_action_section_email ); ?>"><span><?php echo esc_html( $call_to_action_section_email ); ?></span></a>
                                    </h4>
                                <?php endif; ?>
                            </div><!-- /.contact -->
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6">
                            <div class="contact">
                                <?php 
                                    if( $call_to_action_section_contact_button_text || $call_to_action_section_contact_button_url ):
                                ?>
                                    <a class="call-to-action-btn" style="background-color: <?php echo esc_attr( $call_to_action_button_color )?>" href="<?php echo esc_attr( $call_to_action_section_contact_button_url ); ?>"><?php echo esc_html( $call_to_action_section_contact_button_text ); ?></a>
                                <?php endif; ?>
                            </div><!-- /.contact-tight -->
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->
                </div><!-- /.right-content -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.slider-bottom-area -->
<?php } elseif( $call_to_action_style == 'two' ){ ?>

<!-- ======call-to-actionr======= --> 
<div class="call-to-action <?php echo esc_attr( $class ); ?>" <?php $allowed_tag = array( 'class' => array() ); echo wp_kses( $bg, $allowed_tag ); ?> >
    <div class="container">
        <div class="row tb">
            <div class="col-md-6 col-sm-6 tb-cell">
                <div class="contact-left-content">                    
                    <?php if( $call_to_action_section_title ): ?>
                        <h3><?php echo esc_html( $call_to_action_section_title ); ?></h3>
                    <?php 
                        endif;
                        if( $call_to_action_section_sub_title ):
                    ?>
                        <h4><?php echo esc_html( $call_to_action_section_sub_title ); ?></h4>
                    <?php endif; ?>
                </div><!-- /.contact-left-content -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6 tb-cell">
                <div class="contact-right-content">
                    <?php if( $call_to_action_section_mobile ): ?>
                        <h4><a href="tel:<?php echo esc_attr( $call_to_action_section_mobile ); ?>"><?php echo esc_html( $call_to_action_section_mobile ); ?></a></h4>
                    <?php 
                        endif;
                        if( $call_to_action_section_email ):
                    ?>
                        <h4><a href="mailto:<?php echo esc_attr( $call_to_action_section_email ); ?>"><span><?php echo esc_html( $call_to_action_section_email ); ?></span></a></h4>
                    <?php 
                        endif;
                        if( $call_to_action_section_contact_button_text || $call_to_action_section_contact_button_url ):
                    ?>
                        <a href="<?php echo esc_attr( $call_to_action_section_contact_button_url ); ?>" class="button"><?php echo esc_html( $call_to_action_section_contact_button_text ); ?></a>
                    <?php endif; ?>
                </div><!-- /.contact-right-content -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.contact-area -->
<?php } ?>