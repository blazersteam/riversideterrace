<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'breadcrumb_separator' => array(
        'label' => esc_html__( 'Breadcrumb Separator', 'houserent' ),
        'type'  => 'text',
        'value' =>  '>',
        'desc'  => esc_html__( 'You can change breadcrumb item separator', 'houserent' ),
    ),
);