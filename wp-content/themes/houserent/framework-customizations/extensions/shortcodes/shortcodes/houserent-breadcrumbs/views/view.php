<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $breadcrumb_separator = houserent_theme_builder_field( $atts['breadcrumb_separator'] );
    $separator = ( $breadcrumb_separator ) ? $breadcrumb_separator : '>' ;

?>
<?php if( function_exists( 'fw_ext_get_breadcrumbs' )): ?>
    <!-- ====== Breadcrumbs-area ====== --> 
    <div class="breadcrumbs-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php echo fw_ext_get_breadcrumbs( $separator ); ?>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumbs-area -->
<?php endif; ?>