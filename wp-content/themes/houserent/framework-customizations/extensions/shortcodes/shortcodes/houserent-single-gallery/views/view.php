<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php if ( is_single() && get_post_type( get_the_ID() ) == 'rental' ) { ?>
<?php 
    $rental_price_conditions = get_post_meta( get_the_ID(),'rental_price_conditions')[0];
    $rental_gallery_item_images = houserent_theme_uny_post_meta('rental_gallery_item_images','');

    if( $rental_gallery_item_images ): ?>
        <div class="corousel-gallery-content">
            <?php if( $rental_gallery_item_images ): ?>
              <div class="gallery">
                   <div class="full-view owl-carousel">
                    <?php 
                        foreach ( $rental_gallery_item_images as $item) { 
                        $attachment_id = $item['attachment_id'];
                        $slide = houserent_theme_settings_image( $attachment_id ,750 ,500 );
                        $slide_full = houserent_theme_settings_image( $attachment_id ,1000 ,648 );
                    ?>
                    <a class="item" href="<?php echo esc_url( $slide_full ); ?>">
                        <img src="<?php echo esc_url( $slide ); ?>" alt="<?php the_title(); ?>">
                    </a>
                    <?php } ?>
                   </div>

                  <div class="list-view owl-carousel">
                  <?php 
                    foreach ( $rental_gallery_item_images as $item) { 
                        $attachment_id = $item['attachment_id'];
                        $slide_small = houserent_theme_settings_image( $attachment_id ,117 ,73 );
                    ?>
                    <div class="item">
                        <img src="<?php echo esc_url( $slide_small ); ?>" alt="<?php the_title(); ?>">
                    </div>
                    <?php } ?>
                    </div>  
                </div> <!-- /.gallery-two -->
            <?php else: ?>
                <div class="single-rental-thumb">                           
                    <?php if ( has_post_thumbnail() ) {
                        echo houserent_theme_get_featured_img(750 ,560, false, 'true');
                    } ?>
                </div><!-- /.single-rental-thumb -->
            <?php endif; ?>
        </div> <!-- /.corousel-gallery-content -->
    <?php endif; ?>
<?php } ?>
