<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php if ( is_single() && get_post_type( get_the_ID() ) == 'rental' ) { ?>
<?php 
	$section_heading_style = houserent_theme_builder_field( $atts['section_heading_title'] );
?>
<div class="apartment-overview">
	<div class="row">
		<div class="col-md-12">
			<?php 
				$rental_location = get_post_meta( get_the_ID(),'rental_location')[0];
				$rental_deposit = get_post_meta( get_the_ID(),'rental_deposit')[0];
				$rental_bedrooms = get_post_meta( get_the_ID(),'rental_bedrooms')[0];
				$rental_baths = get_post_meta( get_the_ID(),'rental_baths')[0];
				$rental_rooms_total = get_post_meta( get_the_ID(),'rental_rooms_total')[0];
				$rental_total_area = get_post_meta( get_the_ID(),'rental_total_area')[0];
				$rental_floor = get_post_meta( get_the_ID(),'rental_floor')[0];
				$rental_total_floor = get_post_meta( get_the_ID(),'rental_total_floor')[0];
				$rental_available_from = get_post_meta( get_the_ID(),'rental_available_from')[0];
				$rental_car_parking = get_post_meta( get_the_ID(),'rental_car_parking')[0];
				$rental_price_conditions = get_post_meta( get_the_ID(),'rental_price_conditions')[0];
			?>

			<h3><?php echo esc_html($section_heading_style); ?></h3>
			<div class="overview">
                <ul>
                	<?php if( $rental_location ): ?>
                        <li>
                            <?php esc_html_e( 'Location', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php echo esc_html( $rental_location ); ?>
                            </span>
                        </li>
                    <?php endif; ?>

                	<?php if( $rental_deposit ): ?>
                        <li>
                            <?php esc_html_e( 'Deposit / Bond', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php 
                            		echo esc_html( houserent_theme_currency() ); 
                            		echo esc_html( $rental_deposit );
                            	?>
                            </span>
                        </li>
                    <?php endif; ?>

                	<?php if( $rental_bedrooms ): ?>
                        <li>
                            <?php esc_html_e( 'Bed Rooms', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php echo esc_html( $rental_bedrooms ); ?>
                            </span>
                        </li>
                    <?php endif; ?>

                	<?php if( $rental_baths ): ?>
                        <li>
                            <?php esc_html_e( 'Baths', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php echo esc_html( $rental_baths ); ?>
                            </span>
                        </li>
                    <?php endif; ?>

        			<?php if( $rental_rooms_total ): ?>
                        <li>
                            <?php esc_html_e( 'Total Rooms', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php echo esc_html( $rental_rooms_total ); ?>
                            </span>
                        </li>
                    <?php endif; ?>

                	<?php if( $rental_total_area ): ?>
                        <li>
                            <?php esc_html_e( 'Total Area (sq. ft)', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php echo esc_html( $rental_total_area ); ?>
                            </span>
                        </li>
                    <?php endif; ?>

                	<?php if( $rental_floor ): ?>
                        <li>
                            <?php esc_html_e( 'Floor', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php echo esc_html( $rental_floor ); ?>
                            </span>
                        </li>
                    <?php endif; ?>

                	<?php if( $rental_total_floor ): ?>
                        <li>
                            <?php esc_html_e( 'Total Floors', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php echo esc_html( $rental_total_floor ); ?>
                            </span>
                        </li>
                    <?php endif; ?>

                	<?php if( $rental_available_from ): ?>
                        <li>
                            <?php esc_html_e( 'Available From', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php echo esc_html( ucfirst( $rental_available_from ) ); ?>
                            </span>
                        </li>
                    <?php endif; ?>

                	<?php if( $rental_car_parking ): ?>
                        <li>
                            <?php esc_html_e( 'Car Parking Per Space', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php echo esc_html( $rental_car_parking ); ?>
                            </span>
                        </li>
                    <?php endif; ?>

                	<?php if( $rental_price_conditions ): ?>
                        <li>
                            <?php esc_html_e( 'Price Conditions', 'houserent' ); ?>
                            <span class="pull-right">
                            	<?php 
                            		echo esc_html( houserent_theme_currency() );
                            		echo esc_html( $rental_price_conditions );
                            	?>
                            </span>
                        </li>
                    <?php endif; ?>
                </ul>
            </div><!-- /.apartment-overview -->
		</div><!-- /.col-md-12 -->
	</div><!-- /.row -->
</div><!-- /.overview -->
<?php } ?>
