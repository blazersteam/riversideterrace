<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'section_heading_title' => array(
	    'label'   => esc_html__( 'Heading Title :', 'houserent' ),
	    'type'    => 'text',
	    'desc'    => esc_html__( 'Put heading title here.', 'houserent' ),
	),
);