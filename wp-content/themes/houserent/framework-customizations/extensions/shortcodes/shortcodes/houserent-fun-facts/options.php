<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'fun_fact_section_background' => array(
        'label' => esc_html__( 'Section Background', 'houserent' ),
        'type'  => 'upload',
        'desc'  => esc_html__( 'Here you can upload section background', 'houserent' ),
    ),
    'fun_fact_items' => array(
        'label'         => esc_html__( 'Add items: ', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you add fun-fact item from here', 'houserent' ),
        'template'      => '{{- fun_fact_item_title }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'fun_fact_item_title' => array(
                'label' => esc_html__( 'Title', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type title here', 'houserent' ),
            ),
            'fun_fact_item_fact_count' => array(
                'label' => esc_html__( 'Fact Count', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Put fact counter value (obviously numeric value)', 'houserent' ),
            ),
            'fun_fact_item_speed' => array(
                'label' => esc_html__( 'Count Speed', 'houserent' ),
                'type'  => 'slider',
                'value' => 1000,                                        
                'properties' => array(
                    'min' => 1000,
                    'max' => 9000,
                    'step' => 1000,
                ),
                'desc'  => esc_html__( 'Put fact counter value (obviously numeric value)', 'houserent' ),
            ),
        ),
    ),
);