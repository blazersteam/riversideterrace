<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $fun_fact_section_background = houserent_theme_builder_field( $atts['fun_fact_section_background'] );
    $fun_fact_items = houserent_theme_builder_field( $atts['fun_fact_items'] );
    if ( $fun_fact_section_background ) {
        $attachment_id = $fun_fact_section_background['attachment_id'];
        $image_url = houserent_theme_settings_image( $attachment_id ,1368 ,275 );
    } else {
        $image_url = '';
    }
?>
<!-- ======fun-fects-area======= -->
<div class="fun-fects-area" style="background-image:url(<?php echo esc_url($image_url); ?>)">
    <div class="container">
        <div class="stat">
            <?php 
                if( $fun_fact_items ):
                foreach ( $fun_fact_items as $item ) {
            ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="milestone-counter">
                <?php if( $item['fun_fact_item_fact_count'] ): ?>
                    <h3 
                    class="stat-count highlight" 
                    data-form="1" 
                    data-to="<?php echo esc_html( $item['fun_fact_item_fact_count'] ); ?>" 
                    data-speed="<?php echo esc_html( $item['fun_fact_item_speed'] ); ?>"
                    ><?php echo esc_html( $item['fun_fact_item_fact_count'] ); ?></h3>
                <?php
                    endif;
                    if( $item['fun_fact_item_title'] ): 
                ?>
                    <div class="milestone-details"><?php echo esc_html( $item['fun_fact_item_title'] ); ?></div>
                <?php endif; ?>
                </div>
            </div>
            <?php
                } endif; 
            ?>
        </div><!-- stat -->
    </div><!-- /.container -->
</div><!-- /.fun-fects-area -->
