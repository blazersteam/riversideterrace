<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php if ( is_single() && get_post_type( get_the_ID() ) == 'rental' ) { ?>
<p class="rent">
<?php
$rental_price_conditions = get_post_meta( get_the_ID(),'rental_price_conditions')[0];
echo ( isset( get_post_meta( get_the_ID(),'rental_price_duration')[0] ) ) ? get_post_meta( get_the_ID(),'rental_price_duration')[0].": " : ' '; 
echo esc_html( houserent_theme_currency() .''. ucfirst( $rental_price_conditions) ); 
?>
</p>
<?php } ?>
