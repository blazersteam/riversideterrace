<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $contact_info_title = houserent_theme_builder_field( $atts['contact_info_title'] );
    $contact_info_title_gradient = houserent_theme_builder_field( $atts['contact_info_title_gradient'] );
    $contact_info_items = houserent_theme_builder_field( $atts['contact_info_items'] );
    $contact_social = houserent_theme_builder_field( $atts['contact_social'] );
    $contact_social_icon = houserent_theme_builder_field( $atts['contact_social_icon'] );
    $contact_social_title = houserent_theme_builder_field( $atts['contact_social_title'] );
    $contact_form_title = houserent_theme_builder_field( $atts['contact_form_title'] );
    $contact_form_icon = houserent_theme_builder_field( $atts['contact_form_icon'] );
    $contact_form_subtitle = houserent_theme_builder_field( $atts['contact_form_subtitle'] );
    $contact_form_shortcode = houserent_theme_builder_field( $atts['contact_form_shortcode'] );

    $text_gradiant = "background: linear-gradient(330deg, ".esc_attr( $contact_info_title_gradient['secondary'] )." 0%, ".esc_attr( $contact_info_title_gradient['primary'] )." 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;";
?>

<!-- ======contact us====== --> 
<div class="contact-us-area">
    <div class="container">
        <?php if( $contact_info_title ): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="heading-content-one">
                    <h2 class="title default-text-gradient" style="<?php echo esc_attr( $text_gradiant ); ?>">
                        <?php 
                            $allowed_tag = array(
                                'br' => array(),
                            );
                            echo wp_kses( $contact_info_title , $allowed_tag ); 
                        ?>
                    </h2>
                </div><!-- /.contactus-heading-content -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
        <?php endif; ?>
        <div class="row">
            <div class="col-md-4">
                <div class="contact-us-content-left">

                    <?php
                        if( $contact_info_items ):
                        foreach ( $contact_info_items as $item ) {
                    ?>

                    <div class="contact">
                        <h4><i class="<?php echo esc_attr( $item['contact_item_icon'] ); ?>"></i><?php echo esc_html( $item['contact_item_title'] ); ?></h4>
                        <p>
                            <?php 
                                $allowed_tag = array(
                                    'br' => array(),
                                );
                                echo wp_kses( $item['contact_item_description'] , $allowed_tag ); 
                            ?>
                        </p>
                    </div><!-- /.contact -->

                    <?php
                        }
                        endif;
                    ?>
                    <?php if( $contact_social ): ?>
                    <div class="contact">
                        <h4>
                            <?php if( $contact_social_icon ): ?>
                            <i class="<?php echo esc_attr( $contact_social_icon ); ?>"></i>
                            <?php 
                                endif;
                                if( $contact_social_title ):
                                    echo esc_html( $contact_social_title );
                                endif; 
                            ?>
                        </h4>
                        <div class="social-icon">
                            <?php foreach ( $contact_social as $s_item ) { ?>
                                <a href="<?php echo esc_url( $s_item['contact_item_url'] ); ?>">
                                    <i class="<?php echo esc_attr( $s_item['contact_social_icon'] ); ?>"></i>
                                </a>
                            <?php } ?>
                        </div><!-- /.Social-icon -->
                    </div><!-- /.contact -->
                    <?php endif; ?>
                </div><!-- /.contactus-content-left -->
            </div><!-- /.col-md-4 -->
            
            <div class="col-md-8">
                <div class="contact-us-content-right">
                    <?php if( $contact_form_title ): ?>
                        <h3><?php echo esc_html( $contact_form_title ); ?></h3>
                    <?php endif; ?>

                    <?php if( $contact_form_icon ): ?>
                        <img src="<?php echo esc_url($contact_form_icon['url'] ); ?>" alt="<?php echo esc_html( $contact_form_title ); ?>" />
                    <?php endif; ?>

                    <div class="input-content clearfix">
                        <?php if( $contact_form_subtitle ): ?>
                            <h4><?php echo esc_html( $contact_form_subtitle ); ?></h4>
                        <?php endif; ?>

                        <?php if( $contact_form_shortcode ): echo do_shortcode($contact_form_shortcode); endif; ?>
                    </div><!-- /.input-content -->
                </div><!-- /.contactus-content-right -->
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.contactus-area -->