<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'contact_info_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Contact us live', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change section title ( HTML <br> tag allowed )', 'houserent' ),
    ),
    'contact_info_title_gradient' => array(
        'label' => esc_html__( 'Title Gradient Colors', 'houserent' ),
        'type'  => 'gradient',
        'value' => array(
                'primary'   => '#21b75f',
                'secondary' => '#31386e',
        ),
        'desc'  => esc_html__( 'Here you can change section title gradient color', 'houserent' ),
    ),
    'contact_info_items' => array(
        'label'         => esc_html__( 'Contact Infos', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you can add contact info item', 'houserent' ),
        'template'      => '{{- contact_item_title }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'contact_item_icon' => array(
                'label' => esc_html__( 'Icon', 'houserent' ),
                'type'  => 'icon',
                'desc'  => esc_html__( 'Choose a icon for item', 'houserent' ),
            ),
            'contact_item_title' => array(
                'label' => esc_html__( 'Title', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type title here', 'houserent' ),
            ),
            'contact_item_description' => array(
                'label' => esc_html__( 'Info description', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type contact info description here ( HTML <br> tag allowed )', 'houserent' ),
            ),
        ),
    ),
    'contact_social_icon' => array(
        'label' => esc_html__( 'Social Section Icon', 'houserent' ),
        'type'  => 'icon',
        'desc'  => esc_html__( 'Here you can choose social section icon', 'houserent' ),
    ),
    'contact_social_title' => array(
        'label' => esc_html__( 'Social Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Social account', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change social section title', 'houserent' ),
    ),
    'contact_social' => array(
        'label'         => esc_html__( 'Social Profiles', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you can add social links', 'houserent' ),
        'template'      => '{{- contact_social_icon }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'contact_social_icon' => array(
                'label' => esc_html__( 'Icon', 'houserent' ),
                'type'  => 'icon',
                'desc'  => esc_html__( 'Choose a icon for item', 'houserent' ),
            ),
            'contact_item_url' => array(
                'label' => esc_html__( 'Link URL', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Put link URL here', 'houserent' ),
            ),
        ),
    ),
    'contact_form_title' => array(
        'label' => esc_html__( 'Contact Form Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Have a question', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change form section title', 'houserent' ),
    ),
    'contact_form_icon' => array(
        'label' => esc_html__( 'Contact Form Icon', 'houserent' ),
        'type'  => 'upload',
        'desc'  => esc_html__( 'Here you can upload form section icon', 'houserent' ),
    ),
    'contact_form_subtitle' => array(
        'label' => esc_html__( 'Contact Form Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Send Us A email', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change form section subtitle', 'houserent' ),
    ),
    'contact_form_shortcode' => array(
        'label' => esc_html__( 'Contact-Form-7 Shortcode', 'houserent' ),
        'type'  => 'text',
        'desc'  => esc_html__( 'Put contact form shortcode here', 'houserent' ),
    ),
);