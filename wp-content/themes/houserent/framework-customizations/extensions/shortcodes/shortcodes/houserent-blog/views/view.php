<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $blog_post_section_title = houserent_theme_builder_field( $atts['blog_post_section_title'] );
    $blog_post_section_sub_title = houserent_theme_builder_field( $atts['blog_post_section_sub_title'] );
    $blog_post_selected_items = houserent_theme_builder_field( $atts['blog_post_selected_items'] );
    $blog_post_section_btn_text = houserent_theme_builder_field( $atts['blog_post_section_btn_text'] );
    $blog_post_section_btn_link = houserent_theme_builder_field( $atts['blog_post_section_btn_link'] );
    $blog_post_per_row = houserent_theme_builder_field( $atts['blog_post_per_row'] );
?>
    <!-- ======blog-area================================== --> 
    <div class="blog-area">
        <div class="container">
            <?php if( $blog_post_section_title || $blog_post_section_sub_title ): ?>
                <div class="col-md-12">
                    <div class="row">
                        <div class="heading-content-one">
                        <?php if( $blog_post_section_title ): ?>
                            <h2 class="title"><?php echo esc_html( $blog_post_section_title ); ?></h2>
                        <?php 
                            endif;
                            if( $blog_post_section_sub_title ):
                        ?>
                            <h5 class="sub-title"><?php echo esc_html( $blog_post_section_sub_title ); ?></h5>
                        <?php endif; ?>
                        </div><!-- /.blog-heading-content -->
                    </div><!-- /.row -->
                </div><!-- /.col-md-12 -->
            <?php 
                endif;
                if( $blog_post_selected_items ):
            ?>
            <div class="row">
                <?php 
                    $i = 1;
                    $post_row = ( $blog_post_per_row ) ? $blog_post_per_row : 3 ;
                    switch ( $post_row ) {
                        case 2:
                            $col_class = "col-md-6";
                            break;
                        case 3:
                            $col_class = "col-md-4";
                            break;
                        case 4:
                            $col_class = "col-md-3";
                            break;
                        
                        default:
                            $col_class = "col-md-4";
                            break;
                    }
                    $query_args = array(
                       'post_type' => 'post',
                       'post__in'  => $blog_post_selected_items,
                       'post_status' => array('publish'), 
                       'ignore_sticky_posts' => true,
                    );
                    $loop = new WP_Query($query_args);
                    while ( $loop->have_posts() ) : $loop->the_post();
                ?>
                    <div class="<?php echo esc_attr( $col_class ); ?> col-sm-6 col-xs-6">
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <?php if( has_post_thumbnail() ): ?>
                            <figure class="post-thumb">
                                <a href="<?php the_permalink(); ?>">
                                    <?php echo houserent_theme_get_featured_img(360, 232, false, "true"); ?>
                                </a>
                            </figure><!-- /.post-thumb -->
                            <?php endif; ?>
                            <div class="post-content">  
                                <div class="entry-meta">
                                    <span class="entry-date">
                                       <?php the_time('F j, Y'); ?>
                                    </span>
                                    <span class="devied"></span>
                                    <span class="entry-category">
                                        <?php the_category( ', ' ); ?>
                                    </span>
                                </div><!-- /.entry-header -->
                                <div class="entry-header">
                                    <h3 class="blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                </div><!-- /.entry-header -->
                                <div class="entry-footer">
                                    <div class="entry-footer-meta">
                                        <span class="view">
                                            <i class="fa fa-eye"></i>
                                            <?php echo houserent_theme_get_post_views ( get_the_id() ); ?>
                                        </span>
                                        <span class="like">
                                            <a href="<?php the_permalink(); ?>">
                                                <i class="fa fa-heart-o"></i>
                                                <?php houserent_theme_total_like(); ?>
                                            </a>
                                        </span>
                                        <span class="comments">
                                            <a href="<?php comments_link(); ?>">
                                                <i class="fa fa-comments"></i>
                                                <?php comments_number( esc_html__( '0', 'houserent' ), esc_html__( '1', 'houserent' ), '%'); ?>
                                            </a>
                                        </span>
                                    </div><!-- /.entry-footer-meta -->
                                </div><!-- /.entry-footer -->
                            </div><!-- /.post-content -->
                        </article><!-- /.post -->
                    </div><!-- /.col-md-6 -->
                    <?php if( $i % $post_row == 0 ): ?>
                        <div class="clearfix hidden-md hidden-lg"></div>
                    <?php endif; ?>
                <?php
                    $i++;
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div><!-- /.row -->
        <?php 
            endif;
            if( $blog_post_section_btn_text ):
        ?>
            <a href="<?php echo esc_url( $blog_post_section_btn_link ); ?>" class="button"><?php echo esc_html( $blog_post_section_btn_text ); ?></a>
        <?php endif; ?>
        </div><!-- /.container -->
    </div><!-- /.Blog-area-->