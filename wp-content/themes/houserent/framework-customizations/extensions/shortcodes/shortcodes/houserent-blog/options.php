<?php if (!defined('FW')) die('Forbidden');

$all_pages = get_posts( array( 'posts_per_page'=>-1,'post_type'=>'post' ) );
$blog_posts = array();
foreach ($all_pages as $key => $value) {
    $blog_posts[$value->ID] = $value->post_title;
}


$options = array(
    'blog_post_section_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Blog', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change section title', 'houserent' ),
    ),
    'blog_post_section_sub_title' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Our News Feed', 'houserent' ),
        'desc'  => esc_html__( 'You can change section subtitle', 'houserent' ),
    ),
    'blog_post_selected_items' => array(
        'type'  => 'multi-select',
        'label' => esc_html__('Select Blog Posts', 'houserent'),
        'desc'  => esc_html__('You can select multiple posts from here.', 'houserent'),
        'population' => 'array',
        'choices' => $blog_posts,
    ),
    'blog_post_per_row' => array(
        'label' => esc_html__( 'Post Per Row', 'houserent' ),
        'type'  => 'slider',
        'value' => 3,                                        
        'properties' => array(
            'min' => 2,
            'max' => 4,
            'step' => 1,
        ),
        'desc'  => esc_html__( 'You can change post per row from here', 'houserent' ),
    ),
    'blog_post_section_btn_text' => array(
        'label' => esc_html__( 'Button Text', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Show All', 'houserent' ),
        'desc'  => esc_html__( 'You can change button text here', 'houserent' ),
    ),
    'blog_post_section_btn_link' => array(
        'label' => esc_html__( 'Link URL', 'houserent' ),
        'type'  => 'text',
        'value'  => '#',
        'desc'  => esc_html__( 'Put button link URL', 'houserent' ),
    ),
);