<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'com_brand_section_color' => array(
        'label' => esc_html__( 'Section Background Gradient Color', 'houserent' ),
        'type'  => 'gradient',
        'value' => array(
                'primary'   => '#21b75f',
                'secondary' => '#31386e',
        ),
        'desc'  => esc_html__( 'Here you can change company brands section background gradient color', 'houserent' ),
    ),
    'com_brand_section_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Company With Us', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change company brands section title', 'houserent' ),
    ),
    'com_brand_items' => array(
        'label'         => esc_html__( 'Add items: ', 'houserent' ),
        'type'          => 'addable-popup',
        'desc'          => esc_html__( 'Here you add company brand item from here', 'houserent' ),
        'template'      => '{{- com_brand_item_name }}',                                    
        'add-button-text' => esc_html__('Add Item', 'houserent'),
        'popup-options' => array(
            'com_brand_item_image' => array(
                'label' => esc_html__( 'Image', 'houserent' ),
                'type'  => 'upload',
                'desc'  => esc_html__( 'Upload image here', 'houserent' ),
            ),
            'com_brand_item_name' => array(
                'label' => esc_html__( 'Title', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type title here', 'houserent' ),
            ),
            'com_brand_item_url' => array(
                'label' => esc_html__( 'Linked URL', 'houserent' ),
                'type'  => 'text',
                'desc'  => esc_html__( 'Type brand URL here', 'houserent' ),
            ),
        ),
    ),
);