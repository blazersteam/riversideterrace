<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $com_brand_section_title = houserent_theme_builder_field( $atts['com_brand_section_title'] );
    $com_brand_section_color = houserent_theme_builder_field( $atts['com_brand_section_color'] );
    $com_brand_items = houserent_theme_builder_field( $atts['com_brand_items'] );
    if( $com_brand_section_color ){
        $color_gradiant = "background: ".esc_attr( $com_brand_section_color['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $com_brand_section_color['primary'] )." 0%, ".esc_attr( $com_brand_section_color['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $com_brand_section_color['primary'] )." 0%, ".esc_attr( $com_brand_section_color['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $com_brand_section_color['primary'] )." 0%, ".esc_attr( $com_brand_section_color['secondary'] )." 100%);";
    } else {
        $color_gradiant = "";
    }
?>

    <!-- ======company-area======= --> 
    <div class="company-logo-area default-template-gradient" style="<?php echo esc_attr( $color_gradiant ); ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo-content">
                        <?php if( $com_brand_section_title ): ?>
                            <h2><?php echo esc_html( $com_brand_section_title ); ?></h2>
                        <?php
                            endif;
                            if( $com_brand_items ):
                        ?>

                        <div class="brand-slider owl-carousel">
                            <?php 
                                foreach ( $com_brand_items as $item ) {
                                if( $item['com_brand_item_image']):
                                $attachment_id =  $item['com_brand_item_image']['attachment_id'];
                                    if( $item['com_brand_item_url'] ):
                            ?>
                                <a href="<?php echo esc_url( $item['com_brand_item_url'] ); ?>" class="item">
                                <?php endif; ?>
                                    <img src="<?php echo esc_url( houserent_theme_settings_image( $attachment_id ,100 ,100 ) ); ?>" alt="<?php echo esc_html( $item['com_brand_item_name'] ); ?>" />
                                <?php if( $item['com_brand_item_url'] ): ?>
                                </a>
                            <?php
                                    endif;
                                endif;
                                } 
                            ?>
                        </div><!-- /.brand-slider --> 
                        <?php endif; ?> 
                    </div><!-- /.logo-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.company-logo-area -->
