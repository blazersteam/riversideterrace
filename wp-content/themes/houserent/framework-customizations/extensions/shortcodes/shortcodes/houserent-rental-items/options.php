<?php if (!defined('FW')) die('Forbidden');


$all_pages = get_posts( array( 'posts_per_page'=>-1,'post_type'=>'rental' ) );
$rental_items = array();
foreach ($all_pages as $key => $value) {
    $rental_items[$value->ID] = $value->post_title;
}

$options = array(
    'rental_item_section_title' => array(
        'label' => esc_html__( 'Section Title', 'houserent' ),
        'type'  => 'text',
        'value' =>  esc_html__( 'Rooms & Apartments', 'houserent' ),
        'desc'  => esc_html__( 'Here you can change section title', 'houserent' ),
    ),
    'rental_item_item_per_row' => array(
        'label' => esc_html__( 'Item Per Row', 'houserent' ),
        'type'  => 'select',
         'value' =>  'three',
        'choices' => array(
            'two' => esc_html__( 'Two', 'houserent' ),
            'three' => esc_html__( 'Three', 'houserent' ),
         ),
        'desc'  => esc_html__( 'You can change post per row setting from here', 'houserent' ),
    ),
    'rental_item_section_sub_title' => array(
        'label' => esc_html__( 'Section Subtitle', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'Find Your Rooms, for your availability', 'houserent' ),
        'desc'  => esc_html__( 'You can change section subtitle', 'houserent' ),
    ),
    'rental_item_selected_items' => array(
        'type'  => 'multi-select',
        'label' => esc_html__('Select Rental Items', 'houserent'),
        'desc'  => esc_html__('You can select multiple item from here.', 'houserent'),
        'population' => 'array',
        'choices' => $rental_items,
    ),
    'rental_item_section_btn_text' => array(
        'label' => esc_html__( 'Button Text', 'houserent' ),
        'type'  => 'text',
        'value' => esc_html__( 'ALL Apartments', 'houserent' ),
        'desc'  => esc_html__( 'You can change button text here', 'houserent' ),
    ),
    'rental_item_section_btn_link' => array(
        'label' => esc_html__( 'Link URL', 'houserent' ),
        'type'  => 'text',
        'value'  => '#',
        'desc'  => esc_html__( 'Put button link URL', 'houserent' ),
    ),
);