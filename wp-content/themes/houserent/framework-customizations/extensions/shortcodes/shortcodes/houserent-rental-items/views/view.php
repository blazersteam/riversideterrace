<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $rental_item_section_title = houserent_theme_builder_field( $atts['rental_item_section_title'] );
    $rental_item_section_sub_title = houserent_theme_builder_field( $atts['rental_item_section_sub_title'] );
    $rental_item_selected_items = houserent_theme_builder_field( $atts['rental_item_selected_items'] );
    $rental_item_section_btn_text = houserent_theme_builder_field( $atts['rental_item_section_btn_text'] );
    $rental_item_section_btn_link = houserent_theme_builder_field( $atts['rental_item_section_btn_link'] );
    $rental_item_item_per_row = houserent_theme_builder_field( $atts['rental_item_item_per_row'] );
    switch ( $rental_item_item_per_row ) {
        case 'two':
            $class = "col-md-6";
            $item = 2;
            break;
        
        case 'three':
            $class = "col-md-4";
            $item = 3;
            break;
        
        default:
            $class = "col-md-4";
            $item = 3;
            break;
    }
?>
 <!-- ======Apartments-area================================== --> 
    <div class="apartments-area">
        <div class="container">
        <?php if( $rental_item_section_title || $rental_item_section_sub_title ): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-content-one border">
                    <?php if( $rental_item_section_title ): ?>
                        <h2 class="title"><?php echo esc_html( $rental_item_section_title ); ?></h2>
                    <?php 
                        endif;
                        if( $rental_item_section_sub_title ):
                    ?>
                        <h5 class="sub-title"><?php echo esc_html( $rental_item_section_sub_title ); ?></h5>
                    <?php endif; ?>
                    </div><!-- /.Apartments-heading-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        <?php endif; ?>
        <?php if( $rental_item_selected_items ): ?>
            <div class="row">
            <?php 
                $i = 1;
                $query_args = array(
                   'post_type' => 'rental',
                   'post__in'  => $rental_item_selected_items,
                   'posts_per_page' => -1,
                );
                $loop = new WP_Query($query_args);
                while ( $loop->have_posts() ) : $loop->the_post();

                $rental_price_conditions = get_post_meta( get_the_ID(),'rental_price_conditions')[0];
                $rental_bedrooms = get_post_meta( get_the_ID(),'rental_bedrooms')[0];
                $rental_baths = get_post_meta( get_the_ID(),'rental_baths')[0];
                $rental_location = get_post_meta( get_the_ID(),'rental_location')[0];
                $rental_gallery_item_images = houserent_theme_uny_post_meta('rental_gallery_item_images','');
                $rental_cat = get_the_terms( get_the_ID(), 'rental_cat' );
            ?>
                <div class="<?php echo esc_attr( $class ); ?> col-sm-6">
                    <div class="apartments-content">

                        <?php if ( has_post_thumbnail() ): ?>
                            <div class="image-content">
                                <a href="<?php the_permalink(); ?>">
                                    <?php  
                                        echo houserent_theme_get_featured_img(360, 270, false, false);
                                    ?>
                                </a>
                            </div><!-- /.image-content -->
                        <?php endif; ?>
                        
                        <div class="text-content">
                            <div class="top-content">
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <?php if( $rental_location ): ?>
                                <span>
                                    <i class="fa fa-map-marker"></i> 
                                    <?php echo esc_html( $rental_location ); ?>
                                </span> 
                                <?php endif; ?>

                                <?php if( $rental_cat ): ?>
                                <span>
                                    <i class="fa fa-home"></i> 
                                    <?php 
                                        if ( $rental_cat && ! is_wp_error( $rental_cat ) ) : 
                                            foreach ( $rental_cat as $term ) {
                                                $term_link = get_term_link( $term );
                                                ?>
                                                    <a href="<?php echo esc_url( $term_link ); ?>"><?php echo esc_html( $term->name ); ?></a>
                                                <?php
                                            }
                                        endif; 
                                    ?>
                                </span> 
                                <?php endif; ?>
                            </div><!-- /.top-content -->

                            <div class="bottom-content clearfix">
                            <?php if( $rental_bedrooms ): ?>
                                <div class="meta-bed-room">
                                    <i class="fa fa-bed"></i>
                                    <?php 
                                        echo esc_html( $rental_bedrooms );
                                        $room_singular = ( $rental_bedrooms <= 1 ) ? esc_html__( ' Bedroom', 'houserent' ) : esc_html__( ' Bedrooms', 'houserent' ) ;
                                        echo esc_html( $room_singular );
                                    ?>
                                </div>
                            <?php 
                                endif;
                                if( $rental_baths ):
                            ?>
                                <div class="meta-bath-room">
                                    <i class="fa fa-bath"></i>
                                    <?php 
                                        echo esc_html( $rental_baths );
                                        $room_baths = ( $rental_baths <= 1 ) ? esc_html__( ' Bathroom', 'houserent' ) : esc_html__( ' Bathrooms', 'houserent' ) ;
                                        echo esc_html( $room_baths );
                                    ?>
                                </div>
                            <?php
                                endif;
                            ?>
                                <span class="clearfix"></span>
                            <?php
                                if( $rental_price_conditions ):
                            ?>
                                <div class="rent-price pull-left">
                                    <?php 
                                        if( $rental_price_conditions ):
                                            echo esc_html( houserent_theme_currency() );
                                            echo esc_html( $rental_price_conditions );
                                        endif; 
                                    ?>
                                </div>
                            <?php 
                                endif;
                            ?>
                            <div class="share-meta dropup pull-right">
                                <?php houserent_theme_rental_share(); ?>
                            </div>
                            </div>
                        </div><!-- /.text-content -->
                    </div><!-- /.partments-content -->
                </div><!-- /.col-md-4 -->
                <?php if( $i % $item == 0 ): ?>
                    <div class="clearfix hidden-md hidden-lg"></div>
                <?php endif; ?>
            <?php
                $i++;
                endwhile;
                wp_reset_postdata();
            ?>

            </div><!-- /.row -->
        <?php 
            endif;
            if( $rental_item_section_btn_text ):
        ?>
            <a href="<?php echo esc_url( $rental_item_section_btn_link ); ?>" class="button nevy-button"><?php echo esc_html( $rental_item_section_btn_text ); ?></a>
        <?php endif; ?>
        </div><!-- /.container -->
    </div><!-- /.Apartments-area-->