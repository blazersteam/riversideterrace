<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'section_heading_style_sec' => array(
        'type'         => 'multi-picker',
        'label'        => false,
        'desc'         => false,
        'picker'       => array(
            'section_heading_style' => array(
                'type'  => 'switch',
                'label' => esc_html__( 'Heading Style', 'houserent' ),
                'right-choice' => array(
                    'value' => 'one',
                    'label' => esc_html__( 'One', 'houserent' )
                ),
                'left-choice'  => array(
                    'value' => 'two',
                    'label' => esc_html__( 'Two', 'houserent' )
                ),
                'value'     => 'one',
                'desc'      => esc_html__( 'You can switch section style from here','houserent' ),
            ),
        ),
        'choices'  => array(
            'one'  => array(
                'section_heading_gradient' => array(
                    'label' => esc_html__( 'Background Gradient', 'houserent' ),
                    'type'  => 'gradient',
                    'value' => array(
                            'primary'   => '#21b75f',
                            'secondary' => '#31386e',
                    ),
                    'desc'  => esc_html__( 'Here you can change section title and heading text gradient color', 'houserent' ),
                ),
            ),
            'two'  => array(
                'section_heading_color'   => array(
                    'type'  => 'color-picker',
                    'label' => esc_html__( 'Background Color', 'houserent' ),
                    'value' => '#ddd8d8',
                    'desc'  => esc_html__( 'You can change heading section background color from here', 'houserent' ),
                ),
            ),
        ),
        'show_borders' => false,
    ),
    'section_heading_title' => array(
        'label'   => esc_html__( 'Heading Title :', 'houserent' ),
        'type'    => 'text',
        'value'    => '',
        'desc'    => esc_html__( 'Put heading title here.', 'houserent' ),
    ),
    'section_heading_sub_title' => array(
        'label'   => esc_html__( 'Heading Subtitle :', 'houserent' ),
        'type'    => 'text',
        'value'    => '',
        'desc'    => esc_html__( 'Put heading subtitle here.', 'houserent' ),
    ),
    'section_heading_margin_top' => array(
        'label' => esc_html__( 'Margin Top', 'houserent' ),
        'type'  => 'text',
        'value' =>  '20px',
        'desc'  => esc_html__( 'You can set how much space you want on the top of the section', 'houserent' ),
    ),
	'section_heading_margin_bottom' => array(
        'label' => esc_html__( 'Margin Bottom', 'houserent' ),
        'type'  => 'text',
        'value' =>  '20px',
        'desc'  => esc_html__( 'You can set how much space you want under the bottom of the section', 'houserent' ),
    ),
);