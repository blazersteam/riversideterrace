<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 
    $section_heading_style = houserent_theme_builder_field( $atts['section_heading_style_sec']['section_heading_style'] );
    $section_heading_title = houserent_theme_builder_field( $atts['section_heading_title'] );
    $section_heading_sub_title = houserent_theme_builder_field( $atts['section_heading_sub_title'] );
    $section_heading_margin_top = houserent_theme_builder_field( $atts['section_heading_margin_top'] );
    $section_heading_margin_bottom = houserent_theme_builder_field( $atts['section_heading_margin_bottom'] );


    $heading_style = houserent_theme_builder_field( $atts['section_heading_style_sec']['section_heading_style'] );
?>
<!-- ====== Page Header ====== --> 
<?php 
if( $section_heading_title || $section_heading_sub_title ): 
    if( $heading_style == 'one' ):
    $section_heading_gradient = houserent_theme_builder_field( $atts['section_heading_style_sec']['one']['section_heading_gradient'] );
    $gradiant_bg = "background: ".esc_attr( $section_heading_gradient['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $section_heading_gradient['primary'] )." 0%, ".esc_attr( $section_heading_gradient['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $section_heading_gradient['primary'] )." 0%, ".esc_attr( $section_heading_gradient['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $section_heading_gradient['primary'] )." 0%, ".esc_attr( $section_heading_gradient['secondary'] )." 100%);";
?>
<div class="page-header default-template-gradient" style="<?php echo esc_attr( $gradiant_bg ); ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                
                <div style="margin-top: <?php echo esc_attr( $section_heading_margin_top );?>; margin-bottom: <?php echo esc_attr( $section_heading_margin_bottom );?>">
                    <?php if( $section_heading_title ): ?>
                        <h2 class="page-title"><?php echo esc_html( $section_heading_title ); ?></h2>
                    <?php 
                        endif;
                        if( $section_heading_sub_title ):
                    ?>
                        <p class="page-description"><?php echo esc_html( $section_heading_sub_title ); ?></p>
                    <?php endif; ?>
                </div><!-- /.default-pd-center -->         
            </div><!-- /.col-md-12 -->
        </div><!-- /.row-->
    </div><!-- /.container-fluid -->           
</div><!-- /.page-header -->
<?php
    elseif( $heading_style == 'two' ):
    $section_heading_color = houserent_theme_builder_field( $atts['section_heading_style_sec']['two']['section_heading_color'] );
    if( $section_heading_color ){
        $color = "background: $section_heading_color;";
    }else{
        $color = "";
    }
?>
    <div class="heading-section bg-gray-color" style="margin-top: <?php echo esc_attr( $section_heading_margin_top );?>; margin-bottom: <?php echo esc_attr( $section_heading_margin_bottom );?>; <?php echo esc_attr( $color ); ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-content-one border">
                        <?php if( $section_heading_title ): ?>
                            <h2 class="title"><?php echo esc_html( $section_heading_title ); ?></h2>
                        <?php 
                            endif;
                            if( $section_heading_sub_title ):
                        ?>
                            <h5 class="sub-title"><?php echo esc_html( $section_heading_sub_title ); ?></h5>
                        <?php endif; ?>
                    </div><!-- /.Apartments-heading-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
<?php endif; ?>
<?php endif; ?>