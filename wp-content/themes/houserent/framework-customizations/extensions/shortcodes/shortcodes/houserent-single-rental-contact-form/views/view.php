<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php if ( is_single() && get_post_type( get_the_ID() ) == 'rental' ) { ?>
    <div class="apartment-sidebar">
        <?php 
            $formTitle = $rental_single_page_header = houserent_theme_get_customizer_field( 'rental_page_contact_type','rental_search_top_search' )['default']['rental_form_title'];
            $formfullname = $rental_single_page_header = houserent_theme_get_customizer_field( 'rental_page_contact_type','rental_search_top_search' )['default']['rental_form_fullname'];
            $formphone = $rental_single_page_header = houserent_theme_get_customizer_field( 'rental_page_contact_type','rental_search_top_search' )['default']['rental_form_phone'];
            $formemail = $rental_single_page_header = houserent_theme_get_customizer_field( 'rental_page_contact_type','rental_search_top_search' )['default']['rental_form_email'];
            $formefamily = $rental_single_page_header = houserent_theme_get_customizer_field( 'rental_page_contact_type','rental_search_top_search' )['default']['rental_form_family'];
            $formechild = $rental_single_page_header = houserent_theme_get_customizer_field( 'rental_page_contact_type','rental_search_top_search' )['default']['rental_form_children'];
            $formeMessege = $rental_single_page_header = houserent_theme_get_customizer_field( 'rental_page_contact_type','rental_search_top_search' )['default']['rental_form_messege'];
        
        ?>
        <?php if(houserent_theme_get_customizer_field( 'rental_page_contact_type','rental_search_top_search' )['rental_search_top_search'] == "default") { ?>
            <?php if($formfullname !== false || $formphone !== false ||  $formemail !== false || $formefamily !== false || $formechild !== false || $formeMessege !== false) : ?>          
            <div class="widget_rental_search clerafix">                 
                <div class="form-border gradient-border">
                    <form action="<?php echo esc_url( home_url( '/' ) ); ?>confirm-booking" method="post" class="advance_search_query booking-form">
                        <div class="form-bg seven">
                            <div class="form-content">
                                <?php if(isset($formTitle)) : ?>
                                <h2 class="form-title"><?php echo esc_html($formTitle); ?></h2>
                                <?php endif ?>
                                <input type="hidden" name="post_id" value="<?php the_ID(); ?>" />
                                <?php if($formfullname == true) : ?>
                                <div class="form-group">
                                   <label><?php esc_html_e( 'Full Name', 'houserent' ); ?></label>
                                   <input type="text" name="full_name" placeholder="<?php esc_html_e( 'Full name', 'houserent' ); ?>">
                                </div><!-- /.form-group -->
                                <?php endif ?>
                                <?php if($formphone == true) : ?>
                                <div class="form-group">
                                    <label><?php esc_html_e( 'Phone Number', 'houserent' ); ?></label>
                                    <input type="tel" name="phone_number" placeholder="<?php esc_html_e( '+99(99)9999-9999', 'houserent' ); ?>">
                                </div><!-- /.form-group -->
                                <?php endif ?>
                                <?php if($formemail == true) : ?>
                                <div class="form-group">
                                    <label><?php esc_html_e( 'Email Address', 'houserent' ); ?></label>
                                    <input type="email" name="email" placeholder="<?php esc_html_e( 'example@domain.com', 'houserent' ); ?>">
                                </div><!-- /.form-group -->
                                <?php endif ?>
                                <?php if($formefamily == true) : ?>
                                <div class="form-group">
                                    <label><?php esc_html_e( 'Family Member', 'houserent' ); ?></label>
                                    <input type="number" step="1" min="1" max="100" name="family_member" value="1"  size="4" class="input-text">
                                </div><!-- /.form-group -->
                                <?php endif ?>
                                <?php if($formechild == true) : ?>
                                <div class="form-group">
                                    <label><?php esc_html_e( 'Children', 'houserent' ); ?></label>
                                    <input type="number" step="1" min="0" max="100" name="children" value="1" size="4" class="input-text">
                                </div><!-- /.form-group -->
                                <?php endif ?>
                                <?php if($formeMessege == true) : ?>
                                <div class="form-group">
                                    <label><?php esc_html_e( 'Your Message', 'houserent' ); ?></label>
                                    <textarea name="message" placeholder="Message" class="form-controller"></textarea>
                                </div><!-- /.form-group -->
                                <?php endif ?>
                                <div class="form-group">
                                    <button type="submit" class="button default-template-gradient button-radius"><?php esc_html_e( 'Request Booking', 'houserent' ); ?></button>
                                </div><!-- /.form-group -->
                            </div><!-- /.form-content -->
                        </div><!-- /.form-bg -->
                    </form> <!-- /.advance_search_query -->
                </div><!-- /.form-border -->
            </div><!-- /.widget_rental_search -->
            <?php endif ?>
        <?php } else {?>
            <div class="widget_rental_search clerafix">                 
                <div class="form-border gradient-border">
                    <div class="cr7-shortcode-panel">                   
                        <?php if ( houserent_theme_get_customizer_field( 'rental_page_contact_type','rental_search_top_search' )['contact_form_7']['rental_form_cr7_shortcode'] ) {
                            echo do_shortcode( houserent_theme_get_customizer_field( 'rental_page_contact_type','rental_search_top_search' )['contact_form_7']['rental_form_cr7_shortcode']); 
                        } ?>
                    </div><!--  /.cr7-shortcode-panel -->
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>
