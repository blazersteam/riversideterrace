<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
?>
<?php 

    $banner_main_style = houserent_theme_builder_field( $atts['banner_main_style']);
    $terms = get_terms( 'rental_cat', array( 'hide_empty' => false, ) );
?>
<?php
    if( $banner_main_style['banner_style'] == 'one' ){

        $default_gradient = Array (
            'primary' => '#31386e',
            'secondary' => '#21b75f',
        );
        $banner_overlay_gradient = (isset( $banner_main_style['one']['banner_overlay_gradient'] ) ) ? $banner_main_style['one']['banner_overlay_gradient'] : $default_gradient ;
        $banner_bg_image = ( isset( $banner_main_style['one']['banner_bg_image'] )) ? $banner_main_style['one']['banner_bg_image'] : '' ;
        $banner_logo_image = ( isset( $banner_main_style['one']['banner_logo_image'] )) ? $banner_main_style['one']['banner_logo_image'] : '' ;
        $banner_title =  ( isset( $banner_main_style['one']['banner_title'] )) ? $banner_main_style['one']['banner_title'] : '' ;
        $banner_sub_title = ( isset( $banner_main_style['one']['banner_sub_title'] )) ? $banner_main_style['one']['banner_sub_title'] : '';
        $banner_button = ( isset( $banner_main_style['one']['banner_button'] ) ) ? $banner_main_style['one']['banner_button'] : '';
        $banner_button_url = $banner_main_style['one']['banner_button_url'];
        $banner_menu_sec = $banner_main_style['one']['banner_menu_sec'];
        $banner_background = ( $banner_bg_image ) ? $banner_bg_image['url'] : '' ;
        $banner_logo_image = ( $banner_logo_image ) ? $banner_logo_image['url'] : '' ;

        $gradiant_bg = "background: ".esc_attr( $banner_overlay_gradient['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $banner_overlay_gradient['primary'] )." 0%, ".esc_attr( $banner_overlay_gradient['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $banner_overlay_gradient['primary'] )." 0%, ".esc_attr( $banner_overlay_gradient['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $banner_overlay_gradient['primary'] )." 0%, ".esc_attr( $banner_overlay_gradient['secondary'] )." 100%);";

?>

<!-- ======slider-area======== --> 
<div class="banner-with-menu-area">
    <div class="main-slide">
        <div class="item">
            <div class="banner-images gradient-transparent-overlay" style="background-image:url(<?php echo esc_url( $banner_background ); ?>);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="banner-text-content">
                            <?php if( $banner_logo_image ): ?>
                                <img src="<?php echo esc_url( $banner_logo_image ); ?>" alt="<?php echo esc_attr( $banner_title ); ?>" />
                            <?php 
                                endif;
                                if( $banner_sub_title ):
                            ?>
                                <h3 class="banner-subtitle"><?php echo esc_html( $banner_sub_title ); ?></h3>
                            <?php 
                                endif;
                                if( $banner_title ):
                            ?>
                                <h2 class="banner-title"><?php echo esc_html( $banner_title ); ?></h2>
                            <?php 
                                endif;
                                if( $banner_button && $banner_button_url ):
                            ?>
                                <a href="<?php echo esc_url( $banner_button_url ); ?>" class="more-link"><?php echo esc_html( $banner_button ); ?></a>
                            <?php endif; ?>
                            </div><!-- /.text-content -->
                            <?php if( $banner_menu_sec == 'show' ): ?>
                            <nav class="site-navigation top-navigation animation-<?php echo esc_attr( houserent_theme_get_customizer_field('header_menu_animation','slide') ); ?>">
                               <div class="menu-wrapper">
                                   <div class="menu-content">
                                       <?php 
                                           wp_nav_menu ( array(
                                               'menu_class' => 'menu-list six',
                                               'container'=> 'ul',
                                               'theme_location' => 'main-menu',
                                               'fallback_cb'       => 'Sh_Custom_Walker::fallback_main_menu',
                                               'walker' => new Sh_Custom_Walker()  
                                           )); 
                                       ?>
                                   </div>
                               </div> <!-- /.menu-wrapper --> 
                            </nav>
                            <?php endif; ?>
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.slider-content -->         
        </div><!-- /.item -->
    </div><!-- /.owl-demo -->           
</div><!-- /.slider-area container-fluid -->

<?php } elseif( $banner_main_style['banner_style'] == 'two' ){ ?>
<?php 
    $banner_form_btn_title = $banner_main_style['two']['banner_form_btn_title'];
    $banner_sub_title = $banner_main_style['two']['banner_sub_title'];
    $bg_type = $banner_main_style['two']['banner_background_color'];

    if( $bg_type['banner_background'] == 'color' ){
        $gradiant_bg = "background: ".esc_attr( $bg_type['color']['banner_bg_solid_color'] )."";
    } elseif ( $bg_type['banner_background'] == 'gradient' ) {
        $banner_overlay_gradient = $bg_type['gradient']['banner_bg_gradient'];
        if( $banner_overlay_gradient ){
            $gradiant_bg = "background: ".esc_attr( $banner_overlay_gradient['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $banner_overlay_gradient['primary'] )." 0%, ".esc_attr( $banner_overlay_gradient['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $banner_overlay_gradient['primary'] )." 0%, ".esc_attr( $banner_overlay_gradient['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $banner_overlay_gradient['primary'] )." 0%, ".esc_attr( $banner_overlay_gradient['secondary'] )." 100%);";
        } else {
            $gradiant_bg = '';
        }
    }
?>
<div class="form-block-content" style="<?php echo esc_attr($gradiant_bg); ?>">
    <!-- ======form-area======= --> 
    <div class="form-area form-four">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form role="search" method="get" class="advance_search_query" action="<?php echo esc_url(home_url('/')); ?>">
                        <div class="form-bg border-radius">
                            <div class="form-content">
                                <div class="form-group living-area">
                                    <label><?php esc_html_e( 'Living Aria', 'houserent' ); ?></label>
                                    <input class="tags search-field" type="text" value="" name="s" placeholder="<?php esc_html_e( 'Where do you want to live?', 'houserent' ); ?>">
                                    <input type="hidden" name="post_type" value="rental" />
                                </div><!-- /.form-group -->

                                <?php if( $terms ): ?>
                                    <div class="form-group category-type">
                                        <label><?php esc_html_e( 'Type', 'houserent' ); ?></label>
                                        <select name="cal_slug">
                                            <option value=""><?php esc_html_e( 'any', 'houserent' ); ?></option>
                                            <?php 
                                                foreach ( $terms as $single_cat ) { 
                                            ?>
                                                <option value="<?php echo esc_attr( $single_cat->slug ); ?>"><?php echo esc_html( $single_cat->name ); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div><!-- /.form-group -->
                                <?php endif; ?>

                                <div class="form-group min-price">
                                    <label><?php esc_html_e( 'Price Range', 'houserent' ); ?></label>
                                    <input type="text" value="" name="min_price" placeholder="<?php esc_html_e( 'Ex - 200', 'houserent' ); ?>">
                                </div><!-- /.form-group -->
                                <div class="form-group max-price">
                                    <label class="invisible"><?php esc_html_e( 'Price Range', 'houserent' ); ?></label>
                                    <input type="text" value="" name="max_price" placeholder="<?php esc_html_e( 'Ex - 5000', 'houserent' ); ?>">
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                   <button type="submit" class=" button nevy-button availity-button"><?php esc_html_e( 'Check Availability', 'houserent' ); ?></button>
                                </div><!-- /.form-group -->
                            </div><!-- /.form-content -->
                        </div><!-- /.form-bg -->
                    </form> <!-- /.advance_search_query -->
                    <?php if( $banner_sub_title ): ?>
                        <h4 class="form-bottom-title text-center"><span><?php echo esc_html( $banner_sub_title ); ?></span></h4>
                    <?php endif; ?>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.form-area -->
</div><!-- /.form-block-content -->

<?php } elseif( $banner_main_style['banner_style'] == 'three' ){ ?>
<?php 
    $map_set = ( isset( $banner_main_style['three']['banner_map'] ) ) ? $banner_main_style['three']['banner_map'] : '';
    $banner_section_map_api = ( isset( $banner_main_style['three']['banner_section_map_api'] ) ) ? $banner_main_style['three']['banner_section_map_api'] : '';
    $banner_section_map_height = ( isset( $banner_main_style['three']['banner_section_map_height'] ) ) ? $banner_main_style['three']['banner_section_map_height'] : '';
    $banner_view = ( isset( $banner_main_style['three']['banner_form_style_sec']['banner_form_style'] ) ) ? $banner_main_style['three']['banner_form_style_sec']['banner_form_style'] : '';

    $location_ = $map_set['location'];
    $venue_ = $map_set['venue'];

    $location = str_replace( ' ', '+', $location_);
    $venue = str_replace( ' ', '+', $venue_);
    $terms = get_terms( 'rental_cat', array( 'hide_empty' => false, ) );
    $terms = ( $terms ) ? $terms : '' ;
   
?>
    <!-- ======google-area======= -->  
    <div class="google-map-area">
        <div class="container-fluid pd-zero">
            <div class="map-content">
            <?php if( $map_set && $banner_section_map_api ): ?>
                <<?php echo 'iframe';?> height=<?php echo esc_attr( $banner_section_map_height ); ?> src="https://www.google.com/maps/embed/v1/place?key=<?php echo esc_attr( $banner_section_map_api ); ?>&q=<?php echo esc_attr( $venue ).','.esc_attr( $location ); ?>" allowfullscreen="allowfullscreen"></<?php echo 'iframe';?>>
            <?php endif; ?>
            </div><!-- /.map-content -->

            <?php
                if( $banner_view == "search" ){
                $gradient = $banner_main_style['three']['banner_form_style_sec']['search']['form_gradient'];
                $bg_gradiant = "background: ".esc_attr( $gradient['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $gradient['primary'] )." 0%, ".esc_attr( $gradient['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $gradient['primary'] )." 0%, ".esc_attr( $gradient['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $gradient['primary'] )." 0%, ".esc_attr( $gradient['secondary'] )." 100%);";
            ?>
                <div class="row">
                    <div class="col-md-4">
                        <div class="widget_rental_search widget-top clerafix" style="<?php echo esc_attr( $bg_gradiant ); ?>">                  
                            <div class="form-border gradient-border">
                                <form role="search" method="get" class="advance_search_query" action="<?php echo esc_url(home_url('/')); ?>">
                                    <div class="form-bg seven">
                                        <div class="form-content">
                                            <div class="form-group living-area">
                                               <label><?php esc_html_e( 'Living Aria', 'houserent' ); ?></label>
                                               <input class="tags search-field" type="text" value="" name="s" placeholder="<?php esc_html_e( 'Where do you want to live?', 'houserent' ); ?>">
                                               <input type="hidden" name="post_type" value="rental" />
                                            </div><!-- /.form-group -->
                                            <div class="form-group category-type">
                                                <label><?php esc_html_e( 'Type', 'houserent' ); ?></label>
                                                <select name="cal_slug">
                                                    <option value=""><?php esc_html_e( 'any', 'houserent' ); ?></option>
                                                    <?php 
                                                        foreach ( $terms as $single_cat ) {  
                                                    ?>
                                                        <option value="<?php echo esc_attr( $single_cat->slug ); ?>"><?php echo esc_html( $single_cat->name ); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div><!-- /.form-group -->
                                            <div class="form-group small min-price">
                                                <label><?php esc_html_e( 'Price', 'houserent' ); ?></label>
                                                <input type="text" value="" name="min_price" placeholder="<?php esc_html_e( 'Ex - 200', 'houserent' ); ?>">
                                            </div><!-- /.form-group -->
                                            <div class="form-group small max max-price">
                                                <label class="invisible"><?php esc_html_e( 'Price', 'houserent' ); ?></label>
                                                <input type="text" value="" name="max_price" placeholder="<?php esc_html_e( 'Ex - 5000', 'houserent' ); ?>">
                                            </div><!-- /.form-group -->
                                            <div class="form-group total-rooms">
                                                <label><?php esc_html_e( 'Rooms', 'houserent' ); ?></label>
                                                <input type="number" value="" name="rental_rooms_total" placeholder="<?php esc_html_e( 'Total rooms', 'houserent' ); ?>">
                                            </div><!-- /.form-group -->
                                            <div class="form-group button-area">
                                                <button type="submit" class="button button-radius default-template-gradient" style="<?php echo esc_attr( $bg_gradiant ); ?>"><?php esc_html_e( 'Check Availability', 'houserent' ); ?></button>
                                            </div><!-- /.form-group -->
                                        </div><!-- /.form-content -->
                                    </div><!-- /.form-bg -->
                                </form> <!-- /.advance_search_query -->
                            </div><!-- /.form-border -->
                        </div><!-- /.widget_rental_search -->
                    </div><!-- /.col-md-5 -->
                </div><!-- /.row -->
            <?php } elseif ( $banner_view == "contact" ) { 
                //$fdgd = "";
            ?>

                <div class="contact-area contact-background">
                <?php 
                    $contact_form = $banner_main_style['three']['banner_form_style_sec']['contact']; 
                ?>
                <?php if( $contact_form['banner_call'] ): ?>
                    <div class="contact">
                        <h4>
                            <i class="fa fa-volume-control-phone"></i>
                            <?php esc_html_e( 'Call', 'houserent' ); ?>
                        </h4>
                        <p><?php echo esc_html( $contact_form['banner_call'] ); ?></p>
                    </div><!-- /.contact -->
                <?php 
                    endif;
                    if( $contact_form['banner_mail'] ):
                ?>
                    <div class="contact">
                        <h4>
                            <i class="fa fa-envelope"></i>
                            <?php esc_html_e( 'Mail', 'houserent' ); ?>
                        </h4>
                        <p><?php echo esc_html( $contact_form['banner_mail'] ); ?></p>
                    </div><!-- /.contact -->
                <?php 
                    endif;
                    if( $contact_form['banner_social_profile'] ):
                ?>
                    <div class="contact">
                        <h4>
                            <i class="fa fa-user"></i>
                            <?php esc_html_e( 'Social account', 'houserent' ); ?>
                        </h4>
                        <div class="social-icon">
                        <?php foreach ( $contact_form['banner_social_profile'] as $social ) { ?>
                            <a href="<?php echo esc_attr( $social['social_profile_url'] ); ?>"><i class="<?php echo esc_attr( $social['social_icons'] ); ?>"></i></a>
                        <?php } ?>
                        </div><!-- /.Social-icon -->
                    </div><!-- /.contact -->
                <?php endif; ?>
                </div><!-- /.contact-area -->
            <?php } ?>
        </div><!-- /.container-fluid -->
    </div><!-- /.google-map-area -->
<?php } ?>