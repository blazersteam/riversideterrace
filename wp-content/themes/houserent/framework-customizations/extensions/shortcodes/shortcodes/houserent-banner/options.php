<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'banner_main_style' => array(
        'type'         => 'multi-picker',
        'label'        => false,
        'desc'         => false,
        'picker'       => array(
            'banner_style' => array(
                'label'   => esc_html__( 'Banner Style', 'houserent' ),
                'type'    => 'select',
                'value'    => 'one',
                'choices' => array(
                    'one' => esc_html__( 'One', 'houserent' ),
                    'two' => esc_html__( 'Two', 'houserent' ),
                    'three' => esc_html__( 'Three', 'houserent' ),
                ),
                'desc'    => esc_html__( 'Select banner style from here', 'houserent' ),
            )
        ),
        'choices'      => array(
            'one'  => array( 
                'banner_bg_image' => array(
                    'label' => esc_html__( 'Banner Background', 'houserent' ),
                    'type'  => 'upload',
                    'value' =>  '',
                    'desc'  => esc_html__( 'You can upload banner background', 'houserent' ),
                ),
                'banner_logo_image' => array(
                    'label' => esc_html__( 'Banner Logo', 'houserent' ),
                    'type'  => 'upload',
                    'value' =>  '',
                    'desc'  => esc_html__( 'You can upload banner logo', 'houserent' ),
                ),
                'banner_sub_title' => array(
                    'label' => esc_html__( 'Banner Subtitle', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  esc_html__( 'Good Service is our passion', 'houserent' ),
                    'desc'  => esc_html__( 'You can change banner subtitle', 'houserent' ),
                ),
                'banner_title' => array(
                    'label' => esc_html__( 'Banner Title', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  esc_html__( 'Awesome apartment Villa', 'houserent' ),
                    'desc'  => esc_html__( 'You can change banner title', 'houserent' ),
                ),
                'banner_button' => array(
                    'label' => esc_html__( 'Banner Button Title', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  esc_html__( 'Booking', 'houserent' ),
                    'desc'  => esc_html__( 'You can change banner button title', 'houserent' ),
                ),
                'banner_button_url' => array(
                    'label' => esc_html__( 'Banner Button URL', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  '#',
                    'desc'  => esc_html__( 'You can change banner button URL', 'houserent' ),
                ),
                'banner_menu_sec'   => array(
                     'type'  => 'switch',
                     'label' => esc_html__( 'Menu Section', 'houserent' ),
                     'right-choice' => array(
                         'value' => 'show',
                         'label' => esc_html__( 'Show', 'houserent' )
                     ),
                     'left-choice'  => array(
                         'value' => 'hide',
                         'label' => esc_html__( 'Hide', 'houserent' )
                     ),
                     'value'        => 'show',
                     'desc'         => esc_html__( 'You can show or hide menu section from banner bottom',             'houserent' ),
                 ),
            ),
            'two'  => array(
                'banner_background_color' => array(
                    'type'         => 'multi-picker',
                    'label'        => false,
                    'desc'         => false,
                    'picker'       => array(
                        'banner_background' => array(
                            'type'  => 'switch',
                            'label' => esc_html__( 'Background Type', 'houserent' ),
                            'right-choice' => array(
                                'value' => 'color',
                                'label' => esc_html__( 'Color', 'houserent' )
                            ),
                            'left-choice'  => array(
                                'value' => 'gradient',
                                'label' => esc_html__( 'Gradient', 'houserent' )
                            ),
                            'value'     => 'gradient',
                            'desc'      => esc_html__( 'You can choose section background color from here','houserent' ),
                        ),
                    ),
                    'choices'  => array(
                        'color'  => array(
                            'banner_bg_solid_color'   => array(
                                 'type'  => 'color-picker',
                                 'label' => esc_html__( 'Solid Color', 'houserent' ),
                                 'value' => '#21b75f',
                                 'desc'  => esc_html__( 'You can change background color from here', 'houserent' ),
                             ),
                        ),
                        'gradient'  => array(
                            'banner_bg_gradient'   => array(
                                'label' => esc_html__( 'Background Gradient', 'houserent' ),
                                'type'  => 'gradient',
                                'value' => array(
                                        'primary'   => '#21b75f',
                                        'secondary' => '#31386e',
                                ),
                                'desc'  => esc_html__( 'Here you can change overlay gradient color', 'houserent' ),
                             ),
                        ),
                    ),
                    'show_borders' => false,
                ),
                'banner_form_btn_title' => array(
                    'label' => esc_html__( 'Form Button Name', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  esc_html__( 'Check Availability', 'houserent' ),
                    'desc'  => esc_html__( 'You can change form button text', 'houserent' ),
                ),
                'banner_sub_title' => array(
                    'label' => esc_html__( 'Banner Subtitle', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  esc_html__( 'Good Service is our passion', 'houserent' ),
                    'desc'  => esc_html__( 'You can change banner subtitle', 'houserent' ),
                ),
            ),
            'three' => array(
                'banner_map' => array(
                    'label' => esc_html__( 'Map Location', 'houserent' ),
                    'type'  => 'map',
                    'desc'  => esc_html__( 'Search and select a location', 'houserent' ),
                ),
                'banner_section_map_api' => array(
                    'label' => esc_html__( 'Google Map API', 'houserent' ),
                    'type'  => 'text',
                    'desc'  => esc_html__( 'Put your google map API here', 'houserent' ),
                ),                
                'banner_section_map_height' => array(
                    'label' => esc_html__( 'Google Map height', 'houserent' ),
                    'type'  => 'text',
                    'value' =>  '855',
                    'desc'  => esc_html__( 'Put your map height', 'houserent' ),
                ),
                'banner_form_style_sec' => array(
                    'type'         => 'multi-picker',
                    'label'        => false,
                    'desc'         => false,
                    'picker'       => array(
                        'banner_form_style' => array(
                            'label'   => esc_html__( 'Banner form', 'houserent' ),
                            'type'    => 'select',
                            'value'    => 'search',
                            'choices' => array(
                                'search' => esc_html__( 'Advanced Search', 'houserent' ),
                                'contact' => esc_html__( 'Contact Info', 'houserent' ),
                            ),
                            'desc'    => esc_html__( 'Select banner style from here', 'houserent' ),
                        )
                    ),
                    'choices'      => array(
                        'search'  => array( 
                            'form_gradient' => array(
                                'label' => esc_html__( 'Background Gradient', 'houserent' ),
                                'type'  => 'gradient',
                                'value' => array(
                                        'primary'   => '#21b75f',
                                        'secondary' => '#31386e',
                                ),
                                'desc'  => esc_html__( 'Here you can change background gradient color', 'houserent' ),
                            ),
                        ),
                        'contact'  => array(
                            'banner_call' => array(
                                'label' => esc_html__( 'Contact Number', 'houserent' ),
                                'type'  => 'text',
                                'desc'  => esc_html__( 'You can change contact number', 'houserent' ),
                            ),
                            'banner_mail' => array(
                                'label' => esc_html__( 'Contact Email', 'houserent' ),
                                'type'  => 'text',
                                'desc'  => esc_html__( 'You can change contact email', 'houserent' ),
                            ),
                            'banner_social_profile' => array(
                                'label'         => esc_html__( 'Social Profile', 'houserent' ),
                                'type'          => 'addable-popup',
                                'desc'          => esc_html__( 'Social profile', 'houserent' ),
                                'template'      => '{{- social_profile_url }}',                                    
                                'add-button-text' => esc_html__('Add social', 'houserent'),
                                'popup-options' => array(
                                    'social_icons' => array(
                                        'type'  => 'icon',
                                        'preview_size' => 'medium',
                                        'modal_size' => 'medium',
                                        'label' => esc_html__( 'Select Icon', 'houserent' ),
                                        'desc'  => esc_html__( 'Please select your social profile icon from here', 'houserent' ),
                                    ),
                                    'social_profile_url' => array(
                                        'label' => esc_html__( 'Social Link', 'houserent' ),
                                        'type'  => 'text',
                                        'desc'  => esc_html__( 'Please input your social profile URL.', 'houserent' ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    'show_borders' => false,
                ),
            ),
        ),
        'show_borders' => false,
    ), 
);