<?php 
/*
    Template Name: Rental Items
*/
get_header(); 

$houserent_plugin = function_exists( 'houserent_theme_custom_posts' );
if($houserent_plugin){
    $args = array(
        'hide_empty' => true,
    );
    $rental_categories = get_terms( 'rental_cat', $args );
    $cat_name_slug = array();
    foreach ($rental_categories as $key => $value) {
        $cat_name_slug[$value->slug] = $value->name;
    }
} else {
    $cat_name_slug = array();
}

$rental_page_loadmore_text = houserent_theme_get_customizer_field('rental_page_loadmore_text','Load More');

$rental_page_title = houserent_theme_get_customizer_field('rental_page_title_sec','rental_page_title','show');
$rental_breadcrumbs = houserent_theme_get_customizer_field( 'rental_breadcrumbs','show' );
$rental_post_per_page = houserent_theme_get_customizer_field( 'rental_post_per_pages','9' );
$houserent_cat = (isset( $_GET['cat'] ) ) ? esc_html( $_GET['cat'] ) : '';
$houserent_cat = ( $houserent_cat ) ? $houserent_cat : $cat_name_slug ;
?>
    <?php 
        if( $rental_page_title == 'show' ): 
        $rental_page_title_sec_bg = houserent_theme_get_customizer_field('rental_page_title_sec','show','rental_page_title_sec_bg','');
        if( $rental_page_title_sec_bg ){
        $gradiant_bg = "background: ".esc_attr( $rental_page_title_sec_bg['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $rental_page_title_sec_bg['primary'] )." 0%, ".esc_attr( $rental_page_title_sec_bg['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $rental_page_title_sec_bg['primary'] )." 0%, ".esc_attr( $rental_page_title_sec_bg['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $rental_page_title_sec_bg['primary'] )." 0%, ".esc_attr( $rental_page_title_sec_bg['secondary'] )." 100%);";
        }else{
            $gradiant_bg = "";
        }
        $rental_page_subtitle = houserent_theme_get_customizer_field('rental_page_title_sec','show','rental_page_subtitle','');
    ?>
        <!-- ====== Page Header ====== --> 
        <div class="page-header default-template-gradient" style="<?php echo esc_attr( $gradiant_bg ); ?>">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                
                        <h2 class="page-title"><?php the_title(); ?></h2>
                        <?php if( $rental_page_subtitle ): ?>
                            <p class="page-description"><?php echo esc_html( $rental_page_subtitle ); ?></p>
                        <?php endif; ?>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row-->
            </div><!-- /.container-fluid -->           
        </div><!-- /.page-header -->
    <?php endif; ?>

    <!-- ======breadcrumbs-area======= --> 
    <?php if( function_exists( 'fw_ext_get_breadcrumbs' ) && $rental_breadcrumbs == 'show' ): ?>
        <div class="breadcrumbs-area">
           <div class="container">
               <div class="row">
                   <div class="col-md-12">
                       <?php echo fw_ext_get_breadcrumbs( '>' ); ?>
                   </div><!-- /.col-md-12 -->
               </div><!-- /.row -->
           </div><!-- /.container -->
       </div>
    <?php endif; ?>

    <!-- ======Apartments-area======== --> 
    <div class="apartments-area four bg-gray-color">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if( $houserent_plugin ){ ?>
                    <div class="apartment-tab-area">
                        <ul role="tablist" class="nav nav-tabs apartment-menu hidden-xs hidden-sm">
                            <li class="active">
                                <a href="#popular-apartment" role="tab" data-toggle="tab">
                                    <?php esc_html_e('Popular', 'houserent'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="#newest-to-oldest" role="tab" data-toggle="tab">
                                    <?php esc_html_e('Date', 'houserent'); ?>
                                    <span><?php esc_html_e('Newest to oldest', 'houserent'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#oldest-to-newest" role="tab" data-toggle="tab">
                                    <?php esc_html_e('Date', 'houserent'); ?>
                                    <span><?php esc_html_e('Older to newest', 'houserent'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#price-height-to-low" role="tab" data-toggle="tab">
                                    <?php esc_html_e('Price', 'houserent'); ?>
                                    <span><?php esc_html_e('Low to high', 'houserent'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#price-low-to-heigh" role="tab" data-toggle="tab">
                                    <?php esc_html_e('Price', 'houserent'); ?>
                                    <span><?php esc_html_e('high to low', 'houserent'); ?></span>
                                </a>
                            </li>
                            <?php if( $cat_name_slug ): ?>
                                <li class="pull-right">
                                    <a href="#" role="tab" class="dropdown-toggle" data-toggle="dropdown">Category<i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="?cat="><?php esc_html_e( 'all', 'houserent' ); ?></a></li>
                                        <?php 
                                            foreach ($cat_name_slug as $key => $cat ) {
                                        ?>
                                        <li><a href="?cat=<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $cat ); ?></a></li>
                                        <?php } ?>        
                                    </ul>
                                </li><!-- /.pull-right -->
                            <?php endif; ?>
                        </ul>

                        <div class="hidden-md hidden-lg">
                            <select class="apartment-menu-mobile">
                                <option value='0'><?php esc_html_e('Popular', 'houserent'); ?></option>
                                <option value='1'>
                                    <?php esc_html_e('Date', 'houserent'); ?>
                                    <span><?php esc_html_e('Newest to oldest', 'houserent'); ?></span>
                                </option>
                                <option value='2'>
                                    <?php esc_html_e('Date', 'houserent'); ?>
                                    <span><?php esc_html_e('Older to newest', 'houserent'); ?></span>
                                </option>
                                <option value='3'>
                                    <?php esc_html_e('Price', 'houserent'); ?>
                                    <span><?php esc_html_e('Low to high', 'houserent'); ?></span>
                                </option>
                                <option value='4'>
                                    <?php esc_html_e('Price', 'houserent'); ?>
                                    <span><?php esc_html_e('high to low', 'houserent'); ?></span>
                                </option>
                                <?php if( $cat_name_slug ): ?>
                                    <optgroup label="<?php esc_html_e('Category', 'houserent'); ?>">
                                        <option value='<?php the_permalink(); ?>?cat='><?php esc_html_e( 'all', 'houserent' ); ?></option>
                                        <?php 
                                            foreach ($cat_name_slug as $key => $cat ) {
                                        ?>
                                        <option value='<?php the_permalink(); ?>?cat=<?php echo esc_attr( $key ); ?>'><?php echo esc_html( $cat ); ?></option>
                                        <?php } ?>
                                    </optgroup>
                                <?php endif; ?>
                            </select>
                        </div>

                        <div class="tab-content">
                        
                            <div role="tabpanel" id="popular-apartment" class="tab-pane fade in active">
                                <div class="row popular-posts">
                                    <?php 
                                        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                                        $i = 1;
                                        $query_args = array(
                                           'post_type' => 'rental',
                                           'paged'          => $paged,
                                           'posts_per_page' => $rental_post_per_page,
                                           'meta_key' => 'houserent_theme_post_views_count',
                                           'orderby' => 'meta_value_num',
                                           'order' => 'DESC',
                                           'tax_query' => array(
                                           'relation' => 'AND',
                                                array(
                                                   'taxonomy' => 'rental_cat',
                                                   'field' => 'slug',
                                                   'terms' => $houserent_cat,
                                                )
                                           ),
                                        );
                                        $loop = new WP_Query($query_args);

                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            get_template_part('template-parts/content','rental');
                                            if( $i % 3 == 0 ):
                                                ?>
                                                    <div class="clearfix"></div>
                                                <?php 
                                            endif;  $i++;
                                        endwhile; 
                                    ?>
                                </div><!-- /.row -->
                                <?php 
                                if ($loop->max_num_pages > 1) { ?>
                                    <div class="ajax-load-more default-template-gradient text-center">
                                      <?php echo get_next_posts_link( $rental_page_loadmore_text , $loop->max_num_pages ); ?>
                                    </div>
                                <?php } wp_reset_postdata(); ?>
                            </div><!-- /.popular-apartment -->

                            <div role="tabpanel" id="newest-to-oldest" class="tab-pane fade in">
                                <div class="row newest-items-content">

                                    <?php 
                                        $i = 1;
                                        $query_args = array(
                                           'post_type' => 'rental',
                                           'paged'          => $paged,
                                           'posts_per_page' => $rental_post_per_page,
                                           'orderby' => 'date',
                                           'order' => 'DESC',
                                           'tax_query' => array(
                                           'relation' => 'AND',
                                                array(
                                                   'taxonomy' => 'rental_cat',
                                                   'field' => 'slug',
                                                   'terms' => $houserent_cat,
                                                )
                                           ),
                                        );
                                        $loop = new WP_Query($query_args);

                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            get_template_part('template-parts/content','rental');
                                            if( $i % 3 == 0 ):
                                                ?>
                                                    <div class="clearfix"></div>
                                                <?php 
                                            endif;  $i++;
                                        endwhile;
                                    ?>
                                </div><!-- /.row -->
                                <?php 
                                if ($loop->max_num_pages > 1) { ?>
                                    <div class="newest-ajax-load-more default-template-gradient text-center">
                                      <?php echo get_next_posts_link( $rental_page_loadmore_text , $loop->max_num_pages ); ?>
                                    </div>
                                <?php } wp_reset_postdata(); ?>
                            </div><!-- /.popular-apartment -->

                            <div role="tabpanel" id="oldest-to-newest" class="tab-pane fade in">
                                <div class="row oldest-to-newest">

                                    <?php 
                                        $i = 1;
                                        $query_args = array(
                                           'post_type' => 'rental',
                                           'paged'          => $paged,
                                           'posts_per_page' => $rental_post_per_page,
                                           'orderby' => 'date',
                                           'order' => 'ASC',
                                           'tax_query' => array(
                                           'relation' => 'AND',
                                                array(
                                                   'taxonomy' => 'rental_cat',
                                                   'field' => 'slug',
                                                   'terms' => $houserent_cat,
                                                )
                                           ),
                                        );
                                        $loop = new WP_Query($query_args);

                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            get_template_part('template-parts/content','rental');
                                            if( $i % 3 == 0 ):
                                                ?>
                                                    <div class="clearfix"></div>
                                                <?php 
                                            endif;  $i++;
                                        endwhile;
                                    ?>
                                </div><!-- /.row -->
                                <?php 
                                if ($loop->max_num_pages > 1) { ?>
                                    <div class="oldest-ajax-load-more default-template-gradient text-center">
                                      <?php echo get_next_posts_link( $rental_page_loadmore_text , $loop->max_num_pages ); ?>
                                    </div>
                                <?php } wp_reset_postdata(); ?>
                            </div><!-- /.popular-apartment -->
                            
                            <div role="tabpanel" id="price-height-to-low" class="tab-pane fade in">
                                <div class="row price-heigh-to-low">
                                    <?php 
                                        $i = 1;
                                        $query_args = array(
                                           'post_type' => 'rental',
                                           'paged'          => $paged,
                                           'posts_per_page' => $rental_post_per_page,
                                           'meta_key' => 'rental_price_conditions',
                                           'orderby' => 'meta_value_num',
                                           'order' => 'ASC',
                                           'tax_query' => array(
                                           'relation' => 'AND',
                                                array(
                                                   'taxonomy' => 'rental_cat',
                                                   'field' => 'slug',
                                                   'terms' => $houserent_cat,
                                                )
                                           ),
                                        );
                                        $loop = new WP_Query($query_args);

                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            get_template_part('template-parts/content','rental');
                                            if( $i % 3 == 0 ):
                                                ?>
                                                    <div class="clearfix"></div>
                                                <?php 
                                            endif;  $i++;
                                        endwhile;
                                    ?>
                                </div><!-- /.row -->
                                <?php 
                                if ($loop->max_num_pages > 1) { ?>
                                    <div class="price-heigh-ajax-load-more default-template-gradient text-center">
                                      <?php echo get_next_posts_link( $rental_page_loadmore_text , $loop->max_num_pages ); ?>
                                    </div>
                                <?php } wp_reset_postdata(); ?>
                            </div><!-- /.popular-apartment -->

                            <div role="tabpanel" id="price-low-to-heigh" class="tab-pane fade in">
                                <div class="row price-low-to-heigh">
                                    <?php 
                                        $i = 1;
                                        $query_args = array(
                                           'post_type' => 'rental',
                                           'paged'          => $paged,
                                           'posts_per_page' => $rental_post_per_page,
                                           'meta_key' => 'rental_price_conditions',
                                           'orderby' => 'meta_value_num',
                                           'order' => 'DESC',
                                           'tax_query' => array(
                                           'relation' => 'AND',
                                                array(
                                                   'taxonomy' => 'rental_cat',
                                                   'field' => 'slug',
                                                   'terms' => $houserent_cat,
                                                )
                                           ),
                                        );
                                        $loop = new WP_Query($query_args);

                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            get_template_part('template-parts/content','rental');
                                            if( $i % 3 == 0 ):
                                                ?>
                                                    <div class="clearfix"></div>
                                                <?php 
                                            endif;  $i++;
                                        endwhile;
                                    ?>
                                </div><!-- /.row -->
                                <?php 
                                if ($loop->max_num_pages > 1) { ?>
                                    <div class="price-low-ajax-load-more default-template-gradient text-center">
                                      <?php echo get_next_posts_link( $rental_page_loadmore_text , $loop->max_num_pages ); ?>
                                    </div>
                                <?php } wp_reset_postdata(); ?>
                            </div><!-- /.popular-apartment -->
                        </div><!-- /.tab-content -->
                    </div><!-- /.apartment-tab-area -->
                    <?php } else{ ?>
                        <h2 class="text-center">
                            <?php esc_html_e( 'Please Active "HouseRent Theme" Plugin', 'houserent' ); ?>
                        </h2><!-- /.text-center -->
                    <?php } ?>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.Apartments-area-->
    <?php get_footer(); ?>