	

<?php 
$related_post_show_condition = houserent_theme_get_customizer_field( 'rental_single_related_sec','rental_single_related','show' );

if ( $related_post_show_condition == 'show' ) :
    $rental_single_rel_title = houserent_theme_get_customizer_field( 'rental_single_related_sec','show','rental_single_rel_title','show' );

    $args = array(
    	'post__not_in' => array($post->ID), 
    	'posts_per_page'=> 3, 
    	'post_type'=> 'rental' ,
    );

    $my_query = new wp_query( $args );
    if( $my_query->have_posts() ) :
?>
	<!-- ======Apartments-area================================== --> 
	<div class="apartments-related-area">
		<div class="container">
			<?php if( $rental_single_rel_title ): ?>
				<div class="row">
					<div class="col-md-12">
						<div class="heading-content-one">
						    <h2 class="title"><?php echo esc_html( $rental_single_rel_title ); ?></h2>
						</div><!-- /.Apartments-heading-content -->
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			<?php endif; ?>

			<div class="row">
	            <?php
	                while( $my_query->have_posts() ) {
	                	$my_query->the_post();
	            		get_template_part('template-parts/content','rental');
	                } // end while loop
	            ?>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.Apartments-area-->

    <?php 
    endif;  
    wp_reset_postdata();
?> 
<?php endif; ?>

