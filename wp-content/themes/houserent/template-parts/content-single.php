<div class="single-main-content">
	<article <?php post_class(); ?> id="post-<?php the_id();?>">
		<div class="entry-header">
			<h2 class="entry-title"><?php the_title(); ?></h2>
		</div><!-- /.entry-header -->
		<div class="entry-meta">
			<?php 
				$date_single = houserent_theme_get_customizer_field('date_single','hide');
				$category_single = houserent_theme_get_customizer_field('category_single','hide');
				$tags_single = houserent_theme_get_customizer_field('tags_single','hide');
				$view_single = houserent_theme_get_customizer_field('view_single','hide');
				$like_single = houserent_theme_get_customizer_field('like_single','hide');
				$social_single = houserent_theme_get_customizer_field('social_single','hide');
			?>

			<?php if( $date_single == 'show' ): ?>
			    <div class="entry-date">
			        <span><?php esc_html_e( 'Date', 'houserent' ); ?></span> <?php the_time('F j, Y'); ?>
			    </div>
		    <?php endif; ?>
		
			<?php if( has_category() && $category_single == 'show' ): ?>
			    <div class="entry-category">
			    	<span><?php esc_html_e( 'Category', 'houserent' ); ?></span>
			        <?php the_category( ', ' ); ?>
			    </div>
		    <?php endif; ?>	
			
			<?php if( has_tag() && $tags_single == 'show' ): ?>
			    <div class="entry-tag">
			    	<span><?php esc_html_e( 'Tags', 'houserent' ); ?></span>
			        <?php the_tags('',', ',''); ?>
			    </div>
		    <?php endif; ?>			
			
			<?php if( function_exists( 'houserent_theme_get_post_views' ) && $view_single == 'show'): ?>
			    <div class="entry-view">
			    	<span><?php esc_html_e( 'View', 'houserent' ); ?></span>
			    	<i class="fa fa-eye"></i>
			    	<?php echo houserent_theme_get_post_views( get_the_id() ); ?>
			    </div>
			<?php endif; ?>

			<?php if( function_exists( 'houserent_theme_get_simple_likes_button' ) && $like_single == 'show' ): ?>
			    <div class="entry-like">
			    	<span><?php esc_html_e( 'Like', 'houserent' ); ?></span>
			    	<?php echo houserent_theme_get_simple_likes_button( get_the_id() ); ?>
			    </div><!-- /.entry-like -->
			<?php endif; ?>

		</div><!-- /.entry-meta -->

	    <?php if( has_post_thumbnail() ): ?>
			<figure class="post-thumb">
			    <?php echo houserent_theme_get_featured_img(1020, 600, false, 'true'); ?>
			</figure><!-- /.post-thumb -->
	    <?php endif; ?>


	    <div class="entry-content">
	        <?php 
	        	the_content();
	        	houserent_theme_wp_link_pages();
	        ?>
	    </div><!-- /.single-text-content -->
	    <?php if( $social_single == 'show' ):
	    	houserent_theme_social_share_link();
	    endif; ?>
	</article><!-- /.post -->
</div><!-- /.single-main-content -->