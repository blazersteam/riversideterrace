<?php 
    $register = ( isset( $_GET['register'] ) ) ? $_GET['register'] : '' ;
    $reset = ( isset( $_GET['reset'] ) ) ? $_GET['reset'] : '' ;
        if( $register == true ){
?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong><?php esc_html_e( 'Success!', 'houserent' ); ?></strong> <?php esc_html_e( 'Check your email for the password and then return to log in.', 'houserent' ); ?>
    </div>
<?php } elseif ( $reset == true ) { ?>
    <div class="alert alert-success alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong><?php esc_html_e( 'Success!', 'houserent' ); ?></strong> <?php esc_html_e( 'Check your email to reset your password.', 'houserent' ); ?>
    </div>
<?php }?>

<!-- ====== Header Mobile area ====== -->
<header class="mobile-header-area bg-gray-color hidden-md hidden-lg">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 tb">
                <div class="mobile-header-block">
                    <div class="menu-area tb-cell">
                        <!--Mobile Main Menu-->
                        <div class="mobile-menu-main hidden-md hidden-lg">
                            <div class="menucontent overlaybg"></div>
                            <div class="menuexpandermain slideRight">
                                <a id="navtoggole-main" class="animated-arrow slideLeft menuclose">
                                    <span></span>
                                </a>
                                <span id="menu-marker"></span>
                            </div><!--/.menuexpandermain-->
                            <div id="mobile-main-nav" class="main-navigation slideLeft">
                                <div class="menu-wrapper">
                                    <div id="main-mobile-container" class="menu-content clearfix"></div>
                                    <?php
                                        $header_top_right_menu = houserent_theme_get_customizer_field('header_top_right_menu','hide');
                                        if( $header_top_right_menu == 'show' ):
                                    ?>
                                    <div class="left-content">
                                        <ul>
                                            <?php 
                                                $header_special_nav = houserent_theme_get_customizer_field('header_special_nav','');
                                                if( $header_special_nav ):
                                                foreach ( $header_special_nav as $item ) { 
                                            ?>
                                            <li>
                                                <a href="<?php echo esc_url( $item['special_nav_url'] ); ?>">
                                                    <?php 
                                                        if ( $item['special_nav_icons']['type'] == 'icon-font' ) {
                                                    ?>
                                                            <i class="<?php echo esc_attr($item['special_nav_icons']['icon-class']);?>"></i>
                                                    <?php
                                                        } else if ( $item['special_nav_icons']['type'] == 'custom-upload' ) { 
                                                            echo wp_get_attachment_image(
                                                                $item['special_nav_icons']['attachment-id'],
                                                                'small',
                                                                true
                                                            );
                                                        }
                                                    ?>
                                                    <?php echo esc_html( $item['special_nav_label'] ); ?>
                                                </a>
                                            </li>
                                            <?php } endif; ?>
                                            <li>
                                                <?php if( is_user_logged_in() ){ ?>
                                                    <a href="#" class="cd-signin"><i class="fa fa-user"></i><?php global $current_user; echo esc_html($current_user->display_name); ?></a>
                                                <?php } else { ?>
                                                    <a href="#" class="cd-signin"><i class="fa fa-address-book"></i><?php esc_html_e(  'Login / Register', 'houserent' ); ?></a>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    </div><!-- /.left-content -->
                                    <?php endif; ?>

                                    <?php 
                                        $header_social_profile = houserent_theme_get_customizer_field('header_social_profile','');
                                        if( $header_social_profile ):
                                    ?>
                                    <div class="social-media">
                                        <h5><?php esc_html_e( 'Follow Us', 'houserent' ); ?></h5>
                                        <ul>

                                            <?php 
                                                foreach ( $header_social_profile as $item ) { 
                                            ?>
                                            <li>
                                                <a href="<?php echo esc_url( $item['social_profile_url'] ); ?>">
                                                    <?php 
                                                        if ( $item['social_icons']['type'] == 'icon-font' ) {
                                                    ?>
                                                            <i class="<?php echo esc_attr($item['social_icons']['icon-class']);?>"></i>
                                                    <?php
                                                        } else if ( $item['social_icons']['type'] == 'custom-upload' ) { 
                                                            echo wp_get_attachment_image(
                                                                $item['social_icons']['attachment-id'],
                                                                'small',
                                                                true
                                                            );
                                                        }
                                                    ?>
                                                </a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div><!-- /.social-media -->
                                    <?php endif; ?>
                                </div>
                            </div><!--/#mobile-main-nav-->
                        </div><!--/.mobile-menu-main-->
                    </div><!-- /.menu-area -->
                    <div class="logo-area tb-cell">
                        <div class="site-logo">
                            <?php houserent_theme_site_logo(); ?>
                        </div><!-- /.site-logo -->
                    </div><!-- /.logo-area -->
                    <?php
                        $header_top_right_menu = houserent_theme_get_customizer_field('header_top_right_menu','hide');
                        if( $header_top_right_menu == 'show' ):
                    ?>
                    <div class="search-block tb-cell">
                        <a href="#" class="main-search"><i class="fa fa-search"></i></a>
                    </div><!-- /.search-block -->
                    <div class="additional-content tb-cell">
                        <a href="#" class="trigger-overlay"><i class="fa fa-sliders"></i></a>
                    </div><!-- /.additional-content -->
                    <?php endif; ?>
                </div><!-- /.mobile-header-block -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</header><!-- /.mobile-header-area --> 

<header class="header-area bg-gray-color style-two hidden-xs hidden-sm">
	<div class="container">
        <div class="header-top-content">
			<div class="row">
				<div class="col-md-4 col-sm-4">
                    <?php 
                        $header_social_profile = houserent_theme_get_customizer_field('header_social_profile','');
                        if( $header_social_profile ):
                    ?>
					<div class="social-media">
						<h5><?php esc_html_e( 'Follow Us', 'houserent' ); ?></h5>
                        <ul>
                            <?php 
                                foreach ( $header_social_profile as $item ) { 
                            ?>
                            <li>
                                <a href="<?php echo esc_url( $item['social_profile_url'] ); ?>">
                                    <?php 
                                        if ( $item['social_icons']['type'] == 'icon-font' ) {
                                    ?>
                                            <i class="<?php echo esc_attr($item['social_icons']['icon-class']);?>"></i>
                                    <?php
                                        } else if ( $item['social_icons']['type'] == 'custom-upload' ) { 
                                            echo wp_get_attachment_image(
                                                $item['social_icons']['attachment-id'],
                                                'small',
                                                true
                                            );
                                        }
                                    ?>
                                </a>
                            </li>
                            <?php } ?>
						</ul>
					</div><!-- /.social-media -->
                    <?php endif; ?>
				</div><!-- /.col-md-4 -->
                <div class="col-md-8 col-sm-8">
                    <?php
                        $header_top_right_menu = houserent_theme_get_customizer_field('header_top_right_menu','hide');
                        if( $header_top_right_menu == 'show' ):
                    ?>
                    <div class="left-content">
                        <ul>
                            <?php 
                                if( $header_special_nav ):
                                foreach ( $header_special_nav as $item ) { 
                            ?>
                            <li>
                                <a href="<?php echo esc_url( $item['special_nav_url'] ); ?>">
                                    <?php 
                                        if ( $item['special_nav_icons']['type'] == 'icon-font' ) {
                                    ?>
                                            <i class="<?php echo esc_attr($item['special_nav_icons']['icon-class']);?>"></i>
                                    <?php
                                        } else if ( $item['special_nav_icons']['type'] == 'custom-upload' ) { 
                                            echo wp_get_attachment_image(
                                                $item['special_nav_icons']['attachment-id'],
                                                'small',
                                                true
                                            );
                                        }
                                    ?>
                                    <?php echo esc_html( $item['special_nav_label'] ); ?>
                                </a>
                            </li>
                            <?php } endif; ?>
                            <li>
                                <?php if( is_user_logged_in() ){ ?>
                                    <a href="#" class="cd-signin"><i class="fa fa-user"></i><?php global $current_user; echo esc_html( $current_user->display_name); ?></a>
                                <?php } else { ?>
                                    <a href="#" class="cd-signin"><i class="fa fa-address-book"></i><?php esc_html_e(  'Login / Register', 'houserent' ); ?></a>
                                <?php } ?>
                            </li>
                           
                            <li>
                                <a href="#" class="main-search"><i class="fa fa-search"></i></a>
                            </li>
                            <li>
                                <a href="#" class="trigger-overlay"><i class="fa fa-bars"></i></a>
                            </li>
                        </ul>
                    </div><!-- /.left-content -->
                    <?php endif; ?>
                </div><!-- /.col-md-8 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.head-top-area -->
</header>
<?php 
    $header_layout = houserent_theme_get_customizer_field('header_layout','');

    $header_one_color = houserent_theme_get_customizer_field('header_one_color','#fff');
    $text_color = ( $header_one_color ) ? "color: $header_one_color ;" : '' ;

    if( $header_layout ){
        $bg_type = $header_layout['two']['header_bottom_bg_color'];
        if( $bg_type['header_bottom_background'] == 'color' ){
            $gradiant_bg = "background-color: ".esc_attr( $bg_type['color']['header_two_bg_color'] ).";";
        } elseif ( $bg_type['header_bottom_background'] == 'gradient') {
            $header_layout_bg = $bg_type['gradient']['header_two_bg_gradient'];
            $gradiant_bg = "background: ".esc_attr( $header_layout_bg['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $header_layout_bg['primary'] )." 0%, ".esc_attr( $header_layout_bg['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $header_layout_bg['primary'] )." 0%, ".esc_attr( $header_layout_bg['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $header_layout_bg['primary'] )." 0%, ".esc_attr( $header_layout_bg['secondary'] )." 100%);";
        }
    } else {
        $gradiant_bg = '';
    }

    $he_bot = ( isset( $_GET['he_bot'] ) ) ? $_GET['he_bot'] : '' ;
    $header_bottom_display = ( isset( $header_layout['two']['header_bottom_display'] ) ) ? $header_layout['two']['header_bottom_display'] : '' ;
    $head_cont = ( $he_bot == 'off' ) ? 'hide' : $header_bottom_display ;

    if( $head_cont == 'show' ):
?>
<header class="header-bottom-content style-two hidden-xs hidden-sm" style="<?php echo esc_attr( $gradiant_bg.$text_color ); ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="site-logo">
                    <?php houserent_theme_site_logo(); ?>
                </div><!-- /.house-logo -->
			</div><!-- /.col-md-4 -->

			<div class="col-md-8 col-sm-8 static-position">
					<nav id="nav-left" class="site-navigation top-navigation animation-<?php echo esc_attr( houserent_theme_get_customizer_field('header_menu_animation','slide') ); ?>">
                        <div class="menu-wrapper">
                            <div class="menu-content">
                                <?php 
                                    wp_nav_menu ( array(
                                        'menu_class' => 'menu-list',
                                        'container'=> 'ul',
                                        'theme_location' => 'main-menu',
                                        'fallback_cb'       => 'Sh_Custom_Walker::fallback_main_menu',
                                        'walker' => new Sh_Custom_Walker()  
                                    )); 
                                ?>
                            </div>
                        </div> <!-- /.menu-wrapper --> 
                    </nav>
			</div><!-- /.col-md-8 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</header><!-- /.header-bottom-area -->
<?php endif; ?>

<div class="header-overlay-content">
    <!-- overlay-menu-item -->
    <div class="overlay overlay-hugeinc gradient-transparent overlay-menu-item">
        <button type="button" class="overlay-close"><?php esc_html_e( 'Close', 'houserent' ); ?></button>
        <nav>
            <?php 
                wp_nav_menu ( array(
                    'menu_class' => 'overlay-menu',
                    'container'=> 'ul',
                    'theme_location' => 'hamburger-menu',
                    'fallback_cb'       => 'Sh_Custom_Walker::fallback_main_menu',
                    'walker' => new Sh_Custom_Walker()  
                )); 
            ?>
        </nav>
    </div> <!-- /.overlay-menu-item -->

    <!-- header-search-content -->
    <div class="gradient-transparent overlay-search">
        <button type="button" class="overlay-close"><?php esc_html_e( 'Close', 'houserent' ); ?></button>
        <div class="header-search-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 full-width-content">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.header-search-content -->  
    <?php get_template_part( 'template-parts/content', 'login-registation' ); ?>
</div><!-- /.header-overlay-content -->
