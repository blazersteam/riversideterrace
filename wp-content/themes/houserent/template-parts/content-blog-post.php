<div class="col-md-6 col-sm-6 col-xs-6">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php if( has_post_thumbnail() ): ?>
        <figure class="post-thumb">
            <a href="<?php the_permalink(); ?>">
                <?php echo houserent_theme_get_featured_img(360, 232, false, "true"); ?>
            </a>
        </figure><!-- /.post-thumb -->
        <?php endif; ?>
        <div class="post-content">  
            <div class="entry-meta">

                <?php if( !get_the_title() && !has_post_thumbnail() ): ?>
                    <span class="entry-date">
                       <a href="<?php the_permalink(); ?>"><?php the_time('F j, Y'); ?></a>
                    </span>
                <?php else: ?>
                    <span class="entry-date">
                       <?php the_time('F j, Y'); ?>
                    </span>
                <?php endif; ?>

                <span class="devied"></span>
                <span class="entry-category">
                    <?php the_category( ', ' ); ?>
                </span>
            </div><!-- /.entry-header -->

            <?php if( get_the_title() ): ?>
                <div class="entry-header">
                    <h3 class="blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                </div><!-- /.entry-header -->
            <?php endif; ?>

            <div class="entry-footer">
                <div class="entry-footer-meta">
                    <span class="view">
                        <i class="fa fa-eye"></i>
                        <?php echo houserent_theme_get_post_views ( get_the_id() ); ?>
                    </span>
                    <span class="like">
                        <a href="<?php the_permalink(); ?>">
                            <i class="fa fa-heart-o"></i>
                            <?php houserent_theme_total_like(); ?>
                        </a>
                    </span>
                    <span class="comments">
                        <a href="<?php comments_link(); ?>">
                            <i class="fa fa-comments"></i>
                            <?php comments_number( esc_html__( '0', 'houserent' ), esc_html__( '1', 'houserent' ), '%'); ?>
                        </a>
                    </span>
                </div><!-- /.entry-footer-meta -->
            </div><!-- /.entry-footer -->
        </div><!-- /.post-content -->
    </article><!-- /.post -->
</div><!-- /.col-md-6 -->