<?php
/**
 * The template for displaying related posts in the single page
 *
 * @package HouseRent
 */
?>

<?php
    $query_type = houserent_theme_get_customizer_field('single_page_related_post_query','author');
    $related_no = 3;
    if ( $query_type == 'author' ) {
        $args = array('post__not_in' => array($post->ID),'posts_per_page'=> $related_no, 'no_found_rows'=> 1, 'author'=> get_the_author_meta( 'ID' ));
    } elseif ( $query_type == 'tag' ) {
        $tags = wp_get_post_tags($post->ID);
        $tags_ids = array();
        foreach($tags as $individual_tag) $tags_ids[] = $individual_tag->term_id;
        $args = array('post__not_in' => array($post->ID),'posts_per_page'=> $related_no, 'no_found_rows'=> 1, 'tag__in'=> $tags_ids );
    } else {
        $categories = get_the_category($post->ID);
        $category_ids = array();
        foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
        $args = array('post__not_in' => array($post->ID), 'posts_per_page'=> $related_no, 'no_found_rows'=> 1, 'category__in'=> $category_ids );
    }       
    
    $my_query = new wp_query( $args );
?>
<?php
    if( $my_query->have_posts() ) :
?>



    <!-- ======blog-area======= --> 
   <div class="ralated-area bg-gray-color">
       <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ralated-heading text-center">
                        <h2><?php esc_html_e( 'Related Post', 'houserent' ); ?></h2>
                    </div><!-- /.blog-heading -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
           <div class="row">
            <?php
                while( $my_query->have_posts() ) {
                $my_query->the_post();
            ?>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <article class="post">
                        <?php if( has_post_thumbnail() ): ?>
                        <figure class="post-thumb">
                            <a href="<?php the_permalink(); ?>">
                                <?php echo houserent_theme_get_featured_img(360, 232, false, false); ?>
                            </a>
                        </figure><!-- /.post-thumb -->
                        <?php endif; ?>
                        <div class="post-content">  
                            <div class="entry-meta">
                                <span class="entry-date">
                                   <?php the_time('F j, Y'); ?>
                                </span>
                                <span class="devied"></span>
                                <span class="entry-category">
                                    <?php the_category( ', ' ); ?>
                                </span>
                            </div><!-- /.entry-header -->
                            <div class="entry-header">
                                <h3 class="blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            </div><!-- /.entry-header -->
                            <div class="entry-footer">
                                <div class="entry-footer-meta">
                                    <span class="view">
                                        <i class="fa fa-eye"></i>
                                        <?php echo houserent_theme_get_post_views ( get_the_id() ); ?>
                                    </span>
                                    <span class="like">
                                        <a href="<?php the_permalink(); ?>">
                                            <i class="fa fa-heart-o"></i>
                                            <?php houserent_theme_total_like(); ?>
                                        </a>
                                    </span>
                                    <span class="comments">
                                        <a href="<?php comments_link(); ?>">
                                            <i class="fa fa-comments"></i>
                                            <?php comments_number( esc_html__( '0', 'houserent' ), esc_html__( '1', 'houserent' ), '%'); ?>
                                        </a>
                                    </span>
                                </div><!-- /.entry-footer-meta -->
                            </div><!-- /.entry-footer -->
                        </div><!-- /.post-content -->
                    </article><!-- /.post -->
                </div><!-- /.col-md-6 -->
            <?php   
                } // end while loop
            ?>
           </div><!-- /.row -->
       </div><!-- /.container -->
   </div><!-- /.blog-main-content -->
<?php 
    endif;  
    wp_reset_postdata();
?> 