<?php
/**
* Houserent functions and definitions
*
* @package Houserent
*/

/**
 * Define theme's constant
 */
if ( ! defined( 'HOUSERENT_THEME_VERSION' ) ) {
	define( 'HOUSERENT_THEME_VERSION', '1.0' );
}

if ( ! defined( 'HOUSERENT_THEME_TEMPLATE_DIR' ) ) {
	define( 'HOUSERENT_THEME_TEMPLATE_DIR', get_template_directory() );
}

if ( ! defined( 'HOUSERENT_THEME_TEMPLATE_DIR_URL' ) ) {
	define( 'HOUSERENT_THEME_TEMPLATE_DIR_URL', get_template_directory_uri() );
}

/**
 * Detect plugin. For use on Front End only.
 * to check is_plugin_active()
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );


/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
if ( ! function_exists( 'houserent_theme_setup' ) ) :

	function houserent_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Houserent, use a find and replace
		 * to change 'houserent' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'houserent', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'custom-header' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'main-menu' => esc_html__( 'Main Menu', 'houserent' ),
			'hamburger-menu' => esc_html__( 'Top Menu', 'houserent' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
			) 
		);

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'standard',
			) 
		);

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'houserent_custom_background_args', array (
			'default-color' => 'f5f5f5',
			'default-image' => '',
		) ) );


		/** 
		 * Enable WP Responsive embedded content
		 *
		 * @since 1.0
		 */
		add_theme_support( 'responsive-embeds' );

		/** 
		 * Enable selective refresh for widgets.
		 *
		 * @since 1.0
		 */
		add_theme_support( 'customize-selective-refresh-widgets' );

		/** 
		 * Enable WP Gutenberg Block Style
		 *
		 * @since 1.0
		 */
		add_theme_support( 'wp-block-styles' );

		/**
		 * Add Editor Style
		 *
		 * @since 1.0
		 */
		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		/**
		 * Enable support for custom Editor Style.
		 *
		 * @since 1.0
		 */
		add_editor_style( 'editor-style.css' );
		add_editor_style( houserent_theme_fonts_url() );
	}
endif; // houserent_theme_setup
add_action( 'after_setup_theme', 'houserent_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */

if ( ! isset( $content_width ) ) {
	$content_width = 950; /* pixels */
}

//CUSTOM CODE
function my_admin_menu() {
		add_menu_page(
			__( 'Invoice history', 'my-textdomain' ),
			__( 'Invoice history', 'my-textdomain' ),
			'manage_options',
			'invoicehistroy',
			'my_admin_page_contents',
			'dashicons-schedule',
			3
		);
	}

	add_action( 'admin_menu', 'my_admin_menu' );


	function my_admin_page_contents() {
		?>
			<h1>
				<?php echo do_shortcode("[wpinv_history]"); ?>
			</h1>
		<?php
	}
	
//CUSTOM CODE
function create_user_from_registration($cfdata) {
    if (!isset($cfdata->posted_data) && class_exists('WPCF7_Submission')) {
        // Contact Form 7 version 3.9 removed $cfdata->posted_data and now
        // we have to retrieve it from an API
        $submission = WPCF7_Submission::get_instance();
        if ($submission) {
            $formdata = $submission->get_posted_data();
        }
    } elseif (isset($cfdata->posted_data)) {
        // For pre-3.9 versions of Contact Form 7
        $formdata = $cfdata->posted_data;
    } else {
        // We can't retrieve the form data
        return $cfdata;
	}

	if ( $cfdata->title() == 'tenant_application') {
			//print_r($formdata);
			
	}
}
add_action('wpcf7_before_send_mail', 'create_user_from_registration', 1);
add_action( 'wp_footer', 'mycustom_wp_footer' );

function mycustom_wp_footer() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7submit', function( event ) {
   if ( '763' == event.detail.contactFormId ) { 
	   //console.log("event",event);
	   if(event.detail.status!=='validation_failed'){
	//document.getElementById("wpexperts_pdf_generate_file").style.display = "block";
	var link = document.getElementById('wpexperts_pdf_generate_file'); link.click();
	   }else{
		  document.getElementById("wpexperts_pdf_generate_file").style.display = "none"; 
	   }
    } 
    
}, false );
</script>
<?php
}


/**
 * Include Register widget function
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/register-widgets.php';

/**
 * Enqueue scripts and styles function
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/enqueue-scripts.php';

/**
 * Custom template tags for this theme.
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/extras.php';

/**
 * Customizer additions.
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/customizer.php';

/**
 * Load Post Like compatibility file.
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/post-like.php';

/**
 * Load Jetpack compatibility file.
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/jetpack.php';

/**
 * Include the TGM_Plugin_Activation class.
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/libs/tgm-plugin-activation/tgm-admin-config.php';

/**
 * Load custom widget
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/widgets/widgets.php';

/**
 * Wordpress comment seciton override 
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/wp-comment-section-override.php';

/**
 * Query function to get post
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/function-for-post.php';

/**
 * Query function to get post
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/theme-functions.php';

/**
 * Popular Post functions
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/popular-post.php';

/**
 * Include ajax functions
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/ajax-functions.php';


/**
 * Custom Unyson functions
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/unyson-functions.php';

/**
 * Include header, Hooks for template header
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/frontend/header.php';

/**
 * Include header, Hooks for template header
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/frontend/footer.php';

/**
 * Include override functions
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/functions/wordpress-override.php';

/**
 * Include image resizer script
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/image-resizer/aq_resizer.php';