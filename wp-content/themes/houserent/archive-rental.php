
<?php get_header(); ?>
<div class="page-header default-template-gradient">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                
                <h2 class="page-title">
                    <?php esc_html_e( 'Rental Items', 'houserent' ); ?>
                </h2>        
            </div><!-- /.col-md-12 -->
        </div><!-- /.row-->
    </div><!-- /.container-fluid -->           
</div>

<?php if( function_exists( 'fw_ext_get_breadcrumbs' )): ?>
    <div class="breadcrumbs-area">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                   <?php echo fw_ext_get_breadcrumbs( '>' ); ?>
               </div><!-- /.col-md-12 -->
           </div><!-- /.row -->
       </div><!-- /.container -->
   </div>
<?php endif; ?>

<div class="blog-area-rental bg-gray-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="blog-content-left">
                    <div class="tab-content">
                        <div class="row">
                            <?php if ( have_posts() ) : ?>
                            <?php /* Start the Loop */ $i = 1; ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php
                                    /* Include the Post-Format-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                     */
                                    get_template_part( 'template-parts/content', 'rental' );
                                ?>
                                <?php if( $i % 3 == 0 ): ?>
                                    <div class="clearfix"></div>
                                <?php 
                                    endif; 
                                    $i++;
                                ?>
                            <?php endwhile; ?>
                            <?php else : ?>
                                <?php get_template_part( 'template-parts/content', 'none' ); ?>
                            <?php endif; ?>
                        </div> <!-- /.row -->
                    </div> <!-- /.tab-content -->
                    <div class="row">
                        <div class="col-md-12">
                            <?php houserent_theme_posts_pagination_nav(); ?>
                        </div> <!-- /.col-md-12 -->
                    </div>  <!-- /.row -->
                </div> <!-- /.blog-content-left -->
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.blog-main-content -->
<?php get_footer(); ?>