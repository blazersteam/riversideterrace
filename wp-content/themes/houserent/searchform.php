<?php
/**
 * The template for displaying search form.
 *
 * @package HouseRent
 */
    $houserent_plugin = function_exists( 'houserent_theme_custom_posts' );
    $search_advance = 'show';
?>
<form action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search" method="get" class="searchform">
    <div class="input-group adv-search">
        <input type="text" name="s" class="form-controller" placeholder="<?php esc_html_e(  'Search....', 'houserent' ); ?>" />
        <div class="input-group-btn">
            <div class="btn-group" role="group">
                <div class="dropdown dropdown-lg">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="fa fa-caret-down"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <div class="form-group">
                            <label><?php esc_html_e( 'Filter by', 'houserent' ); ?></label>

                            <div class="radio">
                                <label><input type="radio" name="post_type" checked value="post"><?php esc_html_e( 'Blog Post', 'houserent' ); ?></label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="post_type" value="rental"><?php esc_html_e( 'Rental', 'houserent' ); ?></label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="search_query" value="form" />
                </div>
                <button type="submit" class="btn btn-primary">
                    <span class="fa fa-search" aria-hidden="true"></span>
                </button>
            </div>
        </div>
    </div>
</form>