<?php get_header(); ?>

<?php if( is_archive()): ?>
    <div class="page-header default-template-gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">  
                    <?php houserent_theme_archive_title('<h1 class="page-title style-two">','</h1>') ?>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row-->
        </div><!-- /.container-fluid -->           
    </div>
<?php endif; ?>

<?php if( function_exists( 'fw_ext_get_breadcrumbs' )): ?>
    <div class="breadcrumbs-area">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                   <?php echo fw_ext_get_breadcrumbs( '>' ); ?>
               </div><!-- /.col-md-12 -->
           </div><!-- /.row -->
       </div><!-- /.container -->
   </div>
<?php endif; ?>

<?php
    $blog_layout_url = (isset( $_GET['blog-layout'] ) ) ? esc_html( $_GET['blog-layout'] ) : '';

    if( $blog_layout_url ){

    switch ( $blog_layout_url ) {
        case 'sidebar_left':
            $class_cont = "col-md-8 col-md-push-4";
            break;
        
        default:
            $class_cont = "col-md-8";
            break;
    }
} else{

    $blog_layout_page_layout_sec = houserent_theme_get_customizer_field('blog_layout_page_layout_sec','blog_layout_page_layout','sidebar_right');
    switch ( $blog_layout_page_layout_sec ) {
        case 'sidebar_left':
            $class_cont = "col-md-8 col-md-push-4";
            break;
        
        default:
            $class_cont = "col-md-8";
            break;

    }

}

?>

<div class="blog-area">
    <div class="container">
        <div class="row">
            <div class="<?php echo esc_attr( $class_cont ); ?>">
                <div class="blog-content-left">
                    <div class="tab-content">
                        <div class="row">
                            <?php if ( have_posts() ) : ?>
                            <?php /* Start the Loop */ $i = 1; ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php
                                    /* Include the Post-Format-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                     */
                                    get_template_part( 'template-parts/content', 'blog-post' );
                                ?>
                                <?php if( $i % 2 == 0 ): ?>
                                    <div class="clearfix"></div>
                                <?php 
                                    endif; 
                                    $i++;
                                ?>
                            <?php endwhile; ?>
                            <?php else : ?>
                                <?php get_template_part( 'template-parts/content', 'none' ); ?>
                            <?php endif; ?>
                        </div> <!-- /.row -->
                    </div> <!-- /.tab-content -->
                    <div class="row">
                        <div class="col-md-12">
                            <?php houserent_theme_posts_pagination_nav(); ?>
                        </div> <!-- /.col-md-12 -->
                    </div>  <!-- /.row -->
                </div> <!-- /.blog-content-left -->
            </div> <!-- /.col-md-8 -->
            <?php get_sidebar(); ?>
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.blog-main-content -->
<?php get_footer(); ?>