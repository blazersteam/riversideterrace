<?php 
/*
    Template Name: Booking
*/
get_header(); 

if( isset( $_POST['post_id'] ) ){
    $post_id = ( isset( $_POST['post_id'] ) ) ? esc_html( $_POST['post_id'] ) : '' ;
    $full_name = ( isset( $_POST['full_name'] ) ) ? esc_html( $_POST['full_name'] ) : '' ;
    $phone_number = ( isset( $_POST['phone_number'] ) ) ? esc_html( $_POST['phone_number'] ) : '' ;
    $email = ( isset( $_POST['email'] ) ) ? esc_html( $_POST['email'] ) : '' ;
    $family_member = ( isset( $_POST['family_member'] ) ) ? esc_html( $_POST['family_member'] ) : '' ;
    $children = ( isset( $_POST['children'] ) ) ? esc_html( $_POST['children'] ) : '' ;
    $message = ( isset( $_POST['message'] ) ) ? esc_html( $_POST['message'] ) : '' ;
    $checkinDate = ( isset( $_POST['rental_date_from'] ) ) ? esc_html( $_POST['rental_date_from'] ) : '' ;
    $checkOutDate = ( isset( $_POST['rental_date_to'] ) ) ? esc_html( $_POST['rental_date_to'] ) : '' ;
    
    $rental_price_conditions = ( get_post_meta( $post_id,'rental_price_conditions')[0] ) ? get_post_meta( $post_id,'rental_price_conditions')[0] : '' ;
    $rental_bedrooms = ( get_post_meta( $post_id,'rental_bedrooms')[0] ) ? get_post_meta( $post_id,'rental_bedrooms')[0] : '' ;
    $rental_baths = ( get_post_meta( $post_id,'rental_baths')[0] ) ? get_post_meta( $post_id,'rental_baths')[0] : '' ;
    $rental_total_area = ( get_post_meta( $post_id,'rental_total_area')[0] ) ? get_post_meta( $post_id,'rental_total_area')[0] : '' ;
    $rental_car_parking = ( get_post_meta( $post_id,'rental_car_parking')[0] ) ? get_post_meta( $post_id,'rental_car_parking')[0] : '' ;
    $rental_price_conditions = ( get_post_meta( $post_id,'rental_price_conditions')[0] ) ? get_post_meta( $post_id,'rental_price_conditions')[0] : '' ;
?>
<!-- ====== Availability Area======= --> 
<div class="availability-area two">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-content-three">
                    <h2 class="title"><?php esc_html_e( 'Booking Details', 'houserent' ); ?></h2>
                    <h5 class="sub-title">
                        <?php esc_html_e( 'Place your ', 'houserent' ); ?>
                        <span>
                            <?php esc_html_e( 'Booking', 'houserent' ); ?>
                        </span>
                    </h5>
                </div><!-- /.Availability-content -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row --> 
        <div class="row">
            <div class="col-md-12">
                <table>
                    <tr>
                        <th><?php esc_html_e( 'Bedrooms', 'houserent' ); ?></th>
                        <th><?php esc_html_e( 'Bath', 'houserent' ); ?></th>
                        <th><?php esc_html_e( 'Size', 'houserent' ); ?></th>
                        <th><?php esc_html_e( 'Car Parking', 'houserent' ); ?></th>
                        <th><?php esc_html_e( 'Rent/Month', 'houserent' ); ?></th>
                    </tr>
                    <tr>
                        <td data-title="<?php esc_html_e( 'Bedrooms', 'houserent' ); ?>">
                            <?php echo esc_html( $rental_bedrooms ); ?>
                        </td>
                        <td data-title="<?php esc_html_e( 'Bath', 'houserent' ); ?>">
                            <?php echo esc_html( $rental_baths ); ?>
                        </td>
                        <td data-title="<?php esc_html_e( 'Size', 'houserent' ); ?>">
                            <?php echo esc_html( $rental_total_area ); ?>
                            <?php esc_html_e( ' sq. ft', 'houserent' ); ?>
                        </td>
                        <td data-title="<?php esc_html_e( 'Car Parking', 'houserent' ); ?>">
                            <?php echo esc_html( $rental_car_parking ); ?>
                        </td>
                        <td data-title="<?php esc_html_e( 'Rent/Month', 'houserent' ); ?>">
                            <?php echo esc_html( houserent_theme_currency() ).esc_html( $rental_price_conditions ); ?>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <?php if($full_name) { ?>
                        <th><?php esc_html_e( 'Name', 'houserent' ); ?></th>
                        <?php } ?>
                        <?php if($phone_number) { ?>
                        <th><?php esc_html_e( 'Phone Number', 'houserent' ); ?></th>
                        <?php } ?>
                        <?php if($email) { ?>
                        <th><?php esc_html_e( 'Email', 'houserent' ); ?></th>
                        <?php } ?>
                        <?php if($family_member) { ?>
                        <th><?php esc_html_e( 'Member', 'houserent' ); ?></th>
                        <?php } ?>
                        <?php if($children) { ?>
                        <th><?php esc_html_e( 'Children', 'houserent' ); ?></th>
                        <?php } ?>
                        <?php if($checkinDate) { ?>
                        <th><?php esc_html_e( 'Check In', 'houserent' ); ?></th>
                        <?php } ?>
                        <?php if($checkOutDate) { ?>
                        <th><?php esc_html_e( 'Check Out', 'houserent' ); ?></th>
                        <?php } ?>
                    </tr>
                    <tr>
                        <?php if($full_name) { ?>
                        <td data-title="<?php esc_html_e( 'Name', 'houserent' ); ?>">
                            <?php echo esc_html( $full_name ); ?>
                        </td>
                        <?php } ?>
                        <?php if($phone_number) { ?>
                        <td data-title="<?php esc_html_e( 'Phone Number', 'houserent' ); ?>">
                            <?php echo esc_html( $phone_number ); ?>
                        </td>
                        <?php } ?>
                        <?php if($email) { ?>
                        <td data-title="<?php esc_html_e( 'Email', 'houserent' ); ?>">
                            <?php echo esc_html( $email ); ?>
                        </td>
                        <?php } ?>
                        <?php if($family_member) { ?>
                        <td data-title="<?php esc_html_e( 'Member', 'houserent' ); ?>">
                            <?php echo esc_html( $family_member ); ?>
                        </td>
                        <?php } ?>
                        <?php if($children) { ?>
                        <td data-title="<?php esc_html_e( 'Children', 'houserent' ); ?>">
                            <?php echo esc_html( $children ); ?>
                        </td>
                        <?php } ?>
                        <?php if($checkinDate) { ?>
                        <td data-title="<?php esc_html_e( 'Check In', 'houserent' ); ?>">
                            <?php echo esc_html( $checkinDate ); ?>
                        </td>
                        <?php } ?>
                        <?php if($checkOutDate) { ?>
                        <td data-title="<?php esc_html_e( 'Check Out', 'houserent' ); ?>">
                            <?php echo esc_html( $checkOutDate ); ?>
                        </td>
                        <?php } ?>
                    </tr>
                </table>
                <div class="text-center">
                    <form id="booking-confirm-form" class="booking-confirm-form" action="#" method="post">
                        <input type="hidden" name="item_url" value="<?php the_permalink( $post_id ); ?>" />
                        <?php if($full_name) { ?>
                        <input type="hidden" name="customer_name" value="<?php echo esc_html( $full_name ); ?>" />
                        <?php } ?>
                        <?php if($phone_number) { ?>
                        <input type="hidden" name="customer_mobile" value="<?php echo esc_html( $phone_number ); ?>" />
                        <?php } ?>
                        <?php if($email) { ?>
                        <input type="hidden" name="customer_email" value="<?php echo esc_html( $email ); ?>" />
                        <?php } ?>
                        <?php if($family_member) { ?>
                        <input type="hidden" name="customer_member" value="<?php echo esc_html( $family_member ); ?>" />
                        <?php } ?>
                        <?php if($children) { ?>
                        <input type="hidden" name="customer_children" value="<?php echo esc_html( $children ); ?>" />
                        <?php } ?>
                        <?php if($checkinDate) { ?>
                        <input type="hidden" name="customer_check_in" value="<?php echo esc_html( $checkinDate ); ?>" />
                        <?php } ?>
                        <?php if($checkOutDate) { ?>
                        <input type="hidden" name="customer_check_out" value="<?php echo esc_html( $checkOutDate ); ?>" />
                        <?php } ?>
                        <input type="hidden" name="customer_message" value="<?php echo esc_html( $message ); ?>" />

                        <button type="submit" class="button nevy-button button-radius default-template-gradient"><?php esc_html_e( 'Confirm Booking', 'houserent' ); ?></button>

                        <a id="item-cancel-button" href="<?php the_permalink( $post_id ); ?>" class="button nevy-button button-radius default-template-gradient"><?php esc_html_e( 'Cancel Booking', 'houserent' ); ?></a>
                    </form>

                    <div class="booking-request-status">
                        <div class="container">
                            <div class="row">                        
                                <div class="col-md-12 text-center">
                                    <div class="modal modal-success">
                                        <div class="modal-dialog">
                                            <div class="modal-content text-center">
                                                <div class="modal-header">
                                                    <i class="glyphicon glyphicon-check success"></i>
                                                </div>
                                                <h3 class="modal-title"><?php esc_html_e('Congratulation', 'houserent'); ?></h3>
                                                <div class="modal-body"> <?php esc_html_e('You request has been sent. We will contact you as soon as possible.', 'houserent'); ?></div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success"><?php esc_html_e('OK', 'houserent'); ?></button>
                                                </div>
                                            </div> <!-- / .modal-content -->
                                        </div> <!-- / .modal-dialog -->
                                    </div>

                                    <div class="modal modal-error">
                                        <div class="modal-dialog">
                                            <div class="modal-content text-center">
                                                <div class="modal-header">
                                                    <i class="glyphicon glyphicon-check error-icon"></i>
                                                </div>
                                                <h3 class="modal-title"><?php esc_html_e('Ops!', 'houserent'); ?></h3>
                                                <div class="modal-body">
                                                    <?php esc_html_e('Something Wrong', 'houserent'); ?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" ><?php esc_html_e('OK', 'houserent'); ?></button>
                                                </div>
                                            </div> <!-- / .modal-content -->
                                        </div> <!-- / .modal-dialog -->
                                    </div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </div><!-- /.free-estimate-submit-status -->

                </div><!-- /.text-center -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container  -->
</div><!-- /.Availability-area -->
<?php } else { ?>
    <h2 class="text-center"><?php esc_html_e('Select a item to confirm booking', 'houserent'); ?></h2><!-- /.text-center -->
<?php
}
get_footer(); 
?>