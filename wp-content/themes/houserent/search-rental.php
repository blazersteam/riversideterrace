<div class="search-renta-archive-area">
<?php 
    $terms = get_terms( 'rental_cat', array( 'hide_empty' => false, ) );
    $search_query = (isset( $_GET['search_query'] ) ) ? esc_html( $_GET['search_query'] ) : '';
    $aria_name = (isset( $_GET['s'] ) ) ? esc_html( $_GET['s'] ) : '';
    $texonomy_slug = (isset( $_GET['cal_slug'] ) ) ? esc_html( $_GET['cal_slug'] ) : '';
    $max_price = (isset( $_GET['max_price'] ) ) ? esc_html( $_GET['max_price'] ) : '';
    $min_price = (isset( $_GET['min_price'] ) ) ? esc_html( $_GET['min_price'] ) : '';
    $total_room = (isset( $_GET['rental_rooms_total'] ) ) ? esc_html( $_GET['rental_rooms_total'] ) : '';
    $room_available = (isset( $_GET['rental_available_from'] ) ) ? esc_html( $_GET['rental_rooms_total'] ) : '';
    $start_date =  (isset( $_GET['rental_date_from'] ) ) ? esc_html( $_GET['rental_date_from'] ) : '';
    $end_date =  (isset( $_GET['rental_date_to'] ) ) ? esc_html( $_GET['rental_date_to'] ) : '';

    $rental_search_top_search = houserent_theme_get_customizer_field('rental_page_title_sec','rental_search_top_search','hide');

?>
    <?php if( !$search_query ): ?>
        <?php
            if( $rental_search_top_search == 'show' ):
                $rental_form_title = houserent_theme_get_customizer_field('rental_page_title_sec','show','rental_form_title','');
                $rental_form_subtitle = houserent_theme_get_customizer_field('rental_page_title_sec','show','rental_form_subtitle','');
        ?>
        <!-- ======Availability-area================================== --> 
        <div class="availability-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading-content-three">
                            <?php if( $rental_form_title ): ?>
                                <h2 class="title"><?php echo esc_html( $rental_form_title ); ?></h2>
                            <?php 
                                endif;
                                if( $rental_form_subtitle ):
                            ?>
                                <h5 class="sub-title"><?php echo esc_html( $rental_form_subtitle ); ?></h5>
                            <?php endif; ?>
                        </div><!-- /.Availability-content -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row --> 
                <div class="row">
                    <form role="search" method="get" class="advance_search_query" action="<?php echo esc_url(home_url('/')); ?>">
                        <div class="col-md-12">
                            <div class="form-content">
                                <?php if( houserent_get_options('rental_form_living_area') == true ) { ?>
                                <div class="form-group living-area">
                                    <label><?php esc_html_e( 'Living Aria', 'houserent' ); ?></label>
                                    <input type="text" class="tags search-field" value="<?php echo esc_html( $aria_name ); ?>" name="s" placeholder="<?php esc_html_e( 'Where do you want to live?', 'houserent' ); ?>">
                                    <input type="hidden" name="post_type" value="rental" />
                                </div><!-- /.form-group -->
                                <?php } ?>
                                
                                <?php if( houserent_get_options('rental_form_category') == true ) { ?>
                                <?php if( $terms ): ?>
                                    <div class="form-group category-type">
                                        <label><?php esc_html_e( 'Type', 'houserent' ); ?></label>
                                        <select name="cal_slug">
                                            <option value=""><?php esc_html_e(  'any', 'houserent' ); ?></option>
                                            <?php 
                                                foreach ( $terms as $single_cat ) {  
                                                $selected = ( $texonomy_slug == $single_cat->slug ) ? 'selected="selected"' : '' ;
                                            ?>
                                                <option <?php echo esc_attr( $selected ); ?> value="<?php echo esc_attr( $single_cat->slug ); ?>"><?php echo esc_html( $single_cat->name ); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div><!-- /.form-group -->
                                <?php endif; ?>
                                <?php } ?>

                                <?php if( houserent_get_options('rental_form_check_in_date') == true ) { ?>
                                <div class="form-group min-price">
                                    <label><?php esc_html_e( 'Check In', 'houserent' ); ?></label>
                                    <input class="datepicker check-in" type="text" value="<?php echo get_query_var( "rental_date_from", "" ); ?>" name="rental_date_from">
                                </div><!-- /.form-group -->
                                <?php } ?>
                                
                                <?php if( houserent_get_options('rental_form_check_out_date') == true ) { ?>
                                <div class="form-group max-price">
                                    <label><?php esc_html_e( 'Check Out', 'houserent' ); ?></label>
                                    <input class="datepicker check-out" type="text" value="<?php echo get_query_var( "rental_date_to", "" ); ?>" name="rental_date_to">
                                </div><!-- /.form-group -->
                                <?php } ?>

                                <?php if( houserent_get_options('rental_form_check_min_price') == true ) { ?>
                                <div class="form-group min-price">
                                    <label><?php esc_html_e( 'Min Price', 'houserent' ); ?></label>
                                    <input type="text" value="<?php echo esc_attr( $min_price ); ?>" name="min_price" placeholder="<?php esc_html_e( 'Ex - 200', 'houserent' ); ?>">
                                </div><!-- /.form-group -->
                                <?php } ?>

                                <?php if( houserent_get_options('rental_form_check_max_price') == true ) { ?>
                                <div class="form-group max-price">
                                    <label><?php esc_html_e( 'Max Price', 'houserent' ); ?></label>
                                    <input type="text" value="<?php echo esc_attr( $max_price ); ?>" name="max_price" placeholder="<?php esc_html_e( 'Ex - 5000', 'houserent' ); ?>">
                                </div><!-- /.form-group -->
                                <?php } ?>

                                <?php if( houserent_get_options('rental_form_check_total_rooms') == true ) { ?>
                                <div class="form-group number-rooms">
                                    <label><?php esc_html_e( 'Rooms', 'houserent' ); ?></label>
                                    <input type="number" value="<?php echo esc_attr( $total_room ); ?>" name="rental_rooms_total" placeholder="<?php esc_html_e( 'Total rooms', 'houserent' ); ?>">
                                </div><!-- /.form-group -->
                                <?php } ?>
                            </div><!-- /.form-content -->
                        </div><!-- /.col-md-12 -->
                        <div class="col-md-12">
                            <button class="button nevy-button availity-button" type="submit"><?php esc_html_e( 'Check Availability', 'houserent' ); ?></button>
                        </div><!-- /.col-md-12 -->
                    </form>
                </div><!-- /.row -->
            </div><!-- /.container  -->
        </div><!-- /.Availability-area -->
        <?php endif; ?>
    <?php endif; ?>
    
    <div class="search-rental-area bg-gray-color">    
        <div class="container">
                <?php if( $search_query ): ?>
                    <div class="row">
                        <?php
                            if ( have_posts() ){ 
                                $i = 1;
                                while ( have_posts() ) : the_post();
                                    get_template_part( 'template-parts/content', 'rental' );
                                    if( $i % 3 == 0 ):
                                        echo '<div class="clearfix"></div>';
                                    endif; 
                                    $i++;
                                endwhile;
                            } else {
                                get_template_part( 'template-parts/content', 'none-rental' );
                            }
                        ?>
                    </div> <!-- /.row -->
                    <div class="row">
                        <div class="col-md-12">
                            <?php houserent_theme_posts_pagination_nav(); ?>
                        </div> <!-- /.col-md-12 -->
                    </div>  <!-- /.row -->
                <?php else: ?>
                    <div class="row search-posts-rental">
                    <?php 
                        $rental_post_per = houserent_theme_get_customizer_field( 'rental_post_per_pages', 9 );
                        $arra = array();
                        if ( $aria_name ) {
                          $arra[] = array(
                              'key'   => 'rental_location',
                              'value' =>  $aria_name,
                            );
                        }
                        if ( $total_room ) {
                          $arra[] = array(
                              'key'   => 'rental_rooms_total',
                              'value' =>  $total_room,
                                'compare' => '=',
                              'type'    => 'numeric'
                            );
                        }

                        if( $start_date ) {
                            $arra[] = array(
                                'key'   => 'rental_item_booked',
                                'value' => '1',
                                'compare' => 'NOT EXISTS',
                            );
                        } elseif( $end_date ) {
                            $arra[] = array(
                                'key'   => 'rental_item_booked',
                                'value' => '1',
                                'compare' => 'NOT EXISTS',
                            );
                        } else {
                            $arra[] = array(
                                'key'   => 'rental_item_booked',
                                'value' => '1',
                                'compare' => 'NOT EXISTS',
                            );
                        }

                        if ( $max_price && $min_price ) {
                          $arra[] = array(
                                  'key'   => 'rental_price_conditions',
                                  'value'   => array( $min_price, $max_price ),
                                  'compare' => 'BETWEEN',
                                  'type'    => 'numeric'
                                );
                        } elseif ( $max_price ) {
                          $arra[] = array(
                              'key'   => 'rental_price_conditions',
                              'value'   => $max_price,
                              'compare' => '<=',
                              'type'    => 'numeric'
                            );
                        } elseif ( $min_price ) {
                          $arra[] = array(
                              'key'   => 'rental_price_conditions',
                              'value'   => $min_price,
                              'compare' => '>',
                              'type'    => 'numeric'
                            );
                        }

                        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                        if( $texonomy_slug ){
                            $args = array(
                                'post_type' => 'rental',
                                'paged'    => $paged,
                                'posts_per_page' => $rental_post_per,
                                'tax_query' => array(
                                    ( $texonomy_slug ) ? 
                                    array(
                                      'taxonomy'  => 'rental_cat',
                                      'field'   => 'slug',
                                  'terms'   => $texonomy_slug,
                                    )
                                   : array(),
                                ),
                                'meta_query' => $arra,
                            );
                        } else {
                            $args = array(
                                'post_type' => 'rental',
                                'paged'     => $paged,
                                'posts_per_page' => $rental_post_per,
                                'meta_query' => $arra,
                            );
                        }
                    // The Query
                    $the_query = new WP_Query( $args );

                    // The Loop
                    if ( $the_query->have_posts() ) {
                        $i = 1;
                        while ( $the_query->have_posts() ) : $the_query->the_post(); 
                            get_template_part( 'template-parts/content', 'rental' );
                            if( $i % 3 == 0 ): 
                                echo '<div class="clearfix"></div>';
                            endif; 
                            $i++;
                        endwhile; 
                    } else {
                        get_template_part( 'template-parts/content', 'none-rental' );
                    }

                    wp_reset_postdata(); ?>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-md-12">
                            <?php houserent_theme_posts_pagination_nav(); ?>
                        </div> <!-- /.col-md-12 -->
                    </div>  <!-- /.row -->
            <?php endif; ?>
        </div><!-- /.container -->
    </div><!-- /.search-rental-area -->
</div>