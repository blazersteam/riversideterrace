(function ($) {
    "use strict"; // use strict to start
    var ajaxurl = houserent.ajaxurl;

    // Item Verification
    function item_verification(form) {
        var form = $(form);
        form.submit(function (event) {
            event.preventDefault();
            var $self = $(this);
            var data = form.serialize(); 
            $.ajax({
                type: "POST",
                dataType: "html",
                url: houserent.ajaxurl,
                data: data,
                beforeSend: function() {
                    $self.children('.houserent-loader').css({'display': 'inline-block'});
                    $self.children('.houserent-loader').children().addClass('spin');
                },
                success: function (data) {          
                    $self.children('.houserent-loader').css({'display': 'none'});
                    $self.children('.houserent-loader').children().removeClass('spin'); 
                    if(data == 'true') {
                        $('.houserent-important-notice.registration-form-container .about-description-success').slideDown('slow');
                        $('.houserent-important-notice.registration-form-container .houserent-registration-form').slideUp();
                        $('.houserent-important-notice.registration-form-container .about-description-success-before').slideUp();
                    } else {
                        $('.houserent-important-notice.registration-form-container .about-description-faild-msg').slideDown('slow');
                        setTimeout(function() {
                            $('.houserent-important-notice.registration-form-container .about-description-faild-msg').slideUp('slow');
                        }, 5500);
                    }
                },
                error: function () { 
                    console.log("Something miss. Please recheck");
                }
            });
        });  
    }
     
    if ($('#houserent_product_registration').length) {
        item_verification('#houserent_product_registration');
    }

    $(function(){
        var windowLoc = window.location;
        var $parentMenuId = $('#toplevel_page_houserent_welcome');
        var $submenuHref = $parentMenuId.find('.wp-submenu li a');
        $submenuHref.each(function() {
            if($(this).parent().hasClass("current")) {
                $(this).parent().removeClass("current");
            }
            if(windowLoc.search === $(this)[0].search) {
                $(this).parent().addClass("current");
            }
            $(this).on('click', function() {
                if($(this).parent().hasClass("current")) {
                    $(this).parent().removeClass("current");
                } else {
                    $(this).parent().addClass("current");
                }
            });
        });
    });

})(jQuery);