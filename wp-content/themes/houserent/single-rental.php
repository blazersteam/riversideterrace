<?php get_header(); ?>

<?php 
    $page_builder = false;

    if ( function_exists( 'fw_ext_page_builder_is_builder_post' ) ) {
        $page_builder = ( fw_ext_page_builder_is_builder_post( $post->ID ) == true ) ? true : false;
    }
    
    if ( $page_builder == 1 ) {
        if ( have_posts() ) : 
            while ( have_posts() ) : the_post(); 
            echo '<div class="apartment-single-area"><div class="family-apartment-content">';
                the_content();
            echo '</div></div>';
            endwhile; 
        endif;
    } else { ?>
		<?php 
			$rental_single_page_header = houserent_theme_get_customizer_field( 'rental_single_page_header_sec','rental_single_page_header','hide' );
			$rental_single_bc = houserent_theme_get_customizer_field( 'rental_single_bc','hide' );
			$rental_single_title = houserent_theme_get_customizer_field( 'rental_single_title','show' );
			$rental_single_price = houserent_theme_get_customizer_field( 'rental_single_price','show' );
			$rental_single_calltoaction_sec = houserent_theme_get_customizer_field( 'rental_single_calltoaction_sec','rental_single_calltoaction','show' );
			$rental_single_info_sec = houserent_theme_get_customizer_field( 'rental_single_info_sec','rental_single_info','show' );
		?>
			<?php 
			if( $rental_single_page_header == 'show' ): 

				$rental_single_page_header_gradient = houserent_theme_get_customizer_field( 'rental_single_page_header_sec','show','rental_single_page_header_gradient','hide' );
				$rental_single_page_header_title = houserent_theme_get_customizer_field( 'rental_single_page_header_sec','show','rental_single_page_header_title','hide' );
				$rental_single_page_header_subtitle = houserent_theme_get_customizer_field( 'rental_single_page_header_sec','show','rental_single_page_header_subtitle','hide' );

				if( $rental_single_page_header_gradient ){
					$gradiant_bg = "background: ".esc_attr( $rental_single_page_header_gradient['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $rental_single_page_header_gradient['primary'] )." 0%, ".esc_attr( $rental_single_page_header_gradient['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $rental_single_page_header_gradient['primary'] )." 0%, ".esc_attr( $rental_single_page_header_gradient['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $rental_single_page_header_gradient['primary'] )." 0%, ".esc_attr( $rental_single_page_header_gradient['secondary'] )." 100%);";
				}else{
					$gradiant_bg = "";
				}
			?>
				<!-- ====== Page Header ====== --> 
				<div class="page-header default-template-gradient" style="<?php echo esc_attr( $gradiant_bg ); ?>">
				    <div class="container">
				        <div class="row">
				            <div class="col-md-12">   
				            	<?php if( $rental_single_page_header_title ): ?>             
				                	<h2 class="page-title"><?php echo esc_html( $rental_single_page_header_title ); ?></h2>
				               	<?php 
				               		endif;
				               		if( $rental_single_page_header_subtitle ):
				               	?>
				                <p class="page-description"><?php echo esc_html( $rental_single_page_header_subtitle ); ?></p>     
				                <?php endif; ?>   
				            </div><!-- /.col-md-12 -->
				        </div><!-- /.row-->
				    </div><!-- /.container-fluid -->           
				</div><!-- /.page-header -->
			<?php endif; ?>

			<?php if( function_exists( 'fw_ext_get_breadcrumbs' ) && $rental_single_bc == 'show' ): ?>
				<div class="breadcrumbs-area">
			       <div class="container">
			           <div class="row">
			               <div class="col-md-12">
			                   <?php echo fw_ext_get_breadcrumbs( '>' ); ?>
			               </div><!-- /.col-md-12 -->
			           </div><!-- /.row -->
			       </div><!-- /.container -->
			   </div>
			<?php endif; ?>

			<div class="apartment-single-area">
			  	<div class="container">
				  	<div class="row">
						<div class="col-md-8">
						  	<?php 
						  	    if ( have_posts() ) { 
						  	    while ( have_posts() ) : the_post();
							  	    $rental_price_conditions = get_post_meta( get_the_ID(),'rental_price_conditions')[0];
					                $rental_gallery_item_images = houserent_theme_uny_post_meta('rental_gallery_item_images','');
							  	    $rental_booking_status = get_post_meta( get_the_ID(),'rental_item_booked', true);
						  	?>
							<?php if( $rental_gallery_item_images || has_post_thumbnail()): ?>
								<div class="corousel-gallery-content">
									<?php if( isset($rental_booking_status) && $rental_booking_status == '1' ) { 

										$start_date = get_post_meta( get_the_ID(),'rental_start_date', true);
										$end_date = get_post_meta( get_the_ID(),'rental_end_date', true);
										$date_from_user = date("Y-m-d");
										
										if( houserent_check_in_range($start_date, $end_date, $date_from_user) == true ) {
										?>
										<h4 class="booked-label"><?php echo esc_html('Booked', 'houserent'); ?></h4>
								  	<?php } } ?>
								  	<?php if( $rental_gallery_item_images ): ?>
									  <div class="gallery">
									       <div class="full-view owl-carousel">
											<?php 
												foreach ( $rental_gallery_item_images as $item) { 
												$attachment_id = $item['attachment_id'];
												$slide = houserent_theme_settings_image( $attachment_id ,750 ,500 );
												$slide_full = houserent_theme_settings_image( $attachment_id ,1000 ,648 );
											?>
									     	  	<a class="item" href="<?php echo esc_url( $slide_full ); ?>">
									     		  	<img src="<?php echo esc_url( $slide ); ?>" alt="<?php the_title(); ?>">
									     	  	</a>
									     	<?php } ?>
									       </div>

									      <div class="list-view owl-carousel">
									      <?php 
									      	foreach ( $rental_gallery_item_images as $item) { 
									      	$attachment_id = $item['attachment_id'];
									      	$slide_small = houserent_theme_settings_image( $attachment_id ,117 ,73 );
									      ?>
									        <div class="item">
									        	<img src="<?php echo esc_url( $slide_small ); ?>" alt="<?php the_title(); ?>">
									        </div>
									     <?php } ?>
									      </div>  
									  </div> <!-- /.gallery-two -->
									<?php else: ?>
										<div class="single-rental-thumb">
											<?php if ( has_post_thumbnail() ) {
											    echo houserent_theme_get_featured_img(750 ,560, false, 'true');
											} ?>
										</div><!-- /.single-rental-thumb -->
									<?php endif; ?>
								</div> <!-- /.corousel-gallery-content -->
							<?php endif; ?>
								<div class="family-apartment-content mobile-extend">
				                    <div class="tb">
				                        <div class="tb-cell">
				                        	<?php if( $rental_single_title == 'show' ): ?>
				    					   		<h3 class="apartment-title"><?php the_title(); ?></h3>
				                        	<?php endif; ?>
				                        </div><!-- /.tb-cell -->
				                        <div class="tb-cell">
				    					   <?php if( $rental_price_conditions && $rental_single_price == 'show' ): ?>
				    					   	<p class="pull-right rent">
				    					   	<?php
				    					   	echo ( isset( get_post_meta( get_the_ID(),'rental_price_duration')[0] ) ) ? get_post_meta( get_the_ID(),'rental_price_duration')[0].": " : ' '; 
				    					   	echo esc_html( houserent_theme_currency() .''. ucfirst( $rental_price_conditions) ); 
				    					   	?>
				    					   	</p>
				    					   <?php endif; ?>
				                        </div><!-- /.tb-cell -->
				                    </div><!-- /.tb -->
									<div class="clearfix"></div><!-- /.clearfix -->
									<?php
									$rental_short_description = ( isset( get_post_meta( get_the_ID(),'rental_short_description')[0] ) ) ? get_post_meta( get_the_ID(),'rental_short_description')[0] : '' ;
										if( $rental_short_description ):
									?>
										<p class="apartment-description default-gradient-before">
											<?php echo esc_html( $rental_short_description ); ?>
										</p>
									<?php endif; ?>
									<div class="apartment-detials">
										<?php the_content(); ?>
									</div>
							
									<div class="apartment-overview">
										<div class="row">
											<div class="col-md-12">
												<?php 
													$rental_location = get_post_meta( get_the_ID(),'rental_location')[0];
													$rental_deposit = get_post_meta( get_the_ID(),'rental_deposit')[0];
													$rental_bedrooms = get_post_meta( get_the_ID(),'rental_bedrooms')[0];
													$rental_baths = get_post_meta( get_the_ID(),'rental_baths')[0];
													$rental_rooms_total = get_post_meta( get_the_ID(),'rental_rooms_total')[0];
													$rental_total_area = get_post_meta( get_the_ID(),'rental_total_area')[0];
													$rental_floor = get_post_meta( get_the_ID(),'rental_floor')[0];
													$rental_total_floor = get_post_meta( get_the_ID(),'rental_total_floor')[0];
													$rental_available_from = get_post_meta( get_the_ID(),'rental_available_from')[0];
													$rental_car_parking = get_post_meta( get_the_ID(),'rental_car_parking')[0];
													$rental_price_conditions = get_post_meta( get_the_ID(),'rental_price_conditions')[0];
												?>
												<h3><?php esc_html_e( 'Rental Overview', 'houserent' ); ?></h3>
												<div class="overview">
				                                    <ul>
				                                    	<?php if( $rental_location ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Location', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php echo esc_html( $rental_location ); ?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>

				                                    	<?php if( $rental_deposit ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Deposit / Bond', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php 
						                                        		echo esc_html( houserent_theme_currency() ); 
						                                        		echo esc_html( $rental_deposit );
						                                        	?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>

				                                    	<?php if( $rental_bedrooms ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Bed Rooms', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php echo esc_html( $rental_bedrooms ); ?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>

				                                    	<?php if( $rental_baths ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Baths', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php echo esc_html( $rental_baths ); ?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>

				                            			<?php if( $rental_rooms_total ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Total Rooms', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php echo esc_html( $rental_rooms_total ); ?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>

				                                    	<?php if( $rental_total_area ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Total Area (sq. ft)', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php echo esc_html( $rental_total_area ); ?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>

				                                    	<?php if( $rental_floor ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Floor', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php echo esc_html( $rental_floor ); ?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>

				                                    	<?php if( $rental_total_floor ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Total Floors', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php echo esc_html( $rental_total_floor ); ?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>

				                                    	<?php if( $rental_available_from ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Available From', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php echo esc_html( ucfirst( $rental_available_from ) ); ?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>

				                                    	<?php if( $rental_car_parking ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Car Parking Per Space', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php echo esc_html( $rental_car_parking ); ?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>

				                                    	<?php if( $rental_price_conditions ): ?>
					                                        <li>
						                                        <?php esc_html_e( 'Price Conditions', 'houserent' ); ?>
						                                        <span class="pull-right">
						                                        	<?php 
						                                        		echo esc_html( houserent_theme_currency() );
						                                        		echo esc_html( $rental_price_conditions );
						                                        	?>
						                                        </span>
						                                    </li>
						                                <?php endif; ?>
				                                    </ul>

				                                    <?php 
				                                    	
				                                    ?>
				                                </div><!-- /.apartment-overview -->
											</div><!-- /.col-md-12 -->
										</div><!-- /.row -->
									</div><!-- /.overview -->

									<?php 
										$rental_indoor_features_items = houserent_theme_uny_post_meta('rental_indoor_features_items','');
										$rental_outdoor_features_items = houserent_theme_uny_post_meta('rental_outdoor_features_items','');
										if( $rental_indoor_features_items || $rental_outdoor_features_items ): 
									?>
									<div class="indoor-features">
										<div class="row">
											<?php if( $rental_indoor_features_items ): ?>
												<div class="col-md-6">
													<h3 class="features-title"><?php esc_html_e( 'Indoor features:', 'houserent' ); ?></h3>
													<ul class="features-list">
													<?php foreach ( $rental_indoor_features_items as $feature ){ ?>
														<li><?php echo esc_html( $feature['rental_indoor_feature'] );?></li>
													<?php } ?>
													</ul>
												</div><!-- /.col-md-6 -->
											<?php 
												endif;
												if( $rental_outdoor_features_items ): 
											?>
												<div class="col-md-6">
													<h3 class="features-title"><?php esc_html_e( 'Outdoor features:', 'houserent' ); ?></h3>
													<ul class="features-list">
													<?php foreach ( $rental_outdoor_features_items as $feature ){ ?>
														<li><?php echo esc_html( $feature['rental_outdoor_feature'] );?></li>
													<?php } ?>
													</ul>
												</div><!-- /.col-md-6 -->
											<?php endif; ?>
										</div><!-- /.row -->
									</div><!-- /.indoor -->
						<?php endif; ?>
					</div><!-- /.family-apartment-content -->
					<div class="hidden-md hidden-lg text-center extend-btn">
					    <span class="extend-icon">
					        <i class="fa fa-angle-down"></i>
					    </span>
					</div>
					<?php 
				  	    endwhile;// End of the loop.
				  	    } else {
				  	        get_template_part( 'template-parts/content', 'none' );
				  	    }
				  	?>
				</div> <!-- /.col-md-8 -->

				<?php get_sidebar('rental'); ?>

			</div> <!-- /.row -->
		</div> <!-- /.container -->
		</div>

			<?php get_template_part( 'template-parts/rental-related-post' ); ?>
			
			<?php 
				if( $rental_single_calltoaction_sec == 'show' ): 

				$call_to_action_gradient = houserent_theme_get_customizer_field( 'rental_single_calltoaction_sec','show','call_to_action_gradient','' );

				if( $call_to_action_gradient ){
					$gradiant_bg = "background: ".esc_attr( $call_to_action_gradient['primary'] ).";background: -moz-linear-gradient(left, ".esc_attr( $call_to_action_gradient['primary'] )." 0%, ".esc_attr( $call_to_action_gradient['secondary'] )." 100%);background: -webkit-linear-gradient(left, ".esc_attr( $call_to_action_gradient['primary'] )." 0%, ".esc_attr( $call_to_action_gradient['secondary'] )." 100%);background: linear-gradient(to right, ".esc_attr( $call_to_action_gradient['primary'] )." 0%, ".esc_attr( $call_to_action_gradient['secondary'] )." 100%);";
				} else {
			        $gradiant_bg = '';
			    }

				$call_to_action_section_title = houserent_theme_get_customizer_field( 'rental_single_calltoaction_sec','show','call_to_action_section_title','' );
				$call_to_action_section_sub_title = houserent_theme_get_customizer_field( 'rental_single_calltoaction_sec','show','call_to_action_section_sub_title','' );
				$call_to_action_section_mobile = houserent_theme_get_customizer_field( 'rental_single_calltoaction_sec','show','call_to_action_section_mobile','' );
				$call_to_action_section_email = houserent_theme_get_customizer_field( 'rental_single_calltoaction_sec','show','call_to_action_section_email','' );
				$call_to_action_section_contact_button_text = houserent_theme_get_customizer_field( 'rental_single_calltoaction_sec','show','call_to_action_section_contact_button_text','' );
				$call_to_action_section_contact_button_url = houserent_theme_get_customizer_field( 'rental_single_calltoaction_sec','show','call_to_action_section_contact_button_url','' );
			?>
			<!-- ====== Call To Action ======== --> 
			<div class="call-to-action default-template-gradient" style="<?php echo esc_attr( $gradiant_bg ); ?>">
			    <div class="container">
			        <div class="row tb">
			            <div class="col-md-6 col-sm-6 tb-cell">
			                <div class="contact-left-content">
			                	<?php if( $call_to_action_section_title ): ?>
			                    <h3><?php echo esc_html( $call_to_action_section_title ); ?></h3>
			                   	<?php 
			                   		endif;
			                   		if( $call_to_action_section_sub_title ):
			                   	?>
				                    <h4><?php echo esc_html( $call_to_action_section_sub_title ); ?></h4>
				                <?php endif; ?>
			                </div><!-- /.contact-left-content -->
			            </div><!-- /.col-md-6 -->
			            <div class="col-md-6 col-sm-6 tb-cell">
			                <div class="contact-right-content">
			                	<?php if( $call_to_action_section_mobile ): ?>
			                	    <h4><a href="tel:<?php echo esc_attr( $call_to_action_section_mobile ); ?>"><?php echo esc_html( $call_to_action_section_mobile ); ?></a></h4>
			                	<?php 
			                	    endif;
			                	    if( $call_to_action_section_email ):
			                	?>
			                	    <h4><a href="mailto:<?php echo esc_attr( $call_to_action_section_email ); ?>"><span><?php echo esc_html( $call_to_action_section_email ); ?></span></a></h4>
			                	<?php 
			                	    endif;
			                	?>

			                   <?php 
		                            if( $call_to_action_section_contact_button_text || $call_to_action_section_contact_button_url ):
		                        ?>
		                            <a href="<?php echo esc_attr( $call_to_action_section_contact_button_url ); ?>" class="button"><?php echo esc_html( $call_to_action_section_contact_button_text ); ?></a>
		                        <?php endif; ?>
			                </div><!-- /.contact-right-content -->
			            </div><!-- /.col-md-6 -->
			        </div><!-- /.row -->
			    </div><!-- /.container -->
			</div><!-- /.call-to-action -->
			<?php 
				endif;
				if( $rental_single_info_sec == 'show' ):
				$support_info_section_title = houserent_theme_get_customizer_field( 'rental_single_info_sec','show','support_info_section_title','' );
				$support_info_section_sub_title = houserent_theme_get_customizer_field( 'rental_single_info_sec','show','support_info_section_sub_title','' );
				$support_info_map = houserent_theme_get_customizer_field( 'rental_single_info_sec','show','support_info_map','' );
				$support_info_section_map_api = houserent_theme_get_customizer_field( 'rental_single_info_sec','show','support_info_section_map_api','' );
				$support_info_items = houserent_theme_get_customizer_field( 'rental_single_info_sec','show','support_info_items','' );
			?>
			<!-- ====== Contact Area ====== --> 
			<!-- ======contact-area================================== --> 
			<div class="contact-area">
			    <div class="container-large-device">
			        <div class="container-fluid">
			        <?php if( $support_info_section_title || $support_info_section_sub_title ): ?>
			            <div class="row">
			                <div class="col-md-12">
			                    <div class="heading-content-two available">
			                    <?php if( $support_info_section_title ): ?>
			                        <h2 class="title">
			                            <?php 
			                                $allowed_tag = array(
			                                    'br' => array(),
			                                    'em' => array(),
			                                );
			                                echo wp_kses( $support_info_section_title, $allowed_tag ); 
			                            ?>
			                        </h2>
			                    <?php 
			                        endif;
			                        if( $support_info_section_sub_title ):
			                    ?>
			                        <h5 class="sub-title">
			                            <?php 
			                                $allowed_tag = array(
			                                    'br' => array(),
			                                    'em' => array(),
			                                );
			                                echo wp_kses( $support_info_section_sub_title, $allowed_tag ); 
			                            ?>
			                        </h5>
			                    <?php endif; ?>
			                    </div><!-- /.testimonial-heading-content -->
			                </div><!-- /.col-md-12 -->
			            </div><!-- /.row -->
			        <?php 
			            endif;
			        ?>
			            <div class="row">
			                <div class="col-md-7">
			                    <div class="map-left-content">
			                    <?php if( $support_info_map && $support_info_section_map_api ): ?>
			                        <<?php echo 'iframe';?> src="https://www.google.com/maps/embed/v1/view?key=<?php echo esc_attr( $support_info_section_map_api ); ?>&center=<?php echo esc_attr( $support_info_map['coordinates']['lat'] ); ?>,<?php echo esc_attr( $support_info_map['coordinates']['lng'] ); ?>&zoom=15&maptype=roadmap" width="600" height="350"></<?php echo 'iframe';?>>
			                    <?php endif; ?>
			                    </div><!-- /.mapl-left-content -->
			                </div><!-- /.col-md-7 -->
			                <div class="col-md-5">
			                <?php if( $support_info_items ): ?>
			                    <div class="map-right-content">
			                        <div class="row">
			                        <?php 
			                            $i = 1;
			                            foreach ( $support_info_items as $item) {
			                        ?>
			                            <div class="col-md-6 col-sm-6">
			                                <div class="contact">
			                                <?php if( $item['support_info_item_icon'] || $item['support_info_item_title'] ): ?>
			                                    <h4>
			                                    <?php if( $item['support_info_item_icon'] ):  ?>
			                                        <?php 
			                                            if ( $item['support_info_item_icon']['type'] == 'icon-font' ) {
			                                        ?>
			                                            <i class="<?php echo esc_attr($item['support_info_item_icon']['icon-class']);?>"></i>
			                                        <?php
			                                            } else if ( $item['support_info_item_icon']['type'] == 'custom-upload' ) {
			                                                    $image_url = houserent_theme_settings_image( $item['support_info_item_icon']['attachment-id'] ,30 ,30 );
			                                                ?>
			                                                    <img class="icon-image" src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( $item['support_info_item_title'] ); ?>" />
			                                                <?php
			                                            }
			                                        ?>
			                                    <?php 
			                                        endif;
			                                        if( $item['support_info_item_title'] ):
			                                    ?>
			                                        <?php echo esc_html( $item['support_info_item_title'] ); ?>
			                                    <?php endif; ?>
			                                    </h4>
			                                <?php 
			                                    endif;
			                                    if( $item['support_info_item_description'] ):
			                                ?>
			                                    <p>
			                                    <?php 
			                                        $allowed_tag = array(
			                                            'a' => array(
			                                                'href' => array(),
			                                                'title' => array()
			                                            ),
			                                            'i' => array(
			                                                'class' => array(),
			                                            ),
			                                            'br' => array(),
			                                            'em' => array(),
			                                            'strong' => array(),
			                                            'h2' => array(),
			                                            'h3' => array(),
			                                            'h4' => array(),
			                                            'ul' => array(),
			                                            'ol' => array(),
			                                            'li' => array(),
			                                        );
			                                        echo wp_kses( $item['support_info_item_description'], $allowed_tag ); 
			                                    ?>
			                                    </p>
			                                <?php endif; ?>
			                                </div><!-- /.contact -->
			                            </div><!-- /.col-md-6 -->
			                        <?php
			                            if ( $i % 2 == 0 ) {
			                                ?><div class="clearfix"></div><!-- /.clearfix --><?php 
			                            }
			                            $i++; 
			                            } 
			                        ?>
			                           
			                    </div><!-- /.map-right-content -->
			                <?php endif; ?>
			                </div><!-- /.col-md-5 -->
			            </div><!-- /.row -->
			        </div><!-- /.container-fluid -->
			    </div>
			</div><!-- /.contact-area -->
			<?php endif; ?>
    <?php }
?>

<?php get_footer(); ?>