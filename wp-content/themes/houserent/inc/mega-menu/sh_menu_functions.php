<?php

/*------------------------------------------*/
/*	Mega menus
/*------------------------------------------*/
//wp_enqueue_scripts
add_action( 'wp_enqueue_scripts', 'sh_mega_menu_scripts_styles');
function sh_mega_menu_scripts_styles() {
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'sh-menu-js', get_template_directory_uri() . '/inc/mega-menu/js/menu.js', array(), '1.0', true );
	wp_localize_script( 'sh-menu-js', 'shAjaxL', array(
		'url' => admin_url( 'admin-ajax.php' ),
		'nonce' => wp_create_nonce( 'ajax-nonce' ),
		)
	);

	// Our stylesheets
	wp_enqueue_style( 'sh-menu-main', get_template_directory_uri() . '/inc/mega-menu/css/menu_main.css' );
}

add_action( 'admin_enqueue_scripts', 'sh_admin_scripts' );
function sh_admin_scripts( $hook_suffix ) {
	wp_enqueue_style('sh-mega-menu-admin', get_template_directory_uri().'/inc/mega-menu/css/admin/menu_admin.css');
}

// Mega menu back end modal box Wrap
add_action( 'admin_head', 'sh_admin_modal_box' );
function sh_admin_modal_box() { ?>
	<div class="sh_modal_box">
		<div class="sh_modal_header"><h1><?php esc_html_e('Select Icon', 'houserent'); ?></h1><a class="media-modal-close" id="sh_modal_close" href="javascript:void(0)" ><span class="media-modal-icon"></span></a></div>
		<div class="sh_modal_content"><span class="sh_modal_loading"></span></div>
		<div class="sh_modal_footer"><a class="sh_modal_save button-primary" href="#"><?php esc_html_e('Save', 'houserent'); ?></a></div>
	</div>
	<div class="sh_media_box_overlay"></div>
<?php }

// ajax Action
add_action( 'wp_ajax_sh_loadIcon', 'sh_icon_container' );

// clear all transient if ?
function sh_clear_transients () {
	global $wpdb;
	$sql = 'DELETE FROM ' . $wpdb->options . ' WHERE option_name LIKE "_transient_%"';
	$wpdb->query($sql);	
}
add_action( 'wp_update_nav_menu', 'sh_clear_transients');

