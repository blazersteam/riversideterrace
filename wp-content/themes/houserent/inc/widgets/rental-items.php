<?php
class Widget_Apartment extends WP_Widget {
    //global $post;
    function __construct() {
        $params = array (
            'description' => esc_html__('Houserent : Rental Item', 'houserent'),
            'name' => esc_html__('Houserent : Rental Item', 'houserent')
        );
        parent::__construct('Widget_Apartment', esc_html__('Houserent : Rental Item', 'houserent'),$params);
    }

    /** @see WP_Widget::form */
    public function form( $instance) {
        extract($instance);
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('title')); ?>"><?php esc_html_e('Title:','houserent'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo esc_attr( $this->get_field_id('title')); ?>"
                name="<?php echo esc_attr( $this->get_field_name('title')); ?>"
                value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
        </p> 
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('product_item')); ?>"><?php esc_html_e('Select Multiple Item:','houserent'); ?></label>
            <?php
                // Check values
               if( $instance) {
                   $select =  $instance['product_item'] ; // Added 
               } else {
                    $select = '';
               }
            ?>
               <select 
                    multiple="multiple" 
                    name="<?php echo esc_attr( $this->get_field_name('product_item')); ?>[]" 
                    id="<?php echo esc_attr( $this->get_field_id('product_item')); ?>" 
                    class="widefat" 
                    size="15" 
                    style="margin-bottom:15px;"
                >
                    <?php
                        $houserent_plugin = function_exists( 'houserent_theme_custom_posts' );
                        $product_args = array( 
                            'post_type'=> ($houserent_plugin) ? 'rental' : 'post',
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'posts_per_page' => -1,
                            'post_status' => 'publish',
                        );

                    // The Query
                    query_posts( $product_args );

                    // The Loop
                    while ( have_posts() ) : the_post();
                       $title = get_the_title();
                       $post_id = get_the_id();
                       $active_selected = (in_array( $post_id, $select ) ) ? ' selected="selected"' : '';
                    ?>
                        <option 
                            value="<?php echo get_the_ID();?>" 
                            <?php if ( $active_selected ) { echo esc_attr( $active_selected ); } ?>
                            style="margin-bottom:3px;" 
                        ><?php echo esc_html( $title ); ?></option>
                    <?php
                        endwhile;
                        wp_reset_postdata();
                    ?>
               </select>

        </p>
        <?php       
    } // end form function

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        foreach ($new_instance as $key => $value) {
            if ( $key == "product_item" ) {
                $instance[$key] = $new_instance[$key];
            } else {                
                $instance[$key] = strip_tags( $new_instance[$key] );
            }
        } // end for each
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);
        $title = apply_filters('widget_title',$title);
        $product_item = apply_filters('widget_product_item',$product_item);
        
       
        echo wp_kses_post( $before_widget );
            if ( !empty( $title ) ) {
                echo wp_kses_post( $before_title)  . esc_html( $title ) . wp_kses_post( $after_title );
            } 
            ?>

            <div class="widget_apartment">
                <?php 
                    $query_args = array(
                       'post_type' => 'rental',
                       'posts_per_page' => -1,
                       'post__in' => $product_item, 
                       'order' => 'DESC',
                    );
                    $loop = new WP_Query($query_args);

                    while ( $loop->have_posts() ) : $loop->the_post();
                        ?>
                            <?php
                                $rental_price_conditions = get_post_meta( get_the_ID(),'rental_price_conditions')[0];
                                $rental_bedrooms = get_post_meta( get_the_ID(),'rental_bedrooms')[0];
                                $rental_baths = get_post_meta( get_the_ID(),'rental_baths')[0];
                                $rental_location = get_post_meta( get_the_ID(),'rental_location')[0];
                                $rental_gallery_item_images = houserent_theme_uny_post_meta('rental_gallery_item_images','');
                                $views_count = get_post_meta( get_the_ID(),'houserent_theme_post_views_count')[0];
                            ?>
                            <div class="apartments-content">
                                <?php if ( has_post_thumbnail() ): ?>
                                    <div class="image-content">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php  
                                                echo houserent_theme_get_featured_img(360, 270, false, "true");
                                            ?>
                                        </a>
                                    </div><!-- /.image-content -->
                                <?php endif; ?>
                                
                                <div class="text-content">
                                    <div class="top-content">
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <?php if( $rental_location ): ?>
                                        <span>
                                            <i class="fa fa-map-marker"></i> 
                                            <?php echo esc_html( $rental_location ); ?>
                                        </span> 
                                        <?php endif; ?>
                                    </div><!-- /.top-content -->

                                    <div class="bottom-content clearfix">
                                    <?php if( $rental_bedrooms ): ?>
                                        <div class="meta-bed-room">
                                            <i class="fa fa-bed"></i>
                                            <?php 
                                                echo esc_html( $rental_bedrooms );
                                                $room_singular = ( $rental_bedrooms <= 1 ) ? esc_html__( ' Bedroom', 'houserent' ) : esc_html__( ' Bedrooms', 'houserent' ) ;
                                                echo esc_html( $room_singular );
                                            ?>
                                        </div>
                                    <?php 
                                        endif;
                                        if( $rental_baths ):
                                    ?>
                                        <div class="meta-bath-room">
                                            <i class="fa fa-bath"></i>
                                            <?php 
                                                echo esc_html( $rental_baths );
                                                $room_baths = ( $rental_baths <= 1 ) ? esc_html__( ' Bathroom', 'houserent' ) : esc_html__( ' Bathrooms', 'houserent' ) ;
                                                echo esc_html( $room_baths );
                                            ?>
                                        </div>
                                    <?php
                                        endif;
                                    ?>
                                        <span class="clearfix"></span>
                                    <?php
                                        if( $rental_price_conditions ):
                                    ?>
                                        <div class="rent-price pull-left">
                                            <?php 
                                                if( $rental_price_conditions ):
                                                    echo esc_html( houserent_theme_currency() );
                                                    echo esc_html( $rental_price_conditions );
                                                endif; 
                                            ?>
                                        </div>
                                    <?php endif; ?>
                                        <div class="share-meta dropup pull-right">
                                            <?php houserent_theme_rental_share(); ?>
                                        </div>
                                    </div><!-- /.bottom-content -->
                                </div><!-- /.text-content -->
                            </div><!-- /.partments-content -->

                        <?php
                    endwhile;

                ?>
            </div>

            <?php
        echo wp_kses_post( $after_widget );
    } // end widget function
    
} // class Cycle Widget
