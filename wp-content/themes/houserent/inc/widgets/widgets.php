<?php
/**
 * Load and register widgets
 *
 * @package Houserent
 */

$houserent_plugin = function_exists( 'houserent_theme_custom_posts' );
if ($houserent_plugin) {
	require_once HOUSERENT_THEME_TEMPLATE_DIR . '/inc/widgets/rental-category.php';
	require_once HOUSERENT_THEME_TEMPLATE_DIR . '/inc/widgets/rental-items.php';
	require_once HOUSERENT_THEME_TEMPLATE_DIR . '/inc/widgets/rental-search.php';
}


require_once HOUSERENT_THEME_TEMPLATE_DIR . '/inc/widgets/about-me.php';
require_once HOUSERENT_THEME_TEMPLATE_DIR . '/inc/widgets/latest-posts.php';


/**
 * Register widgets
 */

add_action('widgets_init','houserent_theme_register_widgets');
function houserent_theme_register_widgets() {
	$houserent_plugin = function_exists( 'houserent_theme_custom_posts' );
	if ($houserent_plugin) {
		register_widget('Houserent_Rental_Category');
		register_widget('Widget_Apartment');
		register_widget('Rental_Search');
	}
    register_widget('Houserent_About_Me');
    register_widget('Houserent_Latest_Posts');
}