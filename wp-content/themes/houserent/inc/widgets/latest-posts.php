<?php
class Houserent_Latest_Posts extends WP_Widget {

	function __construct() {
		$params = array (
			'description' => esc_html__('Houserent : Latest Posts', 'houserent'),
			'name' => esc_html__('Houserent : Latest Posts', 'houserent')
		);
		parent::__construct('Houserent_Latest_Posts', esc_html__('Houserent : Latest Posts', 'houserent'),$params);
	}

	public function form( $instance) {
		extract($instance);
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'houserent'); ?></label>
			<input
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id('title')); ?>"
				name="<?php echo esc_attr( $this->get_field_name('title')); ?>"
				value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('houserent_theme_latest_post_limit')); ?>"><?php esc_html_e('Number of posts to show:', 'houserent'); ?></label>
			<input 
				id="<?php echo esc_attr( $this->get_field_id('houserent_theme_latest_post_limit')); ?>" 
				type="text" 
				name="<?php echo esc_attr( $this->get_field_name('houserent_theme_latest_post_limit')); ?>"
				value="<?php if( isset($houserent_theme_latest_post_limit) ) echo esc_attr($houserent_theme_latest_post_limit); ?>"
				size="3" />
		</p>
		<?php
	} // end form function

	function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['houserent_theme_latest_post_limit'] = intval( $new_instance['houserent_theme_latest_post_limit'] );
     
        return $instance;
    }

	public function widget($args, $instance) {
		extract($args);
		extract($instance);
		$title = apply_filters('widget_title',$title);
		$houserent_theme_latest_post_limit = apply_filters('widget_houserent_theme_latest_post_limit',$houserent_theme_latest_post_limit);
		if ( empty($houserent_theme_latest_post_limit) ) $houserent_theme_latest_post_limit = 5;

		echo wp_kses_post( $before_widget );
			if ( !empty( $title ) ) {
				echo wp_kses_post( $before_title)  . esc_html( $title ) . wp_kses_post( $after_title );
			}
			?>
			
				<?php 
					$houserent_theme_latest_post = new WP_Query( array( 'posts_per_page' => $houserent_theme_latest_post_limit,  'order' => 'DESC', 'ignore_sticky_posts' => true  ) );
				?>
				<div class="widget-content">
						<?php while ( $houserent_theme_latest_post->have_posts() ) : $houserent_theme_latest_post->the_post(); ?>

						<div class="post-content clearfix">
							
							<?php if ( has_post_thumbnail() ): ?>
	                            <div class="image-content">
	                            	<a href="<?php the_permalink(); ?>">	                                <?php
                                       echo houserent_theme_get_featured_img(75,75, false, 'true'); 
                                    ?>
                                    </a>
	                            </div><!-- /.image-content -->
	                        <?php endif; ?>

                            <div class="post-title">
                            	<?php if( get_the_title() ): ?>
	                                <h5><a href="<?php the_permalink(); ?>"><?php echo houserent_theme_custom_post_excerpt( get_the_title(), 70, '&hellip;'); ?></a></h5>
	                            <?php endif; ?>
                                <span><?php the_time( 'j M, Y' ); ?></span>
                            </div><!-- /.post-title -->
                        </div>

		                <?php
							endwhile;
							wp_reset_postdata();
						?>
                </div> <!-- /.widget-feed -->  
			<?php
		echo wp_kses_post( $after_widget );
	}
}