<?php
class Houserent_About_Me extends WP_Widget {

    function __construct() {
        $params = array (
            'description' => esc_html__('Houserent : About Me Info', 'houserent'),
            'name' => esc_html__('Houserent : About Me', 'houserent')
        );
        parent::__construct('Houserent_About_Me', esc_html__('Houserent : About Me', 'houserent'),$params);
    }

    /** @see WP_Widget::form */
    public function form( $instance) {
        extract($instance);
        ?>        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('title')); ?>"><?php esc_html_e('Title:','houserent'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo esc_attr( $this->get_field_id('title')); ?>"
                name="<?php echo esc_attr( $this->get_field_name('title')); ?>"
                value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
        </p> 
       
       <div class="image-uploader-content ">
       <p>
           <label class="ad-img-lebel"><?php esc_html_e('Image:','houserent'); ?></label>
       
        <?php
          $arg = array(       
            'parent_div_class'=> 'custom-image-upload',                    
            'field_name' => $this->get_field_name('image'),
            'field_id' => 'upload_logo',
            'field_class' => 'upload_image_field',
            
            'upload_button_id' => 'upload_logo_button',
            'upload_button_class' => 'upload_logo_button',
            'upload_button_text' => 'Upload',
            
            'remove_button_id' => 'remove_logo',
            'remove_button_class' => 'remove_logo_button',
            'remove_button_text' => 'Remove'            
            );
            if ( empty($image) ) $image = NULL;
           houserent_theme_add_about_me_media_custom($arg,false,$image);
        ?>
        </p>
       </div><!-- /.image-uploader-content -->

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('description')); ?>"><?php esc_html_e('Description:','houserent'); ?></label>
            <textarea 
                class="widefat" 
                rows="6" 
                cols="20" 
                id="<?php echo esc_attr( $this->get_field_id('description')); ?>" 
                name="<?php echo esc_attr( $this->get_field_name('description')); ?>"><?php if( isset($description) ) echo esc_attr($description); ?></textarea>
        </p> 

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('read_more_btn')); ?>"><?php esc_html_e('Button Label:','houserent'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo esc_attr( $this->get_field_id('read_more_btn')); ?>"
                name="<?php echo esc_attr( $this->get_field_name('read_more_btn')); ?>"
                value="<?php if( isset($read_more_btn) ){ echo esc_attr($read_more_btn);} ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('read_more_url')); ?>"><?php esc_html_e('Read More URL:','houserent'); ?></label>
            <input
                class="widefat"
                type="url"
                id="<?php echo esc_attr( $this->get_field_id('read_more_url')); ?>"
                name="<?php echo esc_attr( $this->get_field_name('read_more_url')); ?>"
                value="<?php if( isset($read_more_url) ) echo esc_url($read_more_url); ?>" />
        </p>

        <?php       
    } // end form function

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        foreach ($new_instance as $key => $value) {
            if ( $key == "description" ) {
                $instance[$key] = $new_instance[$key];
            } else {                
                $instance[$key] = strip_tags( $new_instance[$key] );
            }
        } // end for each
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);
        $title = apply_filters('widget_title',$title);
        $image = apply_filters('widget_image_url',$image);
        $description = apply_filters('widget_description',$description);
        $read_more_btn = apply_filters('widget_read_more_btn',$read_more_btn);
        $read_more_url = apply_filters('widget_read_more_url',$read_more_url);
        
       
        echo wp_kses_post( $before_widget );
            if ( !empty( $title ) ) {
                echo wp_kses_post( $before_title)  . esc_html( $title ) . wp_kses_post( $after_title );
            } ?>
            <div class="widget-about-content">
                <?php 
                    if ( !empty( $image ) ) {
                        echo '<img src="'.esc_url( $image ).'" alt="'.esc_attr($title).'">';
                    }
                ?>
                <?php 
                    if ( !empty( $description ) ) {
                        echo "<p>".esc_html($description)."</p>";
                    }
                ?>
                <?php 
                    if ( !empty( $read_more_url ) && !empty( $read_more_btn ) ) {
                        echo "<a href='".esc_url($read_more_url)."' class='button'>".esc_html( $read_more_btn )."</a>";
                    }
                ?>
            </div><!-- /.widget-content --> 
            <?php
        echo wp_kses_post( $after_widget );
    } // end widget function
    
} // class Cycle Widget

function houserent_theme_add_about_me_media_custom( $arg, $use_custom_buttons = false, $value = "" ){
    
    $defaults = array(
        'useid' => false ,
        'hidden' => true,
        
        'parent_div_class'=> 'custom-image-upload',
        
        'field_label' => 'upload_image_field_label',        
        'field_name' => 'upload_image_field',
        'field_id' => 'upload_image_field',
        'field_class' => 'upload_image_field',
        
        'upload_button_id' => 'upload_logo_button',
        'upload_button_class' => 'upload_logo_button',
        'upload_button_text' => 'Upload',
        
        'remove_button_id' => 'remove_logo_button',
        'remove_button_class' => 'remove_logo_button',
        'remove_button_text' => 'Remove',
        
        'preview_div_class' => 'preview',
        'preview_div_class2' => 'preview remove_box',
        'preview_div_id' => 'preview',
        
        'height' => '100',
        'width' => '100'
                    );
        $arguments = wp_parse_args($arg,$defaults);
        
        extract($arguments);
        //wp_enqueue_media();
    ?>                                   
   <?php if( ! $use_custom_buttons ): ?>
   <div class="<?php echo esc_attr( $parent_div_class ); ?>" id="<?php echo esc_attr( $parent_div_class ); ?>">
   
        <input name="<?php echo esc_attr( $field_name ); ?>" id="<?php echo esc_attr( $field_id ); ?>" class="<?php echo esc_attr( $field_class ); ?>" <?php if($hidden): ?>  type="hidden" <?php else: ?> type="text" <?php endif; ?> value="<?php if ( $value != "") { echo stripslashes($value); }  ?>" />
        
        <input type="button" class="button button-primary <?php echo esc_attr( $upload_button_class ); ?>" id="<?php echo esc_attr( $upload_button_id); ?>"  value="<?php echo esc_attr( $upload_button_text); ?>">
        
        <input type="button" class="button button-primary <?php echo esc_attr( $remove_button_class); ?>" id="<?php echo esc_attr( $remove_button_id); ?>" <?php  if ( $value == "") {  ?> disabled="disabled" <?php } ?> value="<?php echo esc_attr( $remove_button_text); ?>">
        
        <div class="<?php echo esc_attr( $preview_div_class); ?>" style="float: none; <?php  if ( $value == "") { ?> display: none; <?php } ?>">
            <img src="<?php  echo stripslashes($value);  ?>" style="margin: 10px;" width="150" height="100" alt="<?php echo esc_attr('Image Author', 'houserent') ?>">
        </div>   
        <div style="clear: both;"></div>
    </div>
   <?php endif; ?>
    <?php
        $usesep = ($useid) ? "#" : ".";
        if($useid):
        
         $field_class = $field_id;
         $upload_button_class = $upload_button_id;
         $remove_button_class = $remove_button_id;
         $preview_div_class = $preview_div_id;
            
        endif;  
    ?>
    <script type="text/javascript">

    jQuery(document).ready(function($){
        $('<?php echo esc_attr( $usesep ); echo esc_attr( $remove_button_class); ?>').on('click', function(e) {
            <?php if(!$useid): ?>
           $(this).parent().find("<?php echo esc_attr( $usesep.$field_class); ?>").val(""); 
           $(this).parent().find("<?php echo esc_attr( $usesep.$preview_div_class); ?> img").attr("src","").fadeOut("fast");
           <?php else: ?>
           $("<?php echo esc_attr( $usesep.$field_class); ?>").val(""); 
           $("<?php echo esc_attr( $usesep.$preview_div_class); ?> img").attr("src","").fadeOut("fast");
           <?php endif; ?>
           $(this).attr("disabled","disabled");
         return false;   
        });
        var _custom_media = true,
          _orig_send_attachment = wp.media.editor.send.attachment;

      $('<?php echo esc_attr( $usesep.$upload_button_class); ?>').on('click', function(e) {
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);
        var id = button.attr('id').replace('_button', '');
        _custom_media = true;
        wp.media.editor.send.attachment = function(props, attachment){
          if ( _custom_media ) {
              
              <?php if(!$useid): ?>
            button.parent().find("<?php echo esc_attr( $usesep.$field_class); ?>").val(attachment.url);
            button.parent().find("<?php echo esc_attr( $usesep.$preview_div_class); ?> img").attr("src",attachment.url).fadeIn("fast");
            button.parent().find("<?php echo esc_attr( $usesep.$remove_button_class); ?>").removeAttr("disabled");
            if($('.preview img').length > 0){ $('.preview').css('display','block'); };
            <?php else: ?>
            $("<?php echo esc_attr( $usesep.$field_class); ?>").val(attachment.url);
            $("<?php echo esc_attr( $usesep.$preview_div_class); ?> img").attr("src",attachment.url).fadeIn("fast");        
            $("<?php echo esc_attr( $usesep.$remove_button_class); ?>").removeAttr("disabled");
            <?php endif; ?>
          } else {
            return _orig_send_attachment.apply( this, [props, attachment] );
          };
          $('.preview').removeClass('remove_box');
        }

        wp.media.editor.open(button);
        return false;
      });
    });        
    </script>
   <?php  
}