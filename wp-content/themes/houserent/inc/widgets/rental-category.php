<?php
class Houserent_Rental_Category extends WP_Widget {

	function __construct() {
		$params = array (
			'description' => esc_html__('Houserent : Rental Category', 'houserent'),
			'name' => esc_html__('Houserent : Rental Category', 'houserent')
		);
		parent::__construct('Houserent_Rental_Category', esc_html__('Houserent : Rental Category', 'houserent'),$params);
	}

	public function form( $instance) {
		extract($instance);
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'houserent'); ?></label>
			<input
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id('title')); ?>"
				name="<?php echo esc_attr( $this->get_field_name('title')); ?>"
				value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('houserent_theme_cat_text')); ?>"><?php esc_html_e('Before Category Text:', 'houserent'); ?></label>
				<?php 
					$default_text = ( isset($houserent_theme_cat_text) ) ? esc_attr($houserent_theme_cat_text) : esc_html__('Flat for Rent','houserent') ;
				 ?>
			<input 
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_id('houserent_theme_cat_text')); ?>" 
				type="text" 
				name="<?php echo esc_attr( $this->get_field_name('houserent_theme_cat_text')); ?>"
				value="<?php echo esc_html( $default_text ); ?>"
				 />
		</p>
		<?php 
	} // end form function

	function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['houserent_theme_cat_text'] = strip_tags( $new_instance['houserent_theme_cat_text'] );
     
        return $instance;
    }

	public function widget($args, $instance) {
		extract($args);
		extract($instance);
		$title = apply_filters('widget_title',$title);
		if ( empty($houserent_theme_cat_text) ) $houserent_theme_cat_text = esc_html__('Flat for Rent','houserent');

		echo wp_kses_post( $before_widget );
			if ( !empty( $title ) ) {
				echo wp_kses_post( $before_title)  . esc_html( $title ) . wp_kses_post( $after_title );
			}
			?>
				<?php
					$houserent_plugin = function_exists( 'houserent_theme_custom_posts' );
					if($houserent_plugin){
					    $args = array(
					        'hide_empty' => true,
					    );
					    $rental_categories = get_terms( 'rental_cat', $args );
					} else {
					    $rental_categories = array();
					}
				?>
                    <ul>
                    	<?php 
                    	    foreach ( $rental_categories as $cat ) {
                    	    	$term_link = get_term_link( $cat );
                    	?>
	                    	<li>
	                    		<?php echo esc_html( $houserent_theme_cat_text ); ?> 
		                    	<a href="<?php echo esc_url( $term_link ); ?>">
		                    		<?php echo esc_html( $cat->name ); ?>
		                    	</a>
	                    	</li>
                    	<?php } ?>
                    </ul> 
			<?php
		echo wp_kses_post( $after_widget );
	}
}