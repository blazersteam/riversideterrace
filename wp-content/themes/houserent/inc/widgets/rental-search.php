<?php
class Rental_Search extends WP_Widget {
	function __construct() {
		$params = array (
			'description' => esc_html__('Houserent : Rental Search', 'houserent'),
			'name' => esc_html__('Houserent : Rental Search', 'houserent')
		);
		parent::__construct('Rental_Search', esc_html__('Houserent : Rental Search', 'houserent'),$params);
	}

	public function form( $instance) {
		extract($instance);
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'houserent'); ?></label>
			<input
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id('title')); ?>"
				name="<?php echo esc_attr( $this->get_field_name('title')); ?>"
				value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
		</p>
		<?php 
	} // end form function

	function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        $instance['title'] = strip_tags( $new_instance['title'] );
     
        return $instance;
    }

	public function widget($args, $instance) {
		extract($args);
		extract($instance);
		$title = apply_filters('widget_title',$title);
		echo wp_kses_post( $before_widget );
			if ( !empty( $title ) ) {
				echo wp_kses_post( $before_title)  . esc_html( $title ) . wp_kses_post( $after_title );
			}

			$terms = get_terms( 'rental_cat', array( 'hide_empty' => false, ) );
			?>
				<div class="form-border gradient-border">
					<form role="search" method="get" class="advance_search_query" action="<?php echo esc_url(home_url('/')); ?>">
					    <div class="form-bg seven">
					        <div class="form-content">
					            <div class="form-group living-area">
					               <label><?php esc_html_e( 'Living Aria', 'houserent' ); ?></label>
					               <input type="text" class="tags search-field" value="" name="s" placeholder="<?php esc_html_e( 'Where do you want to live?', 'houserent' ); ?>">
					               <input type="hidden" name="post_type" value="rental" />
					            </div><!-- /.form-group -->
					            <div class="form-group category-type">
					                <label><?php esc_html_e( 'Type', 'houserent' ); ?></label>
					                <select name="cal_slug">
					                    <option value=""><?php esc_html_e(  'any', 'houserent' ); ?></option>
					                    <?php 
					                        foreach ( $terms as $single_cat ) {  
					                    ?>
					                        <option value="<?php echo esc_attr( $single_cat->slug ); ?>"><?php echo esc_html( $single_cat->name ); ?></option>
					                    <?php } ?>
					                </select>
					            </div><!-- /.form-group -->
					            <div class="form-group small min-price">
					                <label><?php esc_html_e( 'Price', 'houserent' ); ?></label>
					                <input type="text" value="" name="min_price" placeholder="<?php esc_html_e( 'Ex - 200', 'houserent' ); ?>">
					            </div><!-- /.form-group -->
					            <div class="form-group small max max-price">
					                <label class="invisible"><?php esc_html_e( 'Price', 'houserent' ); ?></label>
					                <input type="text" value="" name="max_price" placeholder="<?php esc_html_e( 'Ex - 5000', 'houserent' ); ?>">
					            </div><!-- /.form-group -->
					            <div class="form-group total-rooms">
					                <label><?php esc_html_e( 'Rooms', 'houserent' ); ?></label>
					                <input type="number" value="" name="rental_rooms_total" placeholder="<?php esc_html_e( 'Total rooms', 'houserent' ); ?>">
					            </div><!-- /.form-group -->
					            <div class="form-group button-area">
					                <button type="submit" class="button button-radius default-template-gradient" ><?php esc_html_e( 'Check Availability', 'houserent' ); ?></button>
					            </div><!-- /.form-group -->
					        </div><!-- /.form-content -->
					    </div><!-- /.form-bg -->
					</form> <!-- /.advance_search_query -->
				</div>
			<?php
		echo wp_kses_post( $after_widget );
	}
}