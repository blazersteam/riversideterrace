<?php

if( !function_exists('houserent_theme_hex_2_rgba') ):
    function houserent_theme_hex_2_rgba($color, $opacity = false) {
         $default = 'rgb(0,0,0)';
        //Return default if no color provided
        if(empty($color))
              return $default; 
     
        //Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
            $color = substr( $color, 1 );
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }

        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);

        //Check if opacity is set(rgba or rgb)
        if($opacity){
            if(abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
            $output = 'rgb('.implode(",",$rgb).')';
        }

        //Return rgb(a) color string
        return $output;
    }
endif;

function houserent_theme_color_scheme() { 

    switch( houserent_theme_get_customizer_field('color_scheme_primary', 'color_variation_primary','') ) {
        case 'one': 
            $primary_color = "#FCA22A";            
            break;
        case 'two': 
            $primary_color = "#1ABC9C";
            break;
        case 'three': 
            $primary_color = "#D2527F";
            break;
        case 'four': 
            $primary_color = "#CC6054";
            break;
        case 'five': 
            $primary_color = "#667A61";
            break;
        case 'six': 
            $primary_color = "#F06D7D";
            break;
        case 'seven': 
            $primary_color = "#95A5A6";
            break;
        case 'custom': 
            $primary_color = houserent_theme_get_customizer_field('color_scheme_primary','custom','custom_color','');
            break;
        default:$primary_color = "#FCA22A";
            break;
    }

    switch( houserent_theme_get_customizer_field('color_scheme_secondary', 'color_variation_secoendry','') ) {
        case 'one': 
            $secondary_color = "#21b360";            
            break;
        case 'two': 
            $secondary_color = "#1ABC9C";
            break;
        case 'three': 
            $secondary_color = "#D2527F";
            break;
        case 'four': 
            $secondary_color = "#CC6054";
            break;
        case 'five': 
            $secondary_color = "#667A61";
            break;
        case 'six': 
            $secondary_color = "#F06D7D";
            break;
        case 'seven': 
            $secondary_color = "#95A5A6";
            break;
        case 'custom': 
            $secondary_color = houserent_theme_get_customizer_field('color_scheme_secondary','custom','custom_color','');
            break;
        default:$secondary_color = "#21b360";
            break;
    }

    $default_gradient = Array (
        'primary' => '#31386e',
        'secondary' => '#21b75f',
    );
    $color_scheme_gradient = houserent_theme_get_customizer_field('color_scheme_gradient', $default_gradient );
    //gradients
    $gradient_primary = $color_scheme_gradient['primary'];
    $gradient_secondary = $color_scheme_gradient['secondary'];

?>

    /**** Primary Color ****/
    .pagination li a:hover {color: <?php echo esc_attr( $primary_color );?>;}
    label a {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .overlay-search .btn-group .btn:last-child {background-color: <?php echo esc_attr( $primary_color );?> !important;border-color: <?php echo esc_attr( $primary_color );?>;}
    .left-content li a:hover {
        color: <?php echo esc_attr( $primary_color );?> !important;
    }
    .left-content li a:hover i {
        color: <?php echo esc_attr( $primary_color );?> !important;
    }
    .header-bottom-area.default-template-gradient .menu-list > li > a:hover {
        color: <?php echo esc_attr( $primary_color );?> !important;
    }
    .banner-with-menu-area .menu-list > li > a:hover {
        color: <?php echo esc_attr( $primary_color );?> !important;
    }
    .category-menu.default-template-gradient .category-list a:hover i,
    .category-menu.default-template-gradient .category-list a:hover h4 {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .social-media ul li a:hover {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .slider-text-content h2:before {
        background-color: <?php echo esc_attr( $primary_color );?>;
    }
    .slider-text-content a {
        background-color: <?php echo esc_attr( $primary_color );?>;
    }
    .banner-text-content .more-link {
        background-color: <?php echo esc_attr( $primary_color );?>;  
    }
    .aboutus-area .nav.nav-tabs li.active:before {
        background-color: <?php echo esc_attr( $primary_color );?>;
    }
    .booking {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .blog-area .button, 
    .call-to-action .button {
        background-color: <?php echo esc_attr( $primary_color );?>;
    }
    .call-to-action .contact-left-content h4 {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .testimonial-slider .item .star i {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .testimonial-slider .owl-dot.active {
        background-color: <?php echo esc_attr( $primary_color );?>;
    }
    .yellow-color .button {
        background-color: <?php echo esc_attr( $primary_color );?>;
    }
    .yellow-color .widget-title-area .widget-title {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .widget_houserent_rental_category ul li a {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .widget_rental_search .advance_search_query.booking-form .form-group label {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .commingsoon-count .timer-body-block > div .tab-unit {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .error-page-area .default-pd-center .error-text-content .error-title {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .stat i {
       color: <?php echo esc_attr( $primary_color );?>;
    }
    .call-to-action.style-two .right-content .contact a {
        background-color: <?php echo esc_attr( $primary_color );?> !important;
    }
    .footer-area .bottom-content a {
        color: <?php echo esc_attr( $primary_color );?>;
    }
    .widget.widget_search .btn-primary {
        background: <?php echo esc_attr( $primary_color );?> !important;
        border-color: <?php echo esc_attr( $primary_color );?>;
    }
    .widget-about-content .button {
        background: <?php echo esc_attr( $primary_color );?>;
    }

    /**** Secondary Color ****/
    ::-moz-selection {
        background-color: <?php echo esc_attr( $secondary_color );?>;
    }
    ::selection {
        background-color: <?php echo esc_attr( $secondary_color );?>;
    }
    a:hover,
    a:focus {
        color: <?php echo esc_attr( $secondary_color );?>;
    }
    .header-top-content .left-content ul li a {
        color: <?php echo esc_attr( $secondary_color );?>;
    }
    .menu-list li:hover > a, 
    .menu-list > li > a:hover, 
    .menu-list > li > a.active {
        color: <?php echo esc_attr( $secondary_color );?> !important;
    }
    .button:hover {
        background-color: <?php echo esc_attr( $secondary_color );?>;
    }
    .banner-text-content .more-link:hover {
        background-color: <?php echo esc_attr( $secondary_color );?>;
    }
    .call-to-action.style-two .right-content .contact a:hover {
        background-color: <?php echo esc_attr( $secondary_color );?>;
    }
    .apartments-content .text-content .top-content h3 a:hover {
        color: <?php echo esc_attr( $secondary_color );?>;
    }
    .dropdown-menu li a:hover,
    .share-meta li a:hover {
        color: <?php echo esc_attr( $secondary_color );?>;
    }
    .category-list a:hover i,
    .category-list a:hover h4 {
        color: <?php echo esc_attr( $secondary_color );?>;
    }
    .catagory-left-content .button:hover {
        background-color: <?php echo esc_attr( $secondary_color );?>;
    }
    .call-to-action.style-two .right-content .contact a:hover {
        background-color:  <?php echo esc_attr( $secondary_color );?> !important;
    }


    /**** Gradient Color ****/
    .default-template-gradient {
        background: <?php echo esc_attr($gradient_primary);?>;
        background: -moz-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%);
        background: linear-gradient(to right,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%);
    }
    input.default-template-gradient {
        background: <?php echo esc_attr($gradient_primary);?> !important;
        background: -moz-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%) !important;
        background: -webkit-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%) !important;
        background: linear-gradient(to right,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%)!important;
    }
    .default-gradient-before:before {
        background: <?php echo esc_attr($gradient_primary);?>;
        background: -moz-linear-gradient(left, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%);
        background: linear-gradient(to right,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%);
    }
    .default-text-gradient,
    .gallery-area.four .gallery-left-content h2 {
        background: -moz-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
    }
    @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
        .default-text-gradient,
        .gallery-area.four .gallery-left-content h2 {
            color: <?php echo esc_attr( $gradient_secondary );?>;
        }
    }
    .gradient-transparent {
      background: <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.85) );?>;
      background: -moz-linear-gradient(left, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.85) );?> 0%, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_secondary , 0.85) );?> 100%);
      background: -webkit-linear-gradient(left, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.85) );?> 0%, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_secondary , 0.85) );?> 100%);
      background: linear-gradient(to right, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.85) );?> 0%, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_secondary , 0.85) );?> 100%);
    }
    .gradient-transparent-overlay:before {
      background: <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.85) );?>;
      background: -moz-linear-gradient(left, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.85) );?> 0%, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_secondary , 0.85) );?> 100%);
      background: -webkit-linear-gradient(left, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.85) );?> 0%, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_secondary , 0.85) );?> 100%);
      background: linear-gradient(to right, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.85) );?> 0%, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_secondary , 0.85) );?> 100%);
    }
    .overlay-gradient:before {
      background: <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.45) );?>;
      background: -moz-linear-gradient(left, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.45) );?> 0%, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_secondary , 0.45) );?> 100%);
      background: -webkit-linear-gradient(left, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.45) );?> 0%, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_secondary , 0.45) );?> 100%);
      background: linear-gradient(to right, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_primary , 0.45) );?> 0%, <?php echo esc_attr( houserent_theme_hex_2_rgba( $gradient_secondary , 0.45) );?> 100%);
    }
    .gradient-border:after {
      background: <?php echo esc_attr($gradient_primary);?>;
      background: -moz-linear-gradient(bottom, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
      background: -webkit-linear-gradient(bottom, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
      background: linear-gradient(to bottom, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
      border-radius: 10px;
    }
    .gradient-circle > div {
      background: <?php echo esc_attr($gradient_primary);?>;
      background: -moz-linear-gradient(bottom, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
      background: -webkit-linear-gradient(bottom, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
      background: linear-gradient(to bottom, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
    }
    .apartment-menu li.active>a,
    .apartment-menu>li.active>a, 
    .apartment-menu>li.active>a:focus, 
    .apartment-menu>li.active>a:hover,
    .apartment-menu li a:hover {
        background: <?php echo esc_attr($gradient_primary);?> !important;
        background: -moz-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%) !important;
        background: -webkit-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%) !important;
        background: linear-gradient(to right,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%) !important;
    }
    .no-touch .cd-form input[type=submit]:hover, 
    .no-touch .cd-form input[type=submit]:focus,
    .cd-form input[type=submit] {
      background: <?php echo esc_attr($gradient_primary);?>;
      background: -moz-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
      background: -webkit-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%);
      background: linear-gradient(to right,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%);
      outline: none;
    }
    .contactus-area .submit {
        background: <?php echo esc_attr($gradient_primary);?>;
        background: -moz-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(left,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%); 
        background: linear-gradient(to right,  <?php echo esc_attr($gradient_primary);?> 0%,<?php echo esc_attr( $gradient_secondary );?> 100%);
    }
    .gallery-area .gallery-slider > div.owl-nav > div.owl-prev i {
        background: -moz-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
    }

    .gallery-area .gallery-slider > div.owl-nav > div.owl-next i {
        background: -moz-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
    }

    .apartments-area.post h1 {
        background: -moz-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
    }
    .corousel-gallery-area .family-apartment-content .apartment-description:before {
        background: -moz-linear-gradient(left, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(left, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: linear-gradient(to right, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
    }
    .category-menu.five .category-list:hover h4 {
        background: -moz-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
    }
    .aboutus-area.four .title {
        background: -moz-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
    }

    .about-heading-content h2,  
    .about-content-left h2, 
    .social-media.footer i, 
    .availability-area.two h2 {
        background: -moz-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: -webkit-linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
        background: linear-gradient(330deg, <?php echo esc_attr($gradient_primary);?> 0%, <?php echo esc_attr( $gradient_secondary );?> 100%);
    }

<?php
} // end houserent_theme_color_scheme function
houserent_theme_color_scheme(); // here print the function
