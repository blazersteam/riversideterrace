<?php
/**
 * Hooks for template header
 *
 * @package Houserent
 */

/**
 * Get favicon and home screen icons
 *
 * @since  1.0
 */



if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :
	function houserent_theme_header_icons() {
		$favicon = houserent_theme_get_customizer_field('favicon','url', get_template_directory_uri().'/assets/images/favicon.ico' );
		$header_icons =  ( $favicon ) ? '<link rel="shortcut icon" type="image/x-ico" href="' . esc_url( $favicon ) . '" />' : '';

		$icon_iphone = houserent_theme_get_customizer_field('icon_iphone','url','');
		$header_icons .= ( $icon_iphone ) ? '<link rel="apple-touch-icon" sizes="57x57"  href="' . esc_url( $icon_iphone ) . '" />' : '';

		$icon_iphone_retina = houserent_theme_get_customizer_field('icon_iphone_retina','url','');
		$header_icons .= ( $icon_iphone_retina ) ? '<link rel="apple-touch-icon" sizes="114x114" href="' . esc_url( $icon_iphone_retina ). '" />' : '';

		$icon_ipad = houserent_theme_get_customizer_field('icon_ipad','url','');
		$header_icons .= ( $icon_ipad ) ? '<link rel="apple-touch-icon" sizes="72x72" href="' . esc_url( $icon_ipad ) . '" />' : '';

		$icon_ipad_retina = houserent_theme_get_customizer_field('icon_ipad_retina','url','');
		$header_icons .= ( $icon_ipad_retina ) ? '<link rel="apple-touch-icon" sizes="144x144" href="' . esc_url( $icon_ipad_retina ) . '" />' : '';

		$allowed_html_array = array(
	        'link' => array(
	            'rel' => array(),
	            'sizes' => array(),
	            'href' => array(),
	        ),
	    );
	    
	    if ( isset ( $header_icons ) ) echo wp_kses( $header_icons, $allowed_html_array );
	}
	add_action( 'wp_head', 'houserent_theme_header_icons' );
endif;



function houserent_theme_typograpy($selector, $font_property) { ?>
	<?php echo (isset( $selector) ) ? $selector : ''; ?> { 
		<?php 
			$style = $weight = "";
			$family = (isset($font_property['family'])) ? $font_property['family'] : '';

			$font_variant = ( isset( $font_property['variation'] ) ) ? preg_split('#(?<=\d)(?=[a-z])#i', $font_property['variation']) : "";

			if ( isset( $font_variant[0] )) {
				if(count($font_variant) == 1) {
					if(ctype_digit($font_variant[0]) == true) { 
						$weight = $font_variant[0];	
					} else {
						$style = $font_variant[0];
					}
				} else {
					$style = $font_variant[1];
					$weight = $font_variant[0];
				}
			} else {
				$style = (isset($font_property['style'])) ? $font_property['style'] : '';
				$weight = (isset($font_property['weight'])) ? $font_property['weight'] : '';
			}

			$family = (isset($font_property['family'])) ? $font_property['family'] : '';
			if ($family != '') echo "font-family: '$family' !important; ";

			if ($style != " ") {
				if ($style == "regular") {
					echo "font-style: normal;";
				} else {
					echo "font-style: $style;";
				}
			}
			
			if ($weight != "") {
				if ($weight == "regular") {
					echo "font-weight: normal;";
				} else {
					echo "font-weight: $weight;";
				}
			}

			$size = (isset($font_property['size'])) ? $font_property['size'] : '';
			if ($size != '') echo "font-size: $size;";

			$line_height = (isset($font_property['line-height'])) ? $font_property['line-height'] : '';
			if ($line_height != '') echo "line-height: $line_height;";

			$latter_spacing = (isset($font_property['letter-spacing'])) ? $font_property['letter-spacing'] : '';
			if ($latter_spacing != '') echo "letter-spacing: $latter_spacing;";

			$color = (isset($font_property['color'])) ? $font_property['color'] : '';
			if ($color != '') echo "color: $color;";
		?>
	}
	<?php
}

function houserent_theme_header_scripts_css() {	
	// Custom CSS
	ob_start();
	houserent_theme_typograpy('body', houserent_theme_get_customizer_field('global_body_fonts',''));
	houserent_theme_typograpy('blockquote', houserent_theme_get_customizer_field('block_quote_fonts',''));
	houserent_theme_typograpy('h1', houserent_theme_get_customizer_field('hading_one_font', ''));
	houserent_theme_typograpy('h2', houserent_theme_get_customizer_field('hading_two_font', ''));
	houserent_theme_typograpy('h3', houserent_theme_get_customizer_field('hading_three_font', ''));
	houserent_theme_typograpy('h4', houserent_theme_get_customizer_field('hading_four_font', ''));
	houserent_theme_typograpy('h5', houserent_theme_get_customizer_field('hading_five_font', ''));
	houserent_theme_typograpy('h6', houserent_theme_get_customizer_field('hading_six_font', ''));
	$custom_code = ob_get_clean();
	$custom_code .= houserent_theme_get_customizer_field('custom_css', '');
	wp_add_inline_style( 'houserent-theme-style', $custom_code );
	ob_start();
	require get_template_directory() . '/inc/frontend/color-schemer.php';
	$color_scheme = ob_get_clean();
	wp_add_inline_style( 'houserent-theme-style', $color_scheme );
}
add_action( 'wp_enqueue_scripts', 'houserent_theme_header_scripts_css', 300 );
