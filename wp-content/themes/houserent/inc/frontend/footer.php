<?php
/**
 * Hooks for template footer
 *
 * @package Houserent
 */

/**
 * Custom scripts  on footer
 *
 * @since  1.0
 */



function houserent_theme_footer_scripts() {

    // Custom javascript
    ob_start(); ?>
        function houserentPopupWindow(url, title, w, h) {
          var left = (screen.width/2)-(w/2);
          var top = (screen.height/2)-(h/2);
          return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
        } 
    <?php $custom_code = ob_get_clean();
    $custom_code .= houserent_theme_get_customizer_field('custom_js','');
    wp_add_inline_script( 'houserent-theme-js', $custom_code );
}
add_action( 'wp_head', 'houserent_theme_footer_scripts', 200 );












