<?php

 /**
 ** Get custom posts
 **/
if ( ! function_exists( 'houserent_theme_get_custom_posts' ) ) :
    function houserent_theme_get_custom_posts($type, $limit = 20, $order = "DESC")
    {
        wp_reset_postdata();
        $args = array(
            "posts_per_page" => $limit,
            "post_type" => $type,
            "post_status" => "publish",
            "orderby" => "ID",
            "order" => $order,
            'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 )
        );
        $custom_posts = get_posts($args);
        return $custom_posts;
    }
endif;

/**
 ** Custom posts from featured with offset
 **/
if ( ! function_exists( 'houserent_theme_get_custom_posts_from_featured_with_offset' ) ) :
    function houserent_theme_get_custom_posts_from_featured_with_offset($type, $limit = 20, $offset, $order = "DESC")
    {
        wp_reset_postdata();
        $args = array(
            "posts_per_page" => $limit,
            "offset"=> $offset,
            "category_name" => "featured",
            "post_type" => $type,
            "orderby" => "ID",
            "order" => $order,
        );
        $custom_posts = get_posts($args);
        return $custom_posts;
    }
endif;

/**
 ** Get posts from cat with offset
 **/
if ( ! function_exists( 'houserent_theme_get_posts_from_cat_with_offset' ) ) :
    function houserent_theme_get_posts_from_cat_with_offset($type, $limit = 20, $offset, $cat_name, $order = "DESC"){ 
        wp_reset_postdata();
        $args = array(
            "posts_per_page" => $limit,
            "offset"=> $offset,
            "category_name" => $cat_name,
            "post_type" => $type,
            "orderby" => "ID",
            "order" => $order,
        );
        $custom_posts = get_posts($args);
        return $custom_posts;
    }
endif;

/**
 ** Get featured img without link
 **/
if ( ! function_exists( 'houserent_theme_get_featured_img_without_link' ) ) :
    function houserent_theme_get_featured_img_without_link( $cropping_size = "") {
        if ( has_post_thumbnail() ) {
            the_post_thumbnail($cropping_size, array( 'alt' => get_the_title()));
        } else {
            ?>
            <img class="no-thumb" src="<?php echo HOUSERENT_THEME_TEMPLATE_DIR_URL; ?>/images/nopreview.png" alt="<?php the_title(); ?>" />
            <?php
        } //end else                            
    } // end function
endif;

/**
 ** Create a function that has a counter
 **/
if ( ! function_exists( 'houserent_theme_sticky_post_count' ) ) :
    function houserent_theme_sticky_post_count() {                    
        $wp_sticky_query = new WP_Query(
            array (
                'post__in'  => get_option( 'sticky_posts' ),
            )
        );
        if ( $wp_sticky_query->have_posts() ) : 
        $i = 0;
        while ( $wp_sticky_query->have_posts() ) : $wp_sticky_query->the_post(); 
            if ( is_sticky() ) {
                $i++;
            }
        endwhile; endif; 
        wp_reset_postdata();
        return $i;
    }
endif;

/**
 ** Create a function that has a counter
 **/
if ( ! function_exists( 'houserent_theme_counter_inside_function' ) ) :
    function houserent_theme_counter_inside_function() {
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 0;
        $get_per_page = $paged*get_option('posts_per_page')-get_option('posts_per_page');
        static $counter = 0;
        ++$counter;
        if ( is_paged() ) {
            $counter_total = $counter+$get_per_page+houserent_theme_sticky_post_count();
            return  ($counter_total < 10 ) ? "0".$counter_total."." : $counter_total."." ;
        } else {
            return ($counter < 10 ) ? "0".$counter."." : $counter."." ;
        }  
    }
endif;

/**
 ** Houserent_theme_get_first_category
 **/
if ( ! function_exists( 'houserent_theme_get_first_category' ) ) :
    function houserent_theme_get_first_category() {
        $categories = get_the_category();
        if ( ! empty( $categories ) ) { ?>
        
        <span class="cat-links">
            <?php
                $i = 1;
                foreach( $categories as $category ):
                    if ( $category->slug == 'featured' || $category->slug == 'hand-picked') continue;
                    echo '<a href="' . get_category_link( $category->term_id ) . '" ' . '>' . $category->name.'</a>';
                    if ( $i == 1 ) break;
                endforeach;  
            ?>
        </span> <!-- /.cat-links -->
        <?php }
    }
endif;

/**
 ** Houserent theme the content
 **/
if ( ! function_exists( 'houserent_theme_the_content' ) ) :
    function houserent_theme_the_content() {
        $houserent_theme_page_type = ( is_home() ) ? houserent_theme_get_customizer_field('blog_layout','blog_layout_opt','five') : houserent_theme_get_customizer_field('blog_archive_layout','blog_archive_layout_opt','five') ;
        if ( $houserent_theme_page_type == 'one' || $houserent_theme_page_type == 'two' || $houserent_theme_page_type == 'five' ) {
            if ( houserent_theme_get_customizer_field('blog_post_excerpt_status','off') == 'on' ) {
                $post_excerpt_lenth = true;
            } else {
                $post_excerpt_lenth = false;
            }
        } else {
            $post_excerpt_lenth = false;
        }

        if ( has_excerpt() ) {
            the_excerpt();
        } else {
            if ( is_home() ) {
                if ( $post_excerpt_lenth ) {
                    add_filter( 'excerpt_length', 'houserent_theme_blog_post_exc_length', 999 );
                    the_excerpt();
                    if( function_exists('houerent_remove_filter_blog_post_exc_length') ) {
                        houerent_remove_filter_blog_post_exc_length();
                    }
                } else {
                    the_content();
                }
            } else {
                if ( $post_excerpt_lenth ) {
                    add_filter( 'excerpt_length', 'houserent_theme_blog_post_exc_length', 999 );
                    the_excerpt();
                    if( function_exists('houerent_remove_filter_blog_post_exc_length') ) {
                        houerent_remove_filter_blog_post_exc_length();
                    }
                } else {
                    the_content();
                }
            }
            houserent_theme_wp_link_pages();
        }
    }
endif;

/**
 ** Get featured img
 **/
if ( ! function_exists( 'houserent_theme_get_featured_img' ) ) :
    function houserent_theme_get_featured_img( $cropping_width, $cropping_height, $url = false ,$heard_crop = 'true' ) {
        if ( has_post_thumbnail() ) {
            $meta_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id()),'full')[0];
            if ( wp_is_mobile() ) {
                $featured_image = houserent_theme_resize( $meta_image_url , 412, 412, true ); 
            } else {
                if ( $heard_crop == 'true' ) {
                    $featured_image = houserent_theme_resize( $meta_image_url , $cropping_width, $cropping_height, true ); 
                } else {
                    $featured_image = houserent_theme_resize( $meta_image_url , $cropping_width, $cropping_height, false ); 
                }
                
            }

            if ($featured_image == false ) {;
                $featured_image = $meta_image_url;
            }


            if ( $url == false ){
            return '<img src="'.esc_url( $featured_image ).'"  alt="'.get_the_title( get_the_id() ).'">';
            } else {
                return esc_url( $featured_image );
            }
        } else {
            return null;
        } 
    } // end function
endif;

/**
 ** This function for single post page post header part ( placed under article tag )
 **/
if ( ! function_exists( 'houserent_theme_single_page_post_header' ) ) :
    function houserent_theme_single_page_post_header() { ?>
        <div class="post-header text-<?php echo esc_attr( houserent_theme_uny_post_meta('meta_post_title_position','center') ); ?>">
            <?php if ( get_the_title() ): ?>
            <h2 class="entry-title"><?php the_title(); ?></h2>
            <?php endif; ?>          
            <div class="entry-meta">

                <?php
                    //show or hide Date and Author meta by theme setting or Customizer. 
                    if ( houserent_theme_get_customizer_field('single_page_date_authormeta','') == "show" ): 
                ?>
                    <span class="entry-author"><i class="fa fa-user"></i>
                        <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>
                    </span>
                    <span class="entry-date"><i class="fa fa-clock-o"></i><?php the_time('F j, Y'); ?></span>
                <?php endif; ?>

                <?php 
                    //show or hide Category by theme setting or Customizer.
                    if ( houserent_theme_get_customizer_field('show_category_single','show') == 'show' && get_the_category() ) : 
                ?>
                    <span class="entry-category"><i class="fa fa-folder-open"></i>
                        <?php the_category(','); ?>
                    </span>
                <?php endif; ?>

            </div> <!--/.entry-meta-->

            <?php 
                //show or hide social Share icons by theme setting or Customizer.
                if ( houserent_theme_get_customizer_field('single_page_social_share','hide') == 'show' ): 
            ?>
                <div class="social-share"><?php houserent_theme_social_share_link(); ?></div>
                <div class="border-bottom-style">
                    <div>
                        <span></span>
                    </div>
                </div><!--/.border-bottom-style-->
            <?php endif; ?>
            
        </div> <!--/.post-header-->
        <?php
    } // end function
endif;

/**
 ** Functions for check post format
 **/
if( !function_exists('houserent_theme_post_formet')):
    function houserent_theme_post_formet() {
        $post_format = get_post_format();
        if ( false === $post_format ) {
            $post_format = "standard";
        }
        return $post_format;
    }
endif;

/**
 ** Functions for check post format
 **/
if( !function_exists('houserent_theme_video_iframe')):
    function houserent_theme_video_iframe( $video_url , $size = 4 ) {
        $video_link_array = houserent_theme_custom_video_url( $video_url, $size);
        if ( $video_link_array ):
            switch ($video_link_array['type']) {
                case 'youtube':
                   ?>
                        <<?php echo 'iframe';?> width="1280" height="650" src="https://www.youtube.com/embed/<?php echo  $video_link_array['id'];?>" allowfullscreen='allowfullscreen'></<?php echo 'iframe';?>>
                   <?php
                   break;

                case 'vimeo':
                   ?>
                        <<?php echo 'iframe';?> src="https://player.vimeo.com/video/<?php echo  $video_link_array['id'];?>" width="1280" height="650" allowfullscreen='allowfullscreen'></<?php echo 'iframe';?>>
                    <?php
                   break;

                case 'dailymotion':
                   ?>
                        <<?php echo 'iframe';?> frameborder="0" width="1280" height="650" src="//www.dailymotion.com/embed/video/<?php echo  $video_link_array['id'];?>" allowfullscreen='allowfullscreen'></<?php echo 'iframe';?>>
                    <?php
                   break;
               
                default:
                   return false;
                   break;
            }
        endif;
    }
endif;

/**
 ** Functions for converts 0 - 5 to stars
 **/
if( !function_exists('houserent_theme_number_to_stars')):
    function houserent_theme_number_to_stars($total_stars) {

        $star_integer = intval($total_stars);

        $buffy = '';

        // this echo full stars
        for ($i = 0; $i < $star_integer; $i++) {
            $buffy .= '<i class="fa fa-star"></i>';
        }

        $star_rest = $total_stars - $star_integer;

        // this echo full star or half or empty star
        if ($star_rest >= 0.25 and $star_rest < 0.75) {
            $buffy .= '<i class="fa fa-star-half-o"></i>';
        } else if ($star_rest >= 0.75) {
            $buffy .= '<i class="fa fa-star"></i>';
        } else if ($total_stars != 5) {
            $buffy .= '<i class="fa fa-star-o"></i>';
        }

        // this echo empty star
        for ($i = 0; $i < 4-$star_integer; $i++) {
            $buffy .= '<i class="fa fa-star-o"></i>';
        }

        return $buffy;
    }
endif;


if( !function_exists('houserent_check_in_range') ) :
    function houserent_check_in_range($start_date, $end_date, $date_from_user) {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }
endif;