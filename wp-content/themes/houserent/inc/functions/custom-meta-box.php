<?php
/**
 * Calls the class on the post edit screen.
 */
function houserent_theme_call_someClass() {
    if( class_exists('houserent_theme_someClass') ) {
        new houserent_theme_someClass();
    }
}
if ( is_admin() ) {
    add_action( 'load-post.php',     'houserent_theme_call_someClass' );
    add_action( 'load-post-new.php', 'houserent_theme_call_someClass' );
}