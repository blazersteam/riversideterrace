<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package Houserent
 */
add_action('wp_ajax_houserent_theme_load_post', 'houserent_theme_load_post');
add_action('wp_ajax_nopriv_houserent_theme_load_post', 'houserent_theme_load_post');
function houserent_theme_load_post() {
   get_template_part( 'template-parts/ajax-content-post' );
   die();
}

/* Favorite Property */
add_action('wp_ajax_houserent_property_favorite', 'houserent_property_favorite');
add_action('wp_ajax_nopriv_houserent_property_favorite', 'houserent_property_favorite');
function houserent_property_favorite() {
    $curent_user_id = get_current_user_id(); 
    $data_nonce = isset($_GET['data_nonce']) ? $_GET['data_nonce'] : '';
    $data_type = isset($_GET['data_type']) ? $_GET['data_type'] : '';
    $fav_id = isset($_GET['fav_id']) ? $_GET['fav_id'] : '';

    if ( wp_verify_nonce( $data_nonce, "houserent-favorite" ) && is_user_logged_in() ) {
        $fav_ids = unserialize( get_user_meta( $curent_user_id, 'houserent_user_favorites', true ) );  
        if( $data_type == 'add' ) {
            if ( !in_array($fav_id, $fav_ids)) {
                $fav_ids[] = $fav_id;
                update_user_meta( $curent_user_id, 'houserent_user_favorites', serialize($fav_ids) ); 
            }
        } else {
            if ( in_array($fav_id, $fav_ids)) { 
                $fav_ids = array_diff($fav_ids, array($fav_id));
                update_user_meta( $curent_user_id, 'houserent_user_favorites', serialize($fav_ids) ); 
            }
        }
    }    
    die();
}

/* Theme Registration */
add_action('wp_ajax_houserent_theme_update_registration', 'houserent_theme_update_registration');
add_action('wp_ajax_nopriv_houserent_theme_update_registration', 'houserent_theme_update_registration');
function houserent_theme_update_registration() { 
    $tf_token = (isset($_REQUEST['tf_token'])) ? $_REQUEST['tf_token'] : '';
    $tf_purchase_code = (isset($_REQUEST['tf_purchase_code'])) ? $_REQUEST['tf_purchase_code'] : '';

    if( $tf_token && $tf_purchase_code ) {
        $url = 'https://api.envato.com/v3/market/buyer/purchase?code='.$tf_purchase_code; 
        $defaults = array(
            'headers' => array(
                'Authorization' => 'Bearer '.$tf_token,
                'User-Agent' => 'SoftHopper Houserent Theme',
            ),
            'timeout' => 300,
        );

        $theme_info = wp_get_theme();
        $theme_info = ( $theme_info->parent() ) ? $theme_info->parent() : $theme_info;
        $theme_name = $theme_info->get('Name'); 
        $author_name = $theme_info->get('Author'); 

        $verification = false;
        $raw_response = wp_remote_get( $url, $defaults );
        $response = json_decode( $raw_response['body'], true );
        if(isset($response['item']['wordpress_theme_metadata'])) {
            $tf_theme_name = (isset($response['item']['wordpress_theme_metadata']['theme_name'])) ? $response['item']['wordpress_theme_metadata']['theme_name'] : '';
            $tf_author_name = (isset($response['item']['wordpress_theme_metadata']['author_name'])) ? $response['item']['wordpress_theme_metadata']['author_name'] : '';
            if($theme_name == $tf_theme_name && $author_name == $tf_author_name) {
                update_option( 'houserent_verification_key', array(
                    'tf_token'         => $tf_token, 
                    'tf_purchase_code' => $tf_purchase_code,
                ) ); 
                $verification = true;
            } 
        } 

        if ($verification == true) {
            echo('true');
        } else {
            echo('false');
        }
    }

    die(); 
}
