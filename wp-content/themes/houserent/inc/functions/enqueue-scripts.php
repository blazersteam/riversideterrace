<?php
/**
 * Enqueue scripts and styles.
 */
function houserent_theme_scripts() {
	global $wp_scripts;
	$protocol = is_ssl() ? 'https' : 'http';

    // enqueue style
    if ( !defined( 'FW' ) || get_option('fw_theme_google_fonts_link', '') == "") :
    wp_enqueue_style( 'houserent-default-font', houserent_theme_fonts_url(), array(), '1.0.0' );
    endif;
    wp_enqueue_style('houserent-theme-plugins', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/css/plugins.css', array(), false, 'all');

    wp_enqueue_style('houserent-theme-all-icons', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/css/house-rent-icon-pack.css', array(), false, 'all');
    wp_enqueue_style ( 'jquery-ui-css', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/css/jquery-ui.min.css' );

    wp_enqueue_style( 'houserent-theme-style', get_stylesheet_uri() );
    
	// enqueue scripts
	wp_enqueue_script('modernizr', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/js/vendor/modernizr-2.8.3.min.js', array(), '2.8.3', false);
    // enqueue script
    wp_enqueue_script('houserent-theme-plugins-js', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/js/plugins.js', array(), false, true);

    wp_enqueue_script( 'jquery-ui-core', false, array( 'jQuery' ) );
    wp_enqueue_script( 'jquery-ui-datepicker' );

    if( is_single() ):
        wp_enqueue_script('houserent-theme-sh-likes-js', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/js/sh-likes-public.js', array(), false, true);

        wp_localize_script( 'houserent-theme-sh-likes-js', 'houserent_like', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'like' => esc_html__( 'Like', 'houserent' ),
            'unlike' => esc_html__( 'Unlike', 'houserent' )
        ) );

    endif;

    wp_enqueue_script('houserent-theme-main-js', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/js/main.js', array(), false, true);
    wp_enqueue_script('houserent-theme-custom-js', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/custom/custom.js', array("jquery"), false, true);

    wp_localize_script("houserent-theme-custom-js", "houserent", array (
            'ajaxurl' => admin_url( "admin-ajax.php" ),  
        )
    );


	$check_rtl = false;
	if ( is_rtl() ) {
		$check_rtl = true;
	}
    
    $map_pointer_fallback = HOUSERENT_THEME_TEMPLATE_DIR_URL.'/images/map-icon.png';
    $map_latitude = houserent_theme_get_customizer_field('map_latitude','43.04446');
    $map_logitude = houserent_theme_get_customizer_field('map_logitude','-76.130791');
    $mouse_while_control = houserent_theme_get_customizer_field('mouse_while_control','false');
    $map_zoom_control = houserent_theme_get_customizer_field('map_zoom_control','false');
    $map_pointer = houserent_theme_get_customizer_field('map_pointer','url',$map_pointer_fallback);

    $rental_page_loadmore_text = houserent_theme_get_customizer_field('rental_page_loadmore_text','');
    $rental_page_loadmore_empty = houserent_theme_get_customizer_field('rental_page_loadmore_empty','');
    $rental_page_loading_text = houserent_theme_get_customizer_field('rental_page_loading_text','');

    $form_validator_name = esc_html__( 'Please enter your full name', 'houserent' );
    $form_validator_mobile = esc_html__( 'Please enter your phone number', 'houserent' );
    $form_validator_message = esc_html__( 'Please provide a message', 'houserent' );
    $form_validator_pass = esc_html__( 'Your message must be at least 20 characters long', 'houserent' );
    $form_validator_email = esc_html__( 'Please enter a valid email address', 'houserent' );

    wp_localize_script("houserent-theme-main-js", "houserent", array (
            "theme_uri" => HOUSERENT_THEME_TEMPLATE_DIR_URL,
        	"home_url" => esc_url( home_url( '/' ) ),
        	"ajaxurl" => esc_url( admin_url( "admin-ajax.php" ) ),
        	"check_rtl" => $check_rtl,
            "search_queries" => houserent_theme_get_all_rental_item(),
            "rental_loadmore_text" => $rental_page_loadmore_text,
            "rental_page_loading_text" => $rental_page_loading_text,
            "rental_nomore_text" => $rental_page_loadmore_empty,
            "form_validator_name" => $form_validator_name,
            "form_validator_mobile" => $form_validator_mobile,
            "form_validator_message" => $form_validator_message,
            "form_validator_pass" => $form_validator_pass,
            "form_validator_email" => $form_validator_email,
    	)
    );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}


    wp_enqueue_script( 'jquery-ui-autocomplete' );
    wp_enqueue_script( 'masonry' );
}
add_action( 'wp_enqueue_scripts', 'houserent_theme_scripts' );


/**
 * Enqueue scripts and styles for WordPress admin panel.
 */
function houserent_theme_admin_panel_scripts($hook) {

    wp_enqueue_style('houserent-theme-admin-css', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/css/backend/admin.css', array(), false, 'all');
    wp_enqueue_style('houserent-theme-all-icons', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/css/house-rent-icon-pack.css', array(), false, 'all');
	// enqueue style
	$protocol = is_ssl() ? 'https' : 'http';   
    wp_enqueue_media();

	// enqueue scripts
	if( $hook == 'widgets.php' ) {		 
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		//wp_enqueue_script('houserent-theme-widget-js', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/js/backend/widget.js', null, null, true);
	}
    wp_enqueue_script('houserent-theme-admin-js', HOUSERENT_THEME_TEMPLATE_DIR_URL . '/assets/js/backend/admin.js', null, null, true);

    wp_localize_script("houserent-theme-admin-js", "houserent", array (  
            "ajaxurl" => esc_url( admin_url( "admin-ajax.php" ) ) 
        )
    );

}
add_action( 'admin_enqueue_scripts', 'houserent_theme_admin_panel_scripts' );


/*
Register Google Fonts
*/
function houserent_theme_fonts_url() {
    $font_url = '';
    
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== esc_html_x( 'on', 'Google font: on or off', 'houserent' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Playfair Display:400,400i,700,700i,900,900i|Poppins:300,400,500,600,700' ), "https://fonts.googleapis.com/css" );
    }
    return $font_url;
}