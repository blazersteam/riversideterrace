<?php

 /**
  ** Functions for move comment field to bottom.
  **/
if ( ! function_exists( 'houserent_theme_move_comment_field_to_bottom' ) ) :
	function houserent_theme_move_comment_field_to_bottom( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;
		return $fields;
	}
	add_filter( 'comment_form_fields', 'houserent_theme_move_comment_field_to_bottom' );
endif;

/**
 ** Functions for comment form.
 **/
if ( ! function_exists( 'houserent_theme_comment_form' ) ) :
	function houserent_theme_comment_form($args) {
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );

		$allowed_html_array = array(
	        'span' => array(),
	        'b' => array(
	        	'class' => array(),
	        )
	    );

		$args['fields'] = array(
	      'author' =>
	        '<div class="col-md-6 pd-right"><p><input id="name" class="form-controller" name="author" required="required" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
	        '" size="30"' . ( $req ? " aria-required='true'" : '' ) . ' placeholder="' . esc_html__( 'Your Name', 'houserent' ) . ( $req ? '*' : '' ) . '" /></p></div>',

	      'email' =>
	        '<div class="col-md-6 pd-left"><p><input id="email" class="form-controller" name="email" required="required" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
	        '" size="30"' . ( $req ? " aria-required='true'" : '' ) . ' placeholder="' . esc_html__( 'Your Email', 'houserent' ) . ( $req ? '*' : '' ) . '" /></p></div>',

	      'url' =>
	        '<div class="col-md-12"><p><input id="url" class="form-controller" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
	        '" size="30" placeholder="' . esc_html__( 'Got a Website?', 'houserent' ) . '" /></p></div>'
	      );
		$args['id_form'] = "comment_form";
		$args['class_form'] = "comment-form";
		$args['id_submit'] = "submit";
		$args['class_submit'] = "submit";
		$args['name_submit'] = "submit";
		$args['title_reply'] = wp_kses( __( '<span>Leave a Reply</span>', 'houserent' ), $allowed_html_array );
		
		$args['title_reply_to'] = esc_html__( 'Leave a Reply to %s', 'houserent' );
		$args['cancel_reply_link'] = esc_html__( 'Cancel Reply', 'houserent' );
		$args['comment_notes_before'] = "";
		$args['comment_notes_after'] = "";
		$args['label_submit'] = esc_html__( 'Post a comment', 'houserent' );
		$args['comment_field'] = '<div class="col-md-12"><p><textarea id="message" class="form-controller" name="comment" aria-required="true" rows="8" cols="45" placeholder="'. esc_html__( 'Your Comment here&hellip;', 'houserent' ) .'" ></textarea></p></div>';
		return $args;
	}
	add_filter('comment_form_defaults', 'houserent_theme_comment_form');
endif;

/**
 ** Functions for comment list.
 **/
if ( ! function_exists( 'houserent_theme_comment_list' ) ) :
	function houserent_theme_comment_list($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;
		extract($args, EXTR_SKIP);

		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}
		$allowed_html_array = array(
	        'span' => array(),
	        'b' => array(
	        	'class' => array(),
	        )
	    ); ?>

		<<?php echo ( $tag) ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
		<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
		<?php endif; ?>

		<div class="comment-meta">
			<div class="comment-author vcard">	
				<div class="author-img">
					<?php echo get_avatar($comment,$size='80'); ?>				
				</div><!-- /.author-img -->
			</div><!-- /.comment-author .vcard -->

			<div class="comment-metadata">
				<?php printf( wp_kses( __( '<b class="fn">%s</b>', 'houserent' ), $allowed_html_array ), get_comment_author_link() ); ?>
				<span class="date">
					<?php
						/* translators: 1: date, 2: time */
						printf( esc_html__('%1$s at %2$s','houserent'), get_comment_date(),  get_comment_time() ); ?><?php edit_comment_link( esc_html__( '(Edit)','houserent' ), '  ', '' );
					?>
				</span>
			</div><!-- /.comment-metadata -->
		</div><!-- /.comment-meta -->

		<div class="comment-details">		
			<div class="comment-content">
				<?php comment_text(); ?>
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<p><em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.','houserent' ); ?></em>
					</p>
				<?php endif; ?>
			</div><!-- /.comment-content -->

			<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- /.reply -->
		</div><!-- /.comment-details -->
			
		<?php if ( 'div' != $args['style'] ) : ?>
		</div><!-- /.comment-body -->
		<?php endif; ?>
	<?php
}
endif;