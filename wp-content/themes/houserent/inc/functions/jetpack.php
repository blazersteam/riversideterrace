<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package Houserent
 */
/* 

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
**/

if ( ! function_exists( 'houserent_theme_jetpack_setup' ) ) :
    function houserent_theme_jetpack_setup() {
    	add_theme_support( 'infinite-scroll', array(
    		'container' => 'main',
    		'render'    => 'houserent_theme_infinite_scroll_render',
    		'footer'    => 'page',
    	) );
    } // end function houserent_theme_jetpack_setup
    add_action( 'after_setup_theme', 'houserent_theme_jetpack_setup' );
endif;

if ( ! function_exists( 'houserent_theme_infinite_scroll_render' ) ) :
    function houserent_theme_infinite_scroll_render() {
    	while ( have_posts() ) {
    		the_post();
    		get_template_part( 'template-parts/content', get_post_format() );
    	}
    } // end function houserent_theme_infinite_scroll_render
endif;