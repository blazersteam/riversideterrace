<?php 

/**
 ** To get unyson Theme Settings value
 **/
if ( ! function_exists( 'houserent_theme_get_settings_field' ) ) :
    function houserent_theme_get_settings_field($option_id, $default_value = null) {
        if ( function_exists( 'fw_get_db_customizer_option' ) ) :
            return fw_get_db_customizer_option($option_id, $default_value = null);
        endif;
    }
endif;


/**
 ** To get unyson post meta value
 **/
if ( ! function_exists( 'houserent_theme_uny_post_meta' ) ) :
    function houserent_theme_uny_post_meta() {
        $post_meta = get_post_meta( get_the_ID(), 'fw_options', true);
        $func_args = func_get_args();
        if ( $func_args > 1 ) {
            switch (func_num_args()) {
                case 2:   
                    if(isset($post_meta[$func_args[0]])) {
                       return $post_meta[$func_args[0]];
                    } else {
                        return $func_args[1];
                    }
                    break;

                case 3:                
                    if(isset($post_meta[$func_args[0]][$func_args[1]])) {
                       return $post_meta[$func_args[0]][$func_args[1]];
                    } else {
                        return $func_args[2];
                    }
                    break;

                case 4:                
                    if(isset($post_meta[$func_args[0]][$func_args[1]][$func_args[2]])) {
                       return $post_meta[$func_args[0]][$func_args[1]][$func_args[2]];
                    } else {
                        return $func_args[3];
                    }
                    break;

                case 5:                
                    if( isset($post_meta[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]] )) {
                       return $post_meta[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]];
                    } else {
                        return $func_args[4];
                    }
                    break;

                case 6:                
                    if( isset($post_meta[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]]) ) {
                       return $post_meta[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]];
                    } else {
                        return $func_args[5];
                    }
                    break;

                case 7:                
                    if( isset($post_meta[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]]) ) {
                       return $post_meta[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]];
                    } else {
                        return $func_args[6];
                    }
                    break;

                case 8:                
                    if( isset($post_meta[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]][$func_args[6]]) ) {
                       return $post_meta[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]][$func_args[6]];
                    } else {
                        return $func_args[7];
                    }
                    break;

                case 9:                
                    if( isset($post_meta[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]][$func_args[6]][$func_args[7]]) ) {
                       return $post_meta[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]][$func_args[6]][$func_args[7]];
                    } else {
                        return $func_args[8];
                    }
                    break;
                
                default:
                    return "";
                    break;
            }
        }
    }
endif;


if ( ! function_exists( 'houserent__get_options' ) ) :
    function houserent_get_options() {
        $result = get_theme_mod('fw_options');
        foreach (func_get_args() as $arg) {
            if(is_array($arg)) {
                if (!empty($result[$arg[0]])) {
                    $result = $result[$arg[0]];
                } else {  
                  $result = $arg[1];
                }
            } else {
                if (!empty($result[$arg])) {
                    $result = $result[$arg];
                } else { 
                    $result = null;
                }
            }
        }
        return $result;
    }
endif;

/**
 ** To get unyson Theme Settings value with fallback
 **/
if ( ! function_exists( 'houserent_theme_get_customizer_field' ) ) :
    function houserent_theme_get_customizer_field() {
        //get_theme_mod('fw_options')['footer_widgets_top_option']['1']['columns']
        $func_args = func_get_args();
        if ( $func_args > 1 ) {
            switch (func_num_args()) {
                case 2: 
                    if (is_array($func_args[0])) {
                        if ( get_theme_mod('fw_options')[$func_args[0][0]] != NULL ) {
                            return get_theme_mod('fw_options')[$func_args[0][0]];
                        } else {
                            return $func_args[0][1];
                        }
                    } else {
                        if ( isset( get_theme_mod('fw_options')[$func_args[0]] ) ) {
                            return get_theme_mod('fw_options')[$func_args[0]];
                        } else {
                            return $func_args[1];
                        }
                    }
                    break;

                case 3:
                    if (is_array($func_args[1])) {
                        if ( !empty( get_theme_mod('fw_options')[$func_args[0]][$func_args[1][0]] )) {
                            return get_theme_mod('fw_options')[$func_args[0]][$func_args[1][0]];
                        } else {
                            return $func_args[1][1];
                        }
                    } else {
                        if ( !empty( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]] )) {
                            return get_theme_mod('fw_options')[$func_args[0]][$func_args[1]];
                        } else {
                            return $func_args[2];
                        }
                    }
                    break;

                case 4:
                    if (is_array($func_args[2])) {                    
                        if ( !empty( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2][0]] )) {
                            return  get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2][0]];
                        } else {
                            return $func_args[2][1];
                        }
                    } else {
                        if ( isset( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]] )) {
                            return get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]];
                        } else {
                            return $func_args[3];
                        }
                    }
                    break;

                case 5:
                    if (is_array($func_args[3])) {                    
                        if ( !empty( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3][0]] )) {
                            return  get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3][0]];
                        } else {
                            return $func_args[3][1];
                        }
                    } else {
                        if ( isset( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]] )) {
                            return get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]];
                        } else {
                            return $func_args[4];
                        }
                    }
                    break;

                case 6:
                    if (is_array($func_args[4])) {                    
                        if ( !empty( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4][0]] )) {
                            return  get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4][0]];
                        } else {
                            return $func_args[4][1];
                        }
                    } else {
                        if ( isset( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]] )) {
                            return get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]];
                        } else {
                            return $func_args[5];
                        }
                    }
                    break;

                case 7:
                    if (is_array($func_args[5])) {                    
                        if ( !empty( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5][0]] )) {
                            return  get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5][0]];
                        } else {
                            return $func_args[5][1];
                        }
                    } else {
                        if ( isset( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]] )) {
                            return get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]];
                        } else {
                            return $func_args[6];
                        }
                    }
                    break;

                case 8:
                    if (is_array($func_args[6])) {                    
                        if ( !empty( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]][$func_args[6][0]] )) {
                            return  get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]][$func_args[6][0]];
                        } else {
                            return $func_args[6][1];
                        }
                    } else {
                        if ( isset( get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]][$func_args[6]] )) {
                            return get_theme_mod('fw_options')[$func_args[0]][$func_args[1]][$func_args[2]][$func_args[3]][$func_args[4]][$func_args[5]][$func_args[6]];
                        } else {
                            return $func_args[7];
                        }
                    }
                    break;
                
                default:
                    return "";
                    break;
            }
        }
    }
endif;


/**
 ** Sync common Houserent_Theme Settings and Customizer options db values
 ** @internal
 **/
class _FW_Houserent_Theme_Sync_Customizer_Options {
    public static function init() {
        add_action('customize_save_after', array(__CLASS__, '_action_after_customizer_save'));

        add_action('fw_settings_form_saved', array(__CLASS__, '_action_after_settings_save'));
        add_action('fw_settings_form_reset', array(__CLASS__, '_action_after_settings_save'));
    }

    /**
     * If a customizer option also exists in settings options, copy its value to settings option value
     */
    public static function _action_after_customizer_save()
    {
        $settings_options = fw_extract_only_options(fw()->theme->get_settings_options());

        foreach (
            array_intersect_key(
                fw_extract_only_options(fw()->theme->get_customizer_options()),
                $settings_options
            )
            as $option_id => $option
        ) {
            if ($option['type'] === $settings_options[$option_id]['type']) {
                fw_set_db_settings_option(
                    $option_id, fw_get_db_customizer_option($option_id)
                );
            }
        }
    }

    /**
     * If a settings option also exists in customizer options, copy its value to customizer option value
     */
    public static function _action_after_settings_save()
    {
        $customizer_options = fw_extract_only_options(fw()->theme->get_customizer_options());

        foreach (
            array_intersect_key(
                fw_extract_only_options(fw()->theme->get_settings_options()),
                $customizer_options
            )
            as $option_id => $option
        ) {
            if ($option['type'] === $customizer_options[$option_id]['type']) {
                fw_set_db_customizer_option(
                    $option_id, fw_get_db_settings_option($option_id)
                );
            }
        }
    }
}
_FW_Houserent_Theme_Sync_Customizer_Options::init();


/**
 ** Function for theme page builder field
 **/
if ( ! function_exists( 'houserent_theme_builder_field' ) ) :
    function houserent_theme_builder_field( $value ) {
        if(isset($value)) {
            return $value;
        } else {
            return null;
        }
    }
endif;


/**
 ** Function for get post meta image url with cropping size and image loading effect.
 **/
if( ! function_exists( 'houserent_theme_unyson_meta_image_url' ) ):
    function houserent_theme_unyson_meta_image_url( $post_id = '', $meta_key = '', $cropping_size = 'large', $blur_effects = false, $url_only = false ) {
        if ( $meta_key && $post_id) {
            $meta_value_product = get_post_meta( $post_id, 'fw_options');
            $attachment_id = ( !empty($meta_value_product[0][$meta_key]['attachment_id']) ) ? $meta_value_product[0][$meta_key]['attachment_id'] :  get_post_thumbnail_id( get_the_id() ) ;
            $meta_image_url_with_size = wp_get_attachment_image_src( $attachment_id, $cropping_size, false )[0];
            $meta_image_src_url = ($meta_image_url_with_size) ? $meta_image_url_with_size : get_the_post_thumbnail_url( $post_id, $cropping_size ) ;

            if ( $url_only == false  && $attachment_id != null ) {
                $blur_effect = houserent_theme_get_customizer_field('image_loading_effect_sec','image_loading_effect','false');
                if ( $blur_effect == 'true' ) {
                    $post_meta_attachment_data = wp_get_attachment_metadata( $attachment_id );

                    $cropping_width = (!empty($post_meta_attachment_data['sizes'][$cropping_size]['width'])) ? $post_meta_attachment_data['sizes'][$cropping_size]['width'] : $post_meta_attachment_data['sizes']['medium']['width'];

                    $cropping_height = (!empty($post_meta_attachment_data['sizes'][$cropping_size]['height'])) ? $post_meta_attachment_data['sizes'][$cropping_size]['height'] : $post_meta_attachment_data['sizes']['medium']['height'];

                    $small_size_meta_image = wp_get_attachment_image_src( $attachment_id, 'full', false )[0];
                    $small_size_meta_image = houserent_theme_resize( $small_size_meta_image , 86, 86, true ); ;

                    $image_blur_loader_condition = ( houserent_theme_get_customizer_field('image_loading_effect_sec','true','image_effect_type_opt','image_effect_type','') == 'loader') ? 'bloom-image-loader' : 'bloom-blur-loader' ;

                    return '<div class="image-bloom '.esc_attr( $image_blur_loader_condition ).'"><div class="bloom-image-thumb"><img src="'.$small_size_meta_image.'" data-thumb-url="'.$meta_image_src_url.'" data-full-width="'.esc_attr( $cropping_width ).'" data-full-height="'.esc_attr( $cropping_height ).'" alt="'.get_the_title( $post_id ).'"></div></div>';

                } elseif ( $blur_effect == 'false' ) {
                    return '<img alt="'. get_the_title( $post_id ) .'" src="'. $meta_image_src_url .'" class="image-responsive">';
                }

            } else {
                return $meta_image_src_url;
            }
        } else {
            return null;
        }

    }
endif;
