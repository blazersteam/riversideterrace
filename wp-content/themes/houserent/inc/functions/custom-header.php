<?php
/**
 * Sample implementation of the Custom Header feature
 * http://codex.wordpress.org/Custom_Headers
 *
 * You can add an optional custom header image to header.php like so ...
 *
 * @package Houserent
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses houserent_theme_header_style()
 * @uses houserent_theme_admin_header_style()
 * @uses houserent_theme_admin_header_image()
 */
function houserent_theme_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'houserent_theme_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 1000,
		'height'                 => 250,
		'flex-height'            => true,
	) ) );
}
add_action( 'after_setup_theme', 'houserent_theme_custom_header_setup' );


function add_editor_style( $stylesheet = 'editor-style.css' ) {
    add_theme_support( 'editor-style' );
 
    if ( ! is_admin() )
        return;
 
    global $editor_styles;
    $editor_styles = (array) $editor_styles;
    $stylesheet    = (array) $stylesheet;
    if ( is_rtl() ) {
        $rtl_stylesheet = str_replace('.css', '-rtl.css', $stylesheet[0]);
        $stylesheet[] = $rtl_stylesheet;
    }
 
    $editor_styles = array_merge( $editor_styles, $stylesheet );
}