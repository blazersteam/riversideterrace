<?php

/**
 ** Functions for change houserent content read  more link.
 **/
if ( ! function_exists( 'houserent_theme_content_more' ) ) :
    function houserent_theme_content_more($more) {        
    if ($more) {   
        return '... <br><br><span class="read-more"><a href="'.get_the_permalink().'" class="more-link">'.esc_html__( 'Continue Reading',  'houserent').' <i class="fa fa-long-arrow-right"></i></a></span>';
        } else {
            return esc_html__( ' ...',  'houserent');
        }
    }

    add_filter( 'excerpt_more', 'houserent_theme_content_more' );
    add_filter( 'the_content_more_link', 'houserent_theme_content_more' );
endif;


/**
 ** Functions for categories post count filter.
 **/
if ( ! function_exists( 'houserent_theme_categories_postcount_filter' ) ) :
    // remove parentheses from category list and add span class to count
    add_filter('wp_list_categories','houserent_theme_categories_postcount_filter');
    function houserent_theme_categories_postcount_filter ($args) {
        $args = str_replace('(', '<span class="count"> ', $args);
        $args = str_replace(')', ' </span>', $args);
       return $args;
    }
endif;


/**
 ** Functions for archive count no brackets.
 **/
if ( ! function_exists( 'houserent_theme_archive_count_no_brackets' ) ) :
    // remove parentheses from archive list and add span class to count
    add_filter('get_archives_link', 'houserent_theme_archive_count_no_brackets');
    function houserent_theme_archive_count_no_brackets($links) {
        $links = str_replace('</a>&nbsp;(', '</a> <span class="count">', $links);
        $links = str_replace(')', ' </span>', $links);
        return $links;
    }
endif;


/**
 ** Functions for blog post excerpt length.
 **/
if ( ! function_exists( 'houserent_theme_blog_post_exc_length' ) ) :
    function houserent_theme_blog_post_exc_length( $length ) {
        if ( is_home() ) {
            $houserent_theme_page_type = houserent_theme_get_customizer_field('blog_layout','blog_layout_opt','one');
            if ( $houserent_theme_page_type == 'one' ) {
                $blog_post_excerpt_lenth = houserent_theme_get_customizer_field('blog_layout','one','layout_one_post_excerpt','25');
            } else if ( $houserent_theme_page_type == 'two' ) {
                $blog_post_excerpt_lenth = houserent_theme_get_customizer_field('blog_layout','two','layout_two_post_excerpt','25');
            } else if ( $houserent_theme_page_type == 'five' ) {
                $blog_post_excerpt_lenth = houserent_theme_get_customizer_field('blog_layout','five','layout_five_post_excerpt','25');
            } else {
                $blog_post_excerpt_lenth = '25';
            }
            return $blog_post_excerpt_lenth;
        } else {
            $houserent_theme_page_type = houserent_theme_get_customizer_field('blog_archive_layout','blog_archive_layout_opt','one');
            if ( $houserent_theme_page_type == 'one' ) {
                $blog_post_excerpt_lenth = houserent_theme_get_customizer_field('blog_archive_layout','one','layout_one_post_excerpt','30');
            } else if ( $houserent_theme_page_type == 'two' ) {
                $blog_post_excerpt_lenth = houserent_theme_get_customizer_field('blog_archive_layout','two','archive_layout_two_post_excerpt','30');
            } else if ( $houserent_theme_page_type == 'five' ) {
                $blog_post_excerpt_lenth = houserent_theme_get_customizer_field('blog_archive_layout','five','archive_layout_five_post_excerpt','30');
            } else {
                $blog_post_excerpt_lenth = '25';
            }
            return $blog_post_excerpt_lenth;
        }
    }
endif;


/**
 ** Functions for custom post excerpt.
 **/
if ( ! function_exists( 'houserent_theme_custom_post_excerpt' ) ) :
    // custom post excerpt with charecter
    function houserent_theme_custom_post_excerpt($string, $length, $dots = "&hellip;") {
        return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
    }
endif;


/**
 ** Functions for custom string limit words.
 **/
if ( ! function_exists( 'houserent_theme_custom_string_limit_words' ) ) :
    // custom post excerpt with words
    function houserent_theme_custom_string_limit_words($string, $word_limit) {
      $words = explode(' ', $string, ($word_limit + 1));
      if(count($words) > $word_limit)
      array_pop($words);
      return implode(' ', $words);
    }
endif;

/**
 ** Functions for get file name.
 **/
if ( ! function_exists( 'houserent_theme_get_file_name_alternative' ) ) :
    function houserent_theme_get_file_name_alternative($pathname){
        $get_file_name_path = explode("/", $pathname);
        return end($get_file_name_path);
    }
endif;

/**
 ** Functions for get current template name.
 **/
if ( ! function_exists( 'houserent_theme_var_template_include' ) ) :
    // to get current template name
    add_filter( 'template_include', 'houserent_theme_var_template_include', 1000 );
    function houserent_theme_var_template_include( $t ) {
        $GLOBALS['current_theme_template'] = houserent_theme_get_file_name_alternative($t);
        return $t;
    }
endif;


/**
 ** Functions for get current template.
 **/
if ( ! function_exists( 'houserent_theme_get_current_template' ) ) :
    function houserent_theme_get_current_template( $echo = false ) {
        if( !isset( $GLOBALS['current_theme_template'] ) )
            return false;
        if( $echo )
            echo houserent_theme_esc_variable( $GLOBALS['current_theme_template'] );
        else
            return $GLOBALS['current_theme_template'];
    }
endif;

/**
 ** Functions for remove unnecessary p and br tag from shortcode.
 **/
if( !function_exists('houserent_theme_fix_shortcodes') ) :
    function houserent_theme_fix_shortcodes($content){
        $array = array (
            '<p>[' => '[',
            ']</p>' => ']',
            ']<br />' => ']'
        );
        $content = strtr($content, $array);
        return $content;   
    }
    add_filter('the_content', 'houserent_theme_fix_shortcodes');
endif;

/**
 * @param FW_Ext_Backups_Demo[] $demos
 * @return FW_Ext_Backups_Demo[]
 */
function houserent_theme_filter_theme_fw_ext_backups_demos($demos) {
    $demos_array = array(
        'demo-one' => array(
            'title' => esc_html__('Demo One', 'houserent'),
            'screenshot' => 'http://demo.softhopper.net/theme-demo-contents/house-rent-demo/screenshot-1.jpg',
            'preview_link' => 'http://demo.softhopper.net/house-rent/',
        ),
        'demo-two' => array(
            'title' => esc_html__('Demo Two', 'houserent'),
            'screenshot' => 'http://demo.softhopper.net/theme-demo-contents/house-rent-demo/screenshot-2.jpg',
            'preview_link' => 'http://demo.softhopper.net/house-rent/home-page-02/?header-style=two',
        ),
        'demo-three' => array(
            'title' => esc_html__('Demo Three', 'houserent'),
            'screenshot' => 'http://demo.softhopper.net/theme-demo-contents/house-rent-demo/screenshot-3.jpg',
            'preview_link' => 'http://demo.softhopper.net/house-rent/home-page-03/?header-color=0e385d',
        ),
        'demo-four' => array(
            'title' => esc_html__('Demo Four', 'houserent'),
            'screenshot' => 'http://demo.softhopper.net/theme-demo-contents/house-rent-demo/screenshot-4.jpg',
            'preview_link' => 'http://demo.softhopper.net/house-rent/home-page-04/?header-style=three',
        ),
        'demo-five' => array(
            'title' => esc_html__('Demo Five', 'houserent'),
            'screenshot' => 'http://demo.softhopper.net/theme-demo-contents/house-rent-demo/screenshot-5.jpg',
            'preview_link' => 'http://demo.softhopper.net/house-rent/home-page-05',
        ),
        'demo-six' => array(
            'title' => esc_html__('Demo Six', 'houserent'),
            'screenshot' => 'http://demo.softhopper.net/theme-demo-contents/house-rent-demo/screenshot-6.jpg',
            'preview_link' => 'http://demo.softhopper.net/house-rent/home-page-06/?header-style=two&he_bot=off',
        ),
        'demo-seven' => array(
            'title' => esc_html__('Demo Seven', 'houserent'),
            'screenshot' => 'http://demo.softhopper.net/theme-demo-contents/house-rent-demo/screenshot-7.jpg',
            'preview_link' => 'http://demo.softhopper.net/house-rent/home-page-07/',
        ),
        'demo-eight' => array(
            'title' => esc_html__('Demo Eight', 'houserent'),
            'screenshot' => 'http://demo.softhopper.net/theme-demo-contents/house-rent-demo/screenshot-8.jpg',
            'preview_link' => 'http://demo.softhopper.net/house-rent/home-page-08/?header-style=two',
        ),
    );

    $download_url = esc_url('http://demo.softhopper.net/theme-demo-contents/house-rent-demo');

    foreach ( $demos_array as $id => $data ) {
        $demo = new FW_Ext_Backups_Demo($id, 'piecemeal', array(
            'url' => $download_url,
            'file_id' => $id,
        ));
        $demo->set_title($data['title']);
        $demo->set_screenshot($data['screenshot']);
        $demo->set_preview_link($data['preview_link']);

        $demos[ $demo->get_id() ] = $demo;

        unset($demo);
    }

    return $demos;
}
add_filter('fw:ext:backups-demo:demos', 'houserent_theme_filter_theme_fw_ext_backups_demos');

/**
 * Include mega menu
 */
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/mega-menu/sh_menu_functions.php';
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/mega-menu/menu.php';
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/mega-menu/nav-menu-roles/nav-menu-roles.php';
require HOUSERENT_THEME_TEMPLATE_DIR . '/inc/mega-menu/icons.php';