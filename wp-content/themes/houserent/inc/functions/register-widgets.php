<?php
/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */


function houserent_theme_widgets_init() {

	$other_settings_color_type = houserent_theme_get_customizer_field('other_settings_color_type','other_settings_color', 'color' );
	if( $other_settings_color_type == 'color' ){
	    $title_color = houserent_theme_get_customizer_field('other_settings_color_type','color','other_settings_color_c', '#21b75f' );
	    $sbcolor_choose = "color: $title_color ;";
	} elseif( $other_settings_color_type == 'gradient' ){
	    $title_gradient = houserent_theme_get_customizer_field('other_settings_color_type','gradient','other_settings_gradient_c', array(
	            'primary' => '#21b75f',
	            'secondary' => '#31386e'
	        ) );
	    $sbcolor_choose = "background: linear-gradient(330deg, ".esc_attr( $title_gradient['secondary'] )." 50%, ".esc_attr( $title_gradient['primary'] )." 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;";
	}

	// Register footer sidebars
	register_sidebar(  array(
		'name'          => esc_html__( 'Sidebar Blog Home', 'houserent' ),
		'id'            => 'sidebar-home-page',
		'description'   => esc_html__( 'This sidebar will show in Blog Home Page', 'houserent' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title-area"><h3 class="widget-title" style="'. esc_attr( $sbcolor_choose ).'">',
		'after_title'   => '</h3></div>',
	) );

	register_sidebar(  array(
		'name'          => esc_html__( 'Sidebar Default', 'houserent' ),
		'id'            => 'sidebar-default',
		'description'   => esc_html__( 'This sidebar will show in blog other page without blog home page', 'houserent' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title-area"><h3 class="widget-title" style="'. esc_attr( $sbcolor_choose ).'">',
		'after_title'   => '</h3></div>',
	) );

	register_sidebar(  array(
		'name'          => esc_html__( 'Sidebar Rental', 'houserent' ),
		'id'            => 'sidebar-rental',
		'description'   => esc_html__( 'This sidebar will show in single rental item page', 'houserent' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title-area"><h3 class="widget-title" style="'. esc_attr( $sbcolor_choose ).'">',
		'after_title'   => '</h3></div>',
	) );

	$footer_title_color_type = houserent_theme_get_customizer_field('footer_title_color_type','footer_title_color', 'color' );
	if( $footer_title_color_type == 'color' ){
	    $title_color = houserent_theme_get_customizer_field('footer_title_color_type','color','footer_title_color_c', '#21b75f' );
	    $color_choose = "color: $title_color ;";
	} elseif( $footer_title_color_type == 'gradient' ){
	    $title_gradient = houserent_theme_get_customizer_field('footer_title_color_type','gradient','footer_title_gradient_c', array(
	            'primary' => '#21b75f',
	            'secondary' => '#31386e'
	        ) );
	    $color_choose = "background: linear-gradient(330deg, ".esc_attr( $title_gradient['secondary'] )." 50%, ".esc_attr( $title_gradient['primary'] )." 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;";
	}

	$theme_footer_widgets_columns = houserent_theme_get_customizer_field( 'footer_widgets_columns', 3 );
	
	for ($i=1; $i <= $theme_footer_widgets_columns; $i++) { 
		register_sidebar(  
			array(
				'name'          => esc_html__( 'Footer Widget -', 'houserent' ).$i,
				'id'            => 'sidebar-footer-'.$i,
				'description'   => esc_html__( 'This sidebar will show in Footer', 'houserent' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<div class="widget-title-area"><h3 class="widget-title" style="'. esc_attr( $color_choose ).'">',
				'after_title'   => '</h3></div>',
			) 
		);
	}

}
add_action( 'widgets_init', 'houserent_theme_widgets_init' );