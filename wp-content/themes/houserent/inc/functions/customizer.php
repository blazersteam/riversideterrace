<?php
/**
 * Houserent Theme Customizer
 *
 * @package Houserent
 */


/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function houserent_theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';

	// Site Title
	$wp_customize->selective_refresh->add_partial( 'blogname', array(
	    'selector' => '.site-title',
	) );	
	$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
	    'selector' => '.site-description',
	) );	

	$wp_customize->selective_refresh->add_partial( 'fw_options[header_layout]', array(
	    'selector' => '#header-top',
	) );	

	$wp_customize->selective_refresh->add_partial( 'fw_options[header_logo]', array(
	    'selector' => '.logo-block',
	) );

	$wp_customize->selective_refresh->add_partial( 'nav_menu_locations[main-menu]', array(
	    'selector' => '.main-menu',
	) );	

	// Slider Block
	$wp_customize->selective_refresh->add_partial( 'fw_options[home_main_sliders]', array(
	    'selector' => '.home .slider-block-area',
	) );

	// Quick Lunch Bar
	$wp_customize->selective_refresh->add_partial( 'fw_options[home_main_promotion_aria]', array(
	    'selector' => '.top-sidebar_area',
	) );

	// Blog Layout
	$wp_customize->selective_refresh->add_partial( 'fw_options[blog_layout]', array(
	    'selector' => '.home .site-content',
	) );

	// Single Post Settings
	$wp_customize->selective_refresh->add_partial( 'fw_options[single_post_page_layout]', array(
	    'selector' => 'body.single-post .content-area',
	) );
	$wp_customize->selective_refresh->add_partial( 'fw_options[single_post_page_column_width]', array(
	    'selector' => 'body.single-post .full-width-col',
	) );
	$wp_customize->selective_refresh->add_partial( 'fw_options[show_category_single]', array(
	    'selector' => 'body.single-post .post-header .entry-category',
	) );		
	$wp_customize->selective_refresh->add_partial( 'fw_options[single_page_date_authormeta]', array(
	    'selector' => 'body.single-post .post-header .entry-author, body.single-post .post-header .entry-date',
	) );		
	$wp_customize->selective_refresh->add_partial( 'fw_options[single_page_social_share]', array(
	    'selector' => 'body.single-post .social-share',
	) );		
	$wp_customize->selective_refresh->add_partial( 'fw_options[single_page_post_pagination]', array(
	    'selector' => 'body.single-post .post-navigation',
	) );	
	$wp_customize->selective_refresh->add_partial( 'fw_options[single_page_related_post]', array(
	    'selector' => 'body.single-post .related-post',
	) );

	//Page Settings	
	$wp_customize->selective_refresh->add_partial( 'fw_options[genarel_post_page_layout]', array(
	    'selector' => 'body.page-template-default.page .single-post-page',
	) );
	$wp_customize->selective_refresh->add_partial( 'fw_options[show_page_title]', array(
	    'selector' => 'body.page-template-default.page .entry-title',
	) );
	$wp_customize->selective_refresh->add_partial( 'fw_options[page_social_share]', array(
	    'selector' => 'body.page-template-default .social-share',
	) );	

	//Archive Page Settings	
	$wp_customize->selective_refresh->add_partial( 'fw_options[blog_archive_layout]', array(
	    'selector' => 'body.archive .site-content',
	) );

	//404 Page Settings	
	$wp_customize->selective_refresh->add_partial( 'fw_options[404_page_background]', array(
	    'selector' => 'body.error404 .header-middle-bg',
	) );
	$wp_customize->selective_refresh->add_partial( 'fw_options[404_page_logo]', array(
	    'selector' => 'body.error404 .error-image',
	) );
	$wp_customize->selective_refresh->add_partial( 'fw_options[404_page_search_box]', array(
	    'selector' => 'body.error404 .error-search',
	) );
	$wp_customize->selective_refresh->add_partial( 'fw_options[404_page_contact_page]', array(
	    'selector' => 'body.error404 .selected_page',
	) );

	//Contact page
	$wp_customize->selective_refresh->add_partial( 'fw_options[map_api_key]', array(
	    'selector' => 'body.page-template-contact-page .gmaps-area',
	) );
	$wp_customize->selective_refresh->add_partial( 'fw_options[contact_action_aria]', array(
	    'selector' => 'body.page-template-contact-page .contact-details-list',
	) );
	$wp_customize->selective_refresh->add_partial( 'fw_options[contact_form]', array(
	    'selector' => 'body.page-template-contact-page #contact-form-wrap',
	) );
	
	// Footer
	$wp_customize->selective_refresh->add_partial( 'fw_options[footer_bg_color_custom]', array(
	    'selector' => '#footer-middle',
	) );

	$wp_customize->selective_refresh->add_partial( 'fw_options[footer_widgets_columns]', array(
	    'selector' => '#footer-middle .row',
	) );

	$wp_customize->selective_refresh->add_partial( 'fw_options[footer_widgets_top_option]', array(
	    'selector' => '.site-footer',
	) );

	$wp_customize->selective_refresh->add_partial( 'fw_options[footer_bottom_bg_color_custom]', array(
	    'selector' => '#footer-bottom',
	) );	
	$wp_customize->selective_refresh->add_partial( 'fw_options[copyright_text]', array(
	    'selector' => '.copyright',
	) );

	 
}
add_action( 'customize_register', 'houserent_theme_customize_register' );




/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function houserent_theme_customize_preview_js() {
	wp_enqueue_script( 'houserent_theme_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
	wp_enqueue_style('houserent-theme-admin-customizer', get_template_directory_uri() . "/css/backend/customizer.css");
}
add_action( 'customize_preview_init', 'houserent_theme_customize_preview_js' );
