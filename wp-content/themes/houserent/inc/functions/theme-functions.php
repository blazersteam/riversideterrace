<?php 


if ( ! function_exists( 'houserent_theme_currency' ) ) :
    function houserent_theme_currency() {
        $rental_currency_symbol = esc_html( houserent_theme_get_customizer_field( 'rental_currency_symbol','$' ) );
        return $rental_currency_symbol;
    }
endif;

/**
 ** escaping variable from direct echo
 **/
if (function_exists('houserent_theme_esc_variable')):
    function houserent_theme_esc_variable( $value ) {
        return $value;
    }
endif;


/**
 ** social icon set
 **/
if ( ! function_exists( 'houserent_theme_social_links' ) ) :
    function houserent_theme_social_links() {
        // show header top right social link
        $socials_icons = houserent_theme_get_customizer_field('header_social_profile','');
        if ( $socials_icons ) {
            foreach ( $socials_icons as $key => $value ) {
             
                if ( $value['social_icons']['type'] == 'icon-font' ) {
            ?>
                    <a target="_blank" href="<?php echo esc_url($value['social_profile_url']); ?>"><i class="<?php echo esc_attr($value['social_icons']['icon-class']);?>"></i></a>
            <?php
                } else if ( $value['social_icons']['type'] == 'custom-upload' ) {
            ?>
                <a target="_blank" href="<?php echo esc_url($value['social_profile_url']); ?>">
                    <?php 
                        echo wp_get_attachment_image(
                            $value['social_icons']['attachment-id'],
                            'small',
                            true
                        );
                     ?>
                </a>
            <?php
                }
            }
        }
    }
endif;

/**
 ** Favorite Button
 **/
function sh_hr_rental_favorite_btn($fav_id) {
    $curent_user_id = get_current_user_id(); 
    $fav_ids = unserialize( get_user_meta( $curent_user_id, 'houserent_user_favorites', true ) );
    if($fav_ids == null) {
        $fav_ids = array();
    }
    $data_type =  ( !in_array($fav_id, $fav_ids) ) ? "add" : "remove";  
    $fav_class = ( !in_array($fav_id, $fav_ids) ) ? "fa fa-star-o" : "fa fa-star";   
    $tooltip_title = ( !in_array($fav_id, $fav_ids) ) ? esc_html__("Add To Favorites", 'houserent') : esc_html__("Remove From Favorites", 'houserent');
    ?>  
    <i class="<?php echo esc_attr($fav_class); ?> property-favorites" data-nonce="<?php echo wp_create_nonce("houserent-favorite"); ?>" data-fav-id="<?php echo esc_attr($fav_id); ?>" data-type="<?php echo esc_attr($data_type); ?>" data-placement="bottom" data-toggle="tooltip" data-original-title="<?php echo esc_attr($tooltip_title); ?>"></i>
<?php }


/**
 * Get page ID by Template Name
 *
 */
if ( ! function_exists( 'sh_hr_page_id_by_tem_name' ) ) {
    function sh_hr_page_id_by_tem_name( $template_name = null ) {
        $template_page_id = null;
        $template_array = get_pages( array (
            'meta_key'   => '_wp_page_template',
            'meta_value' => $template_name
        ) );
        if ( $template_array ) {
            $template_page_id = $template_array[0]->ID;
        }
        return $template_page_id;
    }
}

/**
 ** archive or search page title bar 
 **/
if ( ! function_exists( 'houserent_theme_archive_search_page_title' ) ) :
	function houserent_theme_archive_search_page_title() {
		if ( is_archive() || is_search() ) { ?>
		<div class="page-header">
            <div class="container">
                <div class="row">
                    <?php
			            if ( is_archive() ) {
		                    houserent_theme_archive_title( '<h1 class="page-title">', '</h1>' );
		                } elseif ( is_search() ) {
                    ?>
						<h1 class="page-title"><?php printf( '<span class="browsing-title">'.esc_html__( 'Search Results for', 'houserent' ).'</span>%s', get_search_query() ); ?></h1>
                    <?php
		                }
	                ?>
                </div>
            </div><!-- /.page-header -->
        </div> 
		<?php }
	}
endif;


/**
 ** allowed HTML Tags function
 **/
if ( ! function_exists( 'houserent_theme_html_allow' ) ) :
    function houserent_theme_html_allow() {
    	return $allowed_html_array = array(
            'a' => array(
                'href' => array(),
                'title' => array()
            ),
            'br' => array(),
            'span' => array(),
            'em' => array(),
            'strong' => array(),
            'pre' => array(),
            'code' => array(),
        );
    }
endif;


/**
 ** Footer widgets columns function
 **/
if ( ! function_exists( 'houserent_theme_footer_widgets_columns' ) ) :
    function houserent_theme_footer_widgets_columns() {
        return (defined( 'FW' )) ? fw_get_db_customizer_option('footer_widgets_columns') : '4' ;
    }
endif;


/**
 ** Show Footer sidebar
 **/
if ( ! function_exists( 'houserent_theme_dynamic_sidebar' ) ) :
    function houserent_theme_dynamic_sidebar( $sidebar_id ) {
        if ( ! dynamic_sidebar( $sidebar_id ) ) :

            the_widget('WP_Widget_Search', '', 'before_widget=<aside class="widget clearfix widget_search">&before_title=<div class="widget-title-area"><h5 class="widget-title"><span>&after_title=</span></h5></div>&after_widget=</aside>'); 
            
            the_widget('WP_Widget_Categories', '', 'before_widget=<aside class="widget clearfix widget_categories">&before_title=<div class="widget-title-area"><h5 class="widget-title"><span>&after_title=</span></h5></div>&after_widget=</aside>'); 

            the_widget('WP_Widget_Archives', '', 'before_widget=<aside class="widget clearfix widget_archive">&before_title=<div class="widget-title-area"><h5 class="widget-title"><span>&after_title=</span></h5></div>&after_widget=</aside>'); 

            the_widget('WP_Widget_Tag_Cloud', '', 'before_widget=<aside class="widget clearfix widget_tag_cloud">&before_title=<div class="widget-title-area"><h5 class="widget-title"><span>&after_title=</span></h5></div>&after_widget=</aside>'); 

        endif; // end sidebar widget area 
    }
endif;


/**
 ** Function for get post meta image url with cropping size and image loading effect.
 **/
if( ! function_exists( 'houserent_theme_settings_image' ) ):
    function houserent_theme_settings_image( $attachment_id = '', $cropping_width = '',$cropping_height = '', $hard_crop = true ) {
        if ( $attachment_id) {
            // get image url by id
            $meta_image_url = wp_get_attachment_image_src( $attachment_id, 'full' )[0];
            if ( $cropping_width && $cropping_height) {
                if( wp_is_mobile() ){
                    $featured_image = houserent_theme_resize( $meta_image_url , 412, 412, $hard_crop ); 
                } else {
                    $featured_image = houserent_theme_resize( $meta_image_url , $cropping_width, $cropping_height, $hard_crop ); 
                }

                $featured_image = ($featured_image) ? $featured_image : $meta_image_url ;
                return $featured_image;
            } else {
                return $meta_image_url;
            }
        } else {
            return null;
        }
    }
endif;


/**
 ** Function for process google fonts
 **/
if(!function_exists('houserent_theme_process_google_fonts')):
    function houserent_theme_process_google_fonts() {
        $include_from_google = array();
        $google_fonts = fw_get_google_fonts();

        $typo_fiels = array(
            houserent_theme_get_customizer_field('global_body_fonts', ''), 
            houserent_theme_get_customizer_field('block_quote_fonts', ''), 
            houserent_theme_get_customizer_field('hading_one_font', ''), 
            houserent_theme_get_customizer_field('hading_two_font', ''), 
            houserent_theme_get_customizer_field('hading_three_font', ''), 
            houserent_theme_get_customizer_field('hading_four_font', ''), 
            houserent_theme_get_customizer_field('hading_five_font', ''), 
            houserent_theme_get_customizer_field('hading_six_font', ''), 
        );

        foreach ($typo_fiels as $value) {
            if( isset($google_fonts[$value['family']]) ){
                $include_from_google[$value['family']] =   $google_fonts[$value['family']];
            }
        }

        $google_fonts_links = houserent_theme_get_remote_fonts($include_from_google);
        // set a option in db for save google fonts link
        update_option( 'fw_theme_google_fonts_link', $google_fonts_links );
    }
    add_action('fw_settings_form_saved', 'houserent_theme_process_google_fonts', 999, 2);
    add_action('customize_save_after', 'houserent_theme_process_google_fonts', 999, 2);
    add_action('fw_settings_form_reset', 'houserent_theme_process_google_fonts', 999, 2);
endif;

/**
 ** Function for get remote fonts.
 **/

if (!function_exists('houserent_theme_get_remote_fonts')) :
    function houserent_theme_get_remote_fonts($include_from_google) {
        /**
         * Get remote fonts
         * @param array $include_from_google
         */
        if ( ! sizeof( $include_from_google ) ) {
            return '';
        }

        $html = "https://fonts.googleapis.com/css?family=";

        foreach ( $include_from_google as $font => $styles ) {
            $html .= str_replace( ' ', '+', $font ) . ':' . implode( ',', $styles['variants'] ) . '%7C';
        }

        $html = substr( $html, 0, - 1 );

        return $html;
    }
endif;


/**
 ** Function for print google fonts link.
 **/
if (!function_exists('houserent_theme_print_google_fonts_link')) :
    function houserent_theme_print_google_fonts_link() {
        /**
         * Print google fonts link
         */
        wp_enqueue_style('houserent-theme-custom-google-font', get_option('fw_theme_google_fonts_link', ''), array('houserent-theme-style'), false, 'all');
    }
    add_action( 'wp_enqueue_scripts', 'houserent_theme_print_google_fonts_link' );
endif;
//end goole font


/**
 ** Functions for save extra fields for author profile page.
 **/
if( !function_exists('houserent_theme_sidebar_check')):
    function houserent_theme_sidebar_check(){
        if ( is_home() ) {

            // Post Layout URL Condition Check. 
            $layout_blog_url = ( isset($_GET["blog-style"]) ) ? $_GET["blog-style"]  : "";
            $houserent_theme_page_type = ( $layout_blog_url ) ? $layout_blog_url : houserent_theme_get_customizer_field('blog_layout','blog_layout_opt','five') ;

        } else {

            // Post Layout URL Condition Check. 
            ( isset($_GET["archive-style"]) ) ? $layout_blog_archive_url = $_GET["archive-style"]  : $layout_blog_archive_url = "";
            $houserent_theme_page_type = ( $layout_blog_archive_url ) ? $layout_blog_archive_url : houserent_theme_get_customizer_field('blog_archive_layout','blog_archive_layout_opt','five');
        }



        switch ( $houserent_theme_page_type ) {
            case 'one':
                $layout_class = 'layout-one rightsideber-version';
                $blog_sidebar_status = (is_home()) ? houserent_theme_get_customizer_field('blog_layout', 'one', 'layout_one_page_layout','sidebar_right') : houserent_theme_get_customizer_field('blog_archive_layout', 'one', 'archive_layout_one_page_layout','sidebar_right');
                break;

            case 'two': 
                $layout_class = 'layout-two fullwidth-version';  
                $collumn_class = '12';
                $blog_sidebar_status = (is_home()) ? houserent_theme_get_customizer_field('blog_layout', 'two', 'layout_two_page_layout','sidebar_right') : houserent_theme_get_customizer_field('blog_archive_layout', 'two', 'archive_layout_two_page_layout','sidebar_right');
                break;

            case 'three':  
                $layout_class = 'layout-six'; 
                $collumn_class = '12';
                $blog_sidebar_status = (is_home()) ? houserent_theme_get_customizer_field('blog_layout', 'three', 'layout_three_page_layout','sidebar_right') : houserent_theme_get_customizer_field('blog_archive_layout', 'three', 'archive_layout_three_page_layout','sidebar_right');
                break;

            case 'four':  
                $layout_class = 'layout-seven'; 
                $collumn_class = '9';
                $blog_sidebar_status = (is_home()) ? houserent_theme_get_customizer_field('blog_layout', 'four', 'layout_four_page_layout','sidebar_right') : houserent_theme_get_customizer_field('blog_archive_layout', 'four', 'archive_layout_four_page_layout','sidebar_right');
                break;

            case 'five':
                $layout_class = 'layout-eight';  
                $collumn_class = '9';
                $blog_sidebar_status = (is_home()) ? houserent_theme_get_customizer_field('blog_layout', 'five', 'layout_five_page_layout','sidebar_right') : houserent_theme_get_customizer_field('blog_archive_layout', 'five', 'archive_layout_five_page_layout','sidebar_right');
                break;

            case 'six':
                $layout_class = 'layout-nine'; 
                $blog_sidebar_status = (is_home()) ? houserent_theme_get_customizer_field('blog_layout', 'six', 'layout_six_page_layout','sidebar_right') : houserent_theme_get_customizer_field('blog_archive_layout', 'six', 'archive_layout_six_page_layout','sidebar_right');
                break;
            
            default:
                $layout_class = 'layout-seven'; 
                $collumn_class = '9';
                $blog_sidebar_status = 'sidebar_right';
                break;
        }


        if ( isset($_GET["blog-archive-layout"] ) ) {
            $blog_page_layout = $_GET["blog-archive-layout"];
        } else {
            $blog_page_layout = $blog_sidebar_status;
        }


        if ( houserent_theme_header_style_swicher() == 'five' ) {
            $layout_class = $layout_class.' layout-left-menu ';
            $collumn_class = 'col-md-9';
        } else {
            if ( $blog_page_layout == 'sidebar_left' ){
                $collumn_class = 'col-md-9 col-md-push-3';
            } elseif ( $blog_page_layout == 'sidebar_right' ){
                $collumn_class = 'col-md-9';
            } else {
                $theme_settings_column_width = houserent_theme_get_customizer_field('blog_layout_column_width','9');
                $collumn_class = 'col-md-'.$theme_settings_column_width.' full-width-col';
            }
        }
        return $blog_page_layout;
    }
endif;



/**
 ** Function for conditional header style changing.
 **/
if( ! function_exists( 'houserent_theme_header_style_swicher_swicher' ) ):
    function houserent_theme_header_style_swicher() {
        // Header URL Conditional Change.
        $layout_header_url = ( isset($_GET["header-style"]) ) ?$_GET["header-style"]  : "";
        if ( $layout_header_url ) {
            return $header_part = $layout_header_url;
        } else {
            return $header_part = houserent_theme_get_customizer_field('header_layout', 'header_variation', 'one');
        }
    }
endif;


/**
 ** Functions for save extra fields for author profile page.
 **/
if( !function_exists('houserent_theme_site_logo')):
    function houserent_theme_site_logo(){
        ?>
            <div class="site-logo">
                    <?php 
                        $custom_logo = houserent_theme_get_customizer_field( 'header_logo','url', false );
                        if($custom_logo):
                    ?>
                         <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo-block">
                            <img src="<?php echo houserent_theme_get_customizer_field( 'header_logo','url', '' ); ?>" alt="<?php echo bloginfo('name'); ?>">
                        </a>
                    <?php 
                        else: 
                        $description = get_bloginfo( 'description', 'display' );
                    ?>
                        <h1 class="site-title">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                            <?php if($description): ?>
                                <span class="site-description"><?php echo esc_html( $description); ?></span>
                            <?php endif; ?>
                        </h1>
                    <?php endif; ?>
            </div>
        <?php
    }
endif;


/**
 ** Functions for save extra fields for author profile page.
 **/
if( !function_exists('houserent_theme_mag_excerpt_length')):
    function houserent_theme_mag_excerpt_length($str, $limit) {
        $excerpt = explode(' ', $str, $limit);
        if (count($excerpt)>=$limit) {
            array_pop($excerpt);
            $excerpt = implode(" ",$excerpt).'...';
        } else {
            $excerpt = implode(" ",$excerpt);
        }
        $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
        return $excerpt;
    }
endif;


/**
 * Optimize contact form 7 Scripts
 * Remove contact form 7 Generator tag, styles, and scripts from non contact form 7 pages.
 */
if (!function_exists('houserent_theme_get_all_rental_item') ):
    function houserent_theme_get_all_rental_item(){
        $q = array();
        $args = array(
            'posts_per_page' => '-1',
            'post_type' => 'rental',
        );

        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ):
            while ( $the_query->have_posts() ) : $the_query->the_post(); 
                $q[] = get_post_meta( get_the_ID(),'rental_location')[0];
            endwhile;
        endif;
        wp_reset_postdata();
        return array_unique($q);
    }
endif;

 
/* Convert hexdec color string to rgb(a) string */
if (!function_exists('houserent_theme_hex2rgba') ):
    function houserent_theme_hex2rgba($color, $opacity = false) {
     
        $default = 'rgb(0,0,0)';
     
        //Return default if no color provided
        if(empty($color))
              return $default; 
     
        //Sanitize $color if "#" is provided 
            if ($color[0] == '#' ) {
                $color = substr( $color, 1 );
            }
     
            //Check if color has 6 or 3 characters and get values
            if (strlen($color) == 6) {
                    $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
            } elseif ( strlen( $color ) == 3 ) {
                    $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
            } else {
                    return $default;
            }
     
            //Convert hexadec to rgb
            $rgb =  array_map('hexdec', $hex);
     
            //Check if opacity is set(rgba or rgb)
            if($opacity){
                if(abs($opacity) > 1)
                    $opacity = 1.0;
                $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
            } else {
                $output = 'rgb('.implode(",",$rgb).')';
            }
     
            //Return rgb(a) color string
            return $output;
    }
endif;




if ( !is_page( 'confirm-booking' ) && is_admin() ){ 

    $new_page_title = esc_html__( 'Confirm Booking', 'houserent' );
    $new_page_content = esc_html__( 'This is the booking page content', 'houserent' );
    $new_page_template = 'template-booking.php'; 

    $page_check = get_page_by_title($new_page_title); 
    $new_page = array( 
        'post_type' => 'page', 
        'post_title' => $new_page_title, 
        'post_content' => $new_page_content, 
        'post_status' => 'publish', 
        'post_author' => 1, 
    );
    if(!isset($page_check->ID)){ 
        $new_page_id = wp_insert_post($new_page); 
        if(!empty($new_page_template)){ 
            update_post_meta($new_page_id, '_wp_page_template', $new_page_template); 
        } 
    } 
}
