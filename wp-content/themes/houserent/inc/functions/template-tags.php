<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Houserent
 */

/**
 * This is for post pagination
**/
if ( ! function_exists( 'houserent_theme_posts_pagination_nav' ) ) :

    function houserent_theme_posts_pagination_nav() {
        if ( is_singular() )
            return;

        global $wp_query;

        /** Stop execution if there's only 1 page */
        if( $wp_query->max_num_pages <= 1 )
            return;

        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $max   = intval( $wp_query->max_num_pages );

        /** Add current page to the array */
        if ( $paged >= 1 )
            $links[] = $paged;

        /** Add the pages around the current page to the array */
        if ( $paged >= 3 ) {
            $links[] = $paged - 1;
            $links[] = $paged - 2;
        }

        if ( ( $paged + 2 ) <= $max ) {
            $links[] = $paged + 2;
            $links[] = $paged + 1;
        }
        echo '<div class="pagination-link"> <ul class="pagination">' . "\n";

        /** Previous Post Link */
        if ( get_previous_posts_link() ) {
            printf( '<li class="nav-previous pull-left">%s</li>' . "\n", get_previous_posts_link('<i class="fa fa-angle-left"></i>' ) );
        } else { ?>
            <li class="nav-previous pull-left disabled">
                <a href="#"><i class="fa fa-angle-left"></i></a>
            </li>
        <?php }

      
        /** Link to first page, plus ellipses if necessary */
        if ( ! in_array( 1, $links ) ) {
            $class = 1 == $paged ? ' class="active"' : '';

            printf( '<li%s><a class="page-numbers" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

            if ( ! in_array( 2, $links ) )
                echo '<li><span class="page-numbers dots">&#46;&#46;&#46;</span></li>';
        }

        /** Link to current page, plus 2 pages in either direction if necessary */
        sort( $links );
        foreach ( (array) $links as $link ) {
            $class = $paged == $link ? ' class="active"' : '';
            printf( '<li%s><a class="page-numbers" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
        }

        /** Link to last page, plus ellipses if necessary */
        if ( ! in_array( $max, $links ) ) {
            if ( ! in_array( $max - 1, $links ) )
                echo '<li><span class="page-numbers dots">&hellip;</span></li>' . "\n";

            $class = $paged == $max ? ' class="active"' : '';
            printf( '<li%s><a class="page-numbers curent" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
        }

        /** Next Post Link */
        if ( get_next_posts_link() ) {
            printf( '<li class="nav-next pull-right" data-page-number="'.esc_attr($paged+1).'">%s</li>' . "\n", get_next_posts_link('<i class="fa fa-angle-right"></i>' ) );
        } else { ?>
            <li class="nav-next pull-right disabled">
                <a href="#"><i class="fa fa-angle-right"></i></a>
            </li>
        <?php }
        echo '</ul></div>' . "\n";
    }
endif;


/**
** Prints post format icon
**/
if ( ! function_exists( 'houserent_theme_tag_list' ) ) :
    function houserent_theme_tag_list() {
        if ( has_tag() ) :
            ?>
            <div class="tag clearfix">
                <span class="tags"><?php esc_html_e('Topics:', 'houserent'); ?></span>
                <?php 
                echo get_the_tag_list("", "", "");
                ?>
            </div> <!-- /.tag -->
            <?php
        endif;
    }
endif;


/**
** Prints HTML with meta information for the current post-date/time and author.
**/
if ( ! function_exists( 'houserent_theme_rental_share' ) ) :
    function houserent_theme_rental_share() {
        ?>
          <ul>
             <li class="dropup">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share-alt"></i></a>
                 <ul class="dropdown-menu">
                     <li>
                         <a class="customer share" rel="nofollow" title="<?php esc_html_e(  'Share on Facebook', 'houserent' ); ?>" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
                            <i class="fa fa-facebook"></i>
                         </a>
                     </li>
                     <li>
                         <a class="customer share" rel="nofollow" title="<?php esc_html_e(  'Share on Twitter', 'houserent' ); ?>" href="https://twitter.com/home?status=<?php the_permalink(); ?>" >
                             <i class="fa fa-twitter"></i>
                         </a>
                     </li>
                     <li>
                         <a class="customer share" rel="nofollow" title="<?php esc_html_e(  'Share on GooglePlus', 'houserent' ); ?>" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" >
                         <i class="fa fa-google-plus"></i>
                         </a>
                     </li>
                     <li>
                         <a class="customer share" rel="nofollow" title="<?php esc_html_e(  'Share on Linkedin', 'houserent' ); ?>" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" >
                         <i class="fa fa-linkedin"></i>
                         </a>
                     </li>
                     <li>
                         <a rel="nofollow" title="<?php esc_html_e(  'Share on Pinterest', 'houserent' ); ?>" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
                                <i class="fa fa-pinterest-p"></i>
                            </a>
                     </li>
                 </ul>
             </li>
                <li>
                    <?php sh_hr_rental_favorite_btn( get_the_ID() ); ?>
                </li>
         </ul> 
        <?php
    }
endif;

/**
** Prints HTML with meta information for the current post-date/time and author.
**/
if ( ! function_exists( 'houserent_theme_social_share_link' ) ) :
    function houserent_theme_social_share_link() {

        ?>
        <div class="entry-share">
            <span><?php esc_html_e( 'Share: ', 'houserent' ); ?></span>

                <a class="customer share" rel="nofollow" title="<?php esc_html_e(  'Share on Facebook', 'houserent' ); ?>" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
                   <i class="fa fa-facebook"></i>
                </a>

                <a class="customer share" rel="nofollow" title="<?php esc_html_e(  'Share on Twitter', 'houserent' ); ?>" href="https://twitter.com/home?status=<?php the_permalink(); ?>" >
                    <i class="fa fa-twitter"></i>
                </a>

                <a class="customer share" rel="nofollow" title="<?php esc_html_e(  'Share on GooglePlus', 'houserent' ); ?>" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" >
                <i class="fa fa-google-plus"></i>
                </a>

                <a class="customer share" rel="nofollow" title="<?php esc_html_e(  'Share on Linkedin', 'houserent' ); ?>" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" >
                <i class="fa fa-linkedin"></i>
                </a>

                <a rel="nofollow" title="<?php esc_html_e(  'Share on Pinterest', 'houserent' ); ?>" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
                       <i class="fa fa-pinterest-p"></i>
                   </a> 
        </div><!-- /.share-button -->
<?php    }
endif;


/**
** To show wp_link_pages
**/
if ( ! function_exists( 'houserent_theme_wp_link_pages' ) ) :
    function houserent_theme_wp_link_pages() {
        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'houserent' ),
            'after'  => '</div>',
            'link_before' => '<span>',
            'link_after'  => '</span>',
        ) );
    }
endif;




/**
** Shim for houserent_theme_archive_title().
**/
if ( ! function_exists( 'houserent_theme_archive_title' ) ) :
    function houserent_theme_archive_title( $before = '', $after = '' ) {
        $allowed_html_array = array(
            'span' => array(
                'class' => array(),
            )
        );
        if ( is_category() ) {
            $title = sprintf( wp_kses( __( '<span class="browsing-title">Browsing Category</span>', 'houserent' ), $allowed_html_array ).esc_html__( '%s', 'houserent' ), single_cat_title( '', false ) );
        } elseif ( is_tag() ) {
            $title = sprintf( wp_kses( __( '<span class="browsing-title">Browsing Tag</span>', 'houserent' ), $allowed_html_array ).esc_html__( '%s', 'houserent' ), single_tag_title( '', false ) );
        } elseif ( is_author() ) {
            $title = sprintf( wp_kses( __( '<span class="browsing-title">Browsing Author</span>', 'houserent' ), $allowed_html_array ).esc_html__( '%s', 'houserent' ), get_the_author() );
        } elseif ( is_year() ) {
            $title = sprintf( wp_kses( __( '<span class="browsing-title">Browsing Year</span>', 'houserent' ), $allowed_html_array ).esc_html__( '%s', 'houserent' ), get_the_date( esc_html_x( 'Y', 'yearly archives date format', 'houserent' ) ) );
        } elseif ( is_month() ) {
            $title = sprintf( wp_kses( __( '<span class="browsing-title">Browsing Month</span>', 'houserent' ), $allowed_html_array ).esc_html__( '%s', 'houserent' ), get_the_date( esc_html_x( 'F Y', 'monthly archives date format', 'houserent' ) ) );
        } elseif ( is_day() ) {
            $title = sprintf( wp_kses( __( '<span class="browsing-title">Browsing Day</span>', 'houserent' ), $allowed_html_array ).esc_html__( '%s', 'houserent' ), get_the_date( esc_html_x( 'F j, Y', 'daily archives date format', 'houserent' ) ) );
        } elseif ( is_tax( 'post_format' ) ) {
            if ( is_tax( 'post_format', 'post-format-aside' ) ) {
                $title = wp_kses( __( '<span class="browsing-title">Browsing Post Format</span>', 'houserent' ), $allowed_html_array ).esc_html_x( 'Aside', 'post format archive title', 'houserent' );
            } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
                $title = wp_kses( __( '<span class="browsing-title">Browsing Post Format</span>', 'houserent' ), $allowed_html_array ).esc_html_x( 'Gallery', 'post format archive title', 'houserent' );
            } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
                $title = wp_kses( __( '<span class="browsing-title">Browsing Post Format</span>', 'houserent' ), $allowed_html_array ).esc_html_x( 'Image', 'post format archive title', 'houserent' );
            } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
                $title = wp_kses( __( '<span class="browsing-title">Browsing Post Format</span>', 'houserent' ), $allowed_html_array ).esc_html_x( 'Video', 'post format archive title', 'houserent' );
            } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
                $title = wp_kses( __( '<span class="browsing-title">Browsing Post Format</span>', 'houserent' ), $allowed_html_array ).esc_html_x( 'Quote', 'post format archive title', 'houserent' );
            } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
                $title = wp_kses( __( '<span class="browsing-title">Browsing Post Format</span>', 'houserent' ), $allowed_html_array ).esc_html_x( 'Link', 'post format archive title', 'houserent' );
            } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
                $title = wp_kses( __( '<span class="browsing-title">Browsing Post Format</span>', 'houserent' ), $allowed_html_array ).esc_html_x( 'Status', 'post format archive title', 'houserent' );
            } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
                $title = wp_kses( __( '<span class="browsing-title">Browsing Post Format</span>', 'houserent' ), $allowed_html_array ).esc_html_x( 'Audio', 'post format archive title', 'houserent' );
            } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
                $title = wp_kses( __( '<span class="browsing-title">Browsing Post Format</span>', 'houserent' ), $allowed_html_array ).esc_html_x( 'Chat', 'post format archive title', 'houserent' );
            }
        } elseif ( is_post_type_archive() ) {
            $title = sprintf( __( '<span class="browsing-title">Browsing Archives</span>', 'houserent' ).esc_html__( '%s', 'houserent' ), post_type_archive_title( '', false ) );
        } elseif ( is_tax() ) {
            $tax = get_taxonomy( get_queried_object()->taxonomy );
            /* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
            $title = sprintf( esc_html__( '%1$s: %2$s', 'houserent' ), $tax->labels->singular_name, single_term_title( '', false ) );
        } else {
            $title = esc_html__( 'Browsing Archives', 'houserent' );
        }

        /**
         * Filter the archive title.
         *
         * @param string $title Archive title to be displayed.
         */
        $title = apply_filters( 'get_the_archive_title', $title );

        if ( ! empty( $title ) ) {
            echo ( $before . $title . $after );  // WPCS: XSS OK
        }
    }
endif;


/**
** Shim for the_archive_description().
** Display category, tag, or term description.
**/
if ( ! function_exists( 'the_archive_description' ) ) :
    function the_archive_description( $before = '', $after = '' ) {
    	$description = apply_filters( 'get_the_archive_description', term_description() );

    	if ( ! empty( $description ) ) {
    		/**
    		 * Filter the archive description.
    		 *
    		 * @see term_description()
    		 *
    		 * @param string $description Archive description to be displayed.
    		 */
    		echo houserent_theme_esc_variable( $before ) . $description . $after;  // WPCS: XSS OK
    	}
    }
endif;


/**
** Returns true if a blog has more than 1 category.
**/
if ( ! function_exists( 'houserent_theme_categorized_blog' ) ) :
    function houserent_theme_categorized_blog() {
    	if ( false === ( $all_the_cool_cats = get_transient( 'houserent_theme_categories' ) ) ) {
    		// Create an array of all the categories that are attached to posts.
    		$all_the_cool_cats = get_categories( array(
    			'fields'     => 'ids',
    			'hide_empty' => 1,

    			// We only need to know if there is more than one category.
    			'number'     => 2,
    		) );

    		// Count the number of categories that are attached to the posts.
    		$all_the_cool_cats = count( $all_the_cool_cats );

    		set_transient( 'houserent_theme_categories', $all_the_cool_cats );
    	}

    	if ( $all_the_cool_cats > 1 ) {
    		// This blog has more than 1 category so houserent_theme_categorized_blog should return true.
    		return true;
    	} else {
    		// This blog has only 1 category so houserent_theme_categorized_blog should return false.
    		return false;
    	}
    }
endif;


/**
** Flush out the transients used in houserent_theme_categorized_blog.
**/
if ( ! function_exists( 'houserent_theme_category_transient_flusher' ) ) :
    function houserent_theme_category_transient_flusher() {
    	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    		return;
    	}
    	// Like, beat it. Dig?
    	delete_transient( 'houserent_theme_categories' );
    }
    add_action( 'edit_category', 'houserent_theme_category_transient_flusher' );
    add_action( 'save_post',     'houserent_theme_category_transient_flusher' );
endif;