<?php
/**
 ** Functions for set post views.
 **/
if ( ! function_exists( 'houserent_theme_set_post_views' ) ) :
    function houserent_theme_set_post_views($postID) {
        $count_key = 'houserent_theme_post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if ( $count == '' ){
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '1');
        } else {
            $count++;
            update_post_meta($postID, $count_key, $count);
        }
    }
endif;


/**
 ** Functions for track post views.
 **/
if ( ! function_exists( 'houserent_theme_track_post_views' ) ) :
    function houserent_theme_track_post_views ($post_id) {
        if ( !is_single() ) return;
        if ( empty ( $post_id ) ) {
            global $post;
            $post_id = $post->ID;    
        }
        houserent_theme_set_post_views($post_id);
    }
    add_action( 'wp_head', 'houserent_theme_track_post_views');
endif;


/**
 ** Functions for get post views.
 **/
if ( ! function_exists( 'houserent_theme_get_post_views' ) ) :
    function houserent_theme_get_post_views($postID){
        $count_key = 'houserent_theme_post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if( $count == "" ) {
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
            return esc_html__('0','houserent');
        } elseif( $count == 1 ) {
            return esc_html__('1','houserent');
        }
        return intval($count/2);
    }
endif;


/**
 ** Functions for add column in WP-Admin
 **/
if ( ! function_exists( 'houserent_theme_posts_column_views' ) ) :
    function houserent_theme_posts_column_views($defaults){
        $defaults['post_views'] = esc_html__('Views', 'houserent');
        return $defaults;
    }
    add_filter('manage_posts_columns', 'houserent_theme_posts_column_views');
endif;

/**
 ** Functions for posts custom column views.
 **/
if ( ! function_exists( 'houserent_theme_posts_custom_column_views' ) ) :
    function houserent_theme_posts_custom_column_views($column_name, $id){
        if ($column_name === 'post_views') {
           echo houserent_theme_get_post_views(get_the_ID());
        }
    }
    add_action('manage_posts_custom_column', 'houserent_theme_posts_custom_column_views',5,2);
endif;