<?php get_header(); ?>
    <?php 
    if ( function_exists('the_gutenberg_project') && has_blocks( get_the_ID() ) ) { ?>
    <!-- ====== Page Header ====== --> 
    <div class="page-header default-template-gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                
                    <h2 class="page-title"><?php esc_html_e('Article Details','houserent'); ?></h2>
                </div>
            </div><!-- /.row-->
        </div><!-- /.container-fluid -->           
    </div><!-- /.page-header -->
    <!-- ======single-content-area====== --> 
    <div class="single-content bg-gray-color">
        <div class="container-fluid">
            <div class="single-page-gutenber-content">
                <?php if ( have_posts() ) : ?>

                <?php /* Start the Loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <?php
                        /* Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        get_template_part( 'template-parts/content', 'single' );
                    ?>

                <?php endwhile; ?>

                <?php else : ?>

                    <?php get_template_part( 'template-parts/content', 'none' ); ?>

                <?php endif; ?>

                <!-- ====== comment-area ====== --> 
                <?php
                     // If comments are open or we have at least one comment, load up the comment template
                     if ( comments_open() || get_comments_number() ) :
                       comments_template();
                     endif;
                 ?>
            </div><!-- /.col-md-12 -->
        </div><!--  /.container-fluid -->
    </div>
    <?php } else { ?>
    <?php 
        $breadcrumbs_single = houserent_theme_get_customizer_field('breadcrumbs_single','hide');
        $single_page_related_post = houserent_theme_get_customizer_field('single_page_related_post','hide');

        // Post Layout URL Condition Check. 
        $layout_single_url = ( isset($_GET["single-layout"]) ) ? $_GET["single-layout"] : "";
        $single_page_meta = houserent_theme_uny_post_meta('meta_post_layout','default');
        $theme_settings_panel = houserent_theme_get_customizer_field('single_post_page_layout','full_width');
        if ( $layout_single_url ) {
            $single_page_sidebar_condition = $layout_single_url;
        } elseif ( $single_page_meta != 'default' ) {
            $single_page_sidebar_condition = $single_page_meta;
        } elseif ( $theme_settings_panel ) {
            $single_page_sidebar_condition = $theme_settings_panel;
        } else {
            $single_page_sidebar_condition = 'full_width';
        }

        switch ( $single_page_sidebar_condition ) {
            case 'sidebar_left':
                $single_page_content_class = 'col-md-8 col-md-push-4';
                $single_page_class = 'left-sidebar';
                break;
            case 'sidebar_right':
                $single_page_content_class = 'col-md-8';
                $single_page_class = 'right-sidebar';
                break;
            case 'full_width':
                $theme_settings_column_width = houserent_theme_get_customizer_field('single_post_page_column_width','12');
                $single_page_content_class = 'col-md-'.$theme_settings_column_width.' full-width-content';
                $single_page_class = 'full_width';
                break;
            
            default:
                $single_page_content_class = 'col-md-8';
                $single_page_class = 'right-sidebar';
                break;
        }

        $comm_class = ( comments_open() ) ? ' comment_yes' : ' comment_no' ;
    ?>
    <!-- ====== Page Header ====== --> 
    <div class="page-header default-template-gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                
                    <h2 class="page-title"><?php esc_html_e('Article Details','houserent'); ?></h2>
                </div>
            </div><!-- /.row-->
        </div><!-- /.container-fluid -->           
    </div><!-- /.page-header -->

    <?php if( function_exists( 'fw_ext_get_breadcrumbs' ) && $breadcrumbs_single == 'show' ): ?>
        <div class="breadcrumbs-area bg-gray-color">
           <div class="container">
               <div class="row">
                   <div class="col-md-12">
                       <?php echo fw_ext_get_breadcrumbs( '>' ); ?>
                   </div><!-- /.col-md-12 -->
               </div><!-- /.row -->
           </div><!-- /.container -->
       </div>
    <?php endif; ?>

    <!-- ======single-content-area====== --> 
    <div class="single-content bg-gray-color <?php esc_attr( $comm_class ); ?>">
        <div class="container">
            <div class="row">
                <div class="<?php echo esc_attr( $single_page_content_class ); ?>">
                    <?php if ( have_posts() ) : ?>

                    <?php /* Start the Loop */ ?>
                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php
                            /* Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part( 'template-parts/content', 'single' );
                        ?>

                    <?php endwhile; ?>

                    <?php else : ?>

                        <?php get_template_part( 'template-parts/content', 'none' ); ?>

                    <?php endif; ?>
                </div><!-- /.col-md-12 -->
                <?php get_sidebar(); ?>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.single-content -->

    <?php 
        if( $single_page_related_post == 'show' ):
            get_template_part( 'template-parts/content', 'relatedpost-single' ); 
        endif;
    ?>

   <!-- ====== comment-area ====== --> 
   <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || get_comments_number() ) :
          comments_template();
        endif;
    ?>

    <?php } ?>

<?php get_footer(); ?>