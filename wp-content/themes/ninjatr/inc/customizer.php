<?php
/**
 * Fincorp Theme Customizer
 *
 * @package Fincorp
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function fincorp_customize_register( $wp_customize ) {
	
function fincorp_sanitize_checkbox( $checked ) {
	// Boolean check.
	return ( ( isset( $checked ) && true == $checked ) ? true : false );
}
	
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
		
	$wp_customize->add_setting('color_scheme', array(
		'default' => '#5747cb',
		'sanitize_callback'	=> 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,'color_scheme',array(
			'label' => __('Color Scheme','fincorp'),
			'description'	=> __('Select color from here.','fincorp'),
			'section' => 'colors',
			'settings' => 'color_scheme'
		))
	);
	
	$wp_customize->add_setting('bx-color', array(
		'default' => '#5747cb',
		'sanitize_callback'	=> 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,'bx-color',array(
			'description'	=> __('Select color for facility box title and button.','fincorp'),
			'section' => 'colors',
			'settings' => 'bx-color'
		))
	);
	
	// Slider Section Start		
	$wp_customize->add_section(
        'slider_section',
        array(
            'title' => __('Slider Settings', 'fincorp'),
            'priority' => null,
			'description'	=> __('Recommended image size (1420x567). Slider will work only when you select the static front page.','fincorp'),	
        )
    );
	
	$wp_customize->add_setting('page-setting7',array(
			'default' => '0',
			'capability' => 'edit_theme_options',
			'sanitize_callback'	=> 'absint'
	));
	
	$wp_customize->add_control('page-setting7',array(
			'type'	=> 'dropdown-pages',
			'label'	=> __('Select page for slide one:','fincorp'),
			'section'	=> 'slider_section'
	));	
	
	$wp_customize->add_setting('page-setting8',array(
			'default' => '0',
			'capability' => 'edit_theme_options',	
			'sanitize_callback'	=> 'absint'
	));
	
	$wp_customize->add_control('page-setting8',array(
			'type'	=> 'dropdown-pages',
			'label'	=> __('Select page for slide two:','fincorp'),
			'section'	=> 'slider_section'
	));	
	
	$wp_customize->add_setting('page-setting9',array(
			'default' => '0',
			'capability' => 'edit_theme_options',	
			'sanitize_callback'	=> 'absint'
	));
	
	$wp_customize->add_control('page-setting9',array(
			'type'	=> 'dropdown-pages',
			'label'	=> __('Select page for slide three:','fincorp'),
			'section'	=> 'slider_section'
	));	
	
	$wp_customize->add_setting('slide_text',array(
		'default'	=> __('Our Services','fincorp'),
		'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('slide_text',array(
		'label'	=> __('Add slider link button text.','fincorp'),
		'section'	=> 'slider_section',
		'setting'	=> 'slide_text',
		'type'	=> 'text'
	));
	
	$wp_customize->add_setting('hide_slider',array(
			'default' => true,
			'sanitize_callback' => 'fincorp_sanitize_checkbox',
			'capability' => 'edit_theme_options',
	));	 

	$wp_customize->add_control( 'hide_slider', array(
		   'settings' => 'hide_slider',
    	   'section'   => 'slider_section',
    	   'label'     => __('Check this to hide slider.','fincorp'),
    	   'type'      => 'checkbox'
     ));	
	
	// Slider Section End
	
	// Homepage Section Start		
	$wp_customize->add_section(
        'homepage_section',
        array(
            'title' => __('Services Section', 'fincorp'),
            'priority' => null,
			'description'	=> __('Select pages from services boxes. This boxes will be displayed only when you select the static front page.','fincorp'),	
        )
    );	
	
	$wp_customize->add_setting('section-title',array(
			'default' => __('We are experienced banking services provider','fincorp'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('section-title',array(
			'type'	=> 'text',
			'label'	=> __('Add section title here.','fincorp'),
			'section'	=> 'homepage_section'
	));
	
	$wp_customize->add_setting('page-setting1',array(
			'default' => '0',
			'capability' => 'edit_theme_options',
			'sanitize_callback'	=> 'absint'
	));
	
	$wp_customize->add_control('page-setting1',array(
			'type'	=> 'dropdown-pages',
			'label'	=> __('Select page for box 1:','fincorp'),
			'section'	=> 'homepage_section'
	));	
	
	$wp_customize->add_setting('page-setting2',array(
			'default' => '0',
			'capability' => 'edit_theme_options',	
			'sanitize_callback'	=> 'absint'
	));
	
	$wp_customize->add_control('page-setting2',array(
			'type'	=> 'dropdown-pages',
			'label'	=> __('Select page for box 2:','fincorp'),
			'section'	=> 'homepage_section'
	));	
	
	$wp_customize->add_setting('page-setting3',array(
			'default' => '0',
			'capability' => 'edit_theme_options',	
			'sanitize_callback'	=> 'absint'
	));
	
	$wp_customize->add_control('page-setting3',array(
			'type'	=> 'dropdown-pages',
			'label'	=> __('Select page for box 3:','fincorp'),
			'section'	=> 'homepage_section'
	));		
	
	$wp_customize->add_setting('hide_services',array(
			'default' => true,
			'sanitize_callback' => 'fincorp_sanitize_checkbox',
			'capability' => 'edit_theme_options',
	));	 

	$wp_customize->add_control( 'hide_services', array(
		   'settings' => 'hide_services',
    	   'section'   => 'homepage_section',
    	   'label'     => __('Check this to hide services section.','fincorp'),
    	   'type'      => 'checkbox'
     ));
	 
// Contact Section

	$wp_customize->add_section(
        'contact_section',
        array(
            'title' => __('Contact Info', 'fincorp'),
            'priority' => null,
			'description'	=> __('Add your contact info here.','fincorp'),	
        )
    );	
	
	$wp_customize->add_setting('phone-txt',array(
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('phone-txt',array(
			'type'	=> 'text',
			'label'	=> __('Add phone number here.','fincorp'),
			'section'	=> 'contact_section'
	));
	
	$wp_customize->add_setting('email-txt',array(
			'sanitize_callback'	=> 'sanitize_email'
	));
	
	$wp_customize->add_control('email-txt',array(
			'type'	=> 'text',
			'label'	=> __('Add email address here.','fincorp'),
			'section'	=> 'contact_section'
	));
	
	$wp_customize->add_setting('address-txt',array(
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('address-txt',array(
			'type'	=> 'text',
			'label'	=> __('Add address here.','fincorp'),
			'section'	=> 'contact_section'
	));
	
	$wp_customize->add_setting('street-txt',array(
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('street-txt',array(
			'type'	=> 'text',
			'label'	=> __('Add street name here.','fincorp'),
			'section'	=> 'contact_section'
	));
	
	$wp_customize->add_setting('top-txt',array(
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('top-txt',array(
			'type'	=> 'text',
			'label'	=> __('Add top bar left text here.','fincorp'),
			'section'	=> 'contact_section'
	));
	
	$wp_customize->add_setting('time-txt',array(
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('time-txt',array(
			'type'	=> 'text',
			'label'	=> __('Add top bar right text here.','fincorp'),
			'section'	=> 'contact_section'
	));
	
}
add_action( 'customize_register', 'fincorp_customize_register' );	

function fincorp_css(){
		?>
        <style>
				a, 
				.tm_client strong,
				.postmeta a:hover,
				#sidebar ul li a:hover,
				.blog-post h3.entry-title,
				.sitenav ul li a:hover,
				.sitenav ul li:hover > ul li a:hover{
					color:<?php echo esc_html(get_theme_mod('color_scheme','#5747cb')); ?>;
				}
				a.blog-more:hover,
				.nav-links .current, 
				.nav-links a:hover,
				#commentform input#submit,
				input.search-submit,
				.nivo-controlNav a.active,
				.blog-date .date,
				a.read-more,
				.header-top,
				.copyright-wrapper{
					background-color:<?php echo esc_html(get_theme_mod('color_scheme','#5747cb')); ?>;
				}
				.fourbox h3::after{
					background-color:<?php echo esc_html(get_theme_mod('bx-color','#e04622')); ?>;
				}
				.fourbox:hover h3{
					color:<?php echo esc_html(get_theme_mod('bx-color','#e04622')); ?>;
				}
				.fourbox:hover .pagemore{
					background-color:<?php echo esc_html(get_theme_mod('bx-color','#e04622')); ?>;
					border:1px solid <?php echo esc_html(get_theme_mod('bx-color','#e04622')); ?>;
				}
				
		</style>
	<?php }
add_action('wp_head','fincorp_css');

function fincorp_customize_preview_js() {
	wp_enqueue_script( 'fincorp-customize-preview', get_template_directory_uri() . '/js/customize-preview.js', array( 'customize-preview' ), '20141216', true );
}
add_action( 'customize_preview_init', 'fincorp_customize_preview_js' );