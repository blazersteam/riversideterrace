<?php

/*
Plugin Name: My Plugin
Plugin URI: https://makitweb.com
Description: Plugin to demonstrate CSV import
Version: 0.1
Author: Yogesh singh
Author URI: https://makitweb.com
*/

// Create a new table
function plugin_table(){

   global $wpdb;
   $charset_collate = $wpdb->get_charset_collate();

   $tablename = $wpdb->prefix."customplugin";

   $sql = "CREATE TABLE $tablename (
     id mediumint(11) NOT NULL AUTO_INCREMENT,
     name varchar(80) NOT NULL,
     username varchar(80) NOT NULL,
     email varchar(80) NOT NULL,
     age smallint(3) NOT NULL,
     PRIMARY KEY (id)
   ) $charset_collate;";

   require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql );

}
register_activation_hook( __FILE__, 'plugin_table' );

// Add menu
function plugin_menu() {

   add_menu_page("My Plugin", "My Plugin","manage_options", "myplugin", "displayList",plugins_url('/myplugin/img/icon.png'));

}
add_action("admin_menu", "plugin_menu");

function displayList(){
   include "displaylist.php";
}
