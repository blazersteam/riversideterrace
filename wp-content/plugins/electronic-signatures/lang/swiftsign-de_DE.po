msgid ""
msgstr ""
"Project-Id-Version: electronic-signatures\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-27 13:17+0100\n"
"PO-Revision-Date: 2017-02-01 17:04+0530\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;gettext;gettext_noop;_e\n"
"X-Poedit-Basepath: ..\n"
"X-Generator: Poedit 1.8.11\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Last-Translator: \n"
"Language: de\n"
"X-Poedit-SearchPath-0: .\n"

#: admin/section/swiftsignature-messages.php:10
#: admin/section/swiftsignature-messages.php:22
msgid "Updates & Tips"
msgstr "Updates & Tips"

#: admin/section/swiftsignature-messages.php:11
msgid "Want to subscribe?"
msgstr "Want to subscribe?"

#: admin/section/swiftsignature-messages.php:12
msgid "Setting updated successfully."
msgstr "Setting updated successfully."

#: admin/section/swiftsignature-messages.php:13
msgid "Get Update News, Free Gifts, Best Practices, Tips, Tricks & More Privacy respected; emails never sold or shared. Emails sent approximately 1x/month."
msgstr "Erhalten Sie Update-Nachrichten, kostenlose Geschenke, Best Practices, Tipps, Tricks & mehr. Datenschutz gewährleistet; Wir verkaufen oder teilen E-Mail-Adressen nicht mit Dritten. Sie erhalten etwa eine E-Mail pro Monat"

#: admin/section/swiftsignature-messages.php:14
msgid "Enter email"
msgstr "E-Mail-Adresse eingeben"

#: admin/section/swiftsignature-messages.php:15
msgid "Hook Me Up & Keep Me Posted"
msgstr "Abonnieren & auf dem Laufenden bleiben"

#: admin/section/swiftsignature-messages.php:16
msgid "Updates, News & Best Practices"
msgstr "Updates, News & Best Practices"

#: admin/section/swiftsignature-messages.php:17
msgid "No feeds found..."
msgstr "No feeds found..."

#: admin/section/swiftsignature-messages.php:18
msgid "Want to auto-create the following pages to quickly get you set up?"
msgstr "Want to auto-create the following pages to quickly get you set up?"

#: admin/section/swiftsignature-messages.php:19
msgid "Settings"
msgstr "Einstellungen"

#: admin/section/swiftsignature-messages.php:20
#: admin/section/swiftsignature-messages.php:28
msgid "Signed Docs"
msgstr "Unterschriebene Dokumente"

#: admin/section/swiftsignature-messages.php:21
msgid "Help & Setup"
msgstr "Hilfe & Setup"

#: admin/section/swiftsignature-messages.php:23
msgid "Swift Signature Settings"
msgstr "Swift Signature Settings"

#: admin/section/swiftsignature-messages.php:24
msgid "Date Format"
msgstr "Datumsformat"

#: admin/section/swiftsignature-messages.php:25
msgid "Please see %s for our quick setup guide."
msgstr "Please see %s for our quick setup guide."

#: admin/section/swiftsignature-messages.php:26
msgid "A full list of supported shortcodes can be seen at %s"
msgstr "A full list of supported shortcodes can be seen at %s"

#: admin/section/swiftsignature-messages.php:27
msgid "Still have questions? See %s"
msgstr "Still have questions? See %s"

#: admin/section/swiftsignature-messages.php:29
msgid "Date"
msgstr "Datum"

#: admin/section/swiftsignature-messages.php:30
msgid "Page title"
msgstr "Page title"

#: admin/section/swiftsignature-messages.php:31
msgid "Signor"
msgstr "des Unterschreibenden"

#: admin/section/swiftsignature-messages.php:32
msgid "No records to report - yet."
msgstr "Kein Aufzeichnungen vorhanden - noch."

#: admin/section/swiftsignature-messages.php:33
msgid "Welcome to SwiftCloud's Electronic Signature  System"
msgstr "Welcome to SwiftCloud's Electronic Signature  System"

#: admin/section/swiftsignature-messages.php:34
msgid "Setup Instructions are at"
msgstr "Setup Instructions are at"

#: admin/section/swiftsignature-messages.php:35
msgid "We recommend setting up the basics first before adding more complex systems."
msgstr "We recommend setting up the basics first before adding more complex systems."

#: admin/section/swiftsignature-messages.php:36
msgid "Further help can be seen at"
msgstr "Further help can be seen at"

#: admin/section/swiftsignature-messages.php:37
msgid "A full list of shortcodes can be found at"
msgstr "A full list of shortcodes can be found at"

#: admin/section/swiftsignature-messages.php:38
msgid "General Settings"
msgstr "Allgemeine Einstellungen"

#: admin/section/swiftsignature-messages.php:39
msgid "Setup & Support"
msgstr "Setup & Kundenservice"

#: admin/section/swiftsignature-messages.php:40 #:
#: admin/section/swift-form-debug.php:8
msgid "Save Settings"
msgstr "Save Settings"

#: admin/section/swift-form-debug.php:9
msgid "Beta Sandbox Testing Mode (Not Recommended; used by staff for debugging)"
msgstr "Beta Sandbox Testing Mode (Not Recommended; used by staff for debugging)"

#: admin/section/swift-form-debug.php:10
msgid "Alert: SwiftCloud is in Sandbox Testing Mode: %s to end beta testing mode and use the stable release."
msgstr "Alert: SwiftCloud is in Sandbox Testing Mode: %s to end beta testing mode and use the stable release."

#: admin/section/swift-form-debug.php:11
msgid "Testing formID"
msgstr "Testing formID"

#: admin/section/swift-form-debug.php:12
msgid "Testing capture mode"
msgstr "Testing capture mode"

#: admin/section/swift-form-debug.php:13
msgid "Testing mode is on."
msgstr "Testing mode is on."

#: section/swiftsignature-front-messages.php:7
msgid "Sign  &  Send"
msgstr "Unterschreiben & Senden"

#: section/swiftsignature-front-messages.php:8
msgid "By clicking Sign & Send below you agree to the terms contained herein, and that terms are binding in your jurisdiction as defined in our %s %s"
msgstr "By clicking Sign & Send below you agree to the terms contained herein, and that terms are binding in your jurisdiction as defined in our %s %s"
