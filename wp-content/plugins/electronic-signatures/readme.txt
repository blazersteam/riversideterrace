=== Electronic Signature on SwiftCloud ===
Contributors: SwiftCloud
Donate link: https://SwiftCloud.AI/support/?pr=88
Tags: electronic signature, e-sign, e-signature, waivers
Requires at least: 4.5
Tested up to: 5.2
Stable tag: 1.0

This plugin helps integrate **Electronic Signature** on SwiftCloud with Wordpress.

== Description ==

SwiftCloud offers [shortcode] style Electronic Signature with a backend editor similar to Wordpress, but the actual electronic signature happens on SwiftCloud servers - with your logo, colors, etc - then typically returns the user back to your Wordpress.

The actual signature happens on our servers to ensure integrity and intent i.e. we could not allow potential CSS obfuscation of contracts, as well as solve every possible interference from themes or plugins, so the actual electronic signature now happens on our servers, though these can be fully branded to you and even cname-mapped i.e. https://Secure.YourDomainHere.com etc.

Another upside: Having an independent 3rd party hold your signed docs may be legally superior due to potential modification attacks and thus holding your own signed docs may not be legally defensible. For more information about this we recommend independent licensed legal advice.

**Version 2.4, Spring 2019 Update**
1. e-Signature of either Wordpress-style [shortcode-field] responsive docs OR PDFs (not great on phones, but match exact format i.e. government forms).
2. Full Account Branding with your logo, colors. Contact support for full URL mapping and email autoresponders to signors from your own domain.
3. Contacts manager - lookup any signor by name, email, phone and see their history and associated workrooms, which are auto-generated shared folders on our server.
4. Autoresponders to signors with PDF attachment of their signed doc(s)

SwiftCloud is perfect for sales contracts, liability waivers, permission slips, and more.

Wordpress Digital <a href="https://swiftcloud.ai/products/electronic-signature/?p=210P2?utm_source=viral&utm_medium=WP.org&utm_term=Electronic%20Signature&utm_content=WPplugin&utm_campaign=WPplugin" target="_new">**Electronic Signature Software**</a>, formerly https://SwiftSignature.com, part of <a href="https://swiftcloud.ai/products/electronic-signature/?p=210P2?utm_source=viral&utm_medium=WP.org&utm_term=Electronic%20Signature&utm_content=WPplugin&utm_campaign=WPplugin" target="_new">https://SwiftCloud.AI</a>.

5 minute setup is at <a href="https://SwiftCloud.AI/support/wordpress-electronic-signature-setup?utm_source=native_ads&utm_medium=Wordpress&utm_content=WPplugin&utm_campaign=WPplugin">https://SwiftCloud.AI/support/electronic-signature/wordpress-electronic-signature</a>

Note this plugin requires a <a href="https://swiftcloud.ai/products/electronic-signature/?p=210P2?site_url=https://SwiftCloud.AI/support/&utm_source=Wordpress.org&utm_medium=WPplugin&utm_campaign=WPplugin&pr=88&button=CCTAO" target="_new">SwiftCloud Account</a> (free or paid) for the electronic signatures to work. PDFs will be stored online in your signed wordpress page.

Our number 1 goal for this plugin is as part of a sales stack.

**2.0 10 MINUTE SETUP**

[youtube http://www.youtube.com/watch?v=DExc9qhjY7U]


**Electronic Signature FEATURES**

1. Legally binding electronic signature, timedate stamped with audit trail for court enforcement. Paid accounts have additional features for court deposition or appearance.
2. Single-page mode: Input form + electronic signature(s) on the same page (most common and simple)
3. Multi-page mode: Multi-step / Multi-Page Forms or where the signor info (i.e. name, price, etc.) is pre-filled (from CRM or another webform, even Excel spreadsheet, etc.) i.e. capture your users one form 1, then fill that into an electronic signature contract on step 2.
4. Can be included in a multi-step flow such as input form, then payment, then signature, or signature then payment, etc.
5. Signed documents are archived and held in trust by Swift Signature should court enforcement be needed (paid accounts required for court enforcement, though both free and paid accounts are legally binding in most countries including the U.S. and E.U.)
6. Multi-Party Consecutive or Concurrent (all-or-none, or partial allowed) signatures (Premium account required)
7. Notification and Automation options - add autoresponders, payment flows, and more
8. Sales flow sessions options available (i.e. real-time phone handoff to a client)
9. Conditional Logic: Show/Hide sections based on a checkbox, radio button option, circle-word (a type of radio button, technically, similar to circling answers on paper forms)
10. Editable / NonEditable fields for use with CRMs and other forms - pre-fill fields like name, email and allow the user to edit, and optionally pre-fill other fields but lock them as non-editable (i.e. purchase price, loan terms, etc.). TIP: Use these to pre-fill a price or terms into a sales contract, and it will look like regular text (not an input.. unless you want it to).

**5 Minute Electronic Signature Setup Wizard**
See https://SwiftCloud.AI/support/wordpress-electronic-signature-setup

More Help: See https://SwiftCloud.AI/support/tag/e-sign for our latest updates or help if with debugging and setup.

**SALES USE**

Note Swift Signature can be used for anything, but currently most of our clients are using it for sales or waivers.

The system is designed to allow pre-filling variables (as regular text i.e. for prices or visibly-editable fields (a user's name)).
See https://swiftcloud.ai/support/url-generator-help for help, and https://swiftcloud.ai/support/url-generator for the actual generator. TIP: You can even pre-define the URL generator for sales teams

Certain fields can be read-write (i.e. their name, in case the sales rep spells it wrong), others can be read-only (deal terms), then signed online. If needed, the sales rep could then further modify the data in SwiftCRM, and generate a final signable agreement.

We are a small developer-run company and welcome your ideas and feature requests! Hit us up at https://SwiftCloud.AI/support for anything you need.


== Installation ==
You probably know the drill by now, but if not, here's a step by step suggestion.

tip: See <a href="https://SwiftCloud.AI/support/wordpress-electronic-signature?site_url=https://SwiftCloud.AI/support/&utm_source=Wordpress.org&utm_medium=WPplugin&utm_campaign=WPplugin&pr=88&button=CCTAO">https://SwiftCloud.AI/support/wordpress-electronic-signature</a>

1. Upload the `Electronic Signatures` folder to the `/wp-content/plugins/` directory, or better yet, use Wordpress' native installer at Plugins >> Add New >> (search Swift Signature)
2. Activate the plugin through the 'Plugins' menu in WordPress
3. To install a webform, login at <a href="https://SwiftCloud.AI/support/checkout/product/88#s?site_url=https://SwiftCloud.AI/support/&utm_source=Wordpress.org&utm_medium=WPplugin&utm_campaign=WPplugin&pr=88&button=CCTAO" target="_new">http://SwiftCloud.AI</a> (free signup) and click 'new form',
drag and drop fields to create a form, click save, and then remember the number it gives you. Notice in SwiftForm you can add signature fields.
4. Embed this form into your Wordpress
5. Done.


== Frequently Asked Questions ==

1. How do I set this up in 5 minutes?

[youtube http://www.youtube.com/watch?v=DExc9qhjY7U]

Need more help? See https://SwiftCloud.AI/support

For more help, see our [SwiftCloud Support Section](https://SwiftCloud.AI/support) knowledgebase.

== Screenshots ==

1. Front-end Example

== Changelog ==

= 2.0.2 =
- Updated SwiftCloud.IO to SwiftCloud.AI

= 2.0.1 =
- Bug fixing.

= 2.0 =
- Removed all signature functionality

= 1.5.27 =
- Electronic Signature on Wordpress has been disabled. For the owner of this website, please migrate your doc(s) as per https://swiftcloud.ai/support/wordpress-e-signature-plugin-migration.

= 1.5.26 =
- Updated SwiftCloud form submission url

= 1.5.25 =
- Upgrade notice for SwiftCloud.AI e-Signature system.

= 1.5.24 =
- UI Improvement.

= 1.5.23 =
- UI Improvement.

= 1.5.22 =
- Added Upgrade notice for SwiftCloud e-Signature system.

= 1.5.21 =
- Bug fixing.

= 1.5.20 =
- Added option for thanks redirect.

= 1.5.19 =
- Added Tracking Lead report on dashboard.

= 1.5.18 =
- Added Admin settings for Work with Consumers or Businesses.

= 1.5.17 =
- Added shortcode for Affiliate / Source Name (Hidden).

= 1.5.16 =
- Added option for file upload.

= 1.5.15 =
- Bug fixing.

= 1.5.14 =
- Added 'Phone' field shortcode.

= 1.5.13 =
- Bug fixing.

= 1.5.12 =
- Updated input elements for required option.

= 1.5.11 =
- Allow multiple instance for Check to Agree option.

= 1.5.10 =
- Added new option "Swift Check to Agree".

= 1.5.9 =
- Updated Swiftcloud links to SC.AI.

= 1.5.8 =
- Added Local Capture option for esign.

= 1.5.7 =
- Bug fixing for typing modal.

= 1.5.6 =
- Update pre populate options for form fields with readonly option.

= 1.5.5 =
- Added pre populate options for form fields.

= 1.5.4 =
- Added option for Auto sign based on Name value.

= 1.5.3 =
- Preload Signature and Initial box when user enter their name.

= 1.5.2 =
- Added more options for Show/Hide fields for radio and dropdown.

= 1.5.1 =
- Bug fixing.

= 1.5.0 =
- Bug fixing.

= 1.4.28 =
- Added GMT time in e-signature form.

= 1.4.27 =
- Updated radio & checkbox list buttons with CSS only.
- Added new option: Conditional Logic Show/Hide based on Answers.

= 1.4.26 =
- Bug fixing.

= 1.4.25 =
- Added option for Beta Sandbox Testing Mode.

= 1.4.24 =
- Frontend Plugin translated in Spanish(Español) and Hindi(हिन�?दी) languages.

= 1.4.23 =
- Added swiftForm debug mode.
- Plugin translated in Spanish(Español) and Hindi(हिन�?दी) languages.

= 1.4.22 =
- Added new shortcode [swift_date_dropdown].

= 1.4.21 =
- Added select box for month and year in date picker.

= 1.4.20 =
- Bug fixing.

= 1.4.19 =
- Added option for Editable/NonEditable fields for name, email, text box, text area, URL.

= 1.4.18 =
- Added option for sizing (medium, full line, long) for name, text box, text area, URL.

= 1.4.17 =
- Bug fixing.

= 1.4.16 =
- Bug fixing.

= 1.4.15 =
- Updated help setup.

= 1.4.14=
- Bug fixing.

= 1.4.13=
- Local capture signature form.

= 1.4.12=
- UI changes.

= 1.4.11=
- Add new shortcode [swift_date_long] for long date formate.
- UI changes.

= 1.4.10=
- UI changes.

= 1.4.9=
- Auto generate eSign page.
- UI changes.

= 1.4.8=
- Bug fixing.

= 1.4.7=
- UI changes
- Bug fixing.

= 1.4.6=
- Auto generate page.

= 1.4.4 =
- Allow Typing and to choose from for signature and initials field.

= 1.3.6 =
UI changes.

= 1.3.5 =
UI changes.

= 1.3.4 =
UI changes.
Bug fixing.

= 1.3.3 =
Inclusion of product id.

= 1.3.2 =
Bug fixing.

= 1.3.1 =
Bug fixing.

= 1.3 =
UI changes.

= 1.2 =
Bug fixing.

= 1.1 =
Alpha Release

== Upgrade Notice ==

= 1.0 =
Basic Setup.

== Translations ==

* English - for now, that's all, but if interested we welcome some help! Contact us for a .pot file.