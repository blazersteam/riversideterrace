<?php
/*
Plugin Name: HouseRent Theme
Plugin URI: http://softhopper.net/plugins/houserent-theme-plugin
Description: This plugin contains essential data for houserent wordpress theme, To use HouseRent theme properly you must install this plugin.
Author: SoftHopper
Version: 1.6
Author URI: http://softhopper.net/
*/
/**
 * @package HouseRent Theme Plugin
 * @version 1.6
 */
function houserent_theme_custom_posts(){
    $rental_label = array(
        'name' => esc_html_x('Rental', 'Post Type General Name', 'houserent'),
        'singular_name' => esc_html_x('rental', 'Post Type Singular Name', 'houserent'),
        'menu_name' => esc_html__('Rental', 'houserent'),
        'parent_item_colon' => esc_html__('Parent Rental:', 'houserent'),
        'all_items' => esc_html__('All Rental', 'houserent'),
        'view_item' => esc_html__('View Rental', 'houserent'),
        'add_new_item' => esc_html__('Add New Rental', 'houserent'),
        'add_new' => esc_html__('New Rental', 'houserent'),
        'edit_item' => esc_html__('Edit Rental', 'houserent'),
        'update_item' => esc_html__('Update Rental', 'houserent'),
        'search_items' => esc_html__('Search Rentals', 'houserent'),
        'not_found' => esc_html__('No Rental found', 'houserent'),
        'not_found_in_trash' => esc_html__('No Rental found in Trash', 'houserent'),
    );
    $rental_args = array(
        'label' => esc_html__('Rental', 'houserent'),
        'description' => esc_html__('Rental', 'houserent'),
        'labels' => $rental_label,
        'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
        'taxonomies' => array('rental-category'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-building',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('rental', $rental_args);

    // Add new taxonomy, make it hierarchical (like categories) 
    $rental_taxonomy_labels = array(
        'name'              => esc_html__( 'Rental Categories','houserent' ),
        'singular_name'     => esc_html__( 'Rental Categories','houserent' ),
        'search_items'      => esc_html__( 'Search Rental Category','houserent' ),
        'all_items'         => esc_html__( 'All Rental Category','houserent' ),
        'parent_item'       => esc_html__( 'Parent Rental Category','houserent' ),
        'parent_item_colon' => esc_html__( 'Parent Rental Category:','houserent' ),
        'edit_item'         => esc_html__( 'Edit Rental Category','houserent' ),
        'update_item'       => esc_html__( 'Update Rental Category','houserent' ),
        'add_new_item'      => esc_html__( 'Add New Rental Category','houserent' ),
        'new_item_name'     => esc_html__( 'New Rental Category Name','houserent' ),
        'menu_name'         => esc_html__( 'Rental Category','houserent' ),
    );    

    // Now register the rental taxonomy
    register_taxonomy('rental_cat', array('rental'), array(
        'hierarchical' => true,
        'labels' => $rental_taxonomy_labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'rental_cat' ),
    ));
}

add_action('init', 'houserent_theme_custom_posts', 0);

//this function for Adding Booking Status On Post List
add_filter('manage_edit-rental_columns', 'houserent_post_data_columns');
function houserent_post_data_columns($columns) {
    $columns['booked'] = 'Booking Status';
    return $columns;
}

add_action('manage_posts_custom_column',  'houserent_show_columns');
function houserent_show_columns($name) {
    global $post;
    switch ($name) {
        case 'booked':
            $views = (get_post_meta($post->ID, 'rental_item_booked', true) == true) ? esc_html__('Booked', 'houserent') : '' ;
            $checkin = (get_post_meta($post->ID, 'rental_start_date', true) == true) ? esc_html__('Check In: ', 'houserent') . get_post_meta($post->ID, 'rental_start_date', true) : '' ;
            $checkOut = (get_post_meta($post->ID, 'rental_end_date', true) == true) ? esc_html__('Check In: ', 'houserent') . get_post_meta($post->ID, 'rental_end_date', true) : '' ;
            echo ( "<strong>" . $views . "</strong>" ."<br>". $checkin ."<br>". $checkOut );
    }
}

//this function for free estimated contact form
function houserent_theme_send_booking() {
    $item_url = $_REQUEST['item_url'];
    $customer_name = ($_REQUEST['customer_name']) ? $_REQUEST['customer_name'] : "";
    $customer_mobile = ($_REQUEST['customer_mobile']) ? $_REQUEST['customer_mobile'] : "";
    $customer_email = ($_REQUEST['customer_email']) ? $_REQUEST['customer_email'] : "";
    $customer_member = ($_REQUEST['customer_member']) ? $_REQUEST['customer_member'] : "";
    $customer_children = ($_REQUEST['customer_children']) ? $_REQUEST['customer_children'] : "";
    $customer_message = ($_REQUEST['customer_message']) ? $_REQUEST['customer_message'] : "";
    $customer_check_in = ($_REQUEST['customer_check_in']) ? $_REQUEST['customer_check_in'] : "";
    $customer_check_out = ($_REQUEST['customer_check_out']) ? $_REQUEST['customer_check_out'] : "";

    if ( get_theme_mod('fw_options')['rental_order_reci_mail'] != NULL ) {
        $rece_email =  get_theme_mod('fw_options')['rental_order_reci_mail'];
    } else {
        $rece_email = "";
    }
    $to = $rece_email;

    if(!empty($customer_name)) {
        $custName = esc_html__('Customer Name: ', 'houserent' ) . $customer_name;
    } else {
        $custName = "";
    }
    if(!empty($customer_mobile)) {
        $custMob = esc_html__('Phone No: ', 'houserent') . $customer_mobile;
    } else {
        $custMob = "";
    }   
    if(!empty($customer_email)) {
        $custMail = esc_html__('Email: ', 'houserent') . $customer_email;
    } else {
        $custMail = "";
    }    
    if(!empty($customer_member)) {
        $custMember = esc_html__('Family Member: ', 'houserent') . $customer_member;
    } else {
        $custMember = "";
    }    
    if(!empty($customer_children)) {
        $custChild = esc_html__('Children: ', 'houserent') . $customer_children;
    } else {
        $custChild = "";
    }

    if(!empty($customer_check_in)) {
        $custCheckIn = esc_html__('Check In: ', 'houserent') . $customer_check_in;
    } else {
        $custCheckIn = "";
    }    

    if(!empty($customer_check_out)) {
        $custCheckOut = esc_html__('Check Out: ', 'houserent') . $customer_check_out;
    } else {
        $custCheckOut = "";
    }

    $subject = sprintf(esc_html__('Booking Request Form %s Website', 'houserent' ), get_bloginfo('name'));
    $message = $custName . "\n\n" . $custMob . "\n\n" . $custMail . "\n\n" . $custCheckIn . "\n\n" . $custCheckOut . "\n\n" . $custMember . "\n\n" . $custChild . "\n\n" . esc_html__('Request Item: ', 'houserent') . $item_url . "\n\n" . esc_html__('Message: ', 'houserent') . $customer_message;
    wp_mail($to, $subject, $message);
    die(); // never forget to die() your AJAX reuqests
}
add_action('wp_ajax_nopriv_houserent_theme_send_booking', 'houserent_theme_send_booking');
add_action('wp_ajax_houserent_theme_send_booking', 'houserent_theme_send_booking');

/**
 * Remove Query String
 */
function houserent_theme_remove_query_string_one( $src ){   
    $rqs = explode( '?ver', $src );
    return $rqs[0];
}
if ( is_admin() ) {

} else {
    add_filter( 'script_loader_src', 'houserent_theme_remove_query_string_one', 15, 1 );
    add_filter( 'style_loader_src', 'houserent_theme_remove_query_string_one', 15, 1 );
}

function houserent_theme_remove_query_string_two( $src ){
    $rqs = explode( '&ver', $src );
    return $rqs[0];
}
if ( is_admin() ) {

} else {
    add_filter( 'script_loader_src', 'houserent_theme_remove_query_string_two', 15, 1 );
    add_filter( 'style_loader_src', 'houserent_theme_remove_query_string_two', 15, 1 );
}


if( ! function_exists('houerent_remove_filter_aq_resize') ) {
    function houerent_remove_filter_aq_resize() {
        remove_filter( 'image_resize_dimensions', array( $this, 'aq_upscale' ) );
    }
}
if( ! function_exists('houerent_remove_filter_blog_post_exc_length') ) {
    function houerent_remove_filter_blog_post_exc_length() {
        remove_filter( 'excerpt_length', 'houserent_theme_blog_post_exc_length' );
    }
}

/**
 * The Class Custom Video.
 */
class houserent_theme_someClass {
 
    /**
     * Hook into the appropriate actions when the class is constructed.
     */
    public function __construct() {
        add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
        add_action( 'save_post',      array( $this, 'houserent_theme_save'         ) );
    }
 
    /**
     * Adds the meta box container.
     */
    public function add_meta_box( $post_type ) {
        // Limit meta box to certain post types.
        $post_types = array( 'post' );
 
        if ( in_array( $post_type, $post_types ) ) {
            add_meta_box(
                'some_meta_box_name',
                esc_attr__( 'Featured Video', 'houserent' ),
                array( $this, 'houserent_theme_render_meta_box_content' ),
                $post_type,
                'side',
                'low'
            );
        }
    }
 
    /**
     * Save the meta when the post is saved.
     *
     * @param int $post_id The ID of the post being saved.
     */
    public function houserent_theme_save( $post_id ) {
 
        /*
         * We need to verify this came from the our screen and with proper authorization,
         * because save_post can be triggered at other times.
         */
 
        // Check if our nonce is set.
        if ( ! isset( $_POST['houserent_theme_inner_custom_box_nonce'] ) ) {
            return $post_id;
        }
 
        $nonce = $_POST['houserent_theme_inner_custom_box_nonce'];
 
        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, 'houserent_theme_inner_custom_box' ) ) {
            return $post_id;
        }
 
        /*
         * If this is an autosave, our form has not been submitted,
         * so we don't want to do anything.
         */
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }
 
        // Check the user's permissions.
        if ( 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }
 
        /* OK, it's safe for us to save the data now. */
 
        // Sanitize the user input.
        $mydata = sanitize_text_field( $_POST['post_meta_featured_video'] );
 
        // Update the meta field.
        update_post_meta( $post_id, 'fatured_video_meta', $mydata );
    }
 
 
    /**
     * Render Meta Box content.
     *
     * @param WP_Post $post The post object.
     */
    public function houserent_theme_render_meta_box_content( $post ) {
 
        // Add an nonce field so we can check for it later.
        wp_nonce_field( 'houserent_theme_inner_custom_box', 'houserent_theme_inner_custom_box_nonce' );
 
        // Use get_post_meta to retrieve an existing value from the database.
        $value = get_post_meta( $post->ID, 'fatured_video_meta', true );
 
        // Display the form, using the current value.
        ?>
        <input type="url" id="post_meta_featured_video" name="post_meta_featured_video" value="<?php echo esc_attr( $value ); ?>" size="36" /><br /><br />
        <label for="post_meta_featured_video">
            <?php esc_html_e( 'Paste a video link from Youtube, Vimeo or Dailymotion. it will be embedded in the post and the thumb used as the featured image of this post. You need to choose Video Format from above to use Featured Video.', 'houserent' ); ?>
        </label>
        <?php
    }
}

/**
 * Registration Box
 *
 * @param WP_Post $post The post object.
 */
if( !function_exists('houerent_registration_box') ) {
    function houerent_registration_box() { ?>
        <!-- Registrar Or Sign In-content -->
        <?php
            $user_login = ( isset( $_GET['user_login'] ) ) ? $_GET['user_login'] : '' ;
            $user_email = ( isset( $_GET['user_email'] ) ) ? $_GET['user_email'] : '' ;
        ?>
        <div class="cd-user-modal">
            <div class="cd-user-modal-container">
                <?php
                if ( ! is_user_logged_in() ) {?>

                    <ul class="cd-switcher">
                        <li><a href="#"><?php esc_html_e('Sign in', 'houserent'); ?></a></li>
                        <li><a href="#"><?php esc_html_e('New account', 'houserent'); ?></a></li>
                    </ul>

                    <!-- log in form -->
                    <div id="cd-login"> 
                        <form method="post" action="<?php echo esc_url(home_url('/')); ?>wp-login.php" class="wp-user-form cd-form">
                            <p class="fieldset">
                                <label class="image-replace cd-email"><?php esc_html_e( 'E-mail', 'houserent' ); ?></label>
                                <input 
                                    class="full-width has-padding has-border" 
                                    type="text" 
                                    name="log" 
                                    value="<?php echo esc_attr(stripslashes($user_login)); ?>" 
                                    size="20" 
                                    tabindex="11" 
                                    placeholder="<?php esc_html_e( 'Email', 'houserent' ); ?>" 
                                />
                            </p>

                            <p class="fieldset">
                                <label class="image-replace cd-password"><?php esc_html_e( 'Password', 'houserent' ); ?></label>
                                <input 
                                    class="full-width has-padding has-border"  
                                    type="text"  
                                    placeholder="<?php esc_html_e( 'Password', 'houserent' ); ?>" 
                                    name="pwd" 
                                    value="" 
                                    size="20" 
                                    tabindex="12"
                                >
                                <a href="#" class="hide-password"><?php esc_html_e( 'Hide', 'houserent' ); ?></a>
                            </p>
                            
                            <p class="fieldset">
                                <input 
                                    type="checkbox" 
                                    name="rememberme" 
                                    value="forever" 
                                    checked="checked" 
                                    tabindex="13" 
                                >
                                <label><?php esc_html_e( 'Remember me', 'houserent' ); ?></label>
                            </p>

                            <?php do_action('login_form'); ?>

                            <p class="fieldset">
                                <input class="full-width" name="user-submit" type="submit" value="Login">
                                <input type="hidden" name="redirect_to" value="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>" />
                                <input type="hidden" name="user-cookie" value="1" />
                            </p>
                            
                        </form>
                        <?php echo'<p class="cd-form-bottom-message"><a href="'. wp_lostpassword_url().'" title="Lost Password">Lost Your Password?</a></p>'; ?>

                        <a href="#" class="cd-close-form"><?php esc_html_e('Close', 'houserent'); ?></a>
                    </div> <!-- cd-login -->

                    <!-- sign up form -->
                    <div id="cd-signup">
                        <?php
                            $user_login = ( isset( $_GET['user_login'] ) ) ? $_GET['user_login'] : '' ;
                            $user_email = ( isset( $_GET['user_email'] ) ) ? $_GET['user_email'] : '' ;
                        ?>
                        <form method="post" action="<?php echo site_url('wp-login.php?action=register', 'login_post') ?>" class="wp-user-form cd-form">
                            <p class="fieldset">
                                <label class="image-replace cd-username"><?php esc_html_e('Username', 'houserent'); ?></label>
                                <input 
                                    class="full-width has-padding has-border" 
                                    type="text" 
                                    name="user_login" 
                                    value="<?php echo esc_attr(stripslashes($user_login)); ?>" 
                                    size="20" 
                                    tabindex="101" 
                                    placeholder="<?php esc_html_e('Username', 'houserent'); ?>" 
                                />
                            </p>

                            <p class="fieldset">
                                <label class="image-replace cd-email"><?php esc_html_e('E-mail', 'houserent'); ?></label>
                                <input 
                                    class="full-width has-padding has-border" 
                                    type="email" 
                                    name="user_email" 
                                    value="<?php echo esc_attr(stripslashes($user_email)); ?>" 
                                    size="25" 
                                    placeholder="<?php esc_html_e('E-mail', 'houserent'); ?>"
                                />

                            </p>

                            <?php
                                $registration_terms_conditions = houserent_theme_get_customizer_field('registration_terms_conditions','');
                                if( $registration_terms_conditions ):
                            ?>

                            <p class="fieldset">
                                <input type="checkbox" required="required">
                                <label><?php esc_html_e('I agree to the', 'houserent'); ?> 
                                    <a target="_blank" href="<?php echo esc_url( $registration_terms_conditions ); ?>"><?php esc_html_e('Terms and Conditions', 'houserent'); ?></a>
                                </label>
                            </p>
                            <?php endif; ?>

                            <p class="fieldset">
                                <?php do_action('register_form'); ?>
                                <input class="full-width has-padding" type="submit" name="user-submit" value="<?php esc_html_e('Create account', 'houserent'); ?>" tabindex="103" />
                                <input type="hidden" name="redirect_to" value="<?php echo esc_url($_SERVER['REQUEST_URI'] ); ?>?register=true" />
                                <input type="hidden" name="user-cookie" value="1" />
                            </p>
                        </form>
                        <a href="#" class="cd-close-form"><?php esc_html_e('Close', 'houserent'); ?></a>
                    </div> <!-- cd-signup -->
                    
                    <!-- reset password form -->
                    <div id="cd-reset-password">
                        <p class="cd-form-message">
                            <?php esc_html_e('Lost your password? Please enter your email address. You will receive a link to create a new password.', 'houserent'); ?>
                            
                        </p>

                        <form method="post" action="<?php echo site_url('wp-login.php?action=lostpassword', 'login_post') ?>" class="wp-user-form cd-form">
                            <p class="fieldset">
                                <label class="image-replace cd-email"><?php esc_html_e('E-mail', 'houserent'); ?></label>
                                <input 
                                    class="full-width has-padding has-border" 
                                    type="email" 
                                    name="user_login" 
                                    value="" 
                                    size="20" 
                                    tabindex="1001"  
                                    placeholder="<?php esc_html_e('E-mail', 'houserent'); ?>"
                                />
                                <span class="cd-error-message"> <?php esc_html_e('>Error! Invalid email address', 'houserent'); ?></span>
                            </p>
                            <p class="fieldset">
                                <?php do_action('login_form', 'resetpass'); ?>
                                <input class="full-width has-padding" type="submit" name="user-submit" value="<?php esc_html_e('Reset password', 'houserent'); ?>">
                                <input type="hidden" name="redirect_to" value="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>?reset=true" />
                                <input type="hidden" name="user-cookie" value="1" />
                            </p>
                        </form>

                        <p class="cd-form-bottom-message"><a href="#"><?php esc_html_e('Back to log-in', 'houserent'); ?></a></p>
                    </div> <!-- cd-reset-password -->
                    
                    <?php

                } else { ?>
                        <div class="houserent-theme-login-board"> 
                           <div class="left-thumb">
                               <?php echo get_avatar( get_the_author_meta('ID')); ?>
                           </div> <!-- / . left-thumb -->
                           <div class="right-details">
                               <p><?php esc_html_e( "Welcome:", "houserent" );?> <strong><?php global $current_user; echo $current_user->display_name; ?></strong></p>
                               <div class="dashboard-link">
                                    <i class="fa fa-tachometer"></i><a href="<?php echo esc_url( home_url('/')); ?>wp-admin"><?php esc_html_e( "Dashboard","houserent" );?></a>
                               </div>
                               <div class="your-profile">
                                    <i class="fa fa-user"></i><a href="<?php echo esc_url( home_url('/')); ?>wp-admin//profile.php"><?php esc_html_e( "Profile", "houserent" );?></a>
                               </div>

                                <?php 
                                    $tem_base_name = wp_basename( get_page_template() ); 
                                    $favorites_id = sh_hr_page_id_by_tem_name('templates/template-user-favorites.php');
                                    if($favorites_id) :
                                ?>
                                <div class="your-favorite">
                                    <i class="fa fa-heart"></i><a href="<?php echo get_permalink( $favorites_id ); ?>"><?php echo get_the_title( $favorites_id ); ?></a>
                                </div>
                                <?php endif; ?>

                               <div class="logout-link">
                                    <i class="fa fa-power-off"></i><a href="<?php echo wp_logout_url( esc_url( home_url( '/' ) ) ); ?>"><?php esc_html_e( "Logout", "houserent" );?></a>
                               </div>
                           </div> <!-- / . right-details -->
                       </div> <!-- / .houserent-theme-login-board -->
                    <?php } ?>
                <a href="#" class="cd-close-form"><?php esc_html_e('Close', 'houserent'); ?></a>
            </div> <!-- cd-user-modal-container -->
        </div> <!-- cd-user-modal -->
    <?php }
}


/**
 * Utility to retrieve IP address
 * @since    0.5
 */
if ( ! function_exists( 'houserent_sl_get_ip' ) ) :
    function houserent_sl_get_ip() {
        if ( isset( $_SERVER['HTTP_CLIENT_IP'] ) && ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) && ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = ( isset( $_SERVER['REMOTE_ADDR'] ) ) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
        }
        $ip = filter_var( $ip, FILTER_VALIDATE_IP );
        $ip = ( $ip === false ) ? '0.0.0.0' : $ip;
        return $ip;
    } // houserent_sl_get_ip()
endif;

/**
 ** Functions for get file name.
 **/
if ( ! function_exists( 'houserent_theme_get_file_name' ) ) :
    function houserent_theme_get_file_name(){
        $get_file_name_path = explode("/", $_SERVER['PHP_SELF']);
        return end($get_file_name_path);
    }
endif;