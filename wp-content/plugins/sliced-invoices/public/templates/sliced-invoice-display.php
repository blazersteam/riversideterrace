  <link rel="stylesheet" href="http://etimbo.github.io/jquery-print-preview-plugin/example/css/print.css" type="text/css" media="print" />
    <link rel="stylesheet" href="http://etimbo.github.io/jquery-print-preview-plugin/src/css/print-preview.css" type="text/css" media="screen">
<?php if ( ! defined('ABSPATH') ) {
	exit;
}

/**
 * !! IMPORTANT !!
 *
 * Do not edit this file!
 * To create a new template, simply create a folder in your current theme folder called 'sliced'.
 * You can then copy this file into the 'sliced' folder and edit that copy.
 * This will ensure that your template files are not overwritten if/when you update the Sliced Invoices plugin.
 *
 */


// $GLOBALS['post']->ID =233;
// print_r(sliced_display_invoice_details(232));
// exit();

// print_r($_GET['id']);

// print_r(explode(",",$_GET['id']))
// exit();

if(isset($_GET['ids'])){
	$variable = explode(",",$_GET['ids']);

	$multi = 1;
}else{
	$variable = array($GLOBALS['post']->ID);
	$multi = 0;
}
?>
<style>


div.parint {
    page-break-after: always;
}

/*.parint{
 width: 21cm;
}*/
.sliced-wrap {
    width: 21cm;
}
	.sliced-top-bar {
		display: none;
	}
	</style>
	<?php do_action('sliced_head'); ?>
	<?php do_action('sliced_invoice_head'); ?>



<?php do_action( 'sliced_invoice_before_body' ); ?>



<?php do_action( 'sliced_invoice_after_body' ); ?>
	<div id="print_all">

<?php


foreach ($variable as $key => $value) {
	
	$GLOBALS['post']->ID =$value;


do_action( 'sliced_before_invoice_display' ); ?><!doctype html>


<?Php 
if($multi == 0){ 
?>

<html <?php language_attributes(); ?>>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<title><?php wp_title() ?></title>
	<!--" />-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="noindex,nofollow">
	</head>
	<body class="body sliced-invoice">
<?php  }else{ ?>

<div class="parint">

<?php } ?>
	

	<div class="container sliced-wrap">

	<?php if ( $watermark = sliced_get_invoice_watermark() ) : ?>
		<div class="watermark no-print"><p><?php echo esc_html( $watermark ) ?></p></div>
	<?php endif; ?>

		<!-- ///// Start PDF header -->
		<htmlpageheader name="sliced-pdf-header">

			<div class="row sliced-header">
				<div class="col-xs-12 col-sm-6 sliced-business">
					<?php sliced_display_business(); ?>
				</div>

				<div class="col-xs-12 col-sm-6 sliced-title">
					<h2><?php echo esc_html( sliced_get_invoice_label() ); ?></h2>
				</div>
			</div><!-- END row -->

		</htmlpageheader>
		<!-- End PDF header ///// -->


		<div class="row sliced-upper">
			<div class="col-xs-12 col-sm-6 sliced-from-address sliced-address">
				<?php sliced_display_from_address(); ?>
			</div>

			<div class="col-xs-12 col-sm-5 sliced-details">
				<?php sliced_display_invoice_details(232); ?>
			</div>
		</div><!-- END row -->


		<div class="row sliced-middle">
			<div class="col-xs-12 col-sm-6 sliced-to-address sliced-address">
				<?php sliced_display_to_address(232); ?>
			</div>
		</div><!-- END row -->


		<?php if ( sliced_get_invoice_description() ) : ?>

			<div class="row sliced-lower">
				<div class="col-sm-12 sliced-description">
					<?php echo wpautop( sliced_get_invoice_description(232) ); ?>
				</div>
			</div><!-- END row -->

		<?php endif; ?>


		<div class="row sliced-items">
			<div class="col-sm-12 sliced-line-items">
				<div class="table-responsive">
					<?php sliced_display_line_items(232); ?>
				</div>
			</div>
		</div>
		<div class="row sliced-items">
			<div class="col-xs-12 col-sm-5 sliced-totals">
				<?php sliced_display_invoice_totals(232); ?>
			</div>
		</div><!-- END row -->

		<?php if( sliced_is_payment_method( 'generic' ) || sliced_is_payment_method( 'bank' ) ) : ?>
		<div class="row sliced-payments">
			<div class="col-sm-12">
				<?php if( sliced_is_payment_method( 'generic' ) ) : ?>
					<div class="generic"><?php echo wpautop( sliced_get_business_generic_payment(232) ); ?></div>
				<?php endif; ?>
				<?php if( sliced_is_payment_method( 'bank' ) ) : ?>
					<div class="bank"><?php echo wpautop( sliced_get_business_bank(232) ); ?></div>
				<?php endif; ?>
			</div>
		</div><!-- END row -->
		<?php endif; ?>
		
		<div class="row sliced-footer">
			<div class="col-sm-12">
				<div class="terms-text"><?php echo wpautop( sliced_get_invoice_terms(232) ); ?></div>
			</div>
		</div><!-- END row -->

		<!-- ///// Start PDF footer -->
		<htmlpagefooter name="sliced-pdf-footer">

			<div class="row sliced-footer">
				<div class="col-sm-12">
					<div class="footer-text"><?php echo sliced_get_invoice_footer(); ?></div>
					<div class="print-only"><?php _e( 'Page ', 'sliced-invoices') ?> {PAGENO}/{nbpg}</div>
				</div>
			</div><!-- END row -->

		</htmlpagefooter>
		<!-- End PDF footer ///// -->


	</div> <?php if($multi == 1){ ?>  </div>  <?php } ?>  <!-- END sliced-wrap -->
<?php } ?></div>
<?Php 
if($multi == 1){ 
?>
<!-- <?php do_action( 'sliced_invoice_footer' ); ?>
<?php do_action( 'sliced_template_footer' ); ?> -->
</body>

</html>
<?php } ?>


<?php if($multi ==1){ ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>

    <!--<link rel="stylesheet" href="http://etimbo.github.io/jquery-print-preview-plugin/example/css/960.css" type="text/css" media="screen">-->
    <!--<link rel="stylesheet" href="http://etimbo.github.io/jquery-print-preview-plugin/example/css/screen.css" type="text/css" media="screen" />-->
  
    <!--<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script>-->
    <script src="http://etimbo.github.io/jquery-print-preview-plugin/src/jquery.print-preview.js" type="text/javascript" charset="utf-8"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>


<script type="text/javascript">
    jQuery(document).ready(function() {
        // var pdf = new jsPDF("p", "mm", "A4");
           // var options = { pagesplit: true };
        //     var marginX = 150;
        //     var marginY = 350;
        //     pdf.addHTML($('#pageby_print'), options, function() {
        //     pdf.save("doc.pdf");
        //     });
// alert("Asdasdas");
        // var doc = new jsPDF("p", "mm", "A4");
        // doc.addHTML(jQuery('#print_all').body,function() {
        // doc.save('html.pdf');
        //     // window.close();
        // });

        // var doc = new jsPDF("p", "mm", "A4");
        // doc.addHTML($('#parint').body,options,function() {
        // doc.save('html.pdf');
        //     // window.close();
        // });

// });

// var pdf = new jsPDF('p', 'pt', 'letter');
//   pdf.canvas.height = 72 * 11;
//   pdf.canvas.width = 72 * 8.5;

//   pdf.fromHTML(document.body);

//   pdf.save('test.pdf');

// let doc = new jsPDF('p','pt','a4');

// doc.addHTML(document.body,function() {
//     doc.save('html.pdf');
// });

// jQuery(document).bind('keydown', function(e) {
//                 var code = (e.keyCode ? e.keyCode : e.which);
//                 if (code == 80 && !$('#print_all').length) {
//                     jQuery.printPreview.loadPrintPreview();
//                     return false;
//                 }            
//             });
     
    
    
    
    
    
    

        });
</script>

<?php } ?>