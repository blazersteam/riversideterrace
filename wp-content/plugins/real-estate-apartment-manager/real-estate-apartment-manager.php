<?php
/*
 * Plugin Name: Real Estate Apartment Manager
 * Plugin URI: https://weblizar.com/plugins
 * Description: Manage multiple apartments, buildings, flats, suppliers, clients, bookings and complaints. Receive booking requests and resolve complaints received from clients.
 * Version: 1.2
 * Author: Weblizar
 * Author URI: https://weblizar.com
 * Text Domain: WL-BM
*/

defined( 'ABSPATH' ) || die();

if ( ! defined( 'WL_BM_PLUGIN_URL' ) ) {
	define( 'WL_BM_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'WL_BM_PLUGIN_DIR_PATH' ) ) {
	define( 'WL_BM_PLUGIN_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

final class WL_BM_Apartment_Manager {
	private static $instance = null;

	private function __construct() {
		$this->initialize_hooks();
		$this->setup_database();
	}

	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function initialize_hooks() {
		if ( is_admin() ) {
			require_once WL_BM_PLUGIN_DIR_PATH . 'admin/admin.php';
		}
		require_once WL_BM_PLUGIN_DIR_PATH . 'public/public.php';
	}

	private function setup_database() {
		require_once( WL_BM_PLUGIN_DIR_PATH . 'admin/inc/WL_BM_Database.php' );
		register_activation_hook( __FILE__, array( 'WL_BM_Database', 'activation' ) );
		register_deactivation_hook( __FILE__, array( 'WL_BM_Database', 'deactivation' ) );
		register_uninstall_hook( __FILE__, array( 'WL_BM_Database', 'deactivation' ) );
	}
}
WL_BM_Apartment_Manager::get_instance();
