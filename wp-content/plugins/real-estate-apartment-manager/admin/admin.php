<?php
defined( 'ABSPATH' ) || die();

require_once WL_BM_PLUGIN_DIR_PATH . 'includes/constants.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/WL_BM_Menu.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/WL_BM_Building.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/WL_BM_Flat_Type.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/WL_BM_Flat.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/WL_BM_Supplier.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/WL_BM_Client.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/WL_BM_Complaint.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/WL_BM_Booking.php';

/* Create menu */
add_action( 'admin_menu', array( 'WL_BM_Menu', 'create_menu' ) );

/* Actions for manager: Buildings */
add_action( 'wp_ajax_wlbm-manager-save-building', array( 'WL_BM_Building', 'save' ) );
add_action( 'wp_ajax_wlbm-manager-delete-building', array( 'WL_BM_Building', 'delete' ) );

/* Actions for manager: Flat Types */
add_action( 'wp_ajax_wlbm-manager-save-flat-type', array( 'WL_BM_Flat_Type', 'save' ) );
add_action( 'wp_ajax_wlbm-manager-delete-flat-type', array( 'WL_BM_Flat_Type', 'delete' ) );

/* Actions for manager: Flats */
add_action( 'wp_ajax_wlbm-manager-save-flat', array( 'WL_BM_Flat', 'save' ) );
add_action( 'wp_ajax_wlbm-manager-delete-flat', array( 'WL_BM_Flat', 'delete' ) );

/* Actions for manager: Clients */
add_action( 'wp_ajax_wlbm-manager-save-client', array( 'WL_BM_Client', 'save' ) );
add_action( 'wp_ajax_wlbm-manager-delete-client', array( 'WL_BM_Client', 'delete' ) );
add_action( 'wp_ajax_wlbm-manager-get-flat-detail', array( 'WL_BM_Client', 'get_flat_detail' ) );

/* Actions for manager: Suppliers */
add_action( 'wp_ajax_wlbm-manager-save-supplier', array( 'WL_BM_Supplier', 'save' ) );
add_action( 'wp_ajax_wlbm-manager-delete-supplier', array( 'WL_BM_Supplier', 'delete' ) );

/* Actions for manager: Complaints */
add_action( 'wp_ajax_wlbm-manager-forward-complaint', array( 'WL_BM_Complaint', 'forward' ) );
add_action( 'wp_ajax_wlbm-manager-delete-complaint', array( 'WL_BM_Complaint', 'delete' ) );
add_action( 'wp_ajax_wlbm-manager-resolve-complaint', array( 'WL_BM_Complaint', 'manager_resolve' ) );

/* Actions for client: Complaints */
add_action( 'wp_ajax_wlbm-client-save-complaint', array( 'WL_BM_Complaint', 'save' ) );
add_action( 'wp_ajax_wlbm-client-resolve-complaint', array( 'WL_BM_Complaint', 'client_resolve' ) );

/* Actions for supplier: Complaints */
add_action( 'wp_ajax_wlbm-supplier-respond-complaint', array( 'WL_BM_Complaint', 'respond' ) );

/* Actions for manager: Booking requests */
add_action( 'wp_ajax_wlbm-manager-save-booking', array( 'WL_BM_Booking', 'save' ) );
add_action( 'wp_ajax_wlbm-manager-delete-booking', array( 'WL_BM_Booking', 'delete' ) );
