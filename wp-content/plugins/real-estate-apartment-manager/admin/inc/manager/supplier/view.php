<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_SUPPLIERS, false );

$supplier     = '';
$name         = '';
$phone        = '';
$address      = '';
$email        = '';
$expertise    = '';
$is_active    = '1';
$extra_detail = '';
$buildings    = array();
$username     = '';
$login_email  = '';
if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id     = absint( $_GET['id'] );
	$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID, name, phone, address, email, expertise, extra_detail, is_active, user_id FROM {$wpdb->prefix}wlbm_suppliers WHERE ID = %d", $id ) );
}
if ( ! $supplier ) {
	die();
}
$name         = $supplier->name;
$phone        = $supplier->phone;
$address      = $supplier->address;
$email        = $supplier->email;
$expertise    = $supplier->expertise;
$is_active    = $supplier->is_active;
$extra_detail = $supplier->extra_detail;
$buildings    = $wpdb->get_col( $wpdb->prepare( "SELECT b.name FROM {$wpdb->prefix}wlbm_building_supplier as bs, {$wpdb->prefix}wlbm_buildings as b WHERE bs.building_id = b.ID AND bs.supplier_id = %d", $supplier->ID ) );

if ( count( $buildings ) ) {
	$buildings = implode( ', ', $buildings );
} else {
	$buildings = '-';
}

if ( $supplier->user_id ) {
	$user = get_user_by( 'ID', $supplier->user_id );
	if ( $user ) {
		$username    = $user->user_login;
		$login_email = $user->user_email;
	}
}
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-search"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: name of supplier */
								__( 'View Supplier: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $name )
						);
						?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-6">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<a href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $supplier->ID ); ?>" class="btn btn-sm btn-primary btn-primary">
								<i class="fas fa-edit"></i>&nbsp;
								<?php echo esc_html( 'Edit Supplier', 'WL-BM' ); ?>
							</a>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-users-cog"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Name', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $name ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Buildings', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $buildings ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Address', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $address ? $address : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $phone ? $phone : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Email', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $email ? $email : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Expertise', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $expertise ? $expertise : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Username', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $username ? $username : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Login Email', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $login_email ? $login_email : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Is Active', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $is_active ? __( 'Yes', 'WL-BM') : __( 'No', 'WL-BM') ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $extra_detail ? $extra_detail : '-' ); ?></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
