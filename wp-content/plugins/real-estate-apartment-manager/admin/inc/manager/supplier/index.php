<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_SUPPLIERS, false );

$suppliers = $wpdb->get_results( "SELECT s.ID, s.name, s.address, s.phone, s.email, s.is_active, u.user_login as username, u.user_email as login_email, GROUP_CONCAT(DISTINCT CONCAT('<strong>-</strong> ', b.name) ORDER BY b.name SEPARATOR '<br>') as building_names FROM {$wpdb->prefix}wlbm_suppliers as s LEFT OUTER JOIN {$wpdb->base_prefix}users as u ON s.user_id = u.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_building_supplier as bs ON bs.supplier_id = s.ID LEFT JOIN {$wpdb->prefix}wlbm_buildings as b ON bs.building_id = b.ID GROUP BY s.ID ORDER BY s.ID DESC" );

$suppliers_count = count( $suppliers );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center"><i class="fas fa-users-cog"></i> <?php esc_html_e( 'Suppliers', 'WL-BM' ); ?></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left wlbm-sub-header-left">
							<?php
								printf(
									/* translators: %s: number of suppliers */
									esc_html( _n( 'Showing %s supplier.', 'Showing %s suppliers.', $suppliers_count, 'WL-BM' ) ),
									esc_html( number_format_i18n( $suppliers_count ) )
								);
							?>
						</span>
						<span class="float-right wlbm-sub-header-right">
							<a href="<?php echo esc_url( $page_url . '&action=save' ); ?>" class="btn btn-sm btn-primary">
								<i class="fas fa-plus-square"></i>&nbsp;
								<?php echo esc_html( 'Add New Supplier', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<table class="table table-hover" id="wlbm-supplier-table">
							<thead>
								<tr>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Name', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Email', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Buildings', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Username', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Login Email', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Is Active', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Action', 'WL-BM' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ( $suppliers_count > 0 ) {
									foreach ( $suppliers as $supplier ) {
								?>
								<tr>
									<td><?php echo esc_html( $supplier->name ); ?></td>
									<td><?php echo esc_html( $supplier->phone ? $supplier->phone : '-' ); ?></td>
									<td><?php echo esc_html( $supplier->email ? $supplier->email : '-' ); ?></td>
									<td>
										<?php
										echo wp_kses( $supplier->building_names ? $supplier->building_names : '-', array( 'br' => array(), 'strong' => array() ) );
										?>
									</td>
									<td><?php echo esc_html( $supplier->username ? $supplier->username : '-' ); ?></td>
									<td><?php echo esc_html( $supplier->login_email ? $supplier->login_email : '-' ); ?></td>
									<td><?php echo esc_html( $supplier->is_active ? __( 'Yes', 'WL-BM') : __( 'No', 'WL-BM') ); ?></td>
									<td class="text-nowrap">
										<a class="text-info" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $supplier->ID ); ?>"><i class="fas fa-search"></i></a>&nbsp;&nbsp;
										<a class="text-primary" href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $supplier->ID ); ?>"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
										<a class="text-danger delete-supplier" href="" data-nonce="<?php echo esc_attr( wp_create_nonce( 'delete-supplier-' . $supplier->ID ) ); ?>" data-supplier="<?php echo esc_attr( $supplier->ID ); ?>" data-message-title="<?php esc_attr_e( 'Confirm!', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm!', 'WL-BM' ); ?>" data-delete-user-message="<?php esc_attr_e( 'Also delete user account associated with this supplier?', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Confirm', 'WL-BM' ); ?>"><i class="fas fa-trash"></i></a>
									</td>
								</tr>
								<?php
									}
								} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
