<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_SUPPLIERS, false );

$supplier     = '';
$nonce_action = 'add-supplier';
$name         = '';
$phone        = '';
$address      = '';
$email        = '';
$expertise    = '';
$is_active    = '1';
$extra_detail = '';
$building_ids = array();
$username     = '';
$login_email  = '';
if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id       = absint( $_GET['id'] );
	$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID, name, phone, address, email, expertise, extra_detail, is_active, user_id FROM {$wpdb->prefix}wlbm_suppliers WHERE ID = %d", $id ) );
	if ( $supplier ) {
		$nonce_action = 'edit-supplier-' . $supplier->ID;
		$name         = $supplier->name;
		$phone        = $supplier->phone;
		$address      = $supplier->address;
		$email        = $supplier->email;
		$expertise    = $supplier->expertise;
		$is_active    = $supplier->is_active;
		$extra_detail = $supplier->extra_detail;
		$building_ids = $wpdb->get_col( $wpdb->prepare( "SELECT building_id FROM {$wpdb->prefix}wlbm_building_supplier WHERE supplier_id = %d", $supplier->ID ) );
		if ( $supplier->user_id ) {
			$user = get_user_by( 'ID', $supplier->user_id );
			if ( $user ) {
				$username    = $user->user_login;
				$login_email = $user->user_email;
			}
		}
	}
}

$buildings = $wpdb->get_results( "SELECT ID, name FROM {$wpdb->prefix}wlbm_buildings ORDER BY name ASC" );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
					<?php if ( $supplier ) { ?>
						<i class="fas fa-edit"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: name of supplier */
								__( 'Edit Supplier: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $name )
						);
						?>
					<?php } else { ?>
						<i class="fas fa-plus-square"></i>
						<?php esc_html_e( 'Add New Supplier', 'WL-BM' ); ?>
					<?php } ?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<?php esc_html_e( 'Fill all the required fields (*).', 'WL-BM' ); ?>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-users"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-save-supplier-form">

							<?php $nonce = wp_create_nonce( $nonce_action ); ?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-manager-save-supplier">

							<?php if ( $supplier ) { ?>
							<input type="hidden" name="supplier_id" value="<?php echo esc_attr( $supplier->ID ); ?>">
							<?php } ?>

							<div class="row card-columns">
								<div class="col-md-6">
									<div class="card col">
										<div class="card-header h5 pb-1 pt-1"><?php esc_html_e( 'Buildings Detail', 'WL-BM' ); ?></div>
										<div class="card-body">
											<div class="form-group">
												<label for="wlbm_building" class="font-weight-bold"><?php esc_html_e( 'Buildings', 'WL-BM' ); ?>:</label>
												<select name="building[]" class="form-control selectpicker" data-actions-box="true" data-live-search="true" id="wlbm_building" multiple title="-------- <?php esc_attr_e( 'Select Buildings', 'WL-BM' ); ?> --------">
													<?php foreach ( $buildings as $building ) {
														$option_text = $building->name;
													?>
													 <option <?php selected( in_array( $building->ID, $building_ids ), true, true ); ?> value="<?php echo esc_attr( $building->ID ); ?>">
													 	<?php echo esc_html( $option_text ); ?>
												 	</option>
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="card col">
										<div class="card-header h5 pb-1 pt-1"><?php esc_html_e( 'Supplier Detail', 'WL-BM' ); ?></div>
										<div class="card-body">
											<div class="form-group">
												<label for="wlbm_name" class="font-weight-bold">* <?php esc_html_e( 'Supplier Name', 'WL-BM' ); ?>:</label>
												<input type="text" name="name" class="form-control" id="wlbm_name" placeholder="<?php esc_attr_e( 'Supplier Name', 'WL-BM' ); ?>" value="<?php echo esc_attr( $name ); ?>">
											</div>

											<div class="form-group">
												<label for="wlbm_address" class="font-weight-bold"><?php esc_html_e( 'Address', 'WL-BM' ); ?>:</label>
												<textarea name="address" class="form-control" id="wlbm_address" rows="3" placeholder="<?php esc_attr_e( 'Address', 'WL-BM' ); ?>"><?php echo esc_html( $address ); ?></textarea>
											</div>

											<div class="form-group">
												<label for="wlbm_phone" class="font-weight-bold"><?php esc_html_e( 'Phone', 'WL-BM' ); ?>:</label>
												<input type="text" name="phone" class="form-control" id="wlbm_phone" placeholder="<?php esc_attr_e( 'Phone', 'WL-BM' ); ?>" value="<?php echo esc_attr( $phone ); ?>">
											</div>

											<div class="form-group">
												<label for="wlbm_email" class="font-weight-bold"><?php esc_html_e( 'Email', 'WL-BM' ); ?>:</label>
												<input type="email" name="email" class="form-control" id="wlbm_email" placeholder="<?php esc_attr_e( 'Email', 'WL-BM' ); ?>" value="<?php echo esc_attr( $email ); ?>">
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="card col">
										<div class="card-header h5 pb-1 pt-1"><?php esc_html_e( 'Supplier Login Detail', 'WL-BM' ); ?></div>
										<div class="card-body">
											<?php if ( ! $username ) { ?>
											<div class="form-group">
												<input type="checkbox" name="is_existing_user" class="wlbm-delete-flats-checkbox" id="wlbm_is_existing_user" value="1">&nbsp;
												<label class="form-check-label font-weight-bold" for="wlbm_is_existing_user"><?php esc_html_e( 'Existing user?', 'WL-IM' ); ?></label>
											</div>
											<?php } ?>

											<div class="wlbm-new-user">
												<?php if ( $username ) { ?>
												<div class="mb-2">
													<span class="font-weight-bold"><?php esc_html_e( 'Username', 'WL-BM' ); ?>:</span>&nbsp;
													<span><?php echo esc_html( $username ); ?></span><br>
													<span class="text-secondary"><?php esc_html_e( "The username can't be changed.", 'WL-BM' ); ?></span>
												</div>
												<?php } else { ?>
												<div class="form-group">
													<label for="wlbm_username" class="font-weight-bold"><?php esc_html_e( 'Username', 'WL-BM' ); ?>:</label>
													<input type="text" name="username" class="form-control" id="wlbm_username" placeholder="<?php esc_attr_e( 'Username', 'WL-BM' ); ?>">
												</div>
												<?php } ?>

												<div class="form-group">
													<label for="wlbm_login_email" class="font-weight-bold"><?php esc_html_e( 'Login Email', 'WL-BM' ); ?>:</label>
													<input type="email" name="login_email" class="form-control" id="wlbm_login_email" placeholder="<?php esc_attr_e( 'Login Email', 'WL-BM' ); ?>" value="<?php echo esc_attr( $login_email ); ?>">
												</div>

												<div class="form-group">
													<label for="wlbm_password" class="font-weight-bold"><?php echo esc_attr_x( 'Password', 'label', 'WL-BM' ); ?>:</label>
													<input type="password" name="password" class="form-control" id="wlbm_password" placeholder="<?php echo esc_attr_x( 'Password', 'placeholder', 'WL-BM' ); ?>">
												</div>

												<div class="form-group">
													<label for="wlbm_password_confirm" class="font-weight-bold"><?php echo esc_html_x( 'Confirm Password', 'label', 'WL-BM' ); ?>:</label>
													<input type="password" name="password_confirm" class="form-control" id="wlbm_password_confirm" placeholder="<?php echo esc_attr_x( 'Confirm Password', 'placeholder', 'WL-BM' ); ?>">
												</div>
											</div>

											<?php if ( ! $username ) { ?>
											<div class="wlbm-exisitng-user">
												<div class="form-group">
													<label for="wlbm_exisitng_username" class="font-weight-bold">* <?php esc_html_e( 'Existing Username', 'WL-BM' ); ?>:</label>
													<input type="text" name="existing_username" class="form-control" id="wlbm_exisitng_username" placeholder="<?php esc_attr_e( 'Enter exisitng username', 'WL-BM' ); ?>">
												</div>
											</div>
											<?php } ?>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="card col">
										<div class="card-header h5 pb-1 pt-1"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?></div>
										<div class="card-body">
											<div class="form-group">
												<label for="wlbm_expertise" class="font-weight-bold"><?php esc_html_e( 'Expertise', 'WL-BM' ); ?>:</label>
												<input type="text" name="expertise" class="form-control" id="wlbm_expertise" placeholder="<?php esc_attr_e( 'Expertise', 'WL-BM' ); ?>" value="<?php echo esc_attr( $expertise ); ?>">
											</div>

											<div class="form-group">
												<label for="wlbm_extra_detail" class="font-weight-bold"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?>:</label>
												<textarea name="extra_detail" class="form-control" id="wlbm_extra_detail" rows="3" placeholder="<?php esc_attr_e( 'Extra Detail', 'WL-BM' ); ?>"><?php echo esc_html( $extra_detail ); ?></textarea>
											</div>

											<div class="form-group">
												<input type="checkbox" <?php checked( $is_active, '1', true ); ?> name="is_active" id="wlbm_is_active" value="1">&nbsp;
												<label class="form-check-label font-weight-bold" for="wlbm_is_active"><?php esc_html_e( 'Is Active?', 'WL-IM' ); ?></label>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<?php if ( $supplier ) { ?>
									<span class="float-left">
										<a href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $supplier->ID ); ?>" class="btn btn-sm btn-primary btn-info">
											<i class="fas fa-search"></i>&nbsp;
											<?php echo esc_html( 'View Supplier', 'WL-BM' ); ?>
										</a>
									</span>
									<?php } ?>
									<span class="float-right">
										<button type="submit" class="btn btn-sm btn-primary" id="wlbm-save-supplier-btn">
											<i class="fas fa-save"></i>&nbsp;
											<?php
											if ( $supplier ) {
												esc_html_e( 'Update Supplier', 'WL-BM' );
											} else {
												esc_html_e( 'Add New Supplier', 'WL-BM' );
											}
											?>
										</button>
									</span>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
