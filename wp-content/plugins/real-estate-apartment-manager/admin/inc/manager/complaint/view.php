<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_COMPLAINTS, false );

$complaint    = '';
$nonce_action = '';
$subject      = '';
$building_id  = '';
$building     = '';
$flat         = '';
$floor_number = '';
$description  = '';
$name         = '';
$email        = '';
$phone        = '';
$username     = '';
$login_email  = '';
$date         = '';
$images       = array();
$supplier_ids = array();
if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id        = absint( $_GET['id'] );
	$complaint = $wpdb->get_row( $wpdb->prepare( "SELECT cp.ID, cp.name, cp.phone, cp.subject, cp.description, cp.images, cp.is_resolved, cp.resolved_at, cp.created_at, c.email, f.flat_number, f.floor_number, b.ID as building_id, b.name as building_name, cs.is_responded, cs.responded_at, cs.estimate, u.user_login as username, u.user_email as login_email FROM {$wpdb->prefix}wlbm_complaints as cp LEFT OUTER JOIN {$wpdb->prefix}wlbm_complaint_supplier as cs ON cs.complaint_id = cp.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_clients as c ON c.ID = cp.client_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON f.ID = c.flat_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id LEFT OUTER JOIN {$wpdb->base_prefix}users as u ON c.user_id = u.ID WHERE cp.client_id = c.ID AND cp.ID = %d ORDER BY cp.ID DESC", $id ) );
}
if ( ! $complaint ) {
	die();
}
$nonce_action = 'forward-complaint-' . $complaint->ID;
$subject      = $complaint->subject;
$building_id  = $complaint->building_id;
$building     = $complaint->building_name;
$flat         = $complaint->flat_number;
$floor_number = $complaint->floor_number;
$description  = stripcslashes( $complaint->description );
$name         = $complaint->name;
$email        = $complaint->email;
$phone        = $complaint->phone;
$username     = $complaint->username;
$login_email  = $complaint->login_email;
$date         = date_i18n( 'd-m-Y g:i A', strtotime( $complaint->created_at ) );
$images       = unserialize( $complaint->images );
$supplier_ids = $wpdb->get_col( $wpdb->prepare( "SELECT supplier_id FROM {$wpdb->prefix}wlbm_complaint_supplier WHERE complaint_id = %d", $complaint->ID ) );

if ( $complaint->is_resolved ) {
	$status = '<span class="text-success font-weight-bold">' . esc_html__( 'Resolved', 'WL-BM' ) . '</span>';
} else {
	if ( $complaint->is_responded ) {
		$status = '<span class="text-info font-weight-bold">' . esc_html__( 'Supplier Responded', 'WL-BM' ) . '</span>';
		$status .= '&nbsp;<span class="text-secondary">' . esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->responded_at ) ) ) . '<span>';
	} elseif ( count( $supplier_ids ) ) {
		$status = '<span class="text-danger font-weight-bold">' . esc_html__( 'Unresolved', 'WL-BM' ) . '</span> - <span class="text-primary font-weight-bold">' . esc_html__( 'Forwarded', 'WL-BM' ) . '</span>';
	} else {
		$status = '<span class="text-danger font-weight-bold">' . esc_html__( 'Unresolved', 'WL-BM' ) . '</span>';
	}
}

$suppliers = $wpdb->get_results( $wpdb->prepare( "SELECT s.ID, s.name, s.phone, s.expertise FROM {$wpdb->prefix}wlbm_suppliers as s, {$wpdb->prefix}wlbm_building_supplier as bs WHERE bs.supplier_id = s.ID AND bs.building_id = %d AND s.is_active = '1' ORDER BY s.name ASC", $building_id ) );

$forwarded_suppliers = array();
if ( count( $supplier_ids ) ) {
	$supplier_ids_string = implode( ',', $supplier_ids );
	$forwarded_suppliers = $wpdb->get_results( $wpdb->prepare( "SELECT ID, name, phone, expertise FROM {$wpdb->prefix}wlbm_suppliers WHERE ID in (%s)", $supplier_ids_string ) );
}
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-search"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: subject of complaint */
								__( 'Complaint: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $subject )
						);
						?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="card col">
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Subject', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $subject ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Building', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $building ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Flat', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $flat ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Floor Number', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $floor_number ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Message', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $description ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Date', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $date ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Status', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value">
									<?php echo wp_kses( $status, array( 'span' => array( 'class' => array() ) ) ); ?>
								</span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Client Name', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $name ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $phone ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Email', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $email ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Username', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $username ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Login Email', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $login_email ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Images', 'WL-BM' ); ?></span>:
								<span class="wlbm-list-value">
									<ul class="list-group mt-1 wlbm-complaint-images">
										<?php
										foreach( $images as $image ) : ?>
											<li class="list-group-item list-group-flush">
												<a target="_blank" href="<?php echo esc_url( wp_get_attachment_url( $image ) ); ?>">
													<img class="img-responsive" src="<?php echo esc_url( wp_get_attachment_url( $image ) ); ?>">
												</a>
											</li>
										<?php
										endforeach; ?>
									</ul>
								</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card col">
					<div class="<?php if ( ! $complaint->is_resolved ): echo esc_attr( 'card-header' ); endif; ?>">
						<?php if ( ! $complaint->is_resolved ) { ?>
						<span class="float-left">
							<h6><?php esc_html_e( 'Forward Complaint', 'WL-BM' ); ?></h6>
						</span>
						<?php } ?>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-user"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<?php if ( ! $complaint->is_resolved ) { ?>
					<div class="card-body">
						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-forward-complaint-form">
							<?php $nonce = wp_create_nonce( $nonce_action ); ?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-manager-forward-complaint">

							<input type="hidden" name="complaint_id" value="<?php echo esc_attr( $complaint->ID ); ?>">

							<div class="form-group">
								<label for="wlbm_supplier" class="font-weight-bold"><?php esc_html_e( 'Forward to Supplier', 'WL-BM' ); ?>:</label>
								<select name="supplier[]" class="form-control selectpicker" data-live-search="true" id="wlbm_supplier">
									<option value="">-------- <?php esc_html_e( 'Select Supplier', 'WL-BM' ); ?> --------</option>
									<?php foreach( $suppliers as $supplier ) {
										$option_text = $supplier->name;
										if ( $supplier->phone ) {
											$option_text .= ' (' . $supplier->phone . ')';
										}
										if ( $supplier->expertise ) {
											$option_text .= ' (' . $supplier->expertise . ')';
										}
									?>
									<option <?php selected( in_array( $supplier->ID, $supplier_ids ), true, true ); ?> value="<?php echo esc_attr( $supplier->ID ); ?>">
										<?php echo esc_html( $option_text ); ?>
								 	</option>
									<?php } ?>
								</select>
							</div>

							<div class="clearfix">
								<span class="float-right">
									<button type="submit" class="btn btn-sm btn-outline-primary" id="wlbm-forward-complaint-btn" data-message-title="<?php esc_attr_e( 'Please confirm.', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm the action.', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Forward', 'WL-BM' ); ?>">
										<i class="fas fa-forward"></i>&nbsp;
										<?php esc_html_e( 'Forward Complaint', 'WL-BM' ); ?>
									</button>
								</span>
							</div>

						</form>
					</div>
					<?php } ?>
				</div>
				<?php if ( count( $forwarded_suppliers ) ) { ?>
				<div class="card col">
					<div class="card-header">
						<h6><?php echo esc_html_e( 'Forwarded to Supplier', 'WL-BM' ); ?>: </h6>
					</div>
					<div class="card-body">
						<ul class="list-group">
							<?php foreach( $forwarded_suppliers as $forwarded_supplier ) {
									$output = $forwarded_supplier->name;
									if ( $forwarded_supplier->phone ) {
										$output .= ' (' . $forwarded_supplier->phone . ')';
									}
									if ( $forwarded_supplier->expertise ) {
										$output .= ' (' . $forwarded_supplier->expertise . ')';
									}
							?>
							<li class="list-group-item">
								<span class="wlbm-list-value"><?php echo esc_html( $output ); ?></span>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<?php } ?>
				<?php if ( $complaint->is_responded ) { ?>
				<div class="card col">
					<div class="card-header">
						<h6><?php echo esc_html_e( 'Response Received', 'WL-BM' ); ?>: </h6>
					</div>
					<div class="card-body">
						<ul class="list-group">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Responded On', 'WL-BM' ); ?>: </span>
								<span class="wlbm-list-value"><?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->responded_at ) ) ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Estimated Amount', 'WL-BM' ); ?>: </span>
								<span class="wlbm-list-value"><?php echo esc_html( $complaint->estimate ); ?></span>
							</li>
						</ul>
					</div>
				</div>
				<?php } ?>
				<?php if ( ! $complaint->is_resolved ) { ?>
				<div class="card col">
					<div class="card-header">
						<h6><?php echo esc_html_e( 'Mark as Resolved', 'WL-BM' ); ?>: </h6>
					</div>
					<div class="card-body">
						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-resolve-complaint-form">
							<?php
							$nonce_action = 'resolve-complaint-' . $complaint->ID;
							$nonce        = wp_create_nonce( $nonce_action );
							?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-manager-resolve-complaint">

							<input type="hidden" name="complaint_id" value="<?php echo esc_attr( $complaint->ID ); ?>">

							<div class="clearfix">
								<span class="float-right">
									<button type="submit" class="btn btn-sm btn-outline-success" id="wlbm-resolve-complaint-btn" data-message-title="<?php esc_attr_e( 'Please confirm.', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm the action.', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Resolve', 'WL-BM' ); ?>">
										<i class="fas fa-check"></i>&nbsp;
										<?php esc_html_e( 'Mark as Resolved', 'WL-BM' ); ?>
									</button>
								</span>
							</div>

						</form>
					</div>
				</div>
				<?php } else { ?>
				<div class="card col">
					<div class="card-header">
						<h6><?php echo esc_html_e( 'Resolved', 'WL-BM' ); ?>: </h6>
					</div>
					<div class="card-body">
						<ul class="list-group">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Resolved On', 'WL-BM' ); ?>: </span>
								<span class="wlbm-list-value"><?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->resolved_at ) ) ); ?></span>
							</li>
						</ul>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
