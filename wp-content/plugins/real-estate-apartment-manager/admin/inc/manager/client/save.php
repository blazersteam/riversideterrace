<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_CLIENTS, false );

$client     = '';
$nonce_action = 'add-client';
$name         = '';
$phone        = '';
$address      = '';
$email        = '';
$flat_id      = '';
$is_active    = '1';
$extra_detail = '';
$username     = '';
$login_email  = '';
if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id       = absint( $_GET['id'] );
	$client = $wpdb->get_row( $wpdb->prepare( "SELECT ID, name, phone, address, email, extra_detail, is_active, flat_id, user_id FROM {$wpdb->prefix}wlbm_clients WHERE ID = %d", $id ) );
	if ( $client ) {
		$nonce_action = 'edit-client-' . $client->ID;
		$name         = $client->name;
		$phone        = $client->phone;
		$address      = $client->address;
		$email        = $client->email;
		$flat_id      = $client->flat_id;
		$is_active    = $client->is_active;
		$extra_detail = $client->extra_detail;

		if ( $client->user_id ) {
			$user = get_user_by( 'ID', $client->user_id );
			if ( $user ) {
				$username    = $user->user_login;
				$login_email = $user->user_email;
			}
		}
	}
}

$flats = $wpdb->get_results( "SELECT f.ID, f.flat_number, f.floor_number, b.name as building_name FROM {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON f.building_id = b.ID ORDER BY b.name ASC, f.flat_number ASC" );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
					<?php if ( $client ) { ?>
						<i class="fas fa-edit"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: name of client */
								__( 'Edit Client: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $name )
						);
						?>
					<?php } else { ?>
						<i class="fas fa-plus-square"></i>
						<?php esc_html_e( 'Add New Client', 'WL-BM' ); ?>
					<?php } ?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<?php esc_html_e( 'Fill all the required fields (*).', 'WL-BM' ); ?>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-users"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-save-client-form">

							<?php $nonce = wp_create_nonce( $nonce_action ); ?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-manager-save-client">

							<?php if ( $client ) { ?>
							<input type="hidden" name="client_id" value="<?php echo esc_attr( $client->ID ); ?>">
							<?php } ?>

							<div class="row card-columns">
								<div class="col-md-6">
									<div class="card col">
										<div class="card-header h5 pb-1 pt-1"><?php esc_html_e( 'Flat Detail', 'WL-BM' ); ?></div>
										<div class="card-body">
											<div class="form-group">
												<label for="wlbm_flat" class="font-weight-bold"><?php esc_html_e( 'Flat', 'WL-BM' ); ?>:</label>
												<select name="flat" class="form-control selectpicker" data-live-search="true" id="wlbm_flat">
													<option value="">-------- <?php esc_html_e( 'Select Flat', 'WL-BM' ); ?> --------</option>
													<?php foreach ( $flats as $flat ) {
														$option_text = $flat->flat_number;
														$nonce       = wp_create_nonce( 'get-flat-' . $flat->ID );
													?>
													 <option <?php selected( $flat_id, $flat->ID, true ); ?> data-icon="far fa-building" data-nonce="<?php echo esc_attr( $nonce ); ?>" value="<?php echo esc_attr( $flat->ID ); ?>">
													 	&nbsp;<?php echo esc_html( $option_text ); ?>&nbsp;&nbsp;(<?php echo esc_html( $flat->building_name ); ?>) <?php esc_html_e( 'Floor', 'WL-BM' ); ?> <?php echo esc_attr( $flat->floor_number ); ?>
													 </option>
													<?php } ?>
												</select>
											</div>
											<div class="wlbm-flat-info">
											<?php
											if ( $client && $flat_id ) {
												$flat = $wpdb->get_row( $wpdb->prepare( "SELECT f.flat_number, f.floor_number, f.area, f.price, b.name as building_name FROM {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON f.building_id = b.ID WHERE f.ID = %d GROUP BY f.ID", $flat_id ) );
												if ( $flat ) {
												?>
												<div class="mb-3">
													<ul class="list-group">
														<li class="list-group-item">
															<span class="wlbm-list-key"><?php esc_html_e( 'Flat Number', 'WL-BM'  ); ?></span>:&nbsp;
															<span class="wlbm-list-value"><?php echo esc_html( $flat->flat_number ); ?></span>
														</li>
														<li class="list-group-item">
															<span class="wlbm-list-key"><?php esc_html_e( 'Floor Number', 'WL-BM'  ); ?></span>:&nbsp;
															<span class="wlbm-list-value"><?php echo esc_html( $flat->floor_number ); ?></span>
														</li>
														<li class="list-group-item">
															<span class="wlbm-list-key"><?php esc_html_e( 'Area', 'WL-BM'  ); ?></span>:&nbsp;
															<span class="wlbm-list-value"><?php echo esc_html( $flat->area ); ?></span>
														</li>
														<li class="list-group-item">
															<span class="wlbm-list-key"><?php esc_html_e( 'Price', 'WL-BM'  ); ?></span>:&nbsp;
															<span class="wlbm-list-value"><?php echo esc_html( number_format_i18n( $flat->price, 2 ) ); ?></span>
														</li>
														<li class="list-group-item">
															<span class="wlbm-list-key"><?php esc_html_e( 'Building', 'WL-BM'  ); ?></span>:&nbsp;
															<span class="wlbm-list-value"><?php echo esc_html( $flat->building_name ); ?></span>
														</li>
													</ul>
												</div>
												<?php
												}
											} ?>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="card col">
										<div class="card-header h5 pb-1 pt-1"><?php esc_html_e( 'Client Detail', 'WL-BM' ); ?></div>
										<div class="card-body">
											<div class="form-group">
												<label for="wlbm_name" class="font-weight-bold">* <?php esc_html_e( 'Client Name', 'WL-BM' ); ?>:</label>
												<input type="text" name="name" class="form-control" id="wlbm_name" placeholder="<?php esc_attr_e( 'Client Name', 'WL-BM' ); ?>" value="<?php echo esc_attr( $name ); ?>">
											</div>

											<div class="form-group">
												<label for="wlbm_address" class="font-weight-bold"><?php esc_html_e( 'Address', 'WL-BM' ); ?>:</label>
												<textarea name="address" class="form-control" id="wlbm_address" rows="3" placeholder="<?php esc_attr_e( 'Address', 'WL-BM' ); ?>"><?php echo esc_html( $address ); ?></textarea>
											</div>

											<div class="form-group">
												<label for="wlbm_phone" class="font-weight-bold"><?php esc_html_e( 'Phone', 'WL-BM' ); ?>:</label>
												<input type="text" name="phone" class="form-control" id="wlbm_phone" placeholder="<?php esc_attr_e( 'Phone', 'WL-BM' ); ?>" value="<?php echo esc_attr( $phone ); ?>">
											</div>

											<div class="form-group">
												<label for="wlbm_email" class="font-weight-bold"><?php esc_html_e( 'Email', 'WL-BM' ); ?>:</label>
												<input type="email" name="email" class="form-control" id="wlbm_email" placeholder="<?php esc_attr_e( 'Email', 'WL-BM' ); ?>" value="<?php echo esc_attr( $email ); ?>">
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="card col">
										<div class="card-header h5 pb-1 pt-1"><?php esc_html_e( 'Client Login Detail', 'WL-BM' ); ?></div>
										<div class="card-body">
											<?php if ( ! $username ) { ?>
											<div class="form-group">
												<input type="checkbox" name="is_existing_user" class="wlbm-delete-flats-checkbox" id="wlbm_is_existing_user" value="1">&nbsp;
												<label class="form-check-label font-weight-bold" for="wlbm_is_existing_user"><?php esc_html_e( 'Existing user?', 'WL-IM' ); ?></label>
											</div>
											<?php } ?>

											<div class="wlbm-new-user">
												<?php if ( $username ) { ?>
												<div class="mb-2">
													<span class="font-weight-bold"><?php esc_html_e( 'Username', 'WL-BM' ); ?>:</span>&nbsp;
													<span><?php echo esc_html( $username ); ?></span><br>
													<span class="text-secondary"><?php esc_html_e( "The username can't be changed.", 'WL-BM' ); ?></span>
												</div>
												<?php } else { ?>
												<div class="form-group">
													<label for="wlbm_username" class="font-weight-bold"><?php esc_html_e( 'Username', 'WL-BM' ); ?>:</label>
													<input type="text" name="username" class="form-control" id="wlbm_username" placeholder="<?php esc_attr_e( 'Username', 'WL-BM' ); ?>">
												</div>
												<?php } ?>

												<div class="form-group">
													<label for="wlbm_login_email" class="font-weight-bold"><?php esc_html_e( 'Login Email', 'WL-BM' ); ?>:</label>
													<input type="email" name="login_email" class="form-control" id="wlbm_login_email" placeholder="<?php esc_attr_e( 'Login Email', 'WL-BM' ); ?>" value="<?php echo esc_attr( $login_email ); ?>">
												</div>

												<div class="form-group">
													<label for="wlbm_password" class="font-weight-bold"><?php echo esc_attr_x( 'Password', 'label', 'WL-BM' ); ?>:</label>
													<input type="password" name="password" class="form-control" id="wlbm_password" placeholder="<?php echo esc_attr_x( 'Password', 'placeholder', 'WL-BM' ); ?>">
												</div>

												<div class="form-group">
													<label for="wlbm_password_confirm" class="font-weight-bold"><?php echo esc_html_x( 'Confirm Password', 'label', 'WL-BM' ); ?>:</label>
													<input type="password" name="password_confirm" class="form-control" id="wlbm_password_confirm" placeholder="<?php echo esc_attr_x( 'Confirm Password', 'placeholder', 'WL-BM' ); ?>">
												</div>
											</div>

											<?php if ( ! $username ) { ?>
											<div class="wlbm-exisitng-user">
												<div class="form-group">
													<label for="wlbm_exisitng_username" class="font-weight-bold">* <?php esc_html_e( 'Existing Username', 'WL-BM' ); ?>:</label>
													<input type="text" name="existing_username" class="form-control" id="wlbm_exisitng_username" placeholder="<?php esc_attr_e( 'Enter exisitng username', 'WL-BM' ); ?>">
												</div>
											</div>
											<?php } ?>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="card col">
										<div class="card-header h5 pb-1 pt-1"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?></div>
										<div class="card-body">
											<div class="form-group">
												<label for="wlbm_extra_detail" class="font-weight-bold"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?>:</label>
												<textarea name="extra_detail" class="form-control" id="wlbm_extra_detail" rows="3" placeholder="<?php esc_attr_e( 'Extra Detail', 'WL-BM' ); ?>"><?php echo esc_html( $extra_detail ); ?></textarea>
											</div>

											<div class="form-group">
												<input type="checkbox" <?php checked( $is_active, '1', true ); ?> name="is_active" id="wlbm_is_active" value="1">&nbsp;
												<label class="form-check-label font-weight-bold" for="wlbm_is_active"><?php esc_html_e( 'Is Active?', 'WL-IM' ); ?></label>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<?php if ( $client ) { ?>
									<span class="float-left">
										<a href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $client->ID ); ?>" class="btn btn-sm btn-primary btn-info">
											<i class="fas fa-search"></i>&nbsp;
											<?php echo esc_html( 'View Client', 'WL-BM' ); ?>
										</a>
									</span>
									<?php } ?>
									<span class="float-right">
										<button type="submit" class="btn btn-sm btn-primary" id="wlbm-save-client-btn">
											<i class="fas fa-save"></i>&nbsp;
											<?php
											if ( $client ) {
												esc_html_e( 'Update Client', 'WL-BM' );
											} else {
												esc_html_e( 'Add New Client', 'WL-BM' );
											}
											?>
										</button>
									</span>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
