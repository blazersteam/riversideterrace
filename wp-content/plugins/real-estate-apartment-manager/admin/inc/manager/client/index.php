<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_CLIENTS, false );

$clients = $wpdb->get_results( "SELECT c.ID, c.name, c.address, c.phone, c.email, c.is_active, u.user_login as username, u.user_email as login_email, f.flat_number, b.name as building_name FROM {$wpdb->prefix}wlbm_clients as c LEFT OUTER JOIN {$wpdb->base_prefix}users as u ON c.user_id = u.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON c.flat_id = f.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON f.building_id = b.ID GROUP BY c.ID ORDER BY c.ID DESC" );

$clients_count = count( $clients );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center"><i class="fas fa-users"></i> <?php esc_html_e( 'Clients', 'WL-BM' ); ?></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left wlbm-sub-header-left">
							<?php
								printf(
									/* translators: %s: number of clients */
									esc_html( _n( 'Showing %s client.', 'Showing %s clients.', $clients_count, 'WL-BM' ) ),
									esc_html( number_format_i18n( $clients_count ) )
								);
							?>
						</span>
						<span class="float-right wlbm-sub-header-right">
							<a href="<?php echo esc_url( $page_url . '&action=save' ); ?>" class="btn btn-sm btn-primary">
								<i class="fas fa-plus-square"></i>&nbsp;
								<?php echo esc_html( 'Add New Client', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<table class="table table-hover" id="wlbm-client-table">
							<thead>
								<tr>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Name', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Email', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Flat Number', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Building', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Username', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Login Email', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Is Active', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Action', 'WL-BM' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ( $clients_count > 0 ) {
									foreach ( $clients as $client ) {
								?>
								<tr>
									<td><?php echo esc_html( $client->name ); ?></td>
									<td><?php echo esc_html( $client->phone ? $client->phone : '-' ); ?></td>
									<td><?php echo esc_html( $client->email ? $client->email : '-' ); ?></td>
									<td><?php echo esc_html( $client->flat_number ? $client->flat_number : '-' ); ?></td>
									<td><?php echo esc_html( $client->building_name ? $client->building_name : '-' ); ?></td>
									<td><?php echo esc_html( $client->username ? $client->username : '-' ); ?></td>
									<td><?php echo esc_html( $client->login_email ? $client->login_email : '-' ); ?></td>
									<td><?php echo esc_html( $client->is_active ? __( 'Yes', 'WL-BM') : __( 'No', 'WL-BM') ); ?></td>
									<td class="text-nowrap">
										<a class="text-info" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $client->ID ); ?>"><i class="fas fa-search"></i></a>&nbsp;&nbsp;
										<a class="text-primary" href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $client->ID ); ?>"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
										<a class="text-danger delete-client" href="" data-nonce="<?php echo esc_attr( wp_create_nonce( 'delete-client-' . $client->ID ) ); ?>" data-client="<?php echo esc_attr( $client->ID ); ?>" data-message-title="<?php esc_attr_e( 'Confirm!', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm!', 'WL-BM' ); ?>" data-delete-user-message="<?php esc_attr_e( 'Also delete user account associated with this client?', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Confirm', 'WL-BM' ); ?>"><i class="fas fa-trash"></i></a>
									</td>
								</tr>
								<?php
									}
								} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
