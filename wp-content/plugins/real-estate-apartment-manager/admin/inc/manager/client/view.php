<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_CLIENTS, false );

$client       = '';
$name         = '';
$phone        = '';
$address      = '';
$email        = '';
$flat         = '';
$building     = '';
$is_active    = '';
$extra_detail = '';
$username     = '';
$login_email  = '';
if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id     = absint( $_GET['id'] );
	$client = $wpdb->get_row( $wpdb->prepare( "SELECT c.ID, c.name, c.phone, c.address, c.email, c.extra_detail, c.is_active, c.user_id, f.flat_number, (SELECT b.name FROM {$wpdb->prefix}wlbm_buildings as b WHERE b.ID = f.building_id) as building_name FROM {$wpdb->prefix}wlbm_clients as c LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON c.flat_id = f.ID WHERE c.ID = %d GROUP BY c.ID", $id ) );
}
if ( ! $client ) {
	die();
}
$name         = $client->name;
$phone        = $client->phone;
$address      = $client->address;
$email        = $client->email;
$flat_number  = $client->flat_number;
$building     = $client->building_name;
$is_active    = $client->is_active;
$extra_detail = $client->extra_detail;

if ( $client->user_id ) {
	$user = get_user_by( 'ID', $client->user_id );
	if ( $user ) {
		$username    = $user->user_login;
		$login_email = $user->user_email;
	}
}
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-search"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: name of client */
								__( 'View Client: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $name )
						);
						?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-6">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<a href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $client->ID ); ?>" class="btn btn-sm btn-primary btn-primary">
								<i class="fas fa-edit"></i>&nbsp;
								<?php echo esc_html( 'Edit Client', 'WL-BM' ); ?>
							</a>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-user"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Client Name', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $name ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Flat Number', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $flat_number ? $flat_number : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Building', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $building ? $building : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Address', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $address ? $address : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $phone ? $phone : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Email', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $email ? $email : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Username', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $username ? $username : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Login Email', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $login_email ? $login_email : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Is Active', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $is_active ? __( 'Yes', 'WL-BM') : __( 'No', 'WL-BM') ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $extra_detail ? $extra_detail : '-' ); ?></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
