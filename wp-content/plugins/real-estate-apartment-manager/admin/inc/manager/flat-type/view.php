<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_FLAT_TYPES, false );

$flat_type    = '';
$type         = '';
$flats_count  = 0;
$extra_detail = '';
if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id        = absint( $_GET['id'] );
	$flat_type = $wpdb->get_row( $wpdb->prepare( "SELECT ft.ID, ft.type, ft.extra_detail, COUNT(f.flat_type_id) as flats_count FROM {$wpdb->prefix}wlbm_flat_types as ft LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON ft.ID = f.flat_type_id WHERE ft.ID = %d GROUP BY ft.ID", $id ) );
}
if ( ! $flat_type ) {
	die();
}
$type         = $flat_type->type;
$flats_count  = $flat_type->flats_count;
$extra_detail = $flat_type->extra_detail;
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-search"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: flat type */
								__( 'View Flat Type: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $type )
						);
						?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-6">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<a href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $flat_type->ID ); ?>" class="btn btn-sm btn-primary btn-primary">
								<i class="fas fa-edit"></i>&nbsp;
								<?php echo esc_html( 'Edit Flat Type', 'WL-BM' ); ?>
							</a>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-home"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Flat Type', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $type ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Number of Flats', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( number_format_i18n( $flats_count ) ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $extra_detail ? $extra_detail : '-' ); ?></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
