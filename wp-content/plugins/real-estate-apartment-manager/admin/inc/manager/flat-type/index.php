<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_FLAT_TYPES, false );

$flat_types = $wpdb->get_results( "SELECT ft.ID, ft.type, COUNT(f.flat_type_id) as flats_count FROM {$wpdb->prefix}wlbm_flat_types as ft LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON ft.ID = f.flat_type_id GROUP BY ft.ID ORDER BY ft.type ASC" );

$flat_types_count = count( $flat_types );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center"><i class="fas fa-home"></i> <?php esc_html_e( 'Flat Types', 'WL-BM' ); ?></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left wlbm-sub-header-left">
							<?php
								printf(
									/* translators: %s: number of flat types */
									esc_html( _n( 'Showing %s flat type.', 'Showing %s flat types.', $flat_types_count, 'WL-BM' ) ),
									esc_html( number_format_i18n( $flat_types_count ) )
								);
							?>
						</span>
						<span class="float-right wlbm-sub-header-right">
							<a href="<?php echo esc_url( $page_url . '&action=save' ); ?>" class="btn btn-sm btn-primary">
								<i class="fas fa-plus-square"></i>&nbsp;
								<?php echo esc_html( 'Add New Flat Type', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<table class="table table-hover" id="wlbm-flat-type-table">
							<thead>
								<tr>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Flat Type', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Number of Flats', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Action', 'WL-BM' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ( $flat_types_count > 0 ) {
									foreach ( $flat_types as $flat_type ) {
								?>
								<tr>
									<td><?php echo esc_html( $flat_type->type ); ?></td>
									<td><?php echo esc_html( number_format_i18n( $flat_type->flats_count ) ); ?></td>
									<td class="text-nowrap">
										<a class="text-info" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $flat_type->ID ); ?>"><i class="fas fa-search"></i></a>&nbsp;&nbsp;
										<a class="text-primary" href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $flat_type->ID ); ?>"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
										<a class="text-danger delete-flat-type" href="" data-nonce="<?php echo esc_attr( wp_create_nonce( 'delete-flat-type-' . $flat_type->ID ) ); ?>" data-flat-type="<?php echo esc_attr( $flat_type->ID ); ?>" data-message-title="<?php esc_attr_e( 'Confirm!', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm!', 'WL-BM' ); ?>"data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Confirm', 'WL-BM' ); ?>"><i class="fas fa-trash"></i></a>
									</td>
								</tr>
								<?php
									}
								} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
