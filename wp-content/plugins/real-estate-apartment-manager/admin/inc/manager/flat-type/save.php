<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_FLAT_TYPES, false );

$flat_type    = '';
$nonce_action = 'add-flat-type';
$type         = '';
$extra_detail = '';
if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id        = absint( $_GET['id'] );
	$flat_type = $wpdb->get_row( $wpdb->prepare( "SELECT ID, type, extra_detail FROM {$wpdb->prefix}wlbm_flat_types WHERE ID = %d", $id ) );
	if ( $flat_type ) {
		$nonce_action = 'edit-flat-type-' . $flat_type->ID;
		$type         = $flat_type->type;
		$extra_detail = $flat_type->extra_detail;
	}
}
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
					<?php if ( $flat_type ) { ?>
						<i class="fas fa-edit"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: flat type */
								__( 'Edit Flat Type: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $type )
						);
						?>
					<?php } else { ?>
						<i class="fas fa-plus-square"></i>
						<?php esc_html_e( 'Add New Flat Type', 'WL-BM' ); ?>
					<?php } ?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-6">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<?php esc_html_e( 'Fill all the required fields (*).', 'WL-BM' ); ?>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-home"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-save-flat-type-form">

							<?php $nonce = wp_create_nonce( $nonce_action ); ?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-manager-save-flat-type">

							<?php if ( $flat_type ) { ?>
							<input type="hidden" name="flat_type_id" value="<?php echo esc_attr( $flat_type->ID ); ?>">
							<?php } ?>

							<div class="form-group">
								<label for="wlbm_type" class="font-weight-bold">* <?php esc_html_e( 'Flat Type', 'WL-BM' ); ?>:</label>
								<input type="text" name="type" class="form-control" id="wlbm_type" placeholder="<?php esc_attr_e( 'Flat Type', 'WL-BM' ); ?>" value="<?php echo esc_attr( $type ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_extra_detail" class="font-weight-bold"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?>:</label>
								<textarea name="extra_detail" class="form-control" id="wlbm_extra_detail" rows="3" placeholder="<?php esc_attr_e( 'Extra Detail', 'WL-BM' ); ?>"><?php echo esc_html( $extra_detail ); ?></textarea>
							</div>

							<div>
								<?php if ( $flat_type ) { ?>
								<span class="float-left">
									<a href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $flat_type->ID ); ?>" class="btn btn-sm btn-primary btn-info">
										<i class="fas fa-search"></i>&nbsp;
										<?php echo esc_html( 'View Flat Type', 'WL-BM' ); ?>
									</a>
								</span>
								<?php } ?>
								<span class="float-right">
									<button type="submit" class="btn btn-sm btn-primary" id="wlbm-save-flat-type-btn">
										<i class="fas fa-save"></i>&nbsp;
										<?php
										if ( $flat_type ) {
											esc_html_e( 'Update Flat Type', 'WL-BM' );
										} else {
											esc_html_e( 'Add New Flat Type', 'WL-BM' );
										}
										?>
									</button>
								</span>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
