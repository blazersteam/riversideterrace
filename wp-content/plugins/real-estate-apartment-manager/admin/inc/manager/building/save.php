<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_BUILDINGS, false );

$building     = '';
$nonce_action = 'add-building';
$name         = '';
$phone        = '';
$address      = '';
$email        = '';
$excerpt       = '';
$extra_detail = '';
$images       = '';

$show_in_listing = '1';

if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id       = absint( $_GET['id'] );
	$building = $wpdb->get_row( $wpdb->prepare( "SELECT ID, name, phone, address, email, excerpt, show_in_listing, extra_detail, images FROM {$wpdb->prefix}wlbm_buildings WHERE ID = %d", $id ) );
	if ( $building ) {
		$nonce_action = 'edit-building-' . $building->ID;
		$name         = $building->name;
		$phone        = $building->phone;
		$address      = $building->address;
		$email        = $building->email;
		$excerpt      = $building->excerpt;
		$extra_detail = $building->extra_detail;
		$images       = $building->images ? unserialize( $building->images ) : array();

		$show_in_listing = $building->show_in_listing;
	}
}
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
					<?php if ( $building ) { ?>
						<i class="fas fa-edit"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: name of building */
								__( 'Edit Building: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $name )
						);
						?>
					<?php } else { ?>
						<i class="fas fa-plus-square"></i>
						<?php esc_html_e( 'Add New Building', 'WL-BM' ); ?>
					<?php } ?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<?php esc_html_e( 'Fill all the required fields (*).', 'WL-BM' ); ?>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-building"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-save-building-form">

							<?php $nonce = wp_create_nonce( $nonce_action ); ?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-manager-save-building">

							<?php if ( $building ) { ?>
							<input type="hidden" name="building_id" value="<?php echo esc_attr( $building->ID ); ?>">
							<?php } ?>

							<div class="form-group">
								<label for="wlbm_name" class="font-weight-bold">* <?php esc_html_e( 'Building Name', 'WL-BM' ); ?>:</label>
								<input type="text" name="name" class="form-control" id="wlbm_name" placeholder="<?php esc_attr_e( 'Building Name', 'WL-BM' ); ?>" value="<?php echo esc_attr( $name ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_address" class="font-weight-bold"><?php esc_html_e( 'Address', 'WL-BM' ); ?>:</label>
								<textarea name="address" class="form-control" id="wlbm_address" rows="3" placeholder="<?php esc_attr_e( 'Address', 'WL-BM' ); ?>"><?php echo esc_html( $address ); ?></textarea>
							</div>

							<div class="form-group">
								<label for="wlbm_phone" class="font-weight-bold"><?php esc_html_e( 'Phone', 'WL-BM' ); ?>:</label>
								<input type="text" name="phone" class="form-control" id="wlbm_phone" placeholder="<?php esc_attr_e( 'Phone', 'WL-BM' ); ?>" value="<?php echo esc_attr( $phone ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_email" class="font-weight-bold"><?php esc_html_e( 'Email', 'WL-BM' ); ?>:</label>
								<input type="email" name="email" class="form-control" id="wlbm_email" placeholder="<?php esc_attr_e( 'Email', 'WL-BM' ); ?>" value="<?php echo esc_attr( $email ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_excerpt" class="font-weight-bold"><?php esc_html_e( 'Excerpt (to be appear in listing)', 'WL-BM' ); ?>:</label>
								<textarea name="excerpt" class="form-control" id="wlbm_excerpt" rows="3" placeholder="<?php esc_attr_e( 'Excerpt', 'WL-BM' ); ?>"><?php echo esc_html( stripslashes( $excerpt ) ); ?></textarea>
							</div>

							<div class="form-group">
								<label for="wlbm_extra_detail" class="font-weight-bold"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?>:</label>
								<textarea name="extra_detail" class="form-control" id="wlbm_extra_detail" rows="3" placeholder="<?php esc_attr_e( 'Extra Detail', 'WL-BM' ); ?>"><?php echo esc_html( $extra_detail ); ?></textarea>
							</div>

							<div class="form-group">
								<input type="checkbox" <?php checked( $show_in_listing, '1', true ); ?> name="show_in_listing" id="wlbm_show_in_listing" value="1">&nbsp;
								<label class="form-check-label font-weight-bold" for="wlbm_show_in_listing"><?php esc_html_e( 'Show this Building in Listing?', 'WL-IM' ); ?></label>
							</div>

							<div class="border-top border-bottom pt-3 pb-3">
								<?php if ( $images && is_array( $images ) && count( $images ) ) { ?>
								<div class="wlbm-flat-images row">
									<?php foreach( $images as $key => $image ) { ?>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 mt-4">
										<div class="wlbm-flat-image border border-secondary p-2">
											<input type="hidden" name="images[]" value="<?php echo esc_attr( $image['id'] ); ?>">
											<div class="text-right wlbm-flat-image-remove-btn"><i class="text-danger fas fa-times"></i></div>
											<div class="wlbm-flat-image-preview" style="background-image: url('<?php echo esc_url(wp_get_attachment_image_url( $image['id'], 'large' ) ); ?>');"></div>
											<label for="wlbm-flat-image-label-<?php echo esc_attr( $image['id'] ); ?>"><?php esc_html_e( 'Enter title', 'WL-BM'); ?>:</label><br>
											<input type="text" id="wlbm-flat-image-label-<?php echo esc_attr( $image['id'] ); ?>" class="form-control wlbm-flat-image-title" name="image_titles[]" value="<?php echo esc_attr( $image['title'] ); ?>">
											<label for="wlbm-flat-image-priority-<?php echo esc_attr( $image['id'] ); ?>" class="mt-1"><?php esc_html_e( 'Enter priority', 'WL-BM'); ?>:</label><br>
											<input type="number" id="wlbm-flat-image-priority-<?php echo esc_attr( $image['id'] ); ?>" class="form-control wlbm-flat-image-priority" name="image_priorities[]" value="<?php echo esc_attr( $image['priority'] ); ?>">
										</div>
									</div>
									<?php } ?>
								</div>
								<?php } ?>
								<button type="button" id="wlbm-flat-images-btn" class="mt-3 btn btn-sm btn-outline-primary" data-title="<?php esc_attr_e( 'Upload Flat Images', 'WL-BM' ); ?>" data-button-text="<?php esc_attr_e( 'Choose Image', 'WL-BM' ); ?>" data-image-label="<?php echo esc_attr( 'Enter title', 'WL-BM'); ?>" data-image-priority="<?php echo esc_attr_e( 'Enter priority order', 'WL-BM'); ?>">
									<?php esc_html_e( 'Upload Images', 'WL-BM' ); ?>
								</button>
							</div>

							<div>
								<?php if ( $building ) { ?>
								<span class="float-left">
									<a href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $building->ID ); ?>" class="btn btn-sm btn-primary btn-info">
										<i class="fas fa-search"></i>&nbsp;
										<?php echo esc_html( 'View Building', 'WL-BM' ); ?>
									</a>
								</span>
								<?php } ?>
								<span class="float-right">
									<button type="submit" class="btn btn-sm btn-primary" id="wlbm-save-building-btn">
										<i class="fas fa-save"></i>&nbsp;
										<?php
										if ( $building ) {
											esc_html_e( 'Update Building', 'WL-BM' );
										} else {
											esc_html_e( 'Add New Building', 'WL-BM' );
										}
										?>
									</button>
								</span>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
