<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_BUILDINGS, false );

$building             = '';
$name                 = '';
$flats_count          = 0;
$phone                = '';
$address              = '';
$email                = '';
$active_clients_count = '';
$extra_detail         = '';
$images               = '';

if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id       = absint( $_GET['id'] );
	$building = $wpdb->get_row( $wpdb->prepare( "SELECT b.ID, b.name, b.phone, b.address, b.email, b.extra_detail, b.images, COUNT(f.building_id) as flats_count, (SELECT COUNT(c.ID) FROM {$wpdb->prefix}wlbm_clients as c WHERE c.flat_id = f.ID AND c.is_active = '1') as active_clients_count FROM {$wpdb->prefix}wlbm_buildings as b LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON b.ID = f.building_id WHERE b.ID = %d GROUP BY b.ID", $id ) );
}
if ( ! $building ) {
	die();
}
$name                 = $building->name;
$flats_count          = $building->flats_count;
$phone                = $building->phone;
$address              = $building->address;
$email                = $building->email;
$active_clients_count = $building->active_clients_count;
$extra_detail         = $building->extra_detail;
$images               = $building->images ? unserialize( $building->images ) : array();
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-search"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: name of building */
								__( 'View Building: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $name )
						);
						?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<a href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $building->ID ); ?>" class="btn btn-sm btn-primary btn-primary">
								<i class="fas fa-edit"></i>&nbsp;
								<?php echo esc_html( 'Edit Building', 'WL-BM' ); ?>
							</a>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-building"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Building Name', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $name ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Number of Flats', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( number_format_i18n( $flats_count ) ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Active Clients', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( number_format_i18n( $active_clients_count ) ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Address', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $address ? $address : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $phone ? $phone : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Email', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $email ? $email : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $extra_detail ? $extra_detail : '-' ); ?></span>
							</li>
							<?php if ( $images && is_array( $images ) && count( $images ) ) { ?>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Building Images', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value">
									<br>
									<div class="border-top border-bottom pt-3 pb-3">
										<div class="wlbm-flat-images row">
											<?php foreach( $images as $key => $image ) { ?>
											<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 mt-4">
												<div class="wlbm-flat-image border border-secondary p-2">
													<div class="wlbm-flat-image-preview" style="background-image: url('<?php echo esc_url(wp_get_attachment_image_url( $image['id'], 'large' ) ); ?>');"></div>
													<div class="mb-1">
														<span class="font-weight-bold"><?php esc_html_e( 'Title', 'WL-BM' ); ?>:</span>
														<span><?php echo esc_html( $image['title'] ); ?></span>
													</div>
													<div>
														<span class="font-weight-bold"><?php esc_html_e( 'Priority', 'WL-BM' ); ?>:</span>
														<span><?php echo esc_html( $image['priority'] ); ?></span>
													</div>
												</div>
											</div>
											<?php } ?>
										</div>
									</div>
								</span>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
