<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_BUILDINGS, false );

$buildings = $wpdb->get_results( "SELECT b.ID, b.name, b.address, b.phone, b.email, COUNT(f.building_id) as flats_count, (SELECT COUNT(c.ID) FROM {$wpdb->prefix}wlbm_clients as c WHERE c.flat_id = f.ID AND c.is_active = '1') as active_clients_count FROM {$wpdb->prefix}wlbm_buildings as b LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON b.ID = f.building_id GROUP BY b.ID ORDER BY b.name ASC" );

$buildings_count = count( $buildings );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center"><i class="fas fa-building"></i> <?php esc_html_e( 'Buildings', 'WL-BM' ); ?></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left wlbm-sub-header-left">
							<?php
								printf(
									/* translators: %s: number of buildings */
									esc_html( _n( 'Showing %s building.', 'Showing %s buildings.', $buildings_count, 'WL-BM' ) ),
									esc_html( number_format_i18n( $buildings_count ) )
								);
							?>
						</span>
						<span class="float-right wlbm-sub-header-right">
							<a href="<?php echo esc_url( $page_url . '&action=save' ); ?>" class="btn btn-sm btn-primary">
								<i class="fas fa-plus-square"></i>&nbsp;
								<?php echo esc_html( 'Add New Building', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<table class="table table-hover" id="wlbm-building-table">
							<thead>
								<tr>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Building Name', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Number of Flats', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Active Clients', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Email', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Action', 'WL-BM' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ( $buildings_count > 0 ) {
									foreach ( $buildings as $building ) {
								?>
								<tr>
									<td><?php echo esc_html( $building->name ); ?></td>
									<td><?php echo esc_html( number_format_i18n( $building->flats_count ) ); ?></td>
									<td><?php echo esc_html( number_format_i18n( $building->active_clients_count ) ); ?></td>
									<td><?php echo esc_html( $building->phone ? $building->phone : '-' ); ?></td>
									<td><?php echo esc_html( $building->email ? $building->email : '-' ); ?></td>
									<td class="text-nowrap">
										<a class="text-info" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $building->ID ); ?>"><i class="fas fa-search"></i></a>&nbsp;&nbsp;
										<a class="text-primary" href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $building->ID ); ?>"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
										<a class="text-danger delete-building" href="" data-nonce="<?php echo esc_attr( wp_create_nonce( 'delete-building-' . $building->ID ) ); ?>" data-building="<?php echo esc_attr( $building->ID ); ?>" data-message-title="<?php esc_attr_e( 'Confirm!', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm!', 'WL-BM' ); ?>" data-delete-flats-message="<?php esc_attr_e( 'Also delete all flats associated with this building?', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Confirm', 'WL-BM' ); ?>"><i class="fas fa-trash"></i></a>
									</td>
								</tr>
								<?php
									}
								} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
