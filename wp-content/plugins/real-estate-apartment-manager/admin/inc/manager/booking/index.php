<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_BOOKINGS, false );

$bookings = $wpdb->get_results( "SELECT bk.ID, bk.name, bk.phone, bk.address, bk.email, bk.extra_detail, bk.is_active, bk.created_at, f.flat_number, f.floor_number, ft.type, b.name as building_name FROM {$wpdb->prefix}wlbm_bookings as bk LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON f.ID = bk.flat_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_flat_types as ft ON ft.ID = f.flat_type_id ORDER BY bk.ID DESC" );

$bookings_count = count( $bookings );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center"><i class="fas fa-ticket-alt"></i> <?php esc_html_e( 'Booking Requests', 'WL-BM' ); ?></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left wlbm-sub-header-left">
							<?php
								printf(
									/* translators: %s: number of booking requests */
									esc_html( _n( 'Showing %s booking.', 'Showing %s booking requests.', $bookings_count, 'WL-BM' ) ),
									esc_html( number_format_i18n( $bookings_count ) )
								);
							?>
						</span>
					</div>
					<div class="card-body">
						<table class="table table-hover" id="wlbm-booking-table">
							<thead>
								<tr>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Name', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Address', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Building', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Flat Type', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Flat Number', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Floor Number', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Date', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Is Active', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Action', 'WL-BM' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ( $bookings_count > 0 ) {
									foreach ( $bookings as $booking ) {
								?>
								<tr>
									<td><?php echo esc_html( $booking->name ); ?></td>
									<td><?php echo esc_html( $booking->phone ); ?></td>
									<td><?php echo esc_html( $booking->address ? $booking->address : '-' ); ?></td>
									<td><?php echo esc_html( $booking->building_name ? $booking->building_name : '-' ); ?></td>
									<td><?php echo esc_html( $booking->type ? $booking->type : '-' ); ?></td>
									<td><?php echo esc_html( $booking->flat_number ? $booking->flat_number : '-' ); ?></td>
									<td><?php echo esc_html( $booking->floor_number ? $booking->floor_number : '-' ); ?></td>
									<td><?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $booking->created_at ) ) ); ?></td>
									<td><?php echo esc_html( $booking->is_active ? __( 'Yes', 'WL-BM') : __( 'No', 'WL-BM') ); ?></td>
									<td class="text-nowrap">
										<a class="text-primary" href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $booking->ID ); ?>"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
										<a class="text-danger delete-booking" href="" data-nonce="<?php echo esc_attr( wp_create_nonce( 'delete-booking-' . $booking->ID ) ); ?>" data-booking="<?php echo esc_attr( $booking->ID ); ?>" data-message-title="<?php esc_attr_e( 'Confirm!', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm!', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Confirm', 'WL-BM' ); ?>"><i class="fas fa-trash"></i></a>
									</td>
								</tr>
								<?php
									}
								} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
