<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_BOOKINGS, false );

$booking     = '';
$nonce_action = '';
$name         = '';
$phone        = '';
$address      = '';
$email        = '';
$extra_detail = '';
if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id       = absint( $_GET['id'] );
	$booking = $wpdb->get_row( $wpdb->prepare( "SELECT bk.ID, bk.name, bk.phone, bk.address, bk.email, bk.extra_detail, bk.is_active, bk.created_at, f.flat_number, f.floor_number, ft.type, b.name as building_name FROM {$wpdb->prefix}wlbm_bookings as bk LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON f.ID = bk.flat_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_flat_types as ft ON ft.ID = f.flat_type_id WHERE bk.ID = %d", $id ) );

	if ( ! $booking ) {
		die();
	}

	$nonce_action = 'edit-booking-' . $booking->ID;
	$name         = $booking->name;
	$phone        = $booking->phone;
	$address      = $booking->address ? $booking->address : '-';
	$email        = $booking->email ? $booking->email : '-';
	$extra_detail = $booking->extra_detail;
	$is_active    = $booking->is_active;

	$building_name = $booking->building_name ? $booking->building_name : '-';
	$flat_type     = $booking->type ? $booking->type : '-';
	$flat_number   = $booking->flat_number ? $booking->flat_number : '-';
	$floor_number  = $booking->floor_number ? $booking->floor_number : '-';

	$date = date_i18n( 'd-m-Y g:i A', strtotime( $booking->created_at ) );

} else {
	die();
}
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-edit"></i>
						<?php
						printf(
							wp_kses(
								/* translators: 1: name, 2: phone in booking request */
								__( 'View / Edit Booking Request: <span class="text-secondary">%1$s (%2$s)</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $name ),
							esc_html( $phone )
						);
						?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-6">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<?php esc_html_e( 'Date', 'WL-BM' ); ?>: <?php echo esc_attr( $date ); ?>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-ticket-alt"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush mb-3">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Name', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $name ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $phone ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Address', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $address ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Email', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $email ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Building', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $building_name ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Flat Type', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $flat_type ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Flat Number', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $flat_number ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Floor Number', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $floor_number ); ?></span>
							</li>
						</ul>

						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-save-booking-form">

							<?php $nonce = wp_create_nonce( $nonce_action ); ?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-manager-save-booking">

							<input type="hidden" name="booking_id" value="<?php echo esc_attr( $booking->ID ); ?>">

							<div class="form-group">
								<label for="wlbm_extra_detail" class="font-weight-bold"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?>:</label>
								<textarea name="extra_detail" class="form-control" id="wlbm_extra_detail" rows="3" placeholder="<?php esc_attr_e( 'Extra Detail', 'WL-BM' ); ?>"><?php echo esc_html( $extra_detail ); ?></textarea>
							</div>

							<div class="form-group">
								<input type="checkbox" <?php checked( $is_active, '1', true ); ?> name="is_active" id="wlbm_is_active" value="1">&nbsp;
								<label class="form-check-label font-weight-bold" for="wlbm_is_active"><?php esc_html_e( 'Is Active?', 'WL-IM' ); ?></label>
							</div>

							<div>
								<span class="float-right">
									<button type="submit" class="btn btn-sm btn-primary" id="wlbm-save-booking-btn">
										<i class="fas fa-save"></i>&nbsp;
										<?php esc_html_e( 'Update Booking Request', 'WL-BM' ); ?>
									</button>
								</span>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
