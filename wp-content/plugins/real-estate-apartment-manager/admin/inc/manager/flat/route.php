<?php
defined( 'ABSPATH' ) || die();

$action = '';
if ( isset( $_GET['action'] ) && ! empty( $_GET['action'] ) ) {
	$action = sanitize_text_field( $_GET['action'] );
}

if ( 'save' === $action ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/flat/save.php';
} elseif ( 'view' === $action ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/flat/view.php';
} else {
	require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/flat/index.php';
}
