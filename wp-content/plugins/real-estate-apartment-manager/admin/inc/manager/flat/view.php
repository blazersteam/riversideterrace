<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_FLATS, false );

$flat                 = '';
$flat_number          = '';
$title                = '';
$floor_number         = '';
$area                 = '';
$price                = '';
$rental_price         = '';
$excerpt              = '';
$description          = '';
$building_name        = '';
$type                 = '';
$active_clients_count = 0;
$extra_detail         = '';
$images               = '';

$show_in_listing         = '1';
$rental_price_in_heading = '0';

if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id   = absint( $_GET['id'] );
	$flat = $wpdb->get_row( $wpdb->prepare( "SELECT f.ID, f.flat_number, f.title, f.floor_number, f.area, f.price, f.rental_price, f.excerpt, f.description, f.images, f.extra_detail, f.show_in_listing, f.rental_price_in_heading, b.name as building_name, ft.type, (SELECT COUNT(c.ID) FROM {$wpdb->prefix}wlbm_clients as c WHERE c.flat_id = f.ID AND c.is_active = '1') as active_clients_count FROM {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON f.building_id = b.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_flat_types as ft ON f.flat_type_id = ft.ID WHERE f.ID = %d GROUP BY f.ID", $id ) );
}
if ( ! $flat ) {
	die();
}
$flat_number          = $flat->flat_number;
$title                = $flat->title;
$floor_number         = $flat->floor_number;
$area                 = $flat->area;
$price                = $flat->price;
$rental_price         = $flat->rental_price;
$excerpt              = $flat->excerpt;
$description          = $flat->description;
$building_name        = $flat->building_name;
$type                 = $flat->type;
$active_clients_count = $flat->active_clients_count;
$extra_detail         = $flat->extra_detail;
$images               = $flat->images ? unserialize( $flat->images ) : array();

$show_in_listing         = $flat->show_in_listing;
$rental_price_in_heading = $flat->rental_price_in_heading;
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-search"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: name of flat */
								__( 'View Flat: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $flat_number . ( $building_name ? " - $building_name" : '' ) )
						);
						?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<a href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $flat->ID ); ?>" class="btn btn-sm btn-primary btn-primary">
								<i class="fas fa-edit"></i>&nbsp;
								<?php echo esc_html( 'Edit Flat', 'WL-BM' ); ?>
							</a>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="far fa-building"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Flat Number', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $flat_number ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Flat Title', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $title ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Building', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $building_name ? $building_name : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Flat Type', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $type ? $type : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Floor Number', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $floor_number ? $floor_number : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Area', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $area ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Sell Price', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( number_format_i18n( $price ) ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Rental Price per Month', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( number_format_i18n( $rental_price ) ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Active Clients', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( number_format_i18n( $active_clients_count ) ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $extra_detail ? $extra_detail : '-' ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Show this Flat in Listing', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $show_in_listing ? __( 'Yes', 'WL-BM') : __( 'No', 'WL-BM') ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Show Rental Price per Month in Heading', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $rental_price_in_heading ? __( 'Yes', 'WL-BM') : __( 'No', 'WL-BM') ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Excerpt (to be appear in listing)', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><br><?php echo esc_html( $excerpt ? $excerpt : '-' ); ?></span>
							</li>
							<?php if ( $images && is_array( $images ) && count( $images ) ) { ?>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Flat Images', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value">
									<br>
									<div class="border-top border-bottom pt-3 pb-3">
										<div class="wlbm-flat-images row">
											<?php foreach( $images as $key => $image ) { ?>
											<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 mt-4">
												<div class="wlbm-flat-image border border-secondary p-2">
													<div class="wlbm-flat-image-preview" style="background-image: url('<?php echo esc_url(wp_get_attachment_image_url( $image['id'], 'large' ) ); ?>');"></div>
													<div class="mb-1">
														<span class="font-weight-bold"><?php esc_html_e( 'Title', 'WL-BM' ); ?>:</span>
														<span><?php echo esc_html( $image['title'] ); ?></span>
													</div>
													<div>
														<span class="font-weight-bold"><?php esc_html_e( 'Priority', 'WL-BM' ); ?>:</span>
														<span><?php echo esc_html( $image['priority'] ); ?></span>
													</div>
												</div>
											</div>
											<?php } ?>
										</div>
									</div>
								</span>
							</li>
							<?php } ?>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Description (to be appear in listing)', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><br><?php echo wp_kses_post( $description ? $description : '-' ); ?></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
