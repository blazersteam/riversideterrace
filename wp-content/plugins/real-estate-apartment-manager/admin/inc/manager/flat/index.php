<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_FLATS, false );

$flats = $wpdb->get_results( "
	SELECT f.ID, f.flat_number, f.title, f.floor_number, f.area, f.price, f.rental_price, b.ID as building_id, b.name as building_name, ft.ID as flat_type_id, ft.type, (SELECT COUNT(c.ID) FROM {$wpdb->prefix}wlbm_clients as c WHERE c.flat_id = f.ID AND c.is_active = '1') as active_clients_count FROM {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON f.building_id = b.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_flat_types as ft ON f.flat_type_id = ft.ID ORDER BY b.name ASC, f.flat_number ASC" );

$flats_count = count( $flats );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center"><i class="far fa-building"></i> <?php esc_html_e( 'Flats', 'WL-BM' ); ?></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left wlbm-sub-header-left">
							<?php
								printf(
									/* translators: %s: number of flats */
									esc_html( _n( 'Showing %s flat.', 'Showing %s flats.', $flats_count, 'WL-BM' ) ),
									esc_html( number_format_i18n( $flats_count ) )
								);
							?>
						</span>
						<span class="float-right wlbm-sub-header-right">
							<a href="<?php echo esc_url( $page_url . '&action=save' ); ?>" class="btn btn-sm btn-primary">
								<i class="fas fa-plus-square"></i>&nbsp;
								<?php echo esc_html( 'Add New Flat', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<table class="table table-hover" id="wlbm-flat-table">
							<thead>
								<tr>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Flat Number', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Flat Title', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Building', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Type', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Floor Number', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Area', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Sell Price', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Rental Price', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Active Clients', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Action', 'WL-BM' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ( $flats_count > 0 ) {
									foreach ( $flats as $flat ) {
								?>
								<tr>
									<td><?php echo esc_html( $flat->flat_number ); ?></td>
									<td><?php echo esc_html( $flat->title ); ?></td>
									<td><?php echo esc_html( $flat->building_name ? $flat->building_name : '-' ); ?></td>
									<td><?php echo esc_html( $flat->type ? $flat->type : '-' ); ?></td>
									<td><?php echo esc_html( $flat->floor_number ? $flat->floor_number : '-' ); ?></td>
									<td><?php echo esc_html( $flat->area ? $flat->area : '-' ); ?></td>
									<td><?php echo esc_html( number_format_i18n( $flat->price ) ); ?></td>
									<td><?php echo esc_html( number_format_i18n( $flat->rental_price ) ); ?></td>
									<td><?php echo esc_html( number_format_i18n( $flat->active_clients_count ) ); ?></td>
									<td class="text-nowrap">
										<a class="text-info" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $flat->ID ); ?>"><i class="fas fa-search"></i></a>&nbsp;&nbsp;
										<a class="text-primary" href="<?php echo esc_url( $page_url . '&action=save' . '&id=' . $flat->ID ); ?>"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
										<a class="text-danger delete-flat" href="" data-nonce="<?php echo esc_attr( wp_create_nonce( 'delete-flat-' . $flat->ID ) ); ?>" data-flat="<?php echo esc_attr( $flat->ID ); ?>" data-message-title="<?php esc_attr_e( 'Confirm!', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm!', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Confirm', 'WL-BM' ); ?>"><i class="fas fa-trash"></i></a>
									</td>
								</tr>
								<?php
									}
								} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
