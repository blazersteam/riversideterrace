<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_MANAGER_FLATS, false );

$flat          = '';
$nonce_action  = 'add-flat';
$flat_number   = '';
$floor_number  = '';
$area          = '';
$price         = '';
$rental_price  = '';
$title         = '';
$excerpt       = '';
$description   = '';
$building_id   = '';
$flat_type_id  = '';
$building_name = '';
$extra_detail  = '';
$images        = '';

$show_in_listing         = '1';
$rental_price_in_heading = '0';

if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id   = absint( $_GET['id'] );
	$flat = $wpdb->get_row( $wpdb->prepare( "SELECT f.ID, f.flat_number, f.floor_number, f.area, f.price, f.rental_price, f.title, f.excerpt, f.description, f.images, f.extra_detail, f.show_in_listing, f.rental_price_in_heading, b.ID as building_id, b.name as building_name, ft.ID as flat_type_id FROM {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON f.building_id = b.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_flat_types as ft ON f.flat_type_id = ft.ID WHERE f.ID = %d", $id ) );
	if ( $flat ) {
		$nonce_action  = 'edit-flat-' . $flat->ID;
		$flat_number   = $flat->flat_number;
		$floor_number  = $flat->floor_number;
		$area          = $flat->area;
		$price         = $flat->price;
		$rental_price  = $flat->rental_price;
		$title         = $flat->title;
		$excerpt       = $flat->excerpt;
		$description   = $flat->description;
		$building_id   = $flat->building_id;
		$flat_type_id  = $flat->flat_type_id;
		$building_name = $flat->building_name;
		$extra_detail  = $flat->extra_detail;
		$images        = $flat->images ? unserialize( $flat->images ) : array();

		$show_in_listing         = $flat->show_in_listing;
		$rental_price_in_heading = $flat->rental_price_in_heading;
	}
}

$buildings  = $wpdb->get_results( "SELECT ID, name FROM {$wpdb->prefix}wlbm_buildings ORDER BY name ASC" );
$flat_types = $wpdb->get_results( "SELECT ID, type FROM {$wpdb->prefix}wlbm_flat_types ORDER BY type ASC" );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
					<?php if ( $flat ) { ?>
						<i class="fas fa-edit"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: name of flat */
								__( 'Edit Flat: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $flat_number . ( $building_name ? " - $building_name" : '' ) )
						);
						?>
					<?php } else { ?>
						<i class="fas fa-plus-square"></i>
						<?php esc_html_e( 'Add New Flat', 'WL-BM' ); ?>
					<?php } ?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<?php esc_html_e( 'Fill all the required fields (*).', 'WL-BM' ); ?>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="far fa-building"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-save-flat-form">

							<?php $nonce = wp_create_nonce( $nonce_action ); ?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-manager-save-flat">

							<?php if ( $flat ) { ?>
							<input type="hidden" name="flat_id" value="<?php echo esc_attr( $flat->ID ); ?>">
							<?php } ?>

							<div class="form-group">
								<label for="wlbm_flat_number" class="font-weight-bold">* <?php esc_html_e( 'Flat Number', 'WL-BM' ); ?>:</label>
								<input type="text" name="flat_number" class="form-control" id="wlbm_flat_number" placeholder="<?php esc_attr_e( 'Flat Number', 'WL-BM' ); ?>" value="<?php echo esc_attr( $flat_number ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_building" class="font-weight-bold"><?php esc_html_e( 'Building', 'WL-BM' ); ?>:</label>
								<select name="building" class="form-control selectpicker" data-live-search="true" id="wlbm_building" title="<?php esc_attr_e( 'Select Building', 'WL-BM' ); ?>">
									<option value="">-------- <?php esc_html_e( 'Select Building', 'WL-BM' ); ?> --------</option>
									<?php foreach( $buildings as $building ) { ?> 
									 <option <?php selected( $building_id, $building->ID, true ); ?> value="<?php echo esc_attr( $building->ID ); ?>"><?php echo esc_html( $building->name ); ?></option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="wlbm_flat_type" class="font-weight-bold"><?php esc_html_e( 'Flat Type', 'WL-BM' ); ?>:</label>
								<select name="flat_type" class="form-control selectpicker" data-live-search="true" id="wlbm_flat_type" title="<?php esc_attr_e( 'Select Flat Type', 'WL-BM' ); ?>">
									<option value="">-------- <?php esc_html_e( 'Select Flat Type', 'WL-BM' ); ?> --------</option>
									<?php foreach( $flat_types as $flat_type ) { ?> 
									 <option <?php selected( $flat_type_id, $flat_type->ID, true ); ?> value="<?php echo esc_attr( $flat_type->ID ); ?>"><?php echo esc_html( $flat_type->type ); ?></option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="wlbm_floor_number" class="font-weight-bold"><?php esc_html_e( 'Floor Number', 'WL-BM' ); ?>:</label>
								<input type="text" name="floor_number" class="form-control" id="wlbm_floor_number" placeholder="<?php esc_attr_e( 'Floor Number', 'WL-BM' ); ?>" value="<?php echo esc_attr( $floor_number ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_area" class="font-weight-bold"><?php esc_html_e( 'Area', 'WL-BM' ); ?>:</label>
								<input type="text" name="area" class="form-control" id="wlbm_area" placeholder="<?php esc_attr_e( 'Area', 'WL-BM' ); ?>" value="<?php echo esc_attr( $area ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_price" class="font-weight-bold"><?php esc_html_e( 'Sell Price', 'WL-BM' ); ?>:</label>
								<input type="number" name="price" class="form-control" id="wlbm_price" placeholder="<?php esc_attr_e( 'Sell Price', 'WL-BM' ); ?>" min="0" step="any" value="<?php echo esc_attr( $price ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_rental_price" class="font-weight-bold"><?php esc_html_e( 'Rental Price per Month', 'WL-BM' ); ?>:</label>
								<input type="number" name="rental_price" class="form-control" id="wlbm_rental_price" placeholder="<?php esc_attr_e( 'Rental Price per Month', 'WL-BM' ); ?>" min="0" step="any" value="<?php echo esc_attr( $rental_price ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_title" class="font-weight-bold">* <?php esc_html_e( 'Flat Title (to be appear in listing)', 'WL-BM' ); ?>:</label>
								<input type="text" name="title" class="form-control" id="wlbm_title" placeholder="<?php esc_attr_e( 'Flat Title', 'WL-BM' ); ?>" value="<?php echo esc_attr( $title ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_excerpt" class="font-weight-bold"><?php esc_html_e( 'Excerpt (to be appear in listing)', 'WL-BM' ); ?>:</label>
								<textarea name="excerpt" class="form-control" id="wlbm_excerpt" rows="3" placeholder="<?php esc_attr_e( 'Excerpt', 'WL-BM' ); ?>"><?php echo esc_html( stripslashes( $excerpt ) ); ?></textarea>
							</div>

							<div class="form-group">
								<label for="wlbm_description" class="font-weight-bold"><?php esc_html_e( 'Description (to be appear in listing)', 'WL-BM' ); ?>:</label>
								<?php
								$settings = array(
									'media_buttons' => false,
									'textarea_name' => 'description',
									'textarea_rows' => 6,
									'quicktags'     => array( 'buttons' => 'strong,em,del,ul,ol,li,code,close' ),
								);
								wp_editor( wp_kses_post( stripslashes( $description ) ), 'wlbm_description', $settings );
								?>
							</div>

							<div class="form-group">
								<label for="wlbm_extra_detail" class="font-weight-bold"><?php esc_html_e( 'Extra Detail', 'WL-BM' ); ?>:</label>
								<textarea name="extra_detail" class="form-control" id="wlbm_extra_detail" rows="3" placeholder="<?php esc_attr_e( 'Extra Detail', 'WL-BM' ); ?>"><?php echo esc_html( stripslashes( $extra_detail ) ); ?></textarea>
							</div>

							<div class="form-group">
								<input type="checkbox" <?php checked( $show_in_listing, '1', true ); ?> name="show_in_listing" id="wlbm_show_in_listing" value="1">&nbsp;
								<label class="form-check-label font-weight-bold" for="wlbm_show_in_listing"><?php esc_html_e( 'Show this Flat in Listing?', 'WL-IM' ); ?></label>
							</div>

							<div class="form-group">
								<input type="checkbox" <?php checked( $rental_price_in_heading, '1', true ); ?> name="rental_price_in_heading" id="wlbm_rental_price_in_heading" value="1">&nbsp;
								<label class="form-check-label font-weight-bold" for="wlbm_rental_price_in_heading"><?php esc_html_e( 'Show Rental Price per Month in Heading?', 'WL-IM' ); ?></label>
							</div>

							<div class="border-top border-bottom pt-3 pb-3">
								<?php if ( $images && is_array( $images ) && count( $images ) ) { ?>
								<div class="wlbm-flat-images row">
									<?php foreach( $images as $key => $image ) { ?>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 mt-4">
										<div class="wlbm-flat-image border border-secondary p-2">
											<input type="hidden" name="images[]" value="<?php echo esc_attr( $image['id'] ); ?>">
											<div class="text-right wlbm-flat-image-remove-btn"><i class="text-danger fas fa-times"></i></div>
											<div class="wlbm-flat-image-preview" style="background-image: url('<?php echo esc_url(wp_get_attachment_image_url( $image['id'], 'large' ) ); ?>');"></div>
											<label for="wlbm-flat-image-label-<?php echo esc_attr( $image['id'] ); ?>"><?php esc_html_e( 'Enter title', 'WL-BM'); ?>:</label><br>
											<input type="text" id="wlbm-flat-image-label-<?php echo esc_attr( $image['id'] ); ?>" class="form-control wlbm-flat-image-title" name="image_titles[]" value="<?php echo esc_attr( $image['title'] ); ?>">
											<label for="wlbm-flat-image-priority-<?php echo esc_attr( $image['id'] ); ?>" class="mt-1"><?php esc_html_e( 'Enter priority', 'WL-BM'); ?>:</label><br>
											<input type="number" id="wlbm-flat-image-priority-<?php echo esc_attr( $image['id'] ); ?>" class="form-control wlbm-flat-image-priority" name="image_priorities[]" value="<?php echo esc_attr( $image['priority'] ); ?>">
										</div>
									</div>
									<?php } ?>
								</div>
								<?php } ?>
								<button type="button" id="wlbm-flat-images-btn" class="mt-3 btn btn-sm btn-outline-primary" data-title="<?php esc_attr_e( 'Upload Flat Images', 'WL-BM' ); ?>" data-button-text="<?php esc_attr_e( 'Choose Image', 'WL-BM' ); ?>" data-image-label="<?php echo esc_attr( 'Enter title', 'WL-BM'); ?>" data-image-priority="<?php echo esc_attr_e( 'Enter priority order', 'WL-BM'); ?>">
									<?php esc_html_e( 'Upload Images', 'WL-BM' ); ?>
								</button>
							</div>

							<div class="pt-3">
								<?php if ( $flat ) { ?>
								<span class="float-left">
									<a href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $flat->ID ); ?>" class="btn btn-sm btn-primary btn-info">
										<i class="fas fa-search"></i>&nbsp;
										<?php echo esc_html( 'View Flat', 'WL-BM' ); ?>
									</a>
								</span>
								<?php } ?>
								<span class="float-right">
									<button type="submit" class="btn btn-sm btn-primary" id="wlbm-save-flat-btn">
										<i class="fas fa-save"></i>&nbsp;
										<?php
										if ( $flat ) {
											esc_html_e( 'Update Flat', 'WL-BM' );
										} else {
											esc_html_e( 'Add New Flat', 'WL-BM' );
										}
										?>
									</button>
								</span>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
