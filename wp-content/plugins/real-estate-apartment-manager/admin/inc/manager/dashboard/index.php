<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$buildings_page_url  = menu_page_url( WLBM_MENU_MANAGER_BUILDINGS, false );
$flats_page_url      = menu_page_url( WLBM_MENU_MANAGER_FLATS, false );
$suppliers_page_url  = menu_page_url( WLBM_MENU_MANAGER_SUPPLIERS, false );
$clients_page_url    = menu_page_url( WLBM_MENU_MANAGER_CLIENTS, false );
$complaints_page_url = menu_page_url( WLBM_MENU_MANAGER_COMPLAINTS, false );
$bookings_page_url   = menu_page_url( WLBM_MENU_MANAGER_BOOKINGS, false );

$count = $wpdb->get_row( "SELECT
	(SELECT COUNT(ID) FROM {$wpdb->prefix}wlbm_buildings) as buildings_count,
	(SELECT COUNT(ID) FROM {$wpdb->prefix}wlbm_flats) as flats_count,
	(SELECT COUNT(ID) FROM {$wpdb->prefix}wlbm_clients WHERE is_active = '1') as clients_active_count,
	(SELECT COUNT(ID) FROM {$wpdb->prefix}wlbm_suppliers WHERE is_active = '1') as suppliers_active_count,
	(SELECT COUNT(ID) FROM {$wpdb->prefix}wlbm_complaints) as complaints_total_count,
	(SELECT COUNT(ID) FROM {$wpdb->prefix}wlbm_complaints WHERE is_resolved = '1') as complaints_resolved_count
" );

$pending_clients = $wpdb->get_results( "SELECT c.ID, c.name, c.phone, u.user_registered FROM {$wpdb->prefix}wlbm_clients as c, {$wpdb->base_prefix}users as u WHERE c.user_id = u.ID AND c.is_active = '0' ORDER BY u.ID DESC LIMIT 10" );

$pending_suppliers = $wpdb->get_results( "SELECT s.ID, s.name, s.phone, u.user_registered FROM {$wpdb->prefix}wlbm_suppliers as s, {$wpdb->base_prefix}users as u WHERE s.user_id = u.ID AND s.is_active = '0' ORDER BY u.ID DESC LIMIT 10" );

$complaints_unresolved = $wpdb->get_results( "SELECT cp.ID, cp.subject, cp.created_at FROM {$wpdb->prefix}wlbm_complaints as cp, {$wpdb->prefix}wlbm_clients as c WHERE c.ID = cp.client_id AND cp.is_resolved = '0' ORDER BY cp.ID DESC LIMIT 10" );

$complaints_resolved = $wpdb->get_results( "SELECT cp.ID, cp.subject, cp.resolved_at FROM {$wpdb->prefix}wlbm_complaints as cp, {$wpdb->prefix}wlbm_clients as c WHERE c.ID = cp.client_id AND cp.is_resolved = '1' ORDER BY cp.resolved_at DESC LIMIT 10" );

$booking_requests = $wpdb->get_results( "SELECT bk.ID, bk.name, bk.phone, bk.created_at FROM {$wpdb->prefix}wlbm_bookings as bk, {$wpdb->prefix}wlbm_flats as f WHERE f.ID = bk.flat_id AND bk.is_active = '1' ORDER BY bk.ID DESC LIMIT 10" );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row mb-4">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center"><i class="fas fa-tachometer-alt"></i> <?php esc_html_e( 'Dashboard', 'WL-BM' ); ?></h1>
					<p class="text-center">
						<?php esc_html_e( 'Shortcode for Client and Supplier Registration / Login', 'WL-BM' ); ?>:&nbsp;
						<strong>[ream_account]</strong>
						<br>
						<?php esc_html_e( 'Shortcode for Booking Request Form', 'WL-BM' ); ?>:&nbsp;
						<strong>[ream_booking]</strong>
					</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-4">
				<div class="wlbm-stats-item">
					<div class="row d-flex h-100">
						<div class="col-4 justify-content-center align-self-center">
							<div class="wlbm-stats-icon"><i class="fas fa-building"></i></div>
						</div>
						<div class="col-8 justify-content-center align-self-center text-right">
							<div class="wlbm-stats-count"><?php echo esc_html( $count->buildings_count ); ?></div>
							<div class="wlbm-stats-title"><a href="<?php echo esc_url( $buildings_page_url ); ?>" class="wlbm-stats-item-link"><?php esc_html_e( 'Buildings', 'WL-BM' ); ?></a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-4">
				<div class="wlbm-stats-item">
					<div class="row d-flex h-100">
						<div class="col-4 justify-content-center align-self-center">
							<div class="wlbm-stats-icon"><i class="far fa-building"></i></div>
						</div>
						<div class="col-8 justify-content-center align-self-center text-right">
							<div class="wlbm-stats-count"><?php echo esc_html( $count->flats_count ); ?></div>
							<div class="wlbm-stats-title"><a href="<?php echo esc_url( $flats_page_url ); ?>" class="wlbm-stats-item-link"><?php esc_html_e( 'Flats', 'WL-BM' ); ?></a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-4">
				<div class="wlbm-stats-item">
					<div class="row d-flex h-100">
						<div class="col-4 justify-content-center align-self-center">
							<div class="wlbm-stats-icon"><i class="fas fa-users-cog"></i></div>
						</div>
						<div class="col-8 justify-content-center align-self-center text-right">
							<div class="wlbm-stats-count"><?php echo esc_html( $count->suppliers_active_count ); ?></div>
							<div class="wlbm-stats-title"><a href="<?php echo esc_url( $suppliers_page_url ); ?>" class="wlbm-stats-item-link"><?php esc_html_e( 'Suppliers Active', 'WL-BM' ); ?></a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-4">
				<div class="wlbm-stats-item">
					<div class="row d-flex h-100">
						<div class="col-4 justify-content-center align-self-center">
							<div class="wlbm-stats-icon"><i class="fas fa-users"></i></div>
						</div>
						<div class="col-8 justify-content-center align-self-center text-right">
							<div class="wlbm-stats-count"><?php echo esc_html( $count->clients_active_count ); ?></div>
							<div class="wlbm-stats-title"><a href="<?php echo esc_url( $clients_page_url ); ?>" class="wlbm-stats-item-link"><?php esc_html_e( 'Clients Active', 'WL-BM' ); ?></a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-4">
				<div class="wlbm-stats-item">
					<div class="row d-flex h-100">
						<div class="col-4 justify-content-center align-self-center">
							<div class="wlbm-stats-icon"><i class="fas fa-comment-dots"></i></div>
						</div>
						<div class="col-8 justify-content-center align-self-center text-right">
							<div class="wlbm-stats-count"><?php echo esc_html( $count->complaints_total_count ); ?></div>
							<div class="wlbm-stats-title"><a href="<?php echo esc_url( $complaints_page_url ); ?>" class="wlbm-stats-item-link"><?php esc_html_e( 'Total Complaints', 'WL-BM' ); ?></a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-4">
				<div class="wlbm-stats-item">
					<div class="row d-flex h-100">
						<div class="col-4 justify-content-center align-self-center">
							<div class="wlbm-stats-icon"><i class="fas fa-check"></i></div>
						</div>
						<div class="col-8 justify-content-center align-self-center text-right">
							<div class="wlbm-stats-count"><?php echo esc_html( $count->complaints_resolved_count ); ?></div>
							<div class="wlbm-stats-title"><a href="<?php echo esc_url( $complaints_page_url ); ?>" class="wlbm-stats-item-link"><?php esc_html_e( 'Complaints Resolved', 'WL-BM' ); ?></a></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="text-center wlbm-recent-activities-heading"><?php esc_html_e( "Recent Activities", 'WL-BM' ); ?></div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
						<div class="wlbm-recent-list-title"><?php esc_html_e( "Last 10 Booking Requests", 'WL-BM' ); ?>
							<span class="float-right"><i class="fas fa-ticket-alt"></i></span>
						</div>
						<?php if ( count ( $booking_requests ) ) { ?>
						<ul class="list-group wlbm-recent-list">
							<?php foreach ( $booking_requests as $booking_request ) { ?>
							<li class="list-group-item">
								<span class="wlbm-recent-item-left">
									<a target="_blank" href="<?php echo esc_url( $bookings_page_url . '&action=save' . '&id=' . $booking_request->ID ); ?>">
										<?php echo esc_html( $booking_request->name ); ?>&nbsp;(<?php echo esc_html( $booking_request->phone ); ?>)
									</a>
								</span>
								<span class="wlbm-recent-item-right float-right">
									<?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $booking_request->created_at ) ) ); ?>
								</span>
							</li>
							<?php } ?>
						</ul>
						<?php } else { ?>
							<div class="alert alert-secondary">
								<?php esc_html_e( "There is no booking request.", 'WL-BM' ); ?>
							</div>
						<?php } ?>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 mb-4">
						<div class="wlbm-recent-list-title"><?php esc_html_e( "Last 10 Pending Client Registrations", 'WL-BM' ); ?>
							<span class="float-right"><i class="fas fa-user-clock"></i></span>
						</div>
						<?php if ( count ( $pending_clients ) ) { ?>
						<ul class="list-group wlbm-recent-list">
							<?php foreach ( $pending_clients as $client ) { ?>
							<li class="list-group-item">
								<span class="wlbm-recent-item-left">
									<a target="_blank" href="<?php echo esc_url( $clients_page_url . '&action=save' . '&id=' . $client->ID ); ?>"><?php echo esc_html( $client->name ); ?></a>
								</span>
								<span class="wlbm-recent-item-right float-right">
									<?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $client->user_registered ) ) ); ?>
								</span>
							</li>
							<?php } ?>
						</ul>
						<?php } else { ?>
							<div class="alert alert-secondary">
								<?php esc_html_e( "There is no pending client registration.", 'WL-BM' ); ?>
							</div>
						<?php } ?>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 mb-4">
						<div class="wlbm-recent-list-title"><?php esc_html_e( "Last 10 Pending Supplier Registrations", 'WL-BM' ); ?>
							<span class="float-right"><i class="fas fa-user-clock"></i></span>
						</div>
						<?php if ( count ( $pending_suppliers ) ) { ?>
						<ul class="list-group wlbm-recent-list">
							<?php foreach ( $pending_suppliers as $supplier ) { ?>
							<li class="list-group-item">
								<span class="wlbm-recent-item-left">
									<a target="_blank" href="<?php echo esc_url( $suppliers_page_url . '&action=save' . '&id=' . $supplier->ID ); ?>"><?php echo esc_html( $supplier->name ); ?></a>
								</span>
								<span class="wlbm-recent-item-right float-right">
									<?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $supplier->user_registered ) ) ); ?>
								</span>
							</li>
							<?php } ?>
						</ul>
						<?php } else { ?>
							<div class="alert alert-secondary">
								<?php esc_html_e( "There is no pending supplier registration.", 'WL-BM' ); ?>
							</div>
						<?php } ?>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 mb-4">
						<div class="wlbm-recent-list-title"><?php esc_html_e( "Last 10 Unresolved Complaints", 'WL-BM' ); ?>
							<span class="float-right"><i class="fas fa-times"></i></span>
						</div>
						<?php if ( count ( $complaints_unresolved ) ) { ?>
						<ul class="list-group wlbm-recent-list">
							<?php foreach ( $complaints_unresolved as $complaint ) { ?>
							<li class="list-group-item">
								<span class="wlbm-recent-item-left">
									<a target="_blank" href="<?php echo esc_url( $complaints_page_url . '&action=view' . '&id=' . $complaint->ID ); ?>">
										<?php echo esc_html( $complaint->subject ); ?>
									</a>
								</span>
								<span class="wlbm-recent-item-right float-right">
									<?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->created_at ) ) ); ?>
								</span>
							</li>
							<?php } ?>
						</ul>
						<?php } else { ?>
							<div class="alert alert-secondary"><?php esc_html_e( "There is no unresolved complaint.", 'WL-BM' ); ?></div>
						<?php } ?>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 mb-4">
						<div class="wlbm-recent-list-title"><?php esc_html_e( "Last 10 Resolved Complaints", 'WL-BM' ); ?>
							<span class="float-right"><i class="fas fa-check"></i></span>
						</div>
						<?php if ( count ( $complaints_resolved ) ) { ?>
						<ul class="list-group wlbm-recent-list">
							<?php foreach ( $complaints_resolved as $complaint ) { ?>
							<li class="list-group-item">
								<span class="wlbm-recent-item-left">
									<a target="_blank" href="<?php echo esc_url( $complaints_page_url . '&action=view' . '&id=' . $complaint->ID ); ?>">
										<?php echo esc_html( $complaint->subject ); ?>
									</a>
								</span>
								<span class="wlbm-recent-item-right float-right">
									<?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->resolved_at ) ) ); ?>
								</span>
							</li>
							<?php } ?>
						</ul>
						<?php } else { ?>
							<div class="alert alert-secondary"><?php esc_html_e( "There is no resolved complaint.", 'WL-BM' ); ?></div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
