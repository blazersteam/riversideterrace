<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Client {
	public static function save() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$client_id = isset( $_POST['client_id'] ) ? absint( $_POST['client_id'] ) : '';

			if ( $client_id ) {
				if ( ! wp_verify_nonce( $_POST[ 'edit-client-' . $client_id ], 'edit-client-' . $client_id ) ) {
					die();
				}
			} else {
				if ( ! wp_verify_nonce( $_POST['add-client'], 'add-client' ) ) {
					die();
				}
			}

			// Checks if client exists.
			if ( $client_id ) {
				$client = $wpdb->get_row( $wpdb->prepare( "SELECT ID, user_id FROM {$wpdb->prefix}wlbm_clients WHERE ID = %d", $client_id ) );

				if ( ! $client ) {
					throw new Exception( esc_html__( 'Client not found.', 'WL-BM' ) );
				}
			}

			$flat_id      = isset( $_POST['flat'] ) ? absint( $_POST['flat'] ) : NULL;
			$name         = isset( $_POST['name'] ) ? sanitize_text_field( $_POST['name'] ) : '';
			$address      = isset( $_POST['address'] ) ? sanitize_text_field( $_POST['address'] ) : '';
			$phone        = isset( $_POST['phone'] ) ? sanitize_text_field( $_POST['phone'] ) : '';
			$email        = isset( $_POST['email'] ) ? sanitize_text_field( $_POST['email'] ) : '';
			$extra_detail = isset( $_POST['extra_detail'] ) ? sanitize_text_field( $_POST['extra_detail'] ) : '';
			$is_active    = isset( $_POST['is_active'] ) ? (bool) $_POST['is_active'] : false;

			$username         = isset( $_POST['username'] ) ? sanitize_text_field( $_POST['username'] ) : '';
			$login_email      = isset( $_POST['login_email'] ) ? sanitize_text_field( $_POST['login_email'] ) : '';
			$password         = isset( $_POST['password'] ) ? $_POST['password'] : '';
			$password_confirm = isset( $_POST['password_confirm'] ) ? $_POST['password_confirm'] : '';

			$is_existing_user  = isset( $_POST['is_existing_user'] ) ? (bool) $_POST['is_existing_user'] : '';
			$existing_username = isset( $_POST['existing_username'] ) ? sanitize_text_field( $_POST['existing_username'] ) : '';

			// Start validation.
			$errors = array();

			if ( empty( $name ) ) {
				$errors['name'] = esc_html__( 'Please provide client name.', 'WL-BM' );
			}

			if ( strlen( $name ) > 255 ) {
				$errors['name'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( ! empty( $flat_id ) ) {
				$flat = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_flats WHERE ID = %d", $flat_id ) );
				if ( ! $flat ) {
					$errors['flat'] = esc_html__( 'Please select a valid flat.', 'WL-BM' );
				}
			} else {
				$flat_id = NULL;
			}

			// If existing user is checked then checks if existing username is provided by user.
			if ( ! empty( $is_existing_user ) ) {
				if ( empty( $existing_username ) ) {
					$errors['existing_username'] = esc_html__( 'Please provide client username.', 'WL-BM' );
				}
				if ( strlen( $existing_username ) > 60 ) {
					$errors['existing_username'] = esc_html__( 'Maximum length cannot exceed 60 characters.', 'WL-BM' );
				}
			// If existing user is unchecked.
			} else {
				if ( ! empty( $login_email ) && ! filter_var( $login_email, FILTER_VALIDATE_EMAIL ) ) {
					$errors['login_email'] = esc_html__( 'Please provide a valid email.', 'WL-BM' );
				}
				// If editing client and client is user.
				if ( $client_id && $client->user_id ) {
					if ( ! empty( $password ) && empty( $password_confirm ) ) {
						$errors['password_confirm'] = esc_html__( 'Please confirm password.', 'WL-BM' );
					}
					if ( ! empty( $password ) && ( $password !== $password_confirm ) ) {
						$errors['password'] = esc_html__( 'Passwords do not match.', 'WL-BM' );
					}
				// If new client or editing client.
				} else {
					if ( strlen( $username ) > 60 ) {
						$errors['username'] = esc_html__( 'Maximum length cannot exceed 60 characters.', 'WL-BM' );
					}
					if ( ! empty( $username ) && empty( $password ) ) {
						$errors['password'] = esc_html__( 'Please provide password.', 'WL-BM' );
					}
					if ( ! empty( $username ) && empty( $password_confirm ) ) {
						$errors['password_confirm'] = esc_html__( 'Please confirm password.', 'WL-BM' );
					}
					if ( ! empty( $username ) && ( $password !== $password_confirm ) ) {
						$errors['password'] = esc_html__( 'Passwords do not match.', 'WL-BM' );
					}

					// If new client or editing client and client is not user.
					if ( ! $client_id || ( $client_id && ! $client->user_id ) ) {
						if ( ( ! empty( $login_email ) || ! empty( $password ) ) && empty( $username ) ) {
							$errors['username'] = esc_html__( 'Please provide username.', 'WL-BM' );
						}
					}
				}
			}

			if ( strlen( $phone ) > 255 ) {
				$errors['phone'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( strlen( $email ) > 255 ) {
				$errors['email'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( ! empty( $email ) && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				$errors['email'] = esc_html__( 'Please provide a valid email.', 'WL-BM' );
			}
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				// Data to update or insert.
				$data = array(
					'name'         => $name,
					'phone'        => $phone,
					'email'        => $email,
					'address'      => $address,
					'extra_detail' => $extra_detail,
					'is_active'    => $is_active,
					'flat_id'      => $flat_id
				);

				// If editing client.
				if ( $client_id ) {
					// If existing user is unchecked and username or password or login_email is provided.
					if ( ! $is_existing_user && ( $username || $password || $login_email ) ) {
						// If client is user and password or login_email is provided.
						if ( $client->user_id && ( $password || $login_email ) ) {
							// Update user's password.
							$user = get_user_by( 'ID', $client->user_id );

							$user_data = array(
								'ID' => $user->ID,
							);

							// If password is provided.
							if ( $password ) {
								$user_data['user_pass'] = $password;
							}

							// If login_email is provided.
							if ( $login_email ) {
								$user_data['user_email'] = $login_email;
							}

							$user_id = wp_update_user( $user_data );
							if ( is_wp_error( $user_id ) ) {
								throw new Exception( esc_html( $user_id->get_error_message(), 'WL-BM' ) );
							}
							$user = new WP_User( $user_id );
							$data['user_id']    = $user->ID;
							$data['user_login'] = $user->user_login;
							$data['user_pass']  = $user->user_pass;

						// If client is not user and username is provided.
						} elseif ( ! $client->user_id && $username ) {
							// Insert new user.
							$user_data = array(
								'user_login' => $username,
								'user_pass'  => $password,
							);

							// If login_email is provided.
							if ( $login_email ) {
								$user_data['user_email'] = $login_email;
							}

							$user_id = wp_insert_user( $user_data );
							if ( is_wp_error( $user_id ) ) {
								throw new Exception( esc_html( $user_id->get_error_message(), 'WL-BM' ) );
							}
							$user = new WP_User( $user_id );
							$user->add_cap( WLBM_CLIENT );
							$data['user_id']    = $user->ID;
							$data['user_login'] = $user->user_login;
							$data['user_pass']  = $user->user_pass;
						}

					// If existing user is checked.
					} elseif ( $is_existing_user ) {
						$user = get_user_by( 'login', $existing_username );
						if ( ! $user ) {
							throw new Exception( esc_html__( 'Username does not exist.', 'WL-BM' ) );
						}
						$user_id = $user->ID;

						$client = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_clients WHERE user_id = %d", $user_id ) );
						if ( $client ) {
							throw new Exception( esc_html__( 'Client already eixsts with this username.', 'WL-BM' ) );
						}

						if ( $user_id ) {
							$user->add_cap( WLBM_CLIENT );
							$data['user_id']    = $user_id;
							$data['user_login'] = $user->user_login;
							$data['user_pass']  = $user->user_pass;
						}
					}

					$data['updated_at'] = date( 'Y-m-d H:i:s' );

					$success = $wpdb->update( "{$wpdb->prefix}wlbm_clients", $data, array( 'ID' => $client_id ) );
					$message = esc_html__( 'Client updated successfully.', 'WL-BM' );
					$reset   = false;

				// If new client.
				} else {
					// If existing user is checked.
					if ( $is_existing_user ) {
						$user = get_user_by( 'login', $existing_username );
						if ( ! $user ) {
							throw new Exception( esc_html__( 'Username does not exist.', 'WL-BM' ) );
						}
						$user_id = $user->ID;

						$client = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_clients WHERE user_id = %d", $user_id ) );
						if ( $client ) {
							throw new Exception( esc_html__( 'Client already eixsts with this username.', 'WL-BM' ) );
						}
					// If existing user is unchecked.
					} else {
						// If new username is provided.
						if ( ! empty( $username ) ) {
							// Insert new user.
							$user_data = array(
								'user_login' => $username,
								'user_email' => $login_email,
								'user_pass'  => $password,
							);
							$user_id = wp_insert_user( $user_data );
							if ( is_wp_error( $user_id ) ) {
								throw new Exception( esc_html( $user_id->get_error_message(), 'WL-BM' ) );
							}
							$user = new WP_User( $user_id );
						} else {
							$user_id = NULL;
						}
					}

					if ( $user_id ) {
						$user->add_cap( WLBM_CLIENT );
						$data['user_id']    = $user_id;
						$data['user_login'] = $user->user_login;
						$data['user_pass']  = $user->user_pass;
					}

					$success = $wpdb->insert( "{$wpdb->prefix}wlbm_clients", $data );
					$message = esc_html__( 'Client added successfully.', 'WL-BM' );
					$reset   = true;
				}

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $buffer );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reset' => $reset ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function delete() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$client_id   = isset( $_POST['client_id'] ) ? absint( $_POST['client_id'] ) : '';
			$delete_user = isset( $_POST['delete_user'] ) ? sanitize_text_field( $_POST['delete_user'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'delete-client-' . $client_id ], 'delete-client-' . $client_id ) ) {
				die();
			}

			// Checks if client exists.
			$client = $wpdb->get_row( $wpdb->prepare( "SELECT ID, user_id FROM {$wpdb->prefix}wlbm_clients WHERE ID = %d", $client_id ) );

			if ( ! $client ) {
				throw new Exception( esc_html__( 'Client not found.', 'WL-BM' ) );
			}

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		try {
			$wpdb->query( 'BEGIN;' );

			$user = '';
			if ( $client->user_id ) {
				$user = get_user_by( 'ID', $client->user_id );
			}

			$success = $wpdb->delete( "{$wpdb->prefix}wlbm_clients", array( 'ID' => $client_id ) );
			$message = esc_html__( 'Client deleted successfully.', 'WL-BM' );

			$exception = ob_get_clean();
			if ( ! empty( $exception ) ) {
				throw new Exception( $exception );
			}

			if ( $success === false ) {
				throw new Exception( $wpdb->last_error );
			}

			if ( $user ) {
				// Remove user capability if user exists.
				$user->remove_cap( WLBM_CLIENT );
				if ( $delete_user ) {
					// Delete user account.
					$user_deleted = is_multisite() ? wpmu_delete_user( $user->ID ) : wp_delete_user( $user->ID );
					if ( ! $user_deleted ) {
						throw new Exception( esc_html__( 'Unable to delete user account.', 'WL-BM' ) );
					}
				}
			}

			$wpdb->query( 'COMMIT;' );

			wp_send_json_success( array( 'message' => $message ) );
		} catch ( Exception $exception ) {
			$wpdb->query( 'ROLLBACK;' );
			wp_send_json_error( $exception->getMessage() );
		}
	}

	public static function get_flat_detail() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$flat_id = isset( $_POST['flat_id'] ) ? absint( $_POST['flat_id'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'get-flat-' . $flat_id ], 'get-flat-' . $flat_id ) ) {
				die();
			}

			// Checks if flat exists.
			$flat = $wpdb->get_row( $wpdb->prepare( "SELECT f.flat_number, f.floor_number, f.area, f.price, f.rental_price, b.name as building_name, (SELECT COUNT(c.ID) FROM {$wpdb->prefix}wlbm_clients as c WHERE c.flat_id = f.ID AND c.is_active = '1') as active_clients_count FROM {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON f.building_id = b.ID WHERE f.ID = %d GROUP BY f.ID", $flat_id ) );

			if ( ! $flat ) {
				throw new Exception( esc_html__( 'Flat not found.', 'WL-BM' ) );
			}

			$data = array(
				'label' => array(
					'flat_number'          => esc_html__( 'Flat Number', 'WL-BM'  ),
					'floor_number'         => esc_html__( 'Floor Number', 'WL-BM'  ),
					'area'                 => esc_html__( 'Area', 'WL-BM'  ),
					'price'                => esc_html__( 'Price', 'WL-BM'  ),
					'rental_price'         => esc_html__( 'Rental Price', 'WL-BM'  ),
					'building_name'        => esc_html__( 'Building', 'WL-BM'  ),
					'active_clients_count' => esc_html__( 'Active Clients', 'WL-BM'  ),
				),
				'flat' => array(
					'flat_number'          => esc_html( $flat->flat_number ),
					'floor_number'         => esc_html( $flat->floor_number ),
					'area'                 => esc_html( $flat->area ),
					'price'                => esc_html( number_format_i18n( $flat->price ) ),
					'rental_price'         => esc_html( number_format_i18n( $flat->rental_price ) ),
					'building_name'        => esc_html( $flat->building_name ),
					'active_clients_count' => esc_html( number_format_i18n( $flat->active_clients_count ) )
				),
			);

			wp_send_json_success( $data );

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

	}
}
