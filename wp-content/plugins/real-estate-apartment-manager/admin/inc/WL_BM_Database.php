<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Database {
	/* On plugin activation */
	public static function activation() {
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		require_once ABSPATH . 'wp-admin/includes/upgrade.php';

		/* Create buildings table */
		$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wlbm_buildings (
				ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				name varchar(191) DEFAULT NULL,
				address text DEFAULT NULL,
				phone varchar(255) DEFAULT NULL,
				email varchar(255) DEFAULT NULL,
				images text DEFAULT NULL,
				featured_image bigint(20) UNSIGNED DEFAULT NULL,
				excerpt varchar(255) DEFAULT NULL,
				show_in_listing tinyint(1) NOT NULL DEFAULT '1',
				extra_detail text DEFAULT NULL,
				created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				updated_at timestamp NULL DEFAULT NULL,
				PRIMARY KEY (ID),
				UNIQUE (name)
				) $charset_collate";
		dbDelta( $sql );

		/* Create flat_types table */
		$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wlbm_flat_types (
				ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				type varchar(191) DEFAULT NULL,
				extra_detail text DEFAULT NULL,
				created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				updated_at timestamp NULL DEFAULT NULL,
				PRIMARY KEY (ID),
				UNIQUE (type)
				) $charset_collate";
		dbDelta( $sql );

		/* Create flats table */
		$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wlbm_flats (
				ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				flat_number varchar(191) DEFAULT NULL,
				floor_number varchar(255) DEFAULT NULL,
				area varchar(255) DEFAULT NULL,
				price decimal(12,2) UNSIGNED DEFAULT NULL,
				rental_price decimal(12,2) UNSIGNED DEFAULT NULL,
				title varchar(255) DEFAULT NULL,
				images text DEFAULT NULL,
				featured_image bigint(20) UNSIGNED DEFAULT NULL,
				excerpt text DEFAULT NULL,
				description text DEFAULT NULL,
				show_in_listing tinyint(1) NOT NULL DEFAULT '1',
				rental_price_in_heading tinyint(1) NOT NULL DEFAULT '1',
				extra_detail text DEFAULT NULL,
				flat_type_id bigint(20) UNSIGNED DEFAULT NULL,
				building_id bigint(20) UNSIGNED DEFAULT NULL,
				created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				updated_at timestamp NULL DEFAULT NULL,
				PRIMARY KEY (ID),
				UNIQUE (flat_number, building_id),
				INDEX (flat_type_id),
				INDEX (building_id),
				FOREIGN KEY (flat_type_id) REFERENCES {$wpdb->prefix}wlbm_flat_types (ID) ON DELETE SET NULL,
				FOREIGN KEY (building_id) REFERENCES {$wpdb->prefix}wlbm_buildings (ID) ON DELETE SET NULL
				) $charset_collate";
		dbDelta( $sql );

		/* Create suppliers table */
		$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wlbm_suppliers (
				ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				name varchar(255) DEFAULT NULL,
				address text DEFAULT NULL,
				phone varchar(255) DEFAULT NULL,
				email varchar(255) DEFAULT NULL,
				user_login varchar(60) DEFAULT NULL,
				user_pass varchar(255) DEFAULT NULL,
				expertise varchar(255) DEFAULT NULL,
				extra_detail text DEFAULT NULL,
				is_active tinyint(1) NOT NULL DEFAULT '0',
				user_id bigint(20) UNSIGNED DEFAULT NULL,
				created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				updated_at timestamp NULL DEFAULT NULL,
				PRIMARY KEY (ID),
				INDEX (user_id),
				FOREIGN KEY (user_id) REFERENCES {$wpdb->base_prefix}users (ID) ON DELETE SET NULL
				) $charset_collate";
		dbDelta( $sql );

		/* Create building_supplier table */
		$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wlbm_building_supplier (
				ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				building_id bigint(20) UNSIGNED DEFAULT NULL,
				supplier_id bigint(20) UNSIGNED DEFAULT NULL,
				created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				updated_at timestamp NULL DEFAULT NULL,
				PRIMARY KEY (ID),
				UNIQUE (building_id, supplier_id),
				INDEX (building_id),
				INDEX (supplier_id),
				FOREIGN KEY (building_id) REFERENCES {$wpdb->prefix}wlbm_buildings (ID) ON DELETE CASCADE,
				FOREIGN KEY (supplier_id) REFERENCES {$wpdb->prefix}wlbm_suppliers (ID) ON DELETE CASCADE
				) $charset_collate";
		dbDelta( $sql );

		/* Create clients table */
		$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wlbm_clients (
				ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				name varchar(255) DEFAULT NULL,
				address text DEFAULT NULL,
				phone varchar(255) DEFAULT NULL,
				email varchar(255) DEFAULT NULL,
				user_login varchar(60) DEFAULT NULL,
				user_pass varchar(255) DEFAULT NULL,
				extra_detail text DEFAULT NULL,
				is_active tinyint(1) NOT NULL DEFAULT '0',
				flat_id bigint(20) UNSIGNED DEFAULT NULL,
				user_id bigint(20) UNSIGNED DEFAULT NULL,
				created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				updated_at timestamp NULL DEFAULT NULL,
				PRIMARY KEY (ID),
				INDEX (flat_id),
				INDEX (user_id),
				FOREIGN KEY (flat_id) REFERENCES {$wpdb->prefix}wlbm_flats (ID) ON DELETE SET NULL,
				FOREIGN KEY (user_id) REFERENCES {$wpdb->base_prefix}users (ID) ON DELETE SET NULL
				) $charset_collate";
		dbDelta( $sql );

		/* Create complaints table */
		$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wlbm_complaints (
				ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				client_id bigint(20) UNSIGNED DEFAULT NULL,
				name varchar(255) DEFAULT NULL,
				phone varchar(255) DEFAULT NULL,
				subject varchar(255) DEFAULT NULL,
				description text DEFAULT NULL,
				images text DEFAULT NULL,
				is_resolved tinyint(1) NOT NULL DEFAULT '0',
				resolved_at timestamp NULL DEFAULT NULL,
				created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				updated_at timestamp NULL DEFAULT NULL,
				PRIMARY KEY (ID),
				INDEX (client_id),
				FOREIGN KEY (client_id) REFERENCES {$wpdb->prefix}wlbm_clients (ID) ON DELETE CASCADE
				) $charset_collate";
		dbDelta( $sql );

		/* Create complaint_supplier table */
		$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wlbm_complaint_supplier (
				ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				complaint_id bigint(20) UNSIGNED DEFAULT NULL,
				supplier_id bigint(20) UNSIGNED DEFAULT NULL,
				estimate decimal(12,2) UNSIGNED DEFAULT NULL,
				is_responded tinyint(1) NOT NULL DEFAULT '0',
				responded_at timestamp NULL DEFAULT NULL,
				created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				updated_at timestamp NULL DEFAULT NULL,
				PRIMARY KEY (ID),
				UNIQUE (complaint_id, supplier_id),
				INDEX (complaint_id),
				INDEX (supplier_id),
				FOREIGN KEY (complaint_id) REFERENCES {$wpdb->prefix}wlbm_complaints (ID) ON DELETE CASCADE,
				FOREIGN KEY (supplier_id) REFERENCES {$wpdb->prefix}wlbm_suppliers (ID) ON DELETE CASCADE
				) $charset_collate";
		dbDelta( $sql );

		/* Create bookings table */
		$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wlbm_bookings (
				ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				name varchar(255) DEFAULT NULL,
				phone varchar(255) DEFAULT NULL,
				email varchar(255) DEFAULT NULL,
				address text DEFAULT NULL,
				is_active tinyint(1) NOT NULL DEFAULT '1',
				flat_id bigint(20) UNSIGNED DEFAULT NULL,
				extra_detail text DEFAULT NULL,
				created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				updated_at timestamp NULL DEFAULT NULL,
				PRIMARY KEY (ID),
				INDEX (flat_id),
				FOREIGN KEY (flat_id) REFERENCES {$wpdb->prefix}wlbm_flats (ID) ON DELETE SET NULL
				) $charset_collate";
		dbDelta( $sql );
	}

	/* On plugin deactivation */
	public static function deactivation() {
		delete_option( 'wl-ream-code' );
	}
}
