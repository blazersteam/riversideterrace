<?php
defined( 'ABSPATH' ) || die();

require_once WL_BM_PLUGIN_DIR_PATH . 'includes/helpers/WL_BM_Helper.php';

class WL_BM_Menu {
	/* Add menu */
	public static function create_menu() {
		if ( WL_BM_Helper::wl_bm_lm_valid() ) {
			/* Menu pages for Manager */
			$manager_dashboard = add_menu_page( esc_html__( 'Real Estate Apartment Manager', 'WL-BM' ), esc_html__( 'Apartment Manager', 'WL-BM' ), 'manage_options', WLBM_MENU_FBC_MANAGER, array( 'WL_BM_Menu', 'manager_dashboard' ), 'dashicons-admin-multisite', 27 );
			add_action( 'admin_print_styles-' . $manager_dashboard, array( 'WL_BM_Menu', 'manager_general_assets' ) );

			$manager_dashboard_submenu = add_submenu_page( WLBM_MENU_FBC_MANAGER, esc_html__( 'Real Estate Apartment Manager Dashboard', 'WL-BM' ), esc_html__( 'Dashboard', 'WL-BM' ), 'manage_options', WLBM_MENU_FBC_MANAGER, array( 'WL_BM_Menu', 'manager_dashboard' ) );
			add_action( 'admin_print_styles-' . $manager_dashboard_submenu, array( 'WL_BM_Menu', 'manager_general_assets' ) );

			$manager_building_submenu = add_submenu_page( WLBM_MENU_FBC_MANAGER, esc_html__( 'Buildings', 'WL-BM' ), esc_html__( 'Buildings', 'WL-BM' ), 'manage_options', WLBM_MENU_MANAGER_BUILDINGS, array( 'WL_BM_Menu', 'manager_building' ) );
			add_action( 'admin_print_styles-' . $manager_building_submenu, array( 'WL_BM_Menu', 'manager_general_assets' ) );

			$manager_flat_type_submenu = add_submenu_page( WLBM_MENU_FBC_MANAGER, esc_html__( 'Flat Types', 'WL-BM' ), esc_html__( 'Flat Types', 'WL-BM' ), 'manage_options', WLBM_MENU_MANAGER_FLAT_TYPES, array( 'WL_BM_Menu', 'manager_flat_type' ) );
			add_action( 'admin_print_styles-' . $manager_flat_type_submenu, array( 'WL_BM_Menu', 'manager_general_assets' ) );

			$manager_flat_submenu = add_submenu_page( WLBM_MENU_FBC_MANAGER, esc_html__( 'Flats', 'WL-BM' ), esc_html__( 'Add Flats', 'WL-BM' ), 'manage_options', WLBM_MENU_MANAGER_FLATS, array( 'WL_BM_Menu', 'manager_flat' ) );
			add_action( 'admin_print_styles-' . $manager_flat_submenu, array( 'WL_BM_Menu', 'manager_general_assets' ) );

			$manager_supplier_submenu = add_submenu_page( WLBM_MENU_FBC_MANAGER, esc_html__( 'Suppliers', 'WL-BM' ), esc_html__( 'Suppliers', 'WL-BM' ), 'manage_options', WLBM_MENU_MANAGER_SUPPLIERS, array( 'WL_BM_Menu', 'manager_supplier' ) );
			add_action( 'admin_print_styles-' . $manager_supplier_submenu, array( 'WL_BM_Menu', 'manager_general_assets' ) );

			$manager_client_submenu = add_submenu_page( WLBM_MENU_FBC_MANAGER, esc_html__( 'Clients', 'WL-BM' ), esc_html__( 'Clients', 'WL-BM' ), 'manage_options', WLBM_MENU_MANAGER_CLIENTS, array( 'WL_BM_Menu', 'manager_client' ) );
			add_action( 'admin_print_styles-' . $manager_client_submenu, array( 'WL_BM_Menu', 'manager_general_assets' ) );

			$manager_booking_submenu = add_submenu_page( WLBM_MENU_FBC_MANAGER, esc_html__( 'Bookings', 'WL-BM' ), esc_html__( 'Bookings', 'WL-BM' ), 'manage_options', WLBM_MENU_MANAGER_BOOKINGS, array( 'WL_BM_Menu', 'manager_booking' ) );
			add_action( 'admin_print_styles-' . $manager_booking_submenu, array( 'WL_BM_Menu', 'manager_general_assets' ) );

			$manager_complaint_submenu = add_submenu_page( WLBM_MENU_FBC_MANAGER, esc_html__( 'Complaints', 'WL-BM' ), esc_html__( 'Complaints', 'WL-BM' ), 'manage_options', WLBM_MENU_MANAGER_COMPLAINTS, array( 'WL_BM_Menu', 'manager_complaint' ) );
			add_action( 'admin_print_styles-' . $manager_complaint_submenu, array( 'WL_BM_Menu', 'manager_general_assets' ) );

			/* Menu pages for Client */
			$client_dashboard = add_menu_page( esc_html__( 'Client Area', 'WL-BM' ), esc_html__( 'Client Area', 'WL-BM' ), WLBM_CLIENT, WLBM_MENU_CLIENT_DASHBOARD, array( 'WL_BM_Menu', 'client_dashboard' ), 'dashicons-admin-multisite', 27 );
			add_action( 'admin_print_styles-' . $client_dashboard, array( 'WL_BM_Menu', 'client_general_assets' ) );

			$client_dashboard_submenu = add_submenu_page( WLBM_MENU_CLIENT_DASHBOARD, esc_html__( 'Dashboard', 'WL-BM' ), esc_html__( 'Dashboard', 'WL-BM' ), WLBM_CLIENT, WLBM_MENU_CLIENT_DASHBOARD, array( 'WL_BM_Menu', 'client_dashboard' ) );
			add_action( 'admin_print_styles-' . $client_dashboard_submenu, array( 'WL_BM_Menu', 'client_general_assets' ) );

			$client_complaint_submenu = add_submenu_page( WLBM_MENU_CLIENT_DASHBOARD, esc_html__( 'Complaints', 'WL-BM' ), esc_html__( 'Complaints', 'WL-BM' ), WLBM_CLIENT, WLBM_MENU_CLIENT_COMPLAINTS, array( 'WL_BM_Menu', 'client_complaint' ) );
			add_action( 'admin_print_styles-' . $client_complaint_submenu, array( 'WL_BM_Menu', 'client_general_assets' ) );
			
			//$client_complaint_submenu = add_submenu_page( WLBM_MENU_CLIENT_DASHBOARD, esc_html__( 'invoicehistroy', 'WL-BM' ), esc_html__( 'Invoice History', 'WL-BM' ), WLBM_CLIENT,  'admin.php?page=invoicehistroy');
			//add_action( 'admin_print_styles-' . $client_complaint_submenu, array( 'WL_BM_Menu', 'client_general_assets' ) );

			/* Menu pages for Supplier */
			$supplier_dashboard = add_menu_page( esc_html__( 'Supplier Area', 'WL-BM' ), esc_html__( 'Supplier Area', 'WL-BM' ), WLBM_SUPPLIER, WLBM_MENU_SUPPLIER_DASHBOARD, array( 'WL_BM_Menu', 'supplier_dashboard' ), 'dashicons-admin-multisite', 27 );
			add_action( 'admin_print_styles-' . $supplier_dashboard, array( 'WL_BM_Menu', 'supplier_general_assets' ) );

			$supplier_dashboard_submenu = add_submenu_page( WLBM_MENU_SUPPLIER_DASHBOARD, esc_html__( 'Dashboard', 'WL-BM' ), esc_html__( 'Dashboard', 'WL-BM' ), WLBM_SUPPLIER, WLBM_MENU_SUPPLIER_DASHBOARD, array( 'WL_BM_Menu', 'supplier_dashboard' ) );
			add_action( 'admin_print_styles-' . $supplier_dashboard_submenu, array( 'WL_BM_Menu', 'supplier_general_assets' ) );

			$supplier_complaint_submenu = add_submenu_page( WLBM_MENU_SUPPLIER_DASHBOARD, esc_html__( 'Complaints', 'WL-BM' ), esc_html__( 'Complaints', 'WL-BM' ), WLBM_SUPPLIER, WLBM_MENU_SUPPLIER_COMPLAINTS, array( 'WL_BM_Menu', 'supplier_complaint' ) );
			add_action( 'admin_print_styles-' . $supplier_complaint_submenu, array( 'WL_BM_Menu', 'supplier_general_assets' ) );

			$wl_admin_submenu = add_submenu_page( WLBM_MENU_FBC_MANAGER, esc_html__( 'License', 'WL-BM' ), esc_html__( 'License', 'WL-BM' ), 'manage_options', 'ream_license', array( 'WL_BM_Menu', 'admin_menu' ) );
			add_action( 'admin_print_styles-' . $wl_admin_submenu, array( 'WL_BM_Menu', 'admin_menu_assets' ) );

		} else {
			$wl_admin_menu = add_menu_page( esc_html__( 'Real Estate Apartment Manager', 'WL-BM' ), esc_html__( 'Apartment Manager', 'WL-BM' ), 'manage_options', 'ream_license', array( 'WL_BM_Menu', 'admin_menu' ), 'dashicons-admin-multisite', 27 );
			add_action( 'admin_print_styles-' . $wl_admin_menu, array( 'WL_BM_Menu', 'admin_menu_assets' ) );

			$wl_admin_submenu = add_submenu_page( 'ream_license', esc_html__( 'License', 'WL-BM' ), esc_html__( 'License', 'WL-BM' ), 'manage_options', 'ream_license', array( 'WL_BM_Menu', 'admin_menu' ) );
			add_action( 'admin_print_styles-' . $wl_admin_submenu, array( 'WL_BM_Menu', 'admin_menu_assets' ) );
		}
	}

	public static function admin_menu() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/admin_menu.php';
	}

	public static function admin_menu_assets() {
		wp_enqueue_style( 'wp_bm_lc', WL_BM_PLUGIN_URL . 'assets/css/admin_menu.css' );
	}

	/* Manager: Dashboard submenu */
	public static function manager_dashboard() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/dashboard/route.php';
	}

	/* Manager: Building submenu */
	public static function manager_building() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/building/route.php';
	}

	/* Manager: Flat Type submenu */
	public static function manager_flat_type() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/flat-type/route.php';
	}

	/* Manager: Flat submenu */
	public static function manager_flat() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/flat/route.php';
	}

	/* Manager: Supplier submenu */
	public static function manager_supplier() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/supplier/route.php';
	}

	/* Manager: Client submenu */
	public static function manager_client() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/client/route.php';
	}

	/* Manager: Booking submenu */
	public static function manager_booking() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/booking/route.php';
	}

	/* Manager: Complaint submenu */
	public static function manager_complaint() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/manager/complaint/route.php';
	}

	/* Manager: General assets */
	public static function manager_general_assets() {
		/* Enqueue styles */
		wp_enqueue_style( 'bootstrap4', WL_BM_PLUGIN_URL . 'assets/css/bootstrap.min.css' );
		wp_enqueue_style( 'jquery-confirm', WL_BM_PLUGIN_URL . 'assets/css/jquery-confirm.min.css' );
		wp_enqueue_style( 'font-awesome-free', WL_BM_PLUGIN_URL . 'assets/css/all.min.css' );
		wp_enqueue_style( 'toastr', WL_BM_PLUGIN_URL . 'assets/css/toastr.min.css' );
		wp_enqueue_style( 'bootstrap-select', WL_BM_PLUGIN_URL . 'assets/css/bootstrap-select.min.css' );
		wp_enqueue_style( 'bootstrap-datetimepicker', WL_BM_PLUGIN_URL . 'assets/css/bootstrap-datetimepicker.min.css' );
		wp_enqueue_style( 'wlbm-admin', WL_BM_PLUGIN_URL . 'assets/css/wlbm-admin.css' );

		/* Enqueue scripts */
		wp_enqueue_script( 'popper', WL_BM_PLUGIN_URL . 'assets/js/popper.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'bootstrap', WL_BM_PLUGIN_URL . 'assets/js/bootstrap.min.js', array( 'popper' ), true, true );
		wp_enqueue_script( 'jquery-confirm', WL_BM_PLUGIN_URL . 'assets/js/jquery-confirm.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'toastr', WL_BM_PLUGIN_URL . 'assets/js/toastr.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'bootstrap-select', WL_BM_PLUGIN_URL . 'assets/js/bootstrap-select.min.js', array( 'bootstrap' ), true, true );
		wp_enqueue_script( 'moment', WL_BM_PLUGIN_URL . 'assets/js/moment.min.js', array(), true, true );
		wp_enqueue_script( 'bootstrap-datetimepicker', WL_BM_PLUGIN_URL . 'assets/js/bootstrap-datetimepicker.min.js', array( 'bootstrap', 'moment' ), true, true );
		wp_enqueue_script( 'jQuery-print', WL_BM_PLUGIN_URL . 'assets/js/jQuery.print.js', array( 'jquery' ), true, true );
		wp_enqueue_media();

		self::datatable_export_assets();

		wp_enqueue_script( 'wlbm-admin-manager', WL_BM_PLUGIN_URL . 'assets/js/wlbm-admin-manager.js', array( 'jquery' ), '1.0.0', true );
		wp_enqueue_script( 'wlbm-admin-manager-ajax', WL_BM_PLUGIN_URL . 'assets/js/wlbm-admin-manager-ajax.js', array( 'jquery', 'jquery-form' ), '1.0.0', true );
		wp_localize_script( 'wlbm-admin-manager-ajax', 'wlbmadminurl', admin_url() );
	}

	/* Client: Dashboard submenu */
	public static function client_dashboard() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/client/dashboard/route.php';
	}

	/* Client: Complaint submenu */
	public static function client_complaint() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/client/complaint/route.php';
	}

	/* Client: General assets */
	public static function client_general_assets() {
		/* Enqueue styles */
		wp_enqueue_style( 'bootstrap', WL_BM_PLUGIN_URL . 'assets/css/bootstrap.min.css' );
		wp_enqueue_style( 'jquery-confirm', WL_BM_PLUGIN_URL . 'assets/css/jquery-confirm.min.css' );
		wp_enqueue_style( 'font-awesome-free', WL_BM_PLUGIN_URL . 'assets/css/all.min.css' );
		wp_enqueue_style( 'toastr', WL_BM_PLUGIN_URL . 'assets/css/toastr.min.css' );
		wp_enqueue_style( 'bootstrap-select', WL_BM_PLUGIN_URL . 'assets/css/bootstrap-select.min.css' );
		wp_enqueue_style( 'bootstrap-datetimepicker', WL_BM_PLUGIN_URL . 'assets/css/bootstrap-datetimepicker.min.css' );
		wp_enqueue_style( 'wlbm-admin', WL_BM_PLUGIN_URL . 'assets/css/wlbm-admin.css' );

		/* Enqueue scripts */
		wp_enqueue_script( 'popper', WL_BM_PLUGIN_URL . 'assets/js/popper.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'bootstrap', WL_BM_PLUGIN_URL . 'assets/js/bootstrap.min.js', array( 'popper' ), true, true );
		wp_enqueue_script( 'jquery-confirm', WL_BM_PLUGIN_URL . 'assets/js/jquery-confirm.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'toastr', WL_BM_PLUGIN_URL . 'assets/js/toastr.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'bootstrap-select', WL_BM_PLUGIN_URL . 'assets/js/bootstrap-select.min.js', array( 'bootstrap' ), true, true );
		wp_enqueue_script( 'moment', WL_BM_PLUGIN_URL . 'assets/js/moment.min.js', array(), true, true );
		wp_enqueue_script( 'bootstrap-datetimepicker', WL_BM_PLUGIN_URL . 'assets/js/bootstrap-datetimepicker.min.js', array( 'bootstrap', 'moment' ), true, true );
		wp_enqueue_script( 'jQuery-print', WL_BM_PLUGIN_URL . 'assets/js/jQuery.print.js', array( 'jquery' ), true, true );

		self::datatable_export_assets();

		wp_enqueue_script( 'wlbm-admin-client-ajax', WL_BM_PLUGIN_URL . 'assets/js/wlbm-admin-client-ajax.js', array( 'jquery', 'jquery-form' ), '1.0.0', true );
		wp_localize_script( 'wlbm-admin-client-ajax', 'wlbmadminurl', admin_url() );
	}

	/* Supplier: Dashboard submenu */
	public static function supplier_dashboard() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/supplier/dashboard/route.php';
	}

	/* Supplier: Complaint submenu */
	public static function supplier_complaint() {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/supplier/complaint/route.php';
	}

	/* Supplier: General assets */
	public static function supplier_general_assets() {
		/* Enqueue styles */
		wp_enqueue_style( 'bootstrap', WL_BM_PLUGIN_URL . 'assets/css/bootstrap.min.css' );
		wp_enqueue_style( 'font-awesome-free', WL_BM_PLUGIN_URL . 'assets/css/all.min.css' );
		wp_enqueue_style( 'jquery-confirm', WL_BM_PLUGIN_URL . 'assets/css/jquery-confirm.min.css' );
		wp_enqueue_style( 'toastr', WL_BM_PLUGIN_URL . 'assets/css/toastr.min.css' );
		wp_enqueue_style( 'bootstrap-select', WL_BM_PLUGIN_URL . 'assets/css/bootstrap-select.min.css' );
		wp_enqueue_style( 'bootstrap-datetimepicker', WL_BM_PLUGIN_URL . 'assets/css/bootstrap-datetimepicker.min.css' );
		wp_enqueue_style( 'wlbm-admin', WL_BM_PLUGIN_URL . 'assets/css/wlbm-admin.css' );

		/* Enqueue scripts */
		wp_enqueue_script( 'popper', WL_BM_PLUGIN_URL . 'assets/js/popper.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'bootstrap', WL_BM_PLUGIN_URL . 'assets/js/bootstrap.min.js', array( 'popper' ), true, true );
		wp_enqueue_script( 'jquery-confirm', WL_BM_PLUGIN_URL . 'assets/js/jquery-confirm.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'toastr', WL_BM_PLUGIN_URL . 'assets/js/toastr.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'bootstrap-select', WL_BM_PLUGIN_URL . 'assets/js/bootstrap-select.min.js', array( 'bootstrap' ), true, true );
		wp_enqueue_script( 'moment', WL_BM_PLUGIN_URL . 'assets/js/moment.min.js', array(), true, true );
		wp_enqueue_script( 'bootstrap-datetimepicker', WL_BM_PLUGIN_URL . 'assets/js/bootstrap-datetimepicker.min.js', array( 'bootstrap', 'moment' ), true, true );
		wp_enqueue_script( 'jQuery-print', WL_BM_PLUGIN_URL . 'assets/js/jQuery.print.js', array( 'jquery' ), true, true );

		self::datatable_export_assets();

		wp_enqueue_script( 'wlbm-admin-supplier-ajax', WL_BM_PLUGIN_URL . 'assets/js/wlbm-admin-supplier-ajax.js', array( 'jquery', 'jquery-form' ), '1.0.0', true );
		wp_localize_script( 'wlbm-admin-supplier-ajax', 'wlbmadminurl', admin_url() );
	}

	public static function datatable_export_assets() {
		/* Enqueue styles */
		wp_enqueue_style( 'dataTables-bootstrap4', WL_BM_PLUGIN_URL . 'assets/css/datatable/dataTables.bootstrap4.min.css' );
		wp_enqueue_style( 'responsive-bootstrap4', WL_BM_PLUGIN_URL . 'assets/css/datatable/responsive.bootstrap4.min.css' );
		wp_enqueue_style( 'jquery-dataTables', WL_BM_PLUGIN_URL . 'assets/css/datatable/jquery.dataTables.min.css' );
		wp_enqueue_style( 'buttons-bootstrap4', WL_BM_PLUGIN_URL . 'assets/css/datatable/buttons.bootstrap4.min.css' );

		/* Enqueue scripts */
		wp_enqueue_script( 'jquery-dataTables', WL_BM_PLUGIN_URL . 'assets/js/datatable/jquery.dataTables.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'dataTables-bootstrap4', WL_BM_PLUGIN_URL . 'assets/js/datatable/dataTables.bootstrap4.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'dataTables-responsive', WL_BM_PLUGIN_URL . 'assets/js/datatable/dataTables.responsive.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'responsive-bootstrap4', WL_BM_PLUGIN_URL . 'assets/js/datatable/responsive.bootstrap4.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'dataTables-buttons', WL_BM_PLUGIN_URL . 'assets/js/datatable/dataTables.buttons.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'buttons-bootstrap4', WL_BM_PLUGIN_URL . 'assets/js/datatable/buttons.bootstrap4.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'jszip', WL_BM_PLUGIN_URL . 'assets/js/datatable/jszip.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'vfs_fonts', WL_BM_PLUGIN_URL . 'assets/js/datatable/vfs_fonts.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'buttons-html5', WL_BM_PLUGIN_URL . 'assets/js/datatable/buttons.html5.min.js', array( 'jszip' ), true, true );
	}
}
