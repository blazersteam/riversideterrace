<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Flat_Type {
	public static function save() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$flat_type_id = isset( $_POST['flat_type_id'] ) ? absint( $_POST['flat_type_id'] ) : '';

			if ( $flat_type_id ) {
				if ( ! wp_verify_nonce( $_POST[ 'edit-flat-type-' . $flat_type_id ], 'edit-flat-type-' . $flat_type_id ) ) {
					die();
				}
			} else {
				if ( ! wp_verify_nonce( $_POST['add-flat-type'], 'add-flat-type' ) ) {
					die();
				}
			}

			// Checks if flat_type exists.
			if ( $flat_type_id ) {
				$flat_type = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_flat_types WHERE ID = %d", $flat_type_id ) );

				if ( ! $flat_type ) {
					throw new Exception( esc_html__( 'Flat type not found.', 'WL-BM' ) );
				}
			}

			$type         = isset( $_POST['type'] ) ? sanitize_text_field( $_POST['type'] ) : '';
			$extra_detail = isset( $_POST['extra_detail'] ) ? sanitize_text_field( $_POST['extra_detail'] ) : '';

			// Start validation.
			$errors = array();

			if ( empty( $type ) ) {
				$errors['type'] = esc_html__( 'Please provide flat type.', 'WL-BM' );
			}

			if ( strlen( $type ) > 191 ) {
				$errors['type'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			// Checks if flat_type exists.
			if ( $flat_type_id ) {
				$flat_type_exist = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) as count FROM {$wpdb->prefix}wlbm_flat_types WHERE type = %s AND ID != %d", $type, $flat_type_id ) );
			} else {
				$flat_type_exist = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) as count FROM {$wpdb->prefix}wlbm_flat_types WHERE type = %s", $type ) );
			}

			if ( $flat_type_exist ) {
				throw new Exception( esc_html__( 'Flat type already exists.', 'WL-BM' ) );
			}
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				// Data to update or insert.
				$data = array(
					'type'         => $type,
					'extra_detail' => $extra_detail
				);

				// Checks if update or insert.
				if ( $flat_type_id ) {
					$data['updated_at'] = date( 'Y-m-d H:i:s' );

					$success = $wpdb->update( "{$wpdb->prefix}wlbm_flat_types", $data, array( 'ID' => $flat_type_id ) );
					$message = esc_html__( 'Flat type updated successfully.', 'WL-BM' );
					$reset   = false;
				} else {
					$success = $wpdb->insert( "{$wpdb->prefix}wlbm_flat_types", $data );
					$message = esc_html__( 'Flat type added successfully.', 'WL-BM' );
					$reset   = true;
				}

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $buffer );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reset' => $reset ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function delete() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$flat_type_id  = isset( $_POST['flat_type_id'] ) ? absint( $_POST['flat_type_id'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'delete-flat-type-' . $flat_type_id ], 'delete-flat-type-' . $flat_type_id ) ) {
				die();
			}

			// Checks if flat_type exists.
			$flat_type = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_flat_types WHERE ID = %d", $flat_type_id ) );

			if ( ! $flat_type ) {
				throw new Exception( esc_html__( 'Flat type not found.', 'WL-BM' ) );
			}

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		try {
			$wpdb->query( 'BEGIN;' );

			$success = $wpdb->delete( "{$wpdb->prefix}wlbm_flat_types", array( 'ID' => $flat_type_id ) );
			$message = esc_html__( 'Flat type deleted successfully.', 'WL-BM' );

			$exception = ob_get_clean();
			if ( ! empty( $exception ) ) {
				throw new Exception( $exception );
			}

			if ( $success === false ) {
				throw new Exception( $wpdb->last_error );
			}

			$wpdb->query( 'COMMIT;' );

			wp_send_json_success( array( 'message' => $message ) );
		} catch ( Exception $exception ) {
			$wpdb->query( 'ROLLBACK;' );
			wp_send_json_error( $exception->getMessage() );
		}
	}
}
