<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Flat {
	public static function save() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$flat_id = isset( $_POST['flat_id'] ) ? absint( $_POST['flat_id'] ) : '';

			if ( $flat_id ) {
				if ( ! wp_verify_nonce( $_POST[ 'edit-flat-' . $flat_id ], 'edit-flat-' . $flat_id ) ) {
					die();
				}
			} else {
				if ( ! wp_verify_nonce( $_POST['add-flat'], 'add-flat' ) ) {
					die();
				}
			}

			// Checks if flat exists.
			if ( $flat_id ) {
				$flat = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_flats WHERE id = %d", $flat_id ) );

				if ( ! $flat ) {
					throw new Exception( esc_html__( 'Flat not found.', 'WL-BM' ) );
				}
			}

			$flat_number  = isset( $_POST['flat_number'] ) ? sanitize_text_field( $_POST['flat_number'] ) : '';
			$building_id  = isset( $_POST['building'] ) ? absint( $_POST['building'] ) : NULL;
			$flat_type_id = isset( $_POST['flat_type'] ) ? absint( $_POST['flat_type'] ) : NULL;
			$floor_number = isset( $_POST['floor_number'] ) ? sanitize_text_field( $_POST['floor_number'] ) : '';
			$area         = isset( $_POST['area'] ) ? sanitize_text_field( $_POST['area'] ) : '';
			$price        = number_format( isset( $_POST['price'] ) ? max( (float) sanitize_text_field( $_POST['price'] ), 0 ) : 0, 2, '.', '' );
			$rental_price = number_format( isset( $_POST['rental_price'] ) ? max( (float) sanitize_text_field( $_POST['rental_price'] ), 0 ) : 0, 2, '.', '' );
			$extra_detail = isset( $_POST['extra_detail'] ) ? sanitize_text_field( stripslashes( $_POST['extra_detail'] ) ) : '';
			$title        = isset( $_POST['title'] ) ? sanitize_text_field( $_POST['title'] ) : '';
			$excerpt      = isset( $_POST['excerpt'] ) ? sanitize_text_field( stripslashes( $_POST['excerpt'] ) ) : '';
			$description  = isset( $_POST['description'] ) ? wp_kses_post( stripslashes( $_POST['description'] ) ) : '';

			$show_in_listing         = isset( $_POST['show_in_listing'] ) ? (bool) $_POST['show_in_listing'] : false;
			$rental_price_in_heading = isset( $_POST['rental_price_in_heading'] ) ? (bool) $_POST['rental_price_in_heading'] : false;

			$images           = ( isset( $_POST['images'] ) && is_array( $_POST['images'] ) ) ? $_POST['images'] : array();
			$image_titles     = ( isset( $_POST['image_titles'] ) && is_array( $_POST['image_titles'] ) ) ? $_POST['image_titles'] : array();
			$image_priorities = ( isset( $_POST['image_priorities'] ) && is_array( $_POST['image_priorities'] ) ) ? $_POST['image_priorities'] : array();


			// Start validation.
			$errors = array();

			if ( empty( $flat_number ) ) {
				$errors['flat_number'] = esc_html__( 'Please provide flat number.', 'WL-BM' );
			}

			if ( strlen( $flat_number ) > 191 ) {
				$errors['flat_number'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( empty( $title ) ) {
				$errors['title'] = esc_html__( 'Please provide flat title.', 'WL-BM' );
			}

			if ( strlen( $title ) > 255 ) {
				$errors['title'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			// Checks if flat number exists in the building.
			if ( $flat_id ) {
				$flat_exist = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) as count FROM {$wpdb->prefix}wlbm_flats WHERE flat_number = %s AND building_id = %d AND ID != %d", $flat_number, $building_id, $flat_id ) );
			} else {
				$flat_exist = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) as count FROM {$wpdb->prefix}wlbm_flats WHERE flat_number = %s AND building_id = %d", $flat_number, $building_id ) );
			}

			if ( $flat_exist ) {
				throw new Exception( esc_html__( 'Flat number already exists in this building.', 'WL-BM' ) );
			}

			if ( ! empty( $building_id ) ) {
				$building = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_buildings WHERE ID = %d", $building_id ) );
				if ( ! $building ) {
					$errors['building'] = esc_html__( 'Please select a valid building.', 'WL-BM' );
				}
			} else {
				$building_id = NULL;
			}

			if ( ! empty( $flat_type_id ) ) {
				$flat_type = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_flat_types WHERE ID = %d", $flat_type_id ) );
				if ( ! $flat_type ) {
					$errors['flat_type'] = esc_html__( 'Please select a valid flat type.', 'WL-BM' );
				}
			} else {
				$flat_type_id = NULL;
			}

			if ( strlen( $floor_number ) > 255 ) {
				$errors['floor_number'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( strlen( $area ) > 255 ) {
				$errors['area'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			// Flat Images validation.
			if ( ! ( is_array( $images ) && is_array( $image_titles ) && is_array( $image_priorities ) ) ) {
				throw new Exception( esc_html__( 'Invalid flat images.', 'WL-BM' ) );
			}

			if ( ( count( $images ) !== count( $image_titles ) ) || ( count( $images ) !== count( $image_priorities ) ) ) {
				throw new Exception( esc_html__( 'Invalid flat images.', 'WL-BM' ) );
			}
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				$flat_images    = array();
				$featured_image = NULL;

				if ( count( $images ) > 0 ) {
					$image_priorities = array_map( function( $value ) {
						return (int) $value;
					}, $image_priorities );

					$highest_priority = min( $image_priorities );

					foreach( $images as $key => $value ) {
						$priority = $image_priorities[ $key ];
						$value    = absint( $value );
						array_push(
							$flat_images,
							array(
								'id'       => $value,
								'title'    => sanitize_text_field( $image_titles[ $key ] ),
								'priority' => $priority
							)
						);

						if ( $highest_priority === $priority ) {
							$featured_image = $value;
						}
					}

					$flat_images = serialize( $flat_images );
				} else {
					$flat_images = NULL;
				}

				// Data to update or insert.
				$data = array(
					'flat_number'    => $flat_number,
					'floor_number'   => $floor_number,
					'area'           => $area,
					'price'          => $price,
					'rental_price'   => $rental_price,
					'title'          => $title,
					'excerpt'        => $excerpt,
					'description'    => $description,
					'extra_detail'   => $extra_detail,
					'images'         => $flat_images,
					'featured_image' => $featured_image,
					'building_id'    => $building_id,
					'flat_type_id'   => $flat_type_id
				);

				$data['show_in_listing']         = $show_in_listing;
				$data['rental_price_in_heading'] = $rental_price_in_heading;

				// Checks if update or insert.
				if ( $flat_id ) {
					$data['updated_at'] = date( 'Y-m-d H:i:s' );

					$success = $wpdb->update( "{$wpdb->prefix}wlbm_flats", $data, array( 'ID' => $flat_id ) );
					$message = esc_html__( 'Flat updated successfully.', 'WL-BM' );
					$reset   = false;
				} else {
					$success = $wpdb->insert( "{$wpdb->prefix}wlbm_flats", $data );
					$message = esc_html__( 'Flat added successfully.', 'WL-BM' );
					$reset   = true;
				}

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $buffer );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reset' => $reset ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function delete() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$flat_id = isset( $_POST['flat_id'] ) ? absint( $_POST['flat_id'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'delete-flat-' . $flat_id ], 'delete-flat-' . $flat_id ) ) {
				die();
			}

			// Checks if flat exists.
			$flat = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_flats WHERE id = %d", $flat_id ) );

			if ( ! $flat ) {
				throw new Exception( esc_html__( 'Flat not found.', 'WL-BM' ) );
			}

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		try {
			$wpdb->query( 'BEGIN;' );

			$success = $wpdb->delete( "{$wpdb->prefix}wlbm_flats", array( 'ID' => $flat_id ) );
			$message = esc_html__( 'Flat deleted successfully.', 'WL-BM' );

			$exception = ob_get_clean();
			if ( ! empty( $exception ) ) {
				throw new Exception( $exception );
			}

			if ( $success === false ) {
				throw new Exception( $wpdb->last_error );
			}

			$wpdb->query( 'COMMIT;' );

			wp_send_json_success( array( 'message' => $message ) );
		} catch ( Exception $exception ) {
			$wpdb->query( 'ROLLBACK;' );
			wp_send_json_error( $exception->getMessage() );
		}
	}
}
