<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_CLIENT_COMPLAINTS, false );

$user   = wp_get_current_user();
$client = $wpdb->get_row( $wpdb->prepare( "SELECT c.ID, c.name, c.phone, f.flat_number, f.floor_number, b.name as building_name FROM {$wpdb->prefix}wlbm_clients as c, {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id WHERE c.user_id = %d AND f.ID = c.flat_id AND c.is_active = '1'", $user->ID ) );

if ( ! $client ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/client/partials/check-invalid-client.php';
}

$name          = $client->name;
$phone         = $client->phone;
$flat_number   = $client->flat_number;
$floor_number  = $client->floor_number;
$building_name = $client->building_name;

$complaints = $wpdb->get_results( $wpdb->prepare( "SELECT cp.ID, cp.name, cp.phone, cp.subject, cp.description, cp.is_resolved, cp.created_at, cs.is_responded, cs.responded_at FROM {$wpdb->prefix}wlbm_complaints as cp LEFT OUTER JOIN {$wpdb->prefix}wlbm_complaint_supplier as cs ON cs.complaint_id = cp.ID JOIN {$wpdb->prefix}wlbm_clients as c ON c.ID = cp.client_id AND cp.client_id = %d ORDER BY cp.ID DESC", $client->ID ) );

$complaints_count = count( $complaints );

$complaint_ids = $wpdb->get_col( $wpdb->prepare( "SELECT DISTINCT cs.complaint_id FROM {$wpdb->prefix}wlbm_complaint_supplier as cs, {$wpdb->prefix}wlbm_complaints as cp, {$wpdb->prefix}wlbm_clients as c WHERE cp.ID = cs.complaint_id AND c.ID = cp.client_id AND c.ID = %d", $client->ID ) );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-comment-dots"></i>
						<?php esc_html_e( 'Complaints', 'WL-BM' ); ?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left wlbm-sub-header-left">
							<?php
								printf(
									/* translators: %s: number of complaints */
									esc_html( _n( 'Showing %s complaints.', 'Showing %s complaints.', $complaints_count, 'WL-BM' ) ),
									esc_html( number_format_i18n( $complaints_count ) )
								);
							?>
						</span>
						<span class="float-right wlbm-sub-header-right">
							<a href="<?php echo esc_url( $page_url . '&action=make' ); ?>" class="btn btn-sm btn-primary">
								<i class="fas fa-plus-square"></i>&nbsp;
								<?php echo esc_html( 'Make Complaint', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<table class="table table-hover" id="wlbm-complaint-table">
							<thead>
								<tr>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Subject', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Message', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Date', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Status', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Action', 'WL-BM' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ( $complaints_count > 0 ) {
									foreach ( $complaints as $complaint ) {
										$subject     = $complaint->subject;
										$description = stripcslashes( $complaint->description );
										$description = implode( ' ', array_slice( explode( ' ', $description ), 0, 25 ) ) . ' &hellip;';
										$date        = date_i18n( 'd-m-Y g:i A', strtotime( $complaint->created_at ) );
										if ( $complaint->is_resolved ) {
											$status = '<span class="text-success font-weight-bold">' . esc_html__( 'Resolved', 'WL-BM' ) . '</span>';
										} else {
											if ( $complaint->is_responded ) {
												$status = '<span class="text-info font-weight-bold">' . esc_html__( 'Supplier Responded', 'WL-BM' ) . '</span>';
												$status .= '<br><span class="text-secondary">' . esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->responded_at ) ) ) . '<span>';
											} elseif ( in_array( $complaint->ID, $complaint_ids ) ) {
												$status = '<span class="text-danger font-weight-bold">' . esc_html__( 'Unresolved', 'WL-BM' ) . '</span> - <span class="text-primary font-weight-bold">' . esc_html__( 'Forwarded by Admin', 'WL-BM' ) . '</span>';
											} else {
												$status = '<span class="text-danger font-weight-bold">' . esc_html__( 'Unresolved', 'WL-BM' ) . '</span>';
											}
										}
								?>
								<tr>
									<td class="border-top-0 text-nowrap"><?php echo esc_html( $subject ); ?></td>
									<td><?php echo esc_html( $description ); ?></td>
									<td class="text-nowrap"><?php echo esc_html( $date ); ?></td>
									<td>
										<?php echo wp_kses( $status, array( 'span' => array( 'class' => array() ), 'br' => array() ) ); ?>
									</td>
									<td class="text-nowrap">
										<a class="text-info" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $complaint->ID ); ?>"><i class="fas fa-search"></i></a>
									</td>
								</tr>
								<?php
									}
								} else { ?>
								<tr>
									<td colspan="5" class="text-center font-weight-bold text-secondary">
										<?php esc_html_e( "You haven't made any complaint yet.", 'WL-BM' ); ?>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
