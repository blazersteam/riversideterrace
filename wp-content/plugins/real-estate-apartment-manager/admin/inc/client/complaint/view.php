<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_CLIENT_COMPLAINTS, false );

$user   = wp_get_current_user();
$client = $wpdb->get_row( $wpdb->prepare( "SELECT c.ID, c.name, c.phone, f.flat_number, f.floor_number, b.name as building_name FROM {$wpdb->prefix}wlbm_clients as c, {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id WHERE c.user_id = %d AND f.ID = c.flat_id AND c.is_active = '1'", $user->ID ) );

if ( ! $client ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/client/partials/check-invalid-client.php';
}

$complaint   = '';
$subject     = '';
$description = '';
$status      = '';
$date        = '';
$images      = array();
$estimate    = '';
if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id        = absint( $_GET['id'] );
	$complaint = $wpdb->get_row( $wpdb->prepare( "SELECT cp.ID, cp.subject, cp.description, cp.images, cp.is_resolved, cp.resolved_at, cp.created_at, cs.is_responded, cs.responded_at, cs.estimate, s.name as supplier_name, s.phone as supplier_phone, s.email as supplier_email FROM {$wpdb->prefix}wlbm_complaints as cp LEFT OUTER JOIN {$wpdb->prefix}wlbm_complaint_supplier as cs ON cs.complaint_id = cp.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_suppliers as s ON s.ID = cs.supplier_id WHERE cp.ID = %d AND cp.client_id = %d", $id, $client->ID ) );
}
if ( ! $complaint ) {
	die();
}
$subject      = $complaint->subject;
$description  = stripcslashes( $complaint->description );
$date         = date_i18n( 'd-m-Y g:i A', strtotime( $complaint->created_at ) );
$images       = unserialize( $complaint->images );
$supplier_ids = $wpdb->get_col( $wpdb->prepare( "SELECT supplier_id FROM {$wpdb->prefix}wlbm_complaint_supplier WHERE complaint_id = %d", $complaint->ID ) );
if ( $complaint->is_resolved ) {
	$status = '<span class="text-success font-weight-bold">' . esc_html__( 'Resolved', 'WL-BM' ) . '</span>';
} else {
	if ( $complaint->is_responded ) {
		$status = '<span class="text-info font-weight-bold">' . esc_html__( 'Supplier Responded', 'WL-BM' ) . '</span>';
		$status .= '&nbsp;<span class="text-secondary">' . esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->responded_at ) ) ) . '<span>';

		$estimate = $complaint->estimate;
	} elseif ( count( $supplier_ids ) ) {
		$status = '<span class="text-danger font-weight-bold">' . esc_html__( 'Unresolved', 'WL-BM' ) . '</span> - <span class="text-primary font-weight-bold">' . esc_html__( 'Forwarded by Admin', 'WL-BM' ) . '</span>';
	} else {
		$status = '<span class="text-danger font-weight-bold">' . esc_html__( 'Unresolved', 'WL-BM' ) . '</span>';
	}
}
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-search"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: subject of complaint */
								__( 'Complaint: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $subject )
						);
						?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="card col">
					<div class="card-header">
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-user"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Subject', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $subject ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Message', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $description ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Date', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $date ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Status', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value">
									<?php echo wp_kses( $status, array( 'span' => array( 'class' => array() ) ) ); ?>
								</span>
							</li>
							<?php if ( $estimate ) { ?>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Estimated Amount', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $estimate ); ?></span>
							</li>
							<?php } ?>
							<?php if ( $complaint->is_responded ) { ?>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Supplier Name', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $complaint->supplier_name ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Contact Number', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $complaint->supplier_phone ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Email Address', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $complaint->supplier_email ); ?></span>
							</li>
							<?php } ?>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Images', 'WL-BM' ); ?></span>:
								<span class="wlbm-list-value">
									<ul class="list-group mt-1 wlbm-complaint-images">
										<?php
										foreach( $images as $image ) : ?>
											<li class="list-group-item list-group-flush">
												<a target="_blank" href="<?php echo esc_url( wp_get_attachment_url( $image ) ); ?>">
													<img class="img-responsive" src="<?php echo esc_url( wp_get_attachment_url( $image ) ); ?>">
												</a>
											</li>
										<?php
										endforeach; ?>
									</ul>
								</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<?php if ( ! $complaint->is_resolved ) { ?>
				<div class="card col">
					<div class="card-header">
						<h6><?php echo esc_html_e( 'Mark as Resolved', 'WL-BM' ); ?>: </h6>
					</div>
					<div class="card-body">
						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-resolve-complaint-form">
							<?php
							$nonce_action = 'resolve-complaint-' . $complaint->ID;
							$nonce        = wp_create_nonce( $nonce_action );
							?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-client-resolve-complaint">

							<input type="hidden" name="complaint_id" value="<?php echo esc_attr( $complaint->ID ); ?>">

							<div class="clearfix">
								<span class="float-right">
									<button type="submit" class="btn btn-sm btn-outline-success" id="wlbm-resolve-complaint-btn" data-message-title="<?php esc_attr_e( 'Please confirm.', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Are you sure to mark this complain as resolved?', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Resolve', 'WL-BM' ); ?>">
										<i class="fas fa-check"></i>&nbsp;
										<?php esc_html_e( 'Mark as Resolved', 'WL-BM' ); ?>
									</button>
								</span>
							</div>

						</form>
					</div>
				</div>
				<?php } else { ?>
				<div class="card col">
					<div class="card-header">
						<h6><?php echo esc_html_e( 'Resolved', 'WL-BM' ); ?>: </h6>
					</div>
					<div class="card-body">
						<ul class="list-group">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Resolved On', 'WL-BM' ); ?>: </span><br>
								<span class="wlbm-list-value"><?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->resolved_at ) ) ); ?></span>
							</li>
						</ul>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
