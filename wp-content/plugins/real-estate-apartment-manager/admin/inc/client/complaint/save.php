<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_CLIENT_COMPLAINTS, false );

$user   = wp_get_current_user();
$client = $wpdb->get_row( $wpdb->prepare( "SELECT c.name, c.phone, f.flat_number, f.floor_number, b.name as building_name FROM {$wpdb->prefix}wlbm_clients as c, {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id WHERE c.user_id = %d AND f.ID = c.flat_id AND c.is_active = '1'", $user->ID ) );

if ( ! $client ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/client/partials/check-invalid-client.php';
}

$nonce_action = 'add-complaint';

$name          = $client->name;
$phone         = $client->phone;
$flat_number   = $client->flat_number;
$floor_number  = $client->floor_number;
$building_name = $client->building_name;
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-comment-dots"></i>
						<?php esc_html_e( 'Make Complain', 'WL-BM' ); ?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-md-6">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left">
							<?php esc_html_e( 'Fill all the required fields (*).', 'WL-BM' ); ?>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-comment-dots"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-save-complaint-form">

							<?php $nonce = wp_create_nonce( $nonce_action ); ?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-client-save-complaint">

							<div class="form-group">
								<label for="wlbm_name" class="font-weight-bold">* <?php esc_html_e( 'Your Name', 'WL-BM' ); ?>:</label>
								<input type="text" name="name" class="form-control" id="wlbm_name" placeholder="<?php esc_attr_e( 'Your Name', 'WL-BM' ); ?>" value="<?php echo esc_attr( $name ); ?>">
							</div>

							<?php if ( $building_name ) { ?>
							<div class="mb-2">
								<span class="font-weight-bold"><?php esc_html_e( 'Building', 'WL-BM' ); ?>:</span><br>
								<span class="h6 text-secondary"><?php echo esc_html( $building_name ); ?></span><br>
							</div>
							<?php } ?>

							<div class="mb-2">
								<span class="font-weight-bold"><?php esc_html_e( 'Flat', 'WL-BM' ); ?>:</span><br>
								<span class="h6 text-secondary"><?php echo esc_html( $flat_number ); ?></span><br>
							</div>

							<div class="mb-2">
								<span class="font-weight-bold"><?php esc_html_e( 'Floor Number', 'WL-BM' ); ?>:</span><br>
								<span class="h6 text-secondary"><?php echo esc_html( $floor_number ); ?></span><br>
							</div>

							<div class="form-group">
								<label for="wlbm_phone" class="font-weight-bold">* <?php esc_html_e( 'Contact Number', 'WL-BM' ); ?>:</label>
								<input type="text" name="phone" class="form-control" id="wlbm_phone" placeholder="<?php esc_attr_e( 'Contact Number', 'WL-BM' ); ?>" value="<?php echo esc_attr( $phone ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_subject" class="font-weight-bold">* <?php esc_html_e( 'Subject', 'WL-BM' ); ?>:</label>
								<input type="text" name="subject" class="form-control" id="wlbm_subject" placeholder="<?php esc_attr_e( 'Enter Subject / Title', 'WL-BM' ); ?>">
							</div>

							<div class="form-group">
								<label for="wlbm_description" class="font-weight-bold">* <?php esc_html_e( 'Your Message', 'WL-BM' ); ?>:</label>
								<textarea name="description" class="form-control" id="wlbm_description" rows="6" placeholder="<?php esc_attr_e( 'Your Message', 'WL-BM' ); ?>"></textarea>
							</div>

							<div class="form-group">
								<label for="wlbm_images" class="font-weight-bold"><?php esc_html_e( 'Upload Image(s)', 'WL-BM' ); ?>:</label>
								<br>
								<label for="wlbm_images">(<?php esc_html_e( 'Hold Ctrl to select multiple images', 'WL-BM' ); ?>)</label>
								<br>
								<input type="file" name="images[]" id="wlbm_images" multiple>&nbsp;
							</div>

							<div>
								<span class="float-right">
									<button type="submit" class="btn btn-sm btn-primary" id="wlbm-save-complaint-btn" data-message-title="<?php esc_attr_e( 'Please confirm.', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm the submission.', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Confirm', 'WL-BM' ); ?>">
										<i class="fas fa-share"></i>&nbsp;
										<?php esc_html_e( 'Submit', 'WL-BM' ); ?>
									</button>
								</span>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
