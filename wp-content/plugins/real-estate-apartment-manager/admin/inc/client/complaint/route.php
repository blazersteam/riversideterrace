<?php
defined( 'ABSPATH' ) || die();

$action = '';
if ( isset( $_GET['action'] ) && ! empty( $_GET['action'] ) ) {
	$action = sanitize_text_field( $_GET['action'] );
}

if ( 'make' === $action ) {
	require_once( WL_BM_PLUGIN_DIR_PATH . 'admin/inc/client/complaint/save.php' );
} elseif ( 'view' === $action ) {
	require_once( WL_BM_PLUGIN_DIR_PATH . 'admin/inc/client/complaint/view.php' );
} else {
	require_once( WL_BM_PLUGIN_DIR_PATH . 'admin/inc/client/complaint/index.php' );
}
