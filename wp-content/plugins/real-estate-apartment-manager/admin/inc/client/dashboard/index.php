<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_CLIENT_COMPLAINTS, false );

$user   = wp_get_current_user();
$client = $wpdb->get_row( $wpdb->prepare( "SELECT c.ID, c.name, c.phone, f.flat_number, f.floor_number, b.name as building_name FROM {$wpdb->prefix}wlbm_clients as c, {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id WHERE c.user_id = %d AND f.ID = c.flat_id AND c.is_active = '1'", $user->ID ) );

if ( ! $client ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/client/partials/check-invalid-client.php';
}

$name          = $client->name;
$phone         = $client->phone;
$flat_number   = $client->flat_number;
$floor_number  = $client->floor_number;
$building_name = $client->building_name;

$complaints = $wpdb->get_results( $wpdb->prepare( "SELECT cp.ID, cp.name, cp.phone, cp.subject, cp.description, cp.is_resolved, cp.created_at, cs.is_responded FROM {$wpdb->prefix}wlbm_complaints as cp LEFT OUTER JOIN {$wpdb->prefix}wlbm_complaint_supplier as cs ON cs.complaint_id = cp.ID JOIN {$wpdb->prefix}wlbm_clients as c ON c.ID = cp.client_id AND cp.client_id = %d ORDER BY cp.ID DESC LIMIT 15", $client->ID ) );

$complaints_count = count( $complaints );

$responses = $wpdb->get_results( $wpdb->prepare( "SELECT cp.ID as complaint_id, cp.subject, cp.created_at, s.name as supplier_name, s.phone as supplier_phone, s.email as supplier_email, cs.responded_at, cs.estimate FROM {$wpdb->prefix}wlbm_complaint_supplier as cs, {$wpdb->prefix}wlbm_complaints as cp, {$wpdb->prefix}wlbm_clients as c, {$wpdb->prefix}wlbm_suppliers as s WHERE cp.ID = cs.complaint_id AND c.ID = cp.client_id AND c.ID = %d AND s.ID = cs.supplier_id AND cs.is_responded = '1' AND cp.is_resolved = '0' ORDER BY cs.ID DESC LIMIT 8", $client->ID ) );

$responses_count = count( $responses );

$complaint_ids = $wpdb->get_col( $wpdb->prepare( "SELECT DISTINCT cs.complaint_id FROM {$wpdb->prefix}wlbm_complaint_supplier as cs, {$wpdb->prefix}wlbm_complaints as cp, {$wpdb->prefix}wlbm_clients as c WHERE cp.ID = cs.complaint_id AND c.ID = cp.client_id AND c.ID = %d", $client->ID ) );
?>

<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-tachometer-alt"></i>
						<?php esc_html_e( 'Dashboard', 'WL-BM' ); ?>
					</h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="card col">
					<h2 class="h5 text-center">
						<i class="fas fa-comment-dots"></i>
						<?php esc_html_e( 'Recent Complaints Made By You', 'WL-BM' ); ?>
					</h2>
					<?php if ( $complaints_count ) { ?>
						<ul class="list-group list-group-flush mt-1">
						<?php foreach ( $complaints as $complaint ) { ?>
							<li class="list-group-item">
								<span class="font-weight-bold float-left">
									<a class="text-dark" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $complaint->ID ); ?>">
										<?php echo esc_html( $complaint->subject ); ?>
									</a>
									<br>
									<?php if ( $complaint->is_resolved ) { ?>
									<span class="badge badge-success"><?php esc_html_e( 'Resolved', 'WL-BM' ); ?></span>
									<?php } else { ?>
									<?php if ( $complaint->is_responded ) { ?>
									<span class="badge badge-info"><?php esc_html_e( 'Supplier Responded', 'WL-BM' ) ?></span>
									<?php } elseif ( in_array( $complaint->ID, $complaint_ids ) ) { ?>
									<span class="badge badge-danger">
										<?php esc_html_e( 'Unresolved', 'WL-BM' ); ?>
									</span> - 
									<span class="badge badge-primary">
										<?php esc_html_e( 'Forwarded by Admin', 'WL-BM' ); ?>
									</span>
									<?php } else { ?>
									<span class="badge badge-danger">
										<?php esc_html_e( 'Unresolved', 'WL-BM' ); ?>
									</span>
									<?php }
									}
								?>
								</span>
								<span class="float-right">
									<?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->created_at ) ) ); ?>
								</span>
							</li>
						<?php } ?>
						</ul>
					<?php } else { ?>
					<div class="alert alert-secondary text-center">
						<?php esc_html_e( "You haven't made any complaint yet.", 'WL-BM' ); ?>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card col">
					<h2 class="h5 text-center">
						<i class="fas fa-comment-dots"></i>
						<?php esc_html_e( 'Recent Response Received', 'WL-BM' ); ?>
					</h2>
					<?php if ( $responses_count ) { ?>
						<ul class="list-group list-group-flush mt-1">
						<?php foreach ( $responses as $response ) { ?>
							<li class="list-group-item">
								<span class="float-left">
									<span class="font-weight-bold">
										<a class="text-dark" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $response->complaint_id ); ?>">
											<?php echo esc_html( $response->subject ); ?>
										</a>
									</span>
									<br>
									<ul class="list-group mt-1 card p-2">
										<?php if ( $response->estimate ) { ?>
										<li class="">
											<span class="font-weight-bold"><?php esc_html_e( 'Estimate', 'WL-BM' ); ?>:</span>&nbsp;
											<span><?php echo esc_html( $response->estimate ); ?></span>
										</li>
										<?php } ?>
										<li class="">
											<span class="font-weight-bold"><?php esc_html_e( 'Supplier Name', 'WL-BM' ); ?>:</span>&nbsp;
											<span><?php echo esc_html( $response->supplier_name ); ?></span>
										</li>
										<?php if ( $response->supplier_phone ) { ?>
										<li class="">
											<span class="font-weight-bold"><?php esc_html_e( 'Phone', 'WL-BM' ); ?>:</span>&nbsp;
											<span><?php echo esc_html( $response->supplier_phone ); ?></span>
										</li>
										<?php } ?>
									</ul>
								</span>
								<span class="float-right">
									<?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $response->responded_at ) ) ); ?>
								</span>
							</li>
						<?php } ?>
						</ul>
					<?php } else { ?>
						<div class="alert alert-secondary text-center">
							<?php esc_html_e( 'There is no response.', 'WL-BM' ); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
