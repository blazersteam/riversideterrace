<?php
defined( 'ABSPATH' ) || die();
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row justify-content-md-center">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-times"></i>
						<?php esc_html_e( 'Inactive Client Account', 'WL-BM' ); ?>
					</h1>
					<p class="text-center h6">
						<?php esc_html_e( 'Your client account is either inactive or you are not assigned to any flat.', 'WL-BM' ); ?>
					</p>
					<p class="text-center h6 alert alert-info">
						<?php esc_html_e( 'Please wait for approval or contact the administrator.', 'WL-BM' ); ?>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
die();
