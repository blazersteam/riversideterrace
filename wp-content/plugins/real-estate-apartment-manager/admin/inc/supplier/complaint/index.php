<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_SUPPLIER_COMPLAINTS, false );

$user     = wp_get_current_user();
$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID, name, phone FROM {$wpdb->prefix}wlbm_suppliers WHERE user_id = %d AND is_active = '1'", $user->ID ) );

if ( ! $supplier ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/supplier/partials/check-invalid-supplier.php';
} else {
	$buildings = $wpdb->get_col( $wpdb->prepare( "SELECT b.name FROM {$wpdb->prefix}wlbm_building_supplier as bs, {$wpdb->prefix}wlbm_buildings as b WHERE bs.building_id = b.ID AND bs.supplier_id = %d", $supplier->ID ) );
	if ( ! count( $buildings ) ) {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/supplier/partials/check-invalid-supplier.php';
	}
}

$complaints = $wpdb->get_results( $wpdb->prepare( "SELECT cp.ID, cp.name, cp.phone, cp.subject, cp.is_resolved, cp.created_at, c.email, f.flat_number, b.name as building_name, cs.is_responded, cs.responded_at FROM {$wpdb->prefix}wlbm_complaints as cp LEFT JOIN {$wpdb->prefix}wlbm_complaint_supplier as cs ON cs.complaint_id = cp.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_clients as c ON c.ID = cp.client_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON f.ID = c.flat_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id WHERE cs.supplier_id = %d ORDER BY cs.ID DESC", $supplier->ID ) );

$complaints_count = count( $complaints );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-comment-dots"></i>
						<?php esc_html_e( 'Complaints', 'WL-BM' ); ?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<div class="card-header">
						<span class="h6 float-left wlbm-sub-header-left">
							<?php
								printf(
									/* translators: %s: number of complaints */
									esc_html( _n( 'Showing %s complaints.', 'Showing %s complaints.', $complaints_count, 'WL-BM' ) ),
									esc_html( number_format_i18n( $complaints_count ) )
								);
							?>
						</span>
					</div>
					<div class="card-body">
						<table class="table table-hover" id="wlbm-complaint-table">
							<thead>
								<tr>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Subject', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Building', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Flat', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Date', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Status', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Client Name', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0"><?php esc_html_e( 'Email', 'WL-BM' ); ?></th>
									<th scope="col" class="border-top-0 text-nowrap"><?php esc_html_e( 'Action', 'WL-BM' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ( $complaints_count > 0 ) {
									foreach ( $complaints as $complaint ) {
										$subject  = $complaint->subject;
										$building = $complaint->building_name;
										$flat     = $complaint->flat_number;
										$name     = $complaint->name;
										$email    = $complaint->email;
										$phone    = $complaint->phone;
										$date     = date_i18n( 'd-m-Y g:i A', strtotime( $complaint->created_at ) );
										if ( $complaint->is_resolved ) {
											$status = '<span class="text-success font-weight-bold">' . esc_html__( 'Resolved', 'WL-BM' ) . '</span>';
										} elseif ( $complaint->is_responded ) {
											$status = '<span class="text-info font-weight-bold">' . esc_html__( 'Responded', 'WL-BM' ) . '</span>';
											$status .= '<br><span class="text-secondary">' . esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->responded_at ) ) ) . '<span>';
										} else {
											$status = '<span class="text-danger font-weight-bold">' . esc_html__( 'Unresolved', 'WL-BM' ) . '</span>';
										}
								?>
								<tr>
									<td class="border-top-0 text-nowrap"><?php echo esc_html( $subject ); ?></td>
									<td class="border-top-0"><?php echo esc_html( $building ); ?></td>
									<td class="border-top-0"><?php echo esc_html( $flat ); ?></td>
									<td class="text-nowrap"><?php echo esc_html( $date ); ?></td>
									<td>
										<?php echo wp_kses( $status, array( 'span' => array( 'class' => array() ), 'br' => array() ) ); ?>
									</td>
									<td class="border-top-0"><?php echo esc_html( $name ); ?></td>
									<td class="border-top-0"><?php echo esc_html( $phone ); ?></td>
									<td class="border-top-0"><?php echo esc_html( $email ); ?></td>
									<td class="text-nowrap">
										<a class="text-info" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $complaint->ID ); ?>"><i class="fas fa-search"></i></a>&nbsp;&nbsp;
									</td>
								</tr>
								<?php
									}
								} else { ?>
								<tr>
									<td colspan="10" class="text-center font-weight-bold text-secondary">
										<?php esc_html_e( "There is no complaint yet.", 'WL-BM' ); ?>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
