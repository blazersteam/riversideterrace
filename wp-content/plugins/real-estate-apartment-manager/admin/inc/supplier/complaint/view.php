<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_SUPPLIER_COMPLAINTS, false );

$user     = wp_get_current_user();
$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID, name, phone FROM {$wpdb->prefix}wlbm_suppliers WHERE user_id = %d AND is_active = '1'", $user->ID ) );

if ( ! $supplier ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/supplier/partials/check-invalid-supplier.php';
} else {
	$buildings = $wpdb->get_col( $wpdb->prepare( "SELECT b.name FROM {$wpdb->prefix}wlbm_building_supplier as bs, {$wpdb->prefix}wlbm_buildings as b WHERE bs.building_id = b.ID AND bs.supplier_id = %d", $supplier->ID ) );
	if ( ! count( $buildings ) ) {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/supplier/partials/check-invalid-supplier.php';
	}
}

$complaint    = '';
$nonce_action = '';
$subject      = '';
$building_id  = '';
$building     = '';
$flat         = '';
$floor_number = '';
$description  = '';
$name         = '';
$email        = '';
$phone        = '';
$date         = '';
$images       = array();
$supplier_ids = array();
if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id        = absint( $_GET['id'] );
	$complaint = $wpdb->get_row( $wpdb->prepare( "SELECT cp.ID, cs.is_responded, cs.estimate, cs.responded_at, cp.name, cp.phone, cp.subject, cp.description, cp.images, cp.is_resolved, cp.created_at, c.email, f.flat_number, f.floor_number, b.name as building_name FROM {$wpdb->prefix}wlbm_complaints as cp LEFT JOIN {$wpdb->prefix}wlbm_complaint_supplier as cs ON cs.complaint_id = cp.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_clients as c ON c.ID = cp.client_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON f.ID = c.flat_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id WHERE cs.supplier_id = %d AND cp.ID = %d ORDER BY cs.ID DESC", $supplier->ID, $id ) );
}
if ( ! $complaint ) {
	die();
}
$nonce_action = 'respond-complaint-' . $complaint->ID;
$subject      = $complaint->subject;
$building     = $complaint->building_name;
$flat         = $complaint->flat_number;
$floor_number = $complaint->floor_number;
$description  = stripcslashes( $complaint->description );
$name         = $complaint->name;
$email        = $complaint->email;
$phone        = $complaint->phone;
$date         = date_i18n( 'd-m-Y g:i A', strtotime( $complaint->created_at ) );
$images       = unserialize( $complaint->images );

if ( $complaint->is_resolved ) {
	$status = '<span class="text-success font-weight-bold">' . esc_html__( 'Resolved', 'WL-BM' ) . '</span>';
} else {
	$status = '<span class="text-danger font-weight-bold">' . esc_html__( 'Unresolved', 'WL-BM' ) . '</span>';
}
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-search"></i>
						<?php
						printf(
							wp_kses(
								/* translators: %s: subject of complaint */
								__( 'Complaint: <span class="text-secondary">%s</span>', 'WL-BM' ),
								array(
									'span' => array( 'class' => array() )
								)
							),
							esc_html( $subject )
						);
						?>
					</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="card col">
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Subject', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $subject ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Building', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $building ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Flat', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $flat ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Floor Number', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $floor_number ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Message', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $description ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Date', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $date ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Status', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value">
									<?php echo wp_kses( $status, array( 'span' => array( 'class' => array() ) ) ); ?>
								</span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Client Name', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $name ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Phone', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $phone ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Email', 'WL-BM' ); ?></span>:&nbsp;
								<span class="wlbm-list-value"><?php echo esc_html( $email ); ?></span>
							</li>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Images', 'WL-BM' ); ?></span>:
								<span class="wlbm-list-value">
									<ul class="list-group mt-1 wlbm-complaint-images">
										<?php
										foreach( $images as $image ) : ?>
											<li class="list-group-item list-group-flush">
												<a target="_blank" href="<?php echo esc_url( wp_get_attachment_url( $image ) ); ?>">
													<img class="img-responsive" src="<?php echo esc_url( wp_get_attachment_url( $image ) ); ?>">
												</a>
											</li>
										<?php
										endforeach; ?>
									</ul>
								</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<?php if ( ! $complaint->is_responded ) { ?>
				<div class="card col">
					<div class="card-header">
						<span class="float-left">
							<h6><?php esc_html_e( 'Submit your Response', 'WL-BM' ); ?></h6>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-user"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<?php if ( ! $complaint->is_resolved ) { ?>
						<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="wlbm-respond-complaint-form">
							<?php $nonce = wp_create_nonce( $nonce_action ); ?>
							<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

							<input type="hidden" name="action" value="wlbm-supplier-respond-complaint">

							<input type="hidden" name="complaint_id" value="<?php echo esc_attr( $complaint->ID ); ?>">

							<div class="form-group">
								<label for="wlbm_estimate" class="font-weight-bold"><?php esc_html_e( 'Estimated Amount', 'WL-BM' ); ?>:</label>
								<input type="number" name="estimate" class="form-control" id="wlbm_estimate" step="any" min="0" placeholder="<?php esc_attr_e( 'Enter estimated amount', 'WL-BM' ); ?>">
							</div>

							<div class="clearfix">
								<span class="float-right">
									<button type="submit" class="btn btn-sm btn-primary" id="wlbm-respond-complaint-btn" data-message-title="<?php esc_attr_e( 'Please confirm.', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm the action.', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Submit', 'WL-BM' ); ?>">
										<i class="fas fa-reply"></i>&nbsp;
										<?php esc_html_e( 'Submit', 'WL-BM' ); ?>
									</button>
								</span>
							</div>

						</form>
						<?php } ?>
					</div>
				</div>
				<?php } else { ?>
				<div class="card col">
					<div class="card-header">
						<span class="float-left">
							<h6><?php echo esc_html_e( 'Response Submitted', 'WL-BM' ); ?></h6>
						</span>
						<span class="float-right">
							<a href="<?php echo esc_url( $page_url ); ?>" class="btn btn-sm btn-info">
								<i class="fas fa-user"></i>&nbsp;
								<?php esc_html_e( 'View All', 'WL-BM' ); ?>
							</a>
						</span>
					</div>
					<div class="card-body">
						<ul class="list-group">
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Submitted On', 'WL-BM' ); ?>: </span>
								<span class="wlbm-list-value"><?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->responded_at ) ) ); ?></span>
							</li>
							<?php if ( $complaint->estimate ) { ?>
							<li class="list-group-item">
								<span class="wlbm-list-key"><?php esc_html_e( 'Estimated Amount', 'WL-BM' ); ?>: </span>
								<span class="wlbm-list-value"><?php echo esc_html( $complaint->estimate ); ?></span>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
