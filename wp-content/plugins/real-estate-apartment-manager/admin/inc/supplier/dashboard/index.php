<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$page_url = menu_page_url( WLBM_MENU_SUPPLIER_COMPLAINTS, false );

$user     = wp_get_current_user();
$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID, name, phone FROM {$wpdb->prefix}wlbm_suppliers WHERE user_id = %d AND is_active = '1'", $user->ID ) );

if ( ! $supplier ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/supplier/partials/check-invalid-supplier.php';
} else {
	$buildings = $wpdb->get_col( $wpdb->prepare( "SELECT b.name FROM {$wpdb->prefix}wlbm_building_supplier as bs, {$wpdb->prefix}wlbm_buildings as b WHERE bs.building_id = b.ID AND bs.supplier_id = %d", $supplier->ID ) );
	if ( ! count( $buildings ) ) {
		require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/supplier/partials/check-invalid-supplier.php';
	}
}

$name  = $supplier->name;
$phone = $supplier->phone;

$complaints_unresolved = $wpdb->get_results( $wpdb->prepare( "SELECT cp.ID, cp.subject, cs.created_at FROM {$wpdb->prefix}wlbm_complaints as cp LEFT JOIN {$wpdb->prefix}wlbm_complaint_supplier as cs ON cs.complaint_id = cp.ID WHERE cs.supplier_id = %d AND cp.is_resolved = '0' AND cs.is_responded = '0' ORDER BY cs.ID DESC LIMIT 20", $supplier->ID ) );

$complaints_unresolved_count = count( $complaints_unresolved );

$complaints_responded = $wpdb->get_results( $wpdb->prepare( "SELECT cp.ID, cp.subject, cp.created_at, cp.is_resolved, cp.resolved_at, cs.responded_at, cs.estimate FROM {$wpdb->prefix}wlbm_complaints as cp LEFT JOIN {$wpdb->prefix}wlbm_complaint_supplier as cs ON cs.complaint_id = cp.ID WHERE cs.supplier_id = %d AND cs.is_responded = '1' ORDER BY cs.ID DESC LIMIT 20", $supplier->ID ) );

$complaints_responded_count = count( $complaints_responded );
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card col">
					<h1 class="h3 text-center">
						<i class="fas fa-tachometer-alt"></i>
						<?php esc_html_e( 'Dashboard', 'WL-BM' ); ?>
					</h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="card col">
					<h2 class="h5 text-center">
						<i class="fas fa-comment-dots"></i>
						<?php esc_html_e( 'Complaints which needs your response', 'WL-BM' ); ?>
					</h2>
					<?php if ( $complaints_unresolved_count ) { ?>
						<ul class="list-group list-group-flush mt-1">
						<?php foreach ( $complaints_unresolved as $complaint ) { ?>
							<li class="list-group-item">
								<span class="font-weight-bold float-left">
									<a class="text-dark" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $complaint->ID ); ?>">
										<?php echo esc_html( $complaint->subject ); ?>
									</a>
									<br>
									<span class="badge badge-danger">
										<?php esc_html_e( 'Unresolved', 'WL-BM' ); ?>
									</span>
								</span>
								<span class="float-right">
									<?php echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->created_at ) ) ); ?>
								</span>
							</li>
						<?php } ?>
						</ul>
					<?php } else { ?>
					<div class="alert alert-secondary text-center">
						<?php esc_html_e( "You haven't received any complaint yet.", 'WL-BM' ); ?>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card col">
					<h2 class="h5 text-center">
						<i class="fas fa-comment-dots"></i>
						<?php esc_html_e( 'Complaints where your response was submitted', 'WL-BM' ); ?>
					</h2>
					<?php if ( $complaints_responded_count ) { ?>
						<ul class="list-group list-group-flush mt-1">
						<?php foreach ( $complaints_responded as $complaint ) { ?>
							<li class="list-group-item">
								<span class="float-left">
									<span class="font-weight-bold">
										<a class="text-dark" href="<?php echo esc_url( $page_url . '&action=view' . '&id=' . $complaint->ID ); ?>">
											<?php echo esc_html( $complaint->subject ); ?>
										</a>
										<br>
										<?php if ( $complaint->is_resolved ) { ?>
										<span class="badge badge-success"><?php esc_html_e( 'Resolved', 'WL-BM' ); ?></span>
										<?php } else { ?>
										<span class="badge badge-info"><?php esc_html_e( 'Responded', 'WL-BM' ) ?></span>
										<?php } ?>
										<small>&nbsp;
											<span class="text-dark font-weight-normal"><?php esc_html_e( 'Estimate', 'WL-BM' ) ?>:</span>
											<span class="font-weight-bold"><?php echo esc_html( $complaint->estimate ); ?></span>
										</small>
									</span>
								</span>
								<span class="float-right">
									<?php
									if ( $complaint->is_resolved ) {
										echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->resolved_at ) ) );
									} else {
										echo esc_html( date_i18n( 'd-m-Y g:i A', strtotime( $complaint->responded_at ) ) );
									}
									?>
								</span>
							</li>
						<?php } ?>
						</ul>
					<?php } else { ?>
						<div class="alert alert-secondary text-center">
							<?php esc_html_e( 'There is no response submitted.', 'WL-BM' ); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
