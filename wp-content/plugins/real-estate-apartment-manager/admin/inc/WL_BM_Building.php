<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Building {
	public static function save() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$building_id = isset( $_POST['building_id'] ) ? absint( $_POST['building_id'] ) : '';

			if ( $building_id ) {
				if ( ! wp_verify_nonce( $_POST[ 'edit-building-' . $building_id ], 'edit-building-' . $building_id ) ) {
					die();
				}
			} else {
				if ( ! wp_verify_nonce( $_POST['add-building'], 'add-building' ) ) {
					die();
				}
			}

			// Checks if building exists.
			if ( $building_id ) {
				$building = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_buildings WHERE ID = %d", $building_id ) );

				if ( ! $building ) {
					throw new Exception( esc_html__( 'Building not found.', 'WL-BM' ) );
				}
			}

			$name         = isset( $_POST['name'] ) ? sanitize_text_field( $_POST['name'] ) : '';
			$address      = isset( $_POST['address'] ) ? sanitize_text_field( $_POST['address'] ) : '';
			$phone        = isset( $_POST['phone'] ) ? sanitize_text_field( $_POST['phone'] ) : '';
			$email        = isset( $_POST['email'] ) ? sanitize_text_field( $_POST['email'] ) : '';
			$extra_detail = isset( $_POST['extra_detail'] ) ? sanitize_text_field( $_POST['extra_detail'] ) : '';

			$excerpt = isset( $_POST['excerpt'] ) ? sanitize_text_field( stripslashes( $_POST['excerpt'] ) ) : '';

			$show_in_listing = isset( $_POST['show_in_listing'] ) ? (bool) $_POST['show_in_listing'] : false;

			$images           = ( isset( $_POST['images'] ) && is_array( $_POST['images'] ) ) ? $_POST['images'] : array();
			$image_titles     = ( isset( $_POST['image_titles'] ) && is_array( $_POST['image_titles'] ) ) ? $_POST['image_titles'] : array();
			$image_priorities = ( isset( $_POST['image_priorities'] ) && is_array( $_POST['image_priorities'] ) ) ? $_POST['image_priorities'] : array();

			// Start validation.
			$errors = array();

			if ( empty( $name ) ) {
				$errors['name'] = esc_html__( 'Please provide building name.', 'WL-BM' );
			}

			if ( strlen( $name ) > 191 ) {
				$errors['name'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			// Checks if building name exists.
			if ( $building_id ) {
				$building_exist = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) as count FROM {$wpdb->prefix}wlbm_buildings WHERE name = %s AND ID != %d", $name, $building_id ) );
			} else {
				$building_exist = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) as count FROM {$wpdb->prefix}wlbm_buildings WHERE name = %s", $name ) );
			}

			if ( $building_exist ) {
				throw new Exception( esc_html__( 'Building name already exists.', 'WL-BM' ) );
			}

			if ( strlen( $phone ) > 255 ) {
				$errors['phone'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( strlen( $email ) > 255 ) {
				$errors['email'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( ! empty( $email ) && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				$errors['email'] = esc_html__( 'Please provide a valid email.', 'WL-BM' );
			}

			if ( strlen( $excerpt ) > 255 ) {
				$errors['excerpt'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			// Building Images validation.
			if ( ! ( is_array( $images ) && is_array( $image_titles ) && is_array( $image_priorities ) ) ) {
				throw new Exception( esc_html__( 'Invalid flat images.', 'WL-BM' ) );
			}

			if ( ( count( $images ) !== count( $image_titles ) ) || ( count( $images ) !== count( $image_priorities ) ) ) {
				throw new Exception( esc_html__( 'Invalid flat images.', 'WL-BM' ) );
			}
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				$building_images = array();
				$featured_image  = NULL;

				if ( count( $images ) > 0 ) {
					$image_priorities = array_map( function( $value ) {
						return (int) $value;
					}, $image_priorities );

					$highest_priority = min( $image_priorities );

					foreach( $images as $key => $value ) {
						$priority = $image_priorities[ $key ];
						$value    = absint( $value );
						array_push(
							$building_images,
							array(
								'id'       => $value,
								'title'    => sanitize_text_field( $image_titles[ $key ] ),
								'priority' => $priority
							)
						);

						if ( $highest_priority === $priority ) {
							$featured_image = $value;
						}
					}

					$building_images = serialize( $building_images );
				} else {
					$building_images = NULL;
				}

				// Data to update or insert.
				$data = array(
					'name'            => $name,
					'phone'           => $phone,
					'email'           => $email,
					'address'         => $address,
					'extra_detail'    => $extra_detail,
					'excerpt'         => $excerpt,
					'images'          => $building_images,
					'featured_image'  => $featured_image,
					'show_in_listing' => $show_in_listing
				);

				// Checks if update or insert.
				if ( $building_id ) {
					$data['updated_at'] = date( 'Y-m-d H:i:s' );

					$success = $wpdb->update( "{$wpdb->prefix}wlbm_buildings", $data, array( 'ID' => $building_id ) );
					$message = esc_html__( 'Building updated successfully.', 'WL-BM' );
					$reset   = false;
				} else {
					$success = $wpdb->insert( "{$wpdb->prefix}wlbm_buildings", $data );
					$message = esc_html__( 'Building added successfully.', 'WL-BM' );
					$reset   = true;
				}

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $buffer );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reset' => $reset ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function delete() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$building_id  = isset( $_POST['building_id'] ) ? absint( $_POST['building_id'] ) : '';
			$delete_flats = isset( $_POST['delete_flats'] ) ? sanitize_text_field( $_POST['delete_flats'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'delete-building-' . $building_id ], 'delete-building-' . $building_id ) ) {
				die();
			}

			// Checks if building exists.
			$building = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_buildings WHERE ID = %d", $building_id ) );

			if ( ! $building ) {
				throw new Exception( esc_html__( 'Building not found.', 'WL-BM' ) );
			}

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		try {
			$wpdb->query( 'BEGIN;' );

			if ( $delete_flats ) {
				$success = $wpdb->delete( "{$wpdb->prefix}wlbm_flats", array( 'building_id' => $building_id ) );

				$exception = ob_get_clean();
				if ( ! empty( $exception ) ) {
					throw new Exception( $exception );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}
			}

			$success = $wpdb->delete( "{$wpdb->prefix}wlbm_buildings", array( 'ID' => $building_id ) );
			$message = esc_html__( 'Building deleted successfully.', 'WL-BM' );

			$exception = ob_get_clean();
			if ( ! empty( $exception ) ) {
				throw new Exception( $exception );
			}

			if ( $success === false ) {
				throw new Exception( $wpdb->last_error );
			}

			$wpdb->query( 'COMMIT;' );

			wp_send_json_success( array( 'message' => $message ) );
		} catch ( Exception $exception ) {
			$wpdb->query( 'ROLLBACK;' );
			wp_send_json_error( $exception->getMessage() );
		}
	}
}
