<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Supplier {
	public static function save() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$supplier_id = isset( $_POST['supplier_id'] ) ? absint( $_POST['supplier_id'] ) : '';

			if ( $supplier_id ) {
				if ( ! wp_verify_nonce( $_POST[ 'edit-supplier-' . $supplier_id ], 'edit-supplier-' . $supplier_id ) ) {
					die();
				}
			} else {
				if ( ! wp_verify_nonce( $_POST['add-supplier'], 'add-supplier' ) ) {
					die();
				}
			}

			// Checks if supplier exists.
			if ( $supplier_id ) {
				$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID, user_id FROM {$wpdb->prefix}wlbm_suppliers WHERE ID = %d", $supplier_id ) );

				if ( ! $supplier ) {
					throw new Exception( esc_html__( 'Supplier not found.', 'WL-BM' ) );
				}
			}

			$buildings    = ( isset( $_POST['building'] ) && is_array( $_POST['building'] ) ) ? $_POST['building'] : array();
			$name         = isset( $_POST['name'] ) ? sanitize_text_field( $_POST['name'] ) : '';
			$address      = isset( $_POST['address'] ) ? sanitize_text_field( $_POST['address'] ) : '';
			$phone        = isset( $_POST['phone'] ) ? sanitize_text_field( $_POST['phone'] ) : '';
			$email        = isset( $_POST['email'] ) ? sanitize_text_field( $_POST['email'] ) : '';
			$expertise    = isset( $_POST['expertise'] ) ? sanitize_text_field( $_POST['expertise'] ) : '';
			$extra_detail = isset( $_POST['extra_detail'] ) ? sanitize_text_field( $_POST['extra_detail'] ) : '';
			$is_active    = isset( $_POST['is_active'] ) ? (bool) $_POST['is_active'] : false;

			$username         = isset( $_POST['username'] ) ? sanitize_text_field( $_POST['username'] ) : '';
			$login_email      = isset( $_POST['login_email'] ) ? sanitize_text_field( $_POST['login_email'] ) : '';
			$password         = isset( $_POST['password'] ) ? $_POST['password'] : '';
			$password_confirm = isset( $_POST['password_confirm'] ) ? $_POST['password_confirm'] : '';

			$is_existing_user  = isset( $_POST['is_existing_user'] ) ? (bool) $_POST['is_existing_user'] : '';
			$existing_username = isset( $_POST['existing_username'] ) ? sanitize_text_field( $_POST['existing_username'] ) : '';

			// Start validation.
			$errors = array();

			if ( empty( $name ) ) {
				$errors['name'] = esc_html__( 'Please provide supplier name.', 'WL-BM' );
			}

			if ( strlen( $name ) > 255 ) {
				$errors['name'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			// If existing user is checked then checks if existing username is provided by user.
			if ( ! empty( $is_existing_user ) ) {
				if ( empty( $existing_username ) ) {
					$errors['existing_username'] = esc_html__( 'Please provide supplier username.', 'WL-BM' );
				}
				if ( strlen( $existing_username ) > 60 ) {
					$errors['existing_username'] = esc_html__( 'Maximum length cannot exceed 60 characters.', 'WL-BM' );
				}
			// If existing user is unchecked.
			} else {
				if ( ! empty( $login_email ) && ! filter_var( $login_email, FILTER_VALIDATE_EMAIL ) ) {
					$errors['login_email'] = esc_html__( 'Please provide a valid email.', 'WL-BM' );
				}
				// If editing supplier and supplier is user.
				if ( $supplier_id && $supplier->user_id ) {
					if ( ! empty( $password ) && empty( $password_confirm ) ) {
						$errors['password_confirm'] = esc_html__( 'Please confirm password.', 'WL-BM' );
					}
					if ( ! empty( $password ) && ( $password !== $password_confirm ) ) {
						$errors['password'] = esc_html__( 'Passwords do not match.', 'WL-BM' );
					}
				// If new supplier or editing supplier.
				} else {
					if ( strlen( $username ) > 60 ) {
						$errors['username'] = esc_html__( 'Maximum length cannot exceed 60 characters.', 'WL-BM' );
					}
					if ( ! empty( $username ) && empty( $password ) ) {
						$errors['password'] = esc_html__( 'Please provide password.', 'WL-BM' );
					}
					if ( ! empty( $username ) && empty( $password_confirm ) ) {
						$errors['password_confirm'] = esc_html__( 'Please confirm password.', 'WL-BM' );
					}
					if ( ! empty( $username ) && ( $password !== $password_confirm ) ) {
						$errors['password'] = esc_html__( 'Passwords do not match.', 'WL-BM' );
					}

					// If new supplier or editing supplier and supplier is not user.
					if ( ! $supplier_id || ( $supplier_id && ! $supplier->user_id ) ) {
						if ( ( ! empty( $login_email ) || ! empty( $password ) ) && empty( $username ) ) {
							$errors['username'] = esc_html__( 'Please provide username.', 'WL-BM' );
						}
					}
				}
			}

			if ( strlen( $phone ) > 255 ) {
				$errors['phone'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( strlen( $email ) > 255 ) {
				$errors['email'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( ! empty( $email ) && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				$errors['email'] = esc_html__( 'Please provide a valid email.', 'WL-BM' );
			}

			if ( strlen( $expertise ) > 255 ) {
				$errors['expertise'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				// Data to update or insert.
				$data = array(
					'name'         => $name,
					'phone'        => $phone,
					'email'        => $email,
					'address'      => $address,
					'expertise'    => $expertise,
					'extra_detail' => $extra_detail,
					'is_active'    => $is_active
				);

				// If editing supplier.
				if ( $supplier_id ) {
					// If existing user is unchecked and username or password or login_email is provided.
					if ( ! $is_existing_user && ( $username || $password || $login_email ) ) {
						// If supplier is user and password or login_email is provided.
						if ( $supplier->user_id && ( $password || $login_email ) ) {
							// Update user's password.
							$user = get_user_by( 'ID', $supplier->user_id );

							$user_data = array(
								'ID' => $user->ID,
							);

							// If password is provided.
							if ( $password ) {
								$user_data['user_pass'] = $password;
							}

							// If login_email is provided.
							if ( $login_email ) {
								$user_data['user_email'] = $login_email;
							}

							$user_id = wp_update_user( $user_data );
							if ( is_wp_error( $user_id ) ) {
								throw new Exception( esc_html( $user_id->get_error_message(), 'WL-BM' ) );
							}
							$user = new WP_User( $user_id );
							$data['user_id']    = $user->ID;
							$data['user_login'] = $user->user_login;
							$data['user_pass']  = $user->user_pass;

						// If supplier is not user and username is provided.
						} elseif ( ! $supplier->user_id && $username ) {
							// Insert new user.
							$user_data = array(
								'user_login' => $username,
								'user_pass'  => $password,
							);

							// If login_email is provided.
							if ( $login_email ) {
								$user_data['user_email'] = $login_email;
							}

							$user_id = wp_insert_user( $user_data );
							if ( is_wp_error( $user_id ) ) {
								throw new Exception( esc_html( $user_id->get_error_message(), 'WL-BM' ) );
							}
							$user = new WP_User( $user_id );
							$user->add_cap( WLBM_SUPPLIER );
							$data['user_id']    = $user->ID;
							$data['user_login'] = $user->user_login;
							$data['user_pass']  = $user->user_pass;
						}

					// If existing user is checked.
					} elseif ( $is_existing_user ) {
						$user = get_user_by( 'login', $existing_username );
						if ( ! $user ) {
							throw new Exception( esc_html__( 'Username does not exist.', 'WL-BM' ) );
						}
						$user_id = $user->ID;

						$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_suppliers WHERE user_id = %d", $user_id ) );
						if ( $supplier ) {
							throw new Exception( esc_html__( 'Supplier already eixsts with this username.', 'WL-BM' ) );
						}

						if ( $user_id ) {
							$user->add_cap( WLBM_SUPPLIER );
							$data['user_id']    = $user_id;
							$data['user_login'] = $user->user_login;
							$data['user_pass']  = $user->user_pass;
						}
					}

					$data['updated_at'] = date( 'Y-m-d H:i:s' );

					$success = $wpdb->update( "{$wpdb->prefix}wlbm_suppliers", $data, array( 'ID' => $supplier_id ) );
					$message = esc_html__( 'Supplier updated successfully.', 'WL-BM' );
					$reset   = false;

				// If new supplier.
				} else {
					// If existing user is checked.
					if ( $is_existing_user ) {
						$user = get_user_by( 'login', $existing_username );
						if ( ! $user ) {
							throw new Exception( esc_html__( 'Username does not exist.', 'WL-BM' ) );
						}
						$user_id = $user->ID;

						$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_suppliers WHERE user_id = %d", $user_id ) );
						if ( $supplier ) {
							throw new Exception( esc_html__( 'Supplier already eixsts with this username.', 'WL-BM' ) );
						}
					// If existing user is unchecked.
					} else {
						// If new username is provided.
						if ( ! empty( $username ) ) {
							// Insert new user.
							$user_data = array(
								'user_login' => $username,
								'user_email' => $login_email,
								'user_pass'  => $password,
							);
							$user_id = wp_insert_user( $user_data );
							if ( is_wp_error( $user_id ) ) {
								throw new Exception( esc_html( $user_id->get_error_message(), 'WL-BM' ) );
							}
							$user = new WP_User( $user_id );
						} else {
							$user_id = NULL;
						}
					}

					if ( $user_id ) {
						$user->add_cap( WLBM_SUPPLIER );
						$data['user_id']    = $user_id;
						$data['user_login'] = $user->user_login;
						$data['user_pass']  = $user->user_pass;
					}

					$success = $wpdb->insert( "{$wpdb->prefix}wlbm_suppliers", $data );
					$message = esc_html__( 'Supplier added successfully.', 'WL-BM' );
					$reset   = true;

					$supplier_id = $wpdb->insert_id;
				}

				if ( $supplier_id ) {
					if ( count( $buildings ) > 0 ) {
						$values                  = array();
						$place_holders           = array();
						$place_holders_buildings = array();
						foreach ( $buildings as $building_id ) {
							array_push( $values, $building_id, $supplier_id );
							array_push( $place_holders, '(%d, %d)' );
							array_push( $place_holders_buildings, '%d' );
						}

						// Insert building_supplier records.
						$sql     = "INSERT IGNORE INTO {$wpdb->prefix}wlbm_building_supplier (building_id, supplier_id) VALUES ";
						$sql     .= implode( ', ', $place_holders );
						$success = $wpdb->query( $wpdb->prepare( "$sql ", $values ) );

						// Delete building_supplier records not in array.
						$sql     = "DELETE FROM {$wpdb->prefix}wlbm_building_supplier WHERE supplier_id = %d AND building_id NOT IN (" . implode( ', ', $place_holders_buildings ) . ")";
						array_unshift( $buildings , $supplier_id );
						$success = $wpdb->query( $wpdb->prepare( "$sql ", $buildings ) );
					} else {
						// Delete building_supplier records for supplier.
						$success = $wpdb->delete( "{$wpdb->prefix}wlbm_building_supplier", array( 'supplier_id' => $supplier_id ) );
					}
				}

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $buffer );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reset' => $reset ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function delete() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$supplier_id   = isset( $_POST['supplier_id'] ) ? absint( $_POST['supplier_id'] ) : '';
			$delete_user = isset( $_POST['delete_user'] ) ? sanitize_text_field( $_POST['delete_user'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'delete-supplier-' . $supplier_id ], 'delete-supplier-' . $supplier_id ) ) {
				die();
			}

			// Checks if supplier exists.
			$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID, user_id FROM {$wpdb->prefix}wlbm_suppliers WHERE ID = %d", $supplier_id ) );

			if ( ! $supplier ) {
				throw new Exception( esc_html__( 'Supplier not found.', 'WL-BM' ) );
			}

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		try {
			$wpdb->query( 'BEGIN;' );

			$user = '';
			if ( $supplier->user_id ) {
				$user = get_user_by( 'ID', $supplier->user_id );
			}

			$success = $wpdb->delete( "{$wpdb->prefix}wlbm_suppliers", array( 'ID' => $supplier_id ) );
			$message = esc_html__( 'Supplier deleted successfully.', 'WL-BM' );

			$exception = ob_get_clean();
			if ( ! empty( $exception ) ) {
				throw new Exception( $exception );
			}

			if ( $success === false ) {
				throw new Exception( $wpdb->last_error );
			}

			if ( $user ) {
				// Remove user capability if user exists.
				$user->remove_cap( WLBM_SUPPLIER );
				if ( $delete_user ) {
					// Delete user account.
					$user_deleted = is_multisite() ? wpmu_delete_user( $user->ID ) : wp_delete_user( $user->ID );
					if ( ! $user_deleted ) {
						throw new Exception( esc_html__( 'Unable to delete user account.', 'WL-BM' ) );
					}
				}
			}

			$wpdb->query( 'COMMIT;' );

			wp_send_json_success( array( 'message' => $message ) );
		} catch ( Exception $exception ) {
			$wpdb->query( 'ROLLBACK;' );
			wp_send_json_error( $exception->getMessage() );
		}
	}
}
