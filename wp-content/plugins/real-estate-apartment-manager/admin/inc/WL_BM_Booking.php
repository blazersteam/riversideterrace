<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Booking {
	public static function save() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$booking_id = isset( $_POST['booking_id'] ) ? absint( $_POST['booking_id'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'edit-booking-' . $booking_id ], 'edit-booking-' . $booking_id ) ) {
				die();
			}

			// Checks if booking exists.
			$booking = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_bookings WHERE ID = %d", $booking_id ) );

			if ( ! $booking ) {
				throw new Exception( esc_html__( 'Booking not found.', 'WL-BM' ) );
			}

			$is_active    = isset( $_POST['is_active'] ) ? (bool) $_POST['is_active'] : false;
			$extra_detail = isset( $_POST['extra_detail'] ) ? sanitize_text_field( $_POST['extra_detail'] ) : '';

			// Start validation.
			$errors = array();
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				// Data to update or insert.
				$data = array(
					'extra_detail' => $extra_detail,
					'is_active'    => $is_active
				);

				$data['updated_at'] = date( 'Y-m-d H:i:s' );

				$success = $wpdb->update( "{$wpdb->prefix}wlbm_bookings", $data, array( 'ID' => $booking_id ) );
				$message = esc_html__( 'Booking request updated successfully.', 'WL-BM' );
				$reset   = false;

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $buffer );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reset' => $reset ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function delete() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$booking_id = isset( $_POST['booking_id'] ) ? absint( $_POST['booking_id'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'delete-booking-' . $booking_id ], 'delete-booking-' . $booking_id ) ) {
				die();
			}

			// Checks if booking exists.
			$booking = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_bookings WHERE ID = %d", $booking_id ) );

			if ( ! $booking ) {
				throw new Exception( esc_html__( 'Booking request not found.', 'WL-BM' ) );
			}

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		try {
			$wpdb->query( 'BEGIN;' );

			$success = $wpdb->delete( "{$wpdb->prefix}wlbm_bookings", array( 'ID' => $booking_id ) );
			$message = esc_html__( 'Booking request deleted successfully.', 'WL-BM' );

			$exception = ob_get_clean();
			if ( ! empty( $exception ) ) {
				throw new Exception( $exception );
			}

			if ( $success === false ) {
				throw new Exception( $wpdb->last_error );
			}

			$wpdb->query( 'COMMIT;' );

			wp_send_json_success( array( 'message' => $message ) );
		} catch ( Exception $exception ) {
			$wpdb->query( 'ROLLBACK;' );
			wp_send_json_error( $exception->getMessage() );
		}
	}
}
