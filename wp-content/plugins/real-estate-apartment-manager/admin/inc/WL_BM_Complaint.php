<?php
defined( 'ABSPATH' ) || die();

require_once WL_BM_PLUGIN_DIR_PATH . 'includes/helpers/WL_BM_Helper.php';

class WL_BM_Complaint {
	public static function save() {
		if ( ! current_user_can( WLBM_CLIENT ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$error_message = esc_html__( 'Error occurred while submitting your complain. Please notify the administrator.', 'WL-BM' );

			if ( ! wp_verify_nonce( $_POST['add-complaint'], 'add-complaint' ) ) {
				die();
			}

			$user   = wp_get_current_user();
			$client = $wpdb->get_row( $wpdb->prepare( "SELECT c.ID, c.name, c.phone, f.flat_number, f.floor_number, b.name as building_name FROM {$wpdb->prefix}wlbm_clients as c, {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id WHERE c.user_id = %d AND f.ID = c.flat_id AND c.is_active = '1'", $user->ID ) );

			if ( ! $client ) {
				die();
			}

			$name        = isset( $_POST['name'] ) ? sanitize_text_field( $_POST['name'] ) : '';
			$phone       = isset( $_POST['phone'] ) ? sanitize_text_field( $_POST['phone'] ) : '';
			$subject     = isset( $_POST['subject'] ) ? sanitize_text_field( $_POST['subject'] ) : '';
			$description = isset( $_POST['description'] ) ? sanitize_text_field( $_POST['description'] ) : '';
			$images      = ( isset( $_FILES['images'] ) && is_array( $_FILES['images'] ) ) ? $_FILES['images'] : array();

			// Start validation.
			$errors = array();

			if ( empty( $name ) ) {
				$errors['name'] = esc_html__( 'Please enter your name.', 'WL-BM' );
			}

			if ( strlen( $name ) > 255 ) {
				$errors['name'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( empty( $phone ) ) {
				$errors['phone'] = esc_html__( 'Please enter your contact number.', 'WL-BM' );
			}

			if ( strlen( $phone ) > 255 ) {
				$errors['phone'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( empty( $subject ) ) {
				$errors['subject'] = esc_html__( 'Please enter subject or title.', 'WL-BM' );
			}

			if ( strlen( $subject ) > 255 ) {
				$errors['subject'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( empty( $description ) ) {
				$errors['description'] = esc_html__( 'Please enter your message.', 'WL-BM' );
			}

			if ( isset( $images["tmp_name"] ) && is_array( $images ) && count( $images ) ) {
				foreach ( $images["tmp_name"] as $key => $image ) {
					$file_type      = $images['type'][ $key ];
					$allowed_images = WL_BM_Helper::get_allowed_images();

					if ( ! in_array( $file_type, $allowed_images ) ) {
						$errors['images[]'] = esc_html__( 'Please provide images in JPG, JPEG or PNG format.', 'WL-BM' );
					}
				}
			}
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $error_message;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				$image_ids = array();
				if ( isset( $images['tmp_name'] ) && is_array( $images ) && count( $images ) ) {
					foreach( $images['tmp_name'] as $key => $image ) {
						if ( $images['name'][ $key ] ) {
							$image = array(
								'name'     => sanitize_file_name( $images['name'][ $key ] ),
								'type'     => $images['type'][ $key ],
								'tmp_name' => $images['tmp_name'][ $key ],
								'error'    => $images['error'][ $key ],
								'size'     => $images['size'][ $key ],
							);
							$_FILES = array( 'image' => $image );
							$image_id = media_handle_upload( 'image', 0 );
							if ( is_wp_error( $image_id ) ) {
								throw new Exception( $image_id->get_error_message() );
							}
							array_push( $image_ids, $image_id );
						}
					}
				}

				$images = serialize( $image_ids );

				$data = array(
					'name'        => $name,
					'phone'       => $phone,
					'subject'     => $subject,
					'description' => $description,
					'images'      => $images,
					'client_id'   => $client->ID,
				);

				$page_url = admin_url( 'admin.php?page=' . WLBM_MENU_CLIENT_COMPLAINTS );

				$success = $wpdb->insert( "{$wpdb->prefix}wlbm_complaints", $data );
				$message = wp_kses(
					__( 'Your complain has been received. Thank you.', 'WL-BM' ) . '<br>' . '<a href="' . esc_url( $page_url . '&action=view' . '&id=' . $wpdb->insert_id ) . '">' . __( 'Click here to view your complain status.', 'WL-BM' ) . '</a>',
					array( 'a' => array( 'href' => array() ), 'br' => array() ) );
				$reset   = true;

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $error_message );
				}

				if ( $success === false ) {
					throw new Exception( $error_message );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reset' => $reset ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function forward() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$complaint_id = isset( $_POST['complaint_id'] ) ? absint( $_POST['complaint_id'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'forward-complaint-' . $complaint_id ], 'forward-complaint-' . $complaint_id ) ) {
				die();
			}

			// Checks if complaint exists.
			$complaint = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_complaints WHERE ID = %d AND is_resolved = '0'", $complaint_id ) );

			if ( ! $complaint ) {
				throw new Exception( esc_html__( 'Complaint not found.', 'WL-BM' ) );
			}

			$suppliers = ( isset( $_POST['supplier'] ) && is_array( $_POST['supplier'] ) ) ? $_POST['supplier'] : array();

			// Start validation.
			$errors = array();

			if ( is_array( $suppliers ) && count( $suppliers ) ) {
				$suppliers = array_filter( $suppliers );
			} else {
				$errors['supplier[]'] = esc_html__( 'Please select supplier to forward this complaint.', 'WL-BM' );
			}
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				if ( count( $suppliers ) > 0 ) {
					$values                  = array();
					$place_holders           = array();
					$place_holders_suppliers = array();
					foreach ( $suppliers as $supplier_id ) {
						array_push( $values, $complaint_id, $supplier_id );
						array_push( $place_holders, '(%d, %d)' );
						array_push( $place_holders_suppliers, '%d' );
					}

					// Insert complaint_supplier records.
					$sql     = "INSERT IGNORE INTO {$wpdb->prefix}wlbm_complaint_supplier (complaint_id, supplier_id) VALUES ";
					$sql     .= implode( ', ', $place_holders );
					$success = $wpdb->query( $wpdb->prepare( "$sql ", $values ) );

					// Delete complaint_supplier records not in array.
					$sql     = "DELETE FROM {$wpdb->prefix}wlbm_complaint_supplier WHERE complaint_id = %d AND supplier_id NOT IN (" . implode( ', ', $place_holders_suppliers ) . ")";
					array_unshift( $suppliers , $complaint_id );
					$success = $wpdb->query( $wpdb->prepare( "$sql ", $suppliers ) );
					$message = esc_html__( 'Complain forward status updated successfully.', 'WL-BM' );
				} else {
					// Delete complaint_supplier records for complaint.
					$success = $wpdb->delete( "{$wpdb->prefix}wlbm_complaint_supplier", array( 'complaint_id' => $complaint_id ) );
					$message = esc_html__( 'Complain forward status reset successfully.', 'WL-BM' );
				}

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $buffer );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reload' => true ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function respond() {
		if ( ! current_user_can( WLBM_SUPPLIER ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$error_message = esc_html__( 'Error occurred while submitting your response. Please notify the administrator.', 'WL-BM' );

			$complaint_id = isset( $_POST['complaint_id'] ) ? absint( $_POST['complaint_id'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'respond-complaint-' . $complaint_id ], 'respond-complaint-' . $complaint_id ) ) {
				die();
			}

			$user     = wp_get_current_user();
			$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID, name, phone FROM {$wpdb->prefix}wlbm_suppliers WHERE user_id = %d AND is_active = '1'", $user->ID ) );

			if ( ! $supplier ) {
				die();
			} else {
				$buildings = $wpdb->get_col( $wpdb->prepare( "SELECT b.name FROM {$wpdb->prefix}wlbm_building_supplier as bs, {$wpdb->prefix}wlbm_buildings as b WHERE bs.building_id = b.ID AND bs.supplier_id = %d", $supplier->ID ) );
				if ( ! count( $buildings ) ) {
					die();
				}
			}

			// Checks if complaint exists.
			$complaint = $wpdb->get_row( $wpdb->prepare( "SELECT cp.ID, cs.ID as complaint_supplier_id FROM {$wpdb->prefix}wlbm_complaints as cp LEFT JOIN {$wpdb->prefix}wlbm_complaint_supplier as cs ON cs.complaint_id = cp.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_clients as c ON c.ID = cp.client_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON f.ID = c.flat_id LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id WHERE cs.supplier_id = %d AND cp.ID = %d AND cp.is_resolved = '0' AND cs.is_responded = '0' ORDER BY cs.ID DESC", $supplier->ID, $complaint_id ) );

			if ( ! $complaint ) {
				throw new Exception( esc_html__( 'Complaint not found.', 'WL-BM' ) );
			}

			$estimate = number_format( isset( $_POST['estimate'] ) ? max( (float) sanitize_text_field( $_POST['estimate'] ), 0 ) : 0, 2, '.', '' );

			// Start validation.
			$errors = array();

			if ( $estimate <= 0 ) {
				$errors['estimate'] = esc_html__( 'Please provide estimated amount.', 'WL-BM' );
			}
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $error_message;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				// Record respond from supplier.
				$data['estimate']     = $estimate;
				$data['is_responded'] = '1';
				$data['updated_at']   = date( 'Y-m-d H:i:s' );
				$data['responded_at'] = $data['updated_at'];
				$success = $wpdb->update(
					"{$wpdb->prefix}wlbm_complaint_supplier",
					$data,
					array( 'ID' => $complaint->complaint_supplier_id )
				);

				$message = esc_html__( 'Your response has been submitted.', 'WL-BM' );

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $error_message );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reload' => true ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function delete() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$complaint_id = isset( $_POST['complaint_id'] ) ? absint( $_POST['complaint_id'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'delete-complaint-' . $complaint_id ], 'delete-complaint-' . $complaint_id ) ) {
				die();
			}

			// Checks if complaint exists.
			$complaint = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_complaints WHERE id = %d", $complaint_id ) );

			if ( ! $complaint ) {
				throw new Exception( esc_html__( 'Complaint not found.', 'WL-BM' ) );
			}

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		try {
			$wpdb->query( 'BEGIN;' );

			$success = $wpdb->delete( "{$wpdb->prefix}wlbm_complaints", array( 'ID' => $complaint_id ) );
			$message = esc_html__( 'Complaint deleted successfully.', 'WL-BM' );

			$exception = ob_get_clean();
			if ( ! empty( $exception ) ) {
				throw new Exception( $exception );
			}

			if ( $success === false ) {
				throw new Exception( $wpdb->last_error );
			}

			$wpdb->query( 'COMMIT;' );

			wp_send_json_success( array( 'message' => $message ) );
		} catch ( Exception $exception ) {
			$wpdb->query( 'ROLLBACK;' );
			wp_send_json_error( $exception->getMessage() );
		}
	}

	public static function manager_resolve() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$complaint_id = isset( $_POST['complaint_id'] ) ? absint( $_POST['complaint_id'] ) : '';

			if ( ! wp_verify_nonce( $_POST[ 'resolve-complaint-' . $complaint_id ], 'resolve-complaint-' . $complaint_id ) ) {
				die();
			}

			// Checks if complaint exists.
			$complaint = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_complaints WHERE ID = %d AND is_resolved = '0'", $complaint_id ) );

			if ( ! $complaint ) {
				throw new Exception( esc_html__( 'Complaint not found.', 'WL-BM' ) );
			}

			// Start validation.
			$errors = array();
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $buffer;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				// Resolve the complaint.
				$data['is_resolved'] = '1';
				$data['updated_at']  = date( 'Y-m-d H:i:s' );
				$data['resolved_at'] = $data['updated_at'];
				$success = $wpdb->update(
					"{$wpdb->prefix}wlbm_complaints",
					$data,
					array( 'ID' => $complaint_id )
				);

				$message = esc_html__( 'Complaint has been marked as resolved.', 'WL-BM' );

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $buffer );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reload' => true ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function client_resolve() {
		if ( ! current_user_can( WLBM_CLIENT ) ) {
			die();
		}

		try {
			ob_start();
			global $wpdb;

			$complaint_id = isset( $_POST['complaint_id'] ) ? absint( $_POST['complaint_id'] ) : '';

			$error_message = esc_html__( 'Error occurred while marking this complain as resolved. Please notify the administrator.', 'WL-BM' );

			if ( ! wp_verify_nonce( $_POST[ 'resolve-complaint-' . $complaint_id ], 'resolve-complaint-' . $complaint_id ) ) {
				die();
			}

			$user   = wp_get_current_user();
			$client = $wpdb->get_row( $wpdb->prepare( "SELECT c.ID, c.name, c.phone, f.flat_number, f.floor_number, b.name as building_name FROM {$wpdb->prefix}wlbm_clients as c, {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id WHERE c.user_id = %d AND f.ID = c.flat_id AND c.is_active = '1'", $user->ID ) );

			if ( ! $client ) {
				die();
			}

			// Checks if complaint exists.
			$complaint = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_complaints WHERE ID = %d AND is_resolved = '0' AND client_id = %d", $complaint_id, $client->ID ) );

			if ( ! $complaint ) {
				throw new Exception( esc_html__( 'Complaint not found.', 'WL-BM' ) );
			}

			// Start validation.
			$errors = array();
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $error_message;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				// Resolve the complaint.
				$data['is_resolved'] = '1';
				$data['updated_at']  = date( 'Y-m-d H:i:s' );
				$data['resolved_at'] = $data['updated_at'];
				$success = $wpdb->update(
					"{$wpdb->prefix}wlbm_complaints",
					$data,
					array( 'ID' => $complaint_id )
				);

				$message = esc_html__( 'Complaint has been marked as resolved.', 'WL-BM' );

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $error_message );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reload' => true ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}
}
