(function($) {
	'use strict';
	$(document).ready(function() {
		jQuery('.selectpicker').selectpicker();

		$(document).on('change', '#wlbm-registration-as', function() {
			var clientForm = $('.wlbm-client');
			var supplierForm = $('.wlbm-supplier');
			if ( 'supplier' === this.value ) {
				clientForm.hide();
				supplierForm.fadeIn();
			} else {
				clientForm.fadeIn();
				supplierForm.hide();
			}
		});

		var registrationForm = $('.wlbm-registration-form');
		var registrationFormShow = $('.wlbm-show-registration-form');
		var loginForm = $('.wlbm-login-form');
		var loginFormShow = $('.wlbm-show-login-form');
		var registrationFormTitle = $('.wlbm-form-registration-title');
		var loginFormTitle = $('.wlbm-form-login-title');

		$(document).on('click', '#wlbm-show-login-form-btn', function() {
			registrationForm.hide();
			registrationFormTitle.hide();
			loginForm.fadeIn();
			loginFormTitle.fadeIn();
			loginFormShow.hide();
			registrationFormShow.fadeIn();
		});

		$(document).on('click', '#wlbm-show-registration-form-btn', function() {
			console.log('test');
			loginForm.hide();
			loginFormTitle.hide();
			registrationForm.fadeIn();
			registrationFormTitle.fadeIn();
			registrationFormShow.hide();
			loginFormShow.fadeIn();
		});
	});
})(jQuery);
