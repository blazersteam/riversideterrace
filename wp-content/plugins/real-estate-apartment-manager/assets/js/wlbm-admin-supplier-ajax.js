(function($) {
	'use strict';
	$(document).ready(function() {

		try {
			// Datatable initialization.
			$('#wlbm-complaint-table').DataTable({
				responsive: true,
				aaSorting: [],
				dom: 'lBfrtip',
				lengthChange: false,
				buttons: [
					'pageLength',
				    {
						extend: 'excel',
						exportOptions: {
							columns: [0,1,2,3]
						}
				    },
				    {
						extend: 'csv',
						exportOptions: {
							columns: [0,1,2,3]
						}
				    },
				]
			});
		} catch(e) {}

		// Loading icon variables.
		var loaderContainer = $('<span/>', {
            'class': 'wlbm-loader ml-2'
        });
        var loader = $('<img/>', {
	        'src': wlbmadminurl + 'images/spinner.gif',
	        'class': 'wlbm-loader-image mb-1'
        });

		// Respond complaint.
		var respondComplaintFormId = '#wlbm-respond-complaint-form';
		var respondComplaintForm = $(respondComplaintFormId);
		var respondComplaintBtn = $('#wlbm-respond-complaint-btn');
		$(document).on('click', '#wlbm-respond-complaint-btn', function(e) {
			e.preventDefault();
			$.confirm({
				title: respondComplaintBtn.data('message-title'),
				content: respondComplaintBtn.data('message-content'),
				type: 'red',
				useBootstrap: false,
				buttons: {
					confirm: {
						text: respondComplaintBtn.data('submit'),
	       				btnClass: 'btn-red',
						action: function () {
							respondComplaintForm.ajaxSubmit({
								beforeSubmit: function(arr, $form, options) {
									$('div.text-danger').remove();
									$(".is-invalid").removeClass("is-invalid");
									$('.wlbm .alert-dismissible').remove();
									respondComplaintBtn.prop('disabled', true);
									loaderContainer.insertAfter(respondComplaintBtn);
									loader.appendTo(loaderContainer);
									return true;
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(respondComplaintFormId);
										toastr.success(response.data.message);
										if(response.data.hasOwnProperty('reload') && response.data.reload) {
											window.location.reload();
										} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
											respondComplaintForm[0].reset();
										}
									} else {
										if(response.data && $.isPlainObject(response.data)) {
											$(respondComplaintFormId + ' :input').each(function() {
												var input = this;
												$(input).removeClass('is-invalid');
												if(response.data[input.name]) {
													var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
													$(input).addClass('is-invalid');
													$(errorSpan).insertAfter(input);
												}
											});
										} else {
											var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
											$(errorSpan).insertBefore(respondComplaintFormId);
											toastr.error(response.data);
										}
									}
								},
								error: function(response) {
									respondComplaintBtn.prop('disabled', false);
									var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
									$(errorSpan).insertBefore(respondComplaintFormId);
									toastr.error(response.data);
								},
								complete: function(event, xhr, settings) {
									respondComplaintBtn.prop('disabled', false);
									loaderContainer.remove();
								}
							});
						}
					},
					cancel: {
						text: respondComplaintBtn.data('cancel'),
						action: function () {
							return;
						}
					}
				}
			});
		});

	});
})(jQuery);
