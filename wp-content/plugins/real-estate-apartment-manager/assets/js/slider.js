(function($) {
	'use strict';
	$(document).ready(function() {
		$('.wlbm-bxslider').bxSlider({
			mode: 'fade',
			captions: true,
			slideWidth: 0,
			controls: true,
			captions: true
		});
	});
})(jQuery);
