(function($) {
	'use strict';
	$(document).ready(function() {

		try {
			// Datatable initialization.
			$('#wlbm-complaint-table').DataTable({
				responsive: true,
				aaSorting: [],
				dom: 'lBfrtip',
				lengthChange: false,
				buttons: [
					'pageLength',
				    {
						extend: 'excel',
						exportOptions: {
							columns: [0,1,2,3]
						}
				    },
				    {
						extend: 'csv',
						exportOptions: {
							columns: [0,1,2,3]
						}
				    },
				]
			});
		} catch(e) {}

		// Loading icon variables.
		var loaderContainer = $('<span/>', {
            'class': 'wlbm-loader ml-2'
        });
        var loader = $('<img/>', {
	        'src': wlbmadminurl + 'images/spinner.gif',
	        'class': 'wlbm-loader-image mb-1'
        });

		// Save complaint.
		var saveComplaintFormId = '#wlbm-save-complaint-form';
		var saveComplaintForm = $(saveComplaintFormId);
		var saveComplaintBtn = $('#wlbm-save-complaint-btn');
		$(document).on('click', '#wlbm-save-complaint-btn', function(e) {
			e.preventDefault();
			$.confirm({
				title: saveComplaintBtn.data('message-title'),
				content: saveComplaintBtn.data('message-content'),
				type: 'red',
				useBootstrap: false,
				buttons: {
					confirm: {
						text: saveComplaintBtn.data('submit'),
	       				btnClass: 'btn-red',
						action: function () {
							saveComplaintForm.ajaxSubmit({
								beforeSubmit: function(arr, $form, options) {
									$('div.text-danger').remove();
									$(".is-invalid").removeClass("is-invalid");
									$('.wlbm .alert-dismissible').remove();
									saveComplaintBtn.prop('disabled', true);
									loaderContainer.insertAfter(saveComplaintBtn);
									loader.appendTo(loaderContainer);
									return true;
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(saveComplaintFormId);
										toastr.success(response.data.message);
										if(response.data.hasOwnProperty('reload') && response.data.reload) {
											window.location.reload();
										} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
											saveComplaintForm[0].reset();
										}
									} else {
										if(response.data && $.isPlainObject(response.data)) {
											$(saveComplaintFormId + ' :input').each(function() {
												var input = this;
												$(input).removeClass('is-invalid');
												if(response.data[input.name]) {
													var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
													$(input).addClass('is-invalid');
													$(errorSpan).insertAfter(input);
												}
											});
										} else {
											var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
											$(errorSpan).insertBefore(saveComplaintFormId);
											toastr.error(response.data);
										}
									}
								},
								error: function(response) {
									saveComplaintBtn.prop('disabled', false);
									var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
									$(errorSpan).insertBefore(saveComplaintFormId);
									toastr.error(response.data);
								},
								complete: function(event, xhr, settings) {
									saveComplaintBtn.prop('disabled', false);
									loaderContainer.remove();
								}
							});
						}
					},
					cancel: {
						text: saveComplaintBtn.data('cancel'),
						action: function () {
							return;
						}
					}
				}
			});
		});

		// Resolve complaint.
		var resolveComplaintFormId = '#wlbm-resolve-complaint-form';
		var resolveComplaintForm = $(resolveComplaintFormId);
		var resolveComplaintBtn = $('#wlbm-resolve-complaint-btn');
		$(document).on('click', '#wlbm-resolve-complaint-btn', function(e) {
			e.preventDefault();
			$.confirm({
				title: resolveComplaintBtn.data('message-title'),
				content: resolveComplaintBtn.data('message-content'),
				type: 'green',
				useBootstrap: false,
				buttons: {
					confirm: {
						text: resolveComplaintBtn.data('submit'),
	       				btnClass: 'btn-green',
						action: function () {
							resolveComplaintForm.ajaxSubmit({
								beforeSubmit: function(arr, $form, options) {
									$('div.text-danger').remove();
									$(".is-invalid").removeClass("is-invalid");
									$('.wlbm .alert-dismissible').remove();
									resolveComplaintBtn.prop('disabled', true);
									loaderContainer.insertAfter(resolveComplaintBtn);
									loader.appendTo(loaderContainer);
									return true;
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(resolveComplaintFormId);
										toastr.success(response.data.message);
										if(response.data.hasOwnProperty('reload') && response.data.reload) {
											window.location.reload();
										} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
											resolveComplaintForm[0].reset();
										}
									} else {
										if(response.data && $.isPlainObject(response.data)) {
											$(resolveComplaintFormId + ' :input').each(function() {
												var input = this;
												$(input).removeClass('is-invalid');
												if(response.data[input.name]) {
													var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
													$(input).addClass('is-invalid');
													$(errorSpan).insertAfter(input);
												}
											});
										} else {
											var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
											$(errorSpan).insertBefore(resolveComplaintFormId);
											toastr.error(response.data);
										}
									}
								},
								error: function(response) {
									resolveComplaintBtn.prop('disabled', false);
									var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
									$(errorSpan).insertBefore(resolveComplaintFormId);
									toastr.error(response.data);
								},
								complete: function(event, xhr, settings) {
									resolveComplaintBtn.prop('disabled', false);
									loaderContainer.remove();
								}
							});
						}
					},
					cancel: {
						text: resolveComplaintBtn.data('cancel'),
						action: function () {
							return;
						}
					}
				}
			});
		});

	});
})(jQuery);
