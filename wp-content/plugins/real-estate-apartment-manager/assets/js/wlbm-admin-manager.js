(function($) {
	'use strict';
	$(document).ready(function() {
		jQuery('.selectpicker').selectpicker();

		// On check existing user.
		$(document).on('change', '#wlbm_is_existing_user', function(event) {
			var checked = $(this).is(':checked');
			var newUser = $('.wlbm-new-user');
			var existingUser = $('.wlbm-exisitng-user');
			if(checked) {
				newUser.hide();
				existingUser.fadeIn();
			} else {
				existingUser.hide();
				newUser.fadeIn();
			}
		});

		// Trigger TinyMCE on submit
		$('#wlbm-save-flat-btn').mousedown(function() {
			tinyMCE.triggerSave();
		});

		// Upload flat images
		var mediaUploader;
		$('#wlbm-flat-images-btn').on('click',function(e) {
			var button = $(this);
			var title = button.data('title');
			var buttonText = button.data('button-text');
			var imageLabel = button.data('image-label');
			var imagePriority = button.data('image-priority');

			e.preventDefault();
			if(mediaUploader) {
				mediaUploader.open();
				return;
			}

			mediaUploader = wp.media.frames.file_frame = wp.media({
				title: title,
				button: {
					text: buttonText
				},
				multiple: true
			});

			mediaUploader.on('select', function() {
				var attachment = mediaUploader.state().get('selection').toJSON();
				var imagesContainer = button.parent().find('.wlbm-flat-images');

				if(attachment.length > 0) {
					if(!imagesContainer.length) {
						$('<div class="wlbm-flat-images row"></div>').prependTo(button.parent());
					}

					var flatImages = $('.wlbm-flat-images');
					for(var i = 0; i < attachment.length; i++) {
						if(!$('input[name="images[]"').filter(function() { return $(this).val() == attachment[i].id; }).length > 0) {
							flatImages.append('' +
								'<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 mt-4">' +
									'<div class="wlbm-flat-image border border-secondary p-2">' +
										'<input type="hidden" name="images[]" value="' + attachment[i].id + '">' +
										'<div class="text-right wlbm-flat-image-remove-btn"><i class="text-danger fas fa-times"></i></div>' +
										'<div class="wlbm-flat-image-preview"></div>' +
										'<label for="wlbm-flat-image-label-' + attachment[i].id + '"' + '>' + imageLabel + ':</label><br>' +
										'<input type="text" id="wlbm-flat-image-label-' + attachment[i].id + '"' + 'class="form-control wlbm-flat-image-title" name="image_titles[]" value="' + attachment[i].title + '">' +
										'<label for="wlbm-flat-image-priority-' + attachment[i].id + '" class="mt-1"' + '>' + imagePriority + ':</label><br>' +
										'<input type="number" id="wlbm-flat-image-priority-' + attachment[i].id + '"' + 'class="form-control wlbm-flat-image-priority" name="image_priorities[]" value="20">' +
									'</div>' +
								'</div>'
							);
							$('.wlbm-flat-image-preview').last().css('background-image','url(' + attachment[i].url + ')');
						}
					}
				}
			});

			mediaUploader.open();
		});

		$(document).on('click', '.wlbm-flat-image-remove-btn', function() {
			$(this).parent().parent().fadeOut(300, function() {
				$(this).remove();
			});
		});

	});
})(jQuery);
