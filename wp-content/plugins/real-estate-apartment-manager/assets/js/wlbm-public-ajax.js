(function($) {
	'use strict';
	$(document).ready(function() {
		// Loading icon variables.
		var loaderContainer = $('<span/>', {
            'class': 'wlbm-loader ml-2'
        });
        var loader = $('<img/>', {
	        'src': wlbmadminurl + 'images/spinner.gif',
	        'class': 'wlbm-loader-image mb-1'
        });

		// Registration.
		var registrationFormId = '#wlbm-registration-form';
		var registrationForm = $(registrationFormId);
		var registrationBtn = $('#wlbm-registration-btn');
		$(document).on('click', '#wlbm-registration-btn', function(e) {
			e.preventDefault();
			$.confirm({
				title: registrationBtn.data('message-title'),
				content: registrationBtn.data('message-content'),
				type: 'red',
				useBootstrap: false,
				buttons: {
					confirm: {
						text: registrationBtn.data('submit'),
	       				btnClass: 'btn-red',
						action: function () {
							registrationForm.ajaxSubmit({
								beforeSubmit: function(arr, $form, options) {
									$('div.text-danger').remove();
									$(".is-invalid").removeClass("is-invalid");
									$('.wlbm .alert-dismissible').remove();
									registrationBtn.prop('disabled', true);
									loaderContainer.insertAfter(registrationBtn);
									loader.appendTo(loaderContainer);
									return true;
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(registrationFormId);
										toastr.success(response.data.message);
										if(response.data.hasOwnProperty('reload') && response.data.reload) {
											window.location.reload();
										} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
											registrationForm[0].reset();
										}
									} else {
										if(response.data && $.isPlainObject(response.data)) {
											$(registrationFormId + ' :input').each(function() {
												var input = this;
												$(input).removeClass('is-invalid');
												if(response.data[input.name]) {
													var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
													$(input).addClass('is-invalid');
													$(errorSpan).insertAfter(input);
												}
											});
										} else {
											var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
											$(errorSpan).insertBefore(registrationFormId);
											toastr.error(response.data);
										}
									}
								},
								error: function(response) {
									registrationBtn.prop('disabled', false);
									var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
									$(errorSpan).insertBefore(registrationFormId);
									toastr.error(response.data);
								},
								complete: function(event, xhr, settings) {
									registrationBtn.prop('disabled', false);
									loaderContainer.remove();
								}
							});
						}
					},
					cancel: {
						text: registrationBtn.data('cancel'),
						action: function () {
							return;
						}
					}
				}
			});
		});

		// Login.
		var loginFormId = '#wlbm-login-form';
		var loginForm = $(loginFormId);
		var loginBtn = $('#wlbm-login-btn');

		loginForm.ajaxForm({
			beforeSubmit: function(arr, $form, options) {
				$('div.text-danger').remove();
				$(".is-invalid").removeClass("is-invalid");
				$('.wlbm .alert-dismissible').remove();
				loginBtn.prop('disabled', true);
				loaderContainer.insertAfter(loginBtn);
				loader.appendTo(loaderContainer);
				return true;
			},
			success: function(response) {
				if(response.success) {
					var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
					$(alertBox).insertBefore(loginFormId);
					toastr.success(response.data.message);
					if(response.data.hasOwnProperty('reload') && response.data.reload) {
						window.location.reload();
					} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
						loginForm[0].reset();
					}
				} else {
					if(response.data && $.isPlainObject(response.data)) {
						$(loginFormId + ' :input').each(function() {
							var input = this;
							$(input).removeClass('is-invalid');
							if(response.data[input.name]) {
								var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
								$(input).addClass('is-invalid');
								$(errorSpan).insertAfter(input);
							}
						});
					} else {
						var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
						$(errorSpan).insertBefore(loginFormId);
						toastr.error(response.data);
					}
				}
			},
			error: function(response) {
				loginBtn.prop('disabled', false);
				var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
				$(errorSpan).insertBefore(loginFormId);
				toastr.error(response.data);
			},
			complete: function(event, xhr, settings) {
				loginBtn.prop('disabled', false);
				loaderContainer.remove();
			}
		});

		// Save booking request.
		var requestBookingFormId = '#wlbm-request-booking-form';
		var requestBookingForm = $(requestBookingFormId);
		var requestBookingBtn = $('#wlbm-request-booking-btn');

		requestBookingForm.ajaxForm({
			beforeSubmit: function(arr, $form, options) {
				$('div.text-danger').remove();
				$(".is-invalid").removeClass("is-invalid");
				$('.wlbm .alert-dismissible').remove();
				requestBookingBtn.prop('disabled', true);
				loaderContainer.insertAfter(requestBookingBtn);
				loader.appendTo(loaderContainer);
				return true;
			},
			success: function(response) {
				if(response.success) {
					var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
					$(alertBox).insertBefore(requestBookingFormId);
					toastr.success(response.data.message);
					if(response.data.hasOwnProperty('reload') && response.data.reload) {
						window.location.reload();
					} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
						requestBookingForm[0].reset();
					}
				} else {
					if(response.data && $.isPlainObject(response.data)) {
						$(requestBookingFormId + ' :input').each(function() {
							var input = this;
							$(input).removeClass('is-invalid');
							if(response.data[input.name]) {
								var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
								$(input).addClass('is-invalid');
								$(errorSpan).insertAfter(input);
							}
						});
					} else {
						var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
						$(errorSpan).insertBefore(requestBookingFormId);
						toastr.error(response.data);
					}
				}
			},
			error: function(response) {
				requestBookingBtn.prop('disabled', false);
				var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
				$(errorSpan).insertBefore(requestBookingFormId);
				toastr.error(response.data);
			},
			complete: function(event, xhr, settings) {
				requestBookingBtn.prop('disabled', false);
				loaderContainer.remove();
			}
		});

	});
})(jQuery);
