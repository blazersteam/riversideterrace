(function($) {
	'use strict';
	$(document).ready(function() {

		try {
			// Datatable initialization.
			$('#wlbm-building-table').DataTable({
				responsive: true,
				aaSorting: [],
				dom: 'lBfrtip',
				lengthChange: false,
				buttons: [
					'pageLength',
				    {
						extend: 'excel',
						exportOptions: {
							columns: [0,1,2,3,4]
						}
				    },
				    {
						extend: 'csv',
						exportOptions: {
							columns: [0,1,2,3,4]
						}
				    },
				]
			});

			$('#wlbm-flat-type-table').DataTable({
				responsive: true,
				aaSorting: [],
				dom: 'lBfrtip',
				lengthChange: false,
				buttons: [
					'pageLength',
				    {
						extend: 'excel',
						exportOptions: {
							columns: [0,1]
						}
				    },
				    {
						extend: 'csv',
						exportOptions: {
							columns: [0,1]
						}
				    },
				]
			});

			$('#wlbm-flat-table').DataTable({
				responsive: true,
				aaSorting: [],
				dom: 'lBfrtip',
				lengthChange: false,
				buttons: [
					'pageLength',
				    {
						extend: 'excel',
						exportOptions: {
							columns: [0,1,2,3,4,5,6,7,8]
						}
				    },
				    {
						extend: 'csv',
						exportOptions: {
							columns: [0,1,2,3,4,5,6,7,8]
						}
				    },
				]
			});

			$('#wlbm-client-table').DataTable({
				responsive: true,
				aaSorting: [],
				dom: 'lBfrtip',
				lengthChange: false,
				buttons: [
					'pageLength',
				    {
						extend: 'excel',
						exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
						}
				    },
				    {
						extend: 'csv',
						exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
						}
				    },
				]
			});

			$('#wlbm-supplier-table').DataTable({
				responsive: true,
				aaSorting: [],
				dom: 'lBfrtip',
				lengthChange: false,
				buttons: [
					'pageLength',
				    {
						extend: 'excel',
						exportOptions: {
							columns: [0,1,2,3,4,5,6]
						}
				    },
				    {
						extend: 'csv',
						exportOptions: {
							columns: [0,1,2,3,4,5,6]
						}
				    },
				]
			});

			$('#wlbm-complaint-table').DataTable({
				responsive: true,
				aaSorting: [],
				dom: 'lBfrtip',
				lengthChange: false,
				buttons: [
					'pageLength',
				    {
						extend: 'excel',
						exportOptions: {
							columns: [0,1,2,3,4,5,6,7,8,9]
						}
				    },
				    {
						extend: 'csv',
						exportOptions: {
							columns: [0,1,2,3,4,5,6,7,8,9]
						}
				    },
				]
			});

			$('#wlbm-booking-table').DataTable({
				responsive: true,
				aaSorting: [],
				dom: 'lBfrtip',
				lengthChange: false,
				buttons: [
					'pageLength',
				    {
						extend: 'excel',
						exportOptions: {
							columns: [0,1,2,3,4,5,6,7,8]
						}
				    },
				    {
						extend: 'csv',
						exportOptions: {
							columns: [0,1,2,3,4,5,6,7,8]
						}
				    },
				]
			});
		} catch(e) {}

		// Loading icon variables.
		var loaderContainer = $('<span/>', {
            'class': 'wlbm-loader ml-2'
        });
        var loader = $('<img/>', {
	        'src': wlbmadminurl + 'images/spinner.gif',
	        'class': 'wlbm-loader-image mb-1'
        });

		// Save building.
		var saveBuildingFormId = '#wlbm-save-building-form';
		var saveBuildingForm = $(saveBuildingFormId);
		var saveBuildingBtn = $('#wlbm-save-building-btn');
		saveBuildingForm.ajaxForm({
			beforeSubmit: function(arr, $form, options) {
				$('div.text-danger').remove();
				$(".is-invalid").removeClass("is-invalid");
				$('.wlbm .alert-dismissible').remove();
				saveBuildingBtn.prop('disabled', true);
				loaderContainer.insertAfter(saveBuildingBtn);
				loader.appendTo(loaderContainer);
				return true;
			},
			success: function(response) {
				if(response.success) {
					var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
					$(alertBox).insertBefore(saveBuildingFormId);
					toastr.success(response.data.message);
					if(response.data.hasOwnProperty('reload') && response.data.reload) {
						window.location.reload();
					} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
						saveBuildingForm[0].reset();
						$('.wlbm-flat-images').remove();
					}
				} else {
					if(response.data && $.isPlainObject(response.data)) {
						$(saveBuildingFormId + ' :input').each(function() {
							var input = this;
							$(input).removeClass('is-invalid');
							if(response.data[input.name]) {
								var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
								$(input).addClass('is-invalid');
								$(errorSpan).insertAfter(input);
							}
						});
					} else {
						var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
						$(errorSpan).insertBefore(saveBuildingFormId);
						toastr.error(response.data);
					}
				}
			},
			error: function(response) {
				saveBuildingBtn.prop('disabled', false);
				var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
				$(errorSpan).insertBefore(saveBuildingFormId);
				toastr.error(response.data);
			},
			complete: function(event, xhr, settings) {
				saveBuildingBtn.prop('disabled', false);
				loaderContainer.remove();
			}
		});

		// Delete building.
		var subHeader = '.wlbm-sub-header-left';
		$(document).on("click", '.delete-building', function(event) {
			event.preventDefault();
			$('.wlbm .alert-dismissible').remove();
			var buildingId = $(this).data('building');
			var nonce = $(this).data('nonce');
			var title = $(this).data('message-title');
			var content = $(this).data('message-content');
			var deleteFlatsMessage = $(this).data('delete-flats-message');
			var cancel = $(this).data('cancel');
			var submit = $(this).data('submit');
			var action = $(this).data('action');
			$.confirm({
				title: title,
				content: '' +
				'<div class="form-check wlbm-confirm-deletion">' +
			    '<input type="checkbox" class="form-check-input wlbm-delete-flats-checkbox" id="wlbm-delete-flats-checkbox" value="1" />&nbsp;' +
			    '<label class="form-check-label" for="wlbm-delete-flats-checkbox">' + deleteFlatsMessage + '</label>' +
			    '</div>',
				type: 'red',
				useBootstrap: false,
				buttons: {
					formSubmit: {
						text: submit,
           				btnClass: 'btn-red',
						action: function () {
							var deleteFlats = this.$content.find('.wlbm-delete-flats-checkbox').is(':checked');
							var delete_flats = '';
							if(deleteFlats) {
								delete_flats = 'delete_flats=1&';
							}
							$.ajax({
								data: "building_id=" + buildingId + "&" + delete_flats + "delete-building-" + buildingId + "=" + nonce + "&action=wlbm-manager-delete-building",
								url: ajaxurl,
								type: "POST",
								beforeSend: function(xhr) {
									$('.wlbm .alert-dismissible').remove();
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(subHeader);
										toastr.success(
											response.data.message,
											'',
											{
												timeOut: 600,
												fadeOut: 600,
												closeButton: true,
												progressBar: true,
												onHidden: function() {
													window.location.reload();
												}
											}
										);
									} else {
										var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.data + '</strong></div>';
										$(errorSpan).insertBefore(subHeader);
									}
								},
								error: function(response) {
									var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.status + '</strong>: ' + response.statusText + '</div>';
									$(errorSpan).insertBefore(subHeader);
								}
							});
						}
					},
					cancel: {
						text: cancel,
						action: function () {
							$('.wlbm .alert-dismissible').remove();
						}
					}
				}
			});
		});

		// Save flat_type.
		var saveFlatTypeFormId = '#wlbm-save-flat-type-form';
		var saveFlatTypeForm = $(saveFlatTypeFormId);
		var saveFlatTypeBtn = $('#wlbm-save-flat-type-btn');
		saveFlatTypeForm.ajaxForm({
			beforeSubmit: function(arr, $form, options) {
				$('div.text-danger').remove();
				$(".is-invalid").removeClass("is-invalid");
				$('.wlbm .alert-dismissible').remove();
				saveFlatTypeBtn.prop('disabled', true);
				loaderContainer.insertAfter(saveFlatTypeBtn);
				loader.appendTo(loaderContainer);
				return true;
			},
			success: function(response) {
				if(response.success) {
					var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
					$(alertBox).insertBefore(saveFlatTypeFormId);
					toastr.success(response.data.message);
					if(response.data.hasOwnProperty('reload') && response.data.reload) {
						window.location.reload();
					} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
						saveFlatTypeForm[0].reset();
					}
				} else {
					if(response.data && $.isPlainObject(response.data)) {
						$(saveFlatTypeFormId + ' :input').each(function() {
							var input = this;
							$(input).removeClass('is-invalid');
							if(response.data[input.name]) {
								var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
								$(input).addClass('is-invalid');
								$(errorSpan).insertAfter(input);
							}
						});
					} else {
						var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
						$(errorSpan).insertBefore(saveFlatTypeFormId);
						toastr.error(response.data);
					}
				}
			},
			error: function(response) {
				saveFlatTypeBtn.prop('disabled', false);
				var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
				$(errorSpan).insertBefore(saveFlatTypeFormId);
				toastr.error(response.data);
			},
			complete: function(event, xhr, settings) {
				saveFlatTypeBtn.prop('disabled', false);
				loaderContainer.remove();
			}
		});

		// Delete flat_type.
		var subHeader = '.wlbm-sub-header-left';
		$(document).on("click", '.delete-flat-type', function(event) {
			event.preventDefault();
			$('.wlbm .alert-dismissible').remove();
			var flatTypeId = $(this).data('flat-type');
			var nonce = $(this).data('nonce');
			var title = $(this).data('message-title');
			var content = $(this).data('message-content');
			var cancel = $(this).data('cancel');
			var submit = $(this).data('submit');
			var action = $(this).data('action');
			$.confirm({
				title: title,
				content: content,
				type: 'red',
				useBootstrap: false,
				buttons: {
					formSubmit: {
						text: submit,
           				btnClass: 'btn-red',
						action: function () {
							$.ajax({
								data: "flat_type_id=" + flatTypeId + "&" + "delete-flat-type-" + flatTypeId + "=" + nonce + "&action=wlbm-manager-delete-flat-type",
								url: ajaxurl,
								type: "POST",
								beforeSend: function(xhr) {
									$('.wlbm .alert-dismissible').remove();
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(subHeader);
										toastr.success(
											response.data.message,
											'',
											{
												timeOut: 600,
												fadeOut: 600,
												closeButton: true,
												progressBar: true,
												onHidden: function() {
													window.location.reload();
												}
											}
										);
									} else {
										var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.data + '</strong></div>';
										$(errorSpan).insertBefore(subHeader);
									}
								},
								error: function(response) {
									var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.status + '</strong>: ' + response.statusText + '</div>';
									$(errorSpan).insertBefore(subHeader);
								}
							});
						}
					},
					cancel: {
						text: cancel,
						action: function () {
							$('.wlbm .alert-dismissible').remove();
						}
					}
				}
			});
		});

		// Save flat.
		var saveFlatFormId = '#wlbm-save-flat-form';
		var saveFlatForm = $(saveFlatFormId);
		var saveFlatBtn = $('#wlbm-save-flat-btn');
		saveFlatForm.ajaxForm({
			beforeSubmit: function(arr, $form, options) {
				$('div.text-danger').remove();
				$(".is-invalid").removeClass("is-invalid");
				$('.wlbm .alert-dismissible').remove();
				saveFlatBtn.prop('disabled', true);
				loaderContainer.insertAfter(saveFlatBtn);
				loader.appendTo(loaderContainer);
				return true;
			},
			success: function(response) {
				if(response.success) {
					var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
					$(alertBox).insertBefore(saveFlatFormId);
					toastr.success(response.data.message);
					if(response.data.hasOwnProperty('reload') && response.data.reload) {
						window.location.reload();
					} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
						saveFlatForm[0].reset();
						$('.selectpicker').selectpicker('refresh');
						$('.wlbm-flat-images').remove();
					}
				} else {
					if(response.data && $.isPlainObject(response.data)) {
						$(saveFlatFormId + ' :input').each(function() {
							var input = this;
							$(input).removeClass('is-invalid');
							if(response.data[input.name]) {
								var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
								$(input).addClass('is-invalid');
								$(errorSpan).insertAfter(input);
							}
						});
					} else {
						var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
						$(errorSpan).insertBefore(saveFlatFormId);
						toastr.error(response.data);
					}
				}
			},
			error: function(response) {
				saveFlatBtn.prop('disabled', false);
				var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
				$(errorSpan).insertBefore(saveFlatFormId);
				toastr.error(response.data);
			},
			complete: function(event, xhr, settings) {
				saveFlatBtn.prop('disabled', false);
				loaderContainer.remove();
			}
		});

		// Delete flat.
		var subHeader = '.wlbm-sub-header-left';
		$(document).on("click", '.delete-flat', function(event) {
			event.preventDefault();
			$('.wlbm .alert-dismissible').remove();
			var flatId = $(this).data('flat');
			var nonce = $(this).data('nonce');
			var title = $(this).data('message-title');
			var content = $(this).data('message-content');
			var cancel = $(this).data('cancel');
			var submit = $(this).data('submit');
			var action = $(this).data('action');
			$.confirm({
				title: title,
				content: content,
				type: 'red',
				useBootstrap: false,
				buttons: {
					confirm: {
						text: submit,
           				btnClass: 'btn-red',
						action: function () {
							$.ajax({
								data: "flat_id=" + flatId + "&" + "delete-flat-" + flatId + "=" + nonce + "&action=wlbm-manager-delete-flat",
								url: ajaxurl,
								type: "POST",
								beforeSend: function(xhr) {
									$('.wlbm .alert-dismissible').remove();
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(subHeader);
										toastr.success(
											response.data.message,
											'',
											{
												timeOut: 600,
												fadeOut: 600,
												closeButton: true,
												progressBar: true,
												onHidden: function() {
													window.location.reload();
												}
											}
										);
									} else {
										var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.data + '</strong></div>';
										$(errorSpan).insertBefore(subHeader);
									}
								},
								error: function(response) {
									var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.status + '</strong>: ' + response.statusText + '</div>';
									$(errorSpan).insertBefore(subHeader);
								}
							});
						}
					},
					cancel: {
						text: cancel,
						action: function () {
							$('.wlbm .alert-dismissible').remove();
						}
					}
				}
			});
		});

		// Get flat detail for client.
		$(document).on('change', '#wlbm_flat', function(event) {
			var flatId = this.value;
			var nonce  = $(this).find(':selected').data('nonce');
			var target = $('.wlbm-flat-info');
			if(flatId && nonce) {
				$.ajax({
					data: "flat_id=" + flatId + "&" + "get-flat-" + flatId + "=" + nonce + "&action=wlbm-manager-get-flat-detail",
					url: ajaxurl,
					type: "POST",
					beforeSend: function(xhr) {
						$('.wlbm .alert-dismissible').remove();
					},
					success: function(response) {
						if(response.success) {
							var flat = response.data.flat;
							var label = response.data.label;
							var html = '' +
							'<div class="mb-3">' +
								'<ul class="list-group">' +
									'<li class="list-group-item"><span class="wlbm-list-key">' + label.flat_number + '</span>:&nbsp;<span class="wlbm-list-value">' + flat.flat_number + '</span></li>' +
									'<li class="list-group-item"><span class="wlbm-list-key">' + label.building_name + '</span>:&nbsp;<span class="wlbm-list-value">' + flat.building_name + '</span></li>' +
									'<li class="list-group-item"><span class="wlbm-list-key">' + label.floor_number + '</span>:&nbsp;<span class="wlbm-list-value">' + flat.floor_number + '</span></li>' +
									'<li class="list-group-item"><span class="wlbm-list-key">' + label.area + '</span>:&nbsp;<span class="wlbm-list-value">' + flat.area + '</span></li>' +
									'<li class="list-group-item"><span class="wlbm-list-key">' + label.price + '</span>:&nbsp;<span class="wlbm-list-value">' + flat.price + '</span></li>' +
									'<li class="list-group-item"><span class="wlbm-list-key">' + label.rental_price + '</span>:&nbsp;<span class="wlbm-list-value">' + flat.rental_price + '</span></li>' +
									'<li class="list-group-item"><span class="wlbm-list-key">' + label.active_clients_count + '</span>:&nbsp;<span class="wlbm-list-value">' + flat.active_clients_count + '</span></li>' +
								'</ul>' +
							'</div>';
							$(target).html(html);
						} else {
							var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.data + '</strong></div>';
							$(target).html(errorSpan);
						}
					},
					error: function(response) {
						var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.status + '</strong>: ' + response.statusText + '</div>';
						$(target).html(errorSpan);
					}
				});
			} else {
				target.html('');
			}
		});

		// Save client.
		var saveClientFormId = '#wlbm-save-client-form';
		var saveClientForm = $(saveClientFormId);
		var saveClientBtn = $('#wlbm-save-client-btn');
		saveClientForm.ajaxForm({
			beforeSubmit: function(arr, $form, options) {
				$('div.text-danger').remove();
				$(".is-invalid").removeClass("is-invalid");
				$('.wlbm .alert-dismissible').remove();
				saveClientBtn.prop('disabled', true);
				loaderContainer.insertAfter(saveClientBtn);
				loader.appendTo(loaderContainer);
				return true;
			},
			success: function(response) {
				if(response.success) {
					var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
					$(alertBox).insertBefore(saveClientFormId);
					toastr.success(response.data.message);
					if(response.data.hasOwnProperty('reload') && response.data.reload) {
						window.location.reload();
					} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
						saveClientForm[0].reset();
						$('.selectpicker').selectpicker('refresh');
					}
				} else {
					if(response.data && $.isPlainObject(response.data)) {
						$(saveClientFormId + ' :input').each(function() {
							var input = this;
							$(input).removeClass('is-invalid');
							if(response.data[input.name]) {
								var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
								$(input).addClass('is-invalid');
								$(errorSpan).insertAfter(input);
							}
						});
					} else {
						var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
						$(errorSpan).insertBefore(saveClientFormId);
						toastr.error(response.data);
					}
				}
			},
			error: function(response) {
				saveClientBtn.prop('disabled', false);
				var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
				$(errorSpan).insertBefore(saveClientFormId);
				toastr.error(response.data);
			},
			complete: function(event, xhr, settings) {
				saveClientBtn.prop('disabled', false);
				loaderContainer.remove();
			}
		});

		// Delete client.
		var subHeader = '.wlbm-sub-header-left';
		$(document).on("click", '.delete-client', function(event) {
			event.preventDefault();
			$('.wlbm .alert-dismissible').remove();
			var clientId = $(this).data('client');
			var nonce = $(this).data('nonce');
			var title = $(this).data('message-title');
			var content = $(this).data('message-content');
			var deleteUserMessage = $(this).data('delete-user-message');
			var cancel = $(this).data('cancel');
			var submit = $(this).data('submit');
			var action = $(this).data('action');
			$.confirm({
				title: title,
				content: '' +
				'<div class="form-check wlbm-confirm-deletion">' +
			    '<input type="checkbox" class="form-check-input wlbm-delete-user-checkbox" id="wlbm-delete-user-checkbox" value="1" />&nbsp;' +
			    '<label class="form-check-label" for="wlbm-delete-user-checkbox">' + deleteUserMessage + '</label>' +
			    '</div>',
				type: 'red',
				useBootstrap: false,
				buttons: {
					formSubmit: {
						text: submit,
           				btnClass: 'btn-red',
						action: function () {
							var deleteUser = this.$content.find('.wlbm-delete-user-checkbox').is(':checked');
							var delete_user = '';
							if(deleteUser) {
								delete_user = 'delete_user=1&';
							}
							$.ajax({
								data: "client_id=" + clientId + "&" + delete_user + "delete-client-" + clientId + "=" + nonce + "&action=wlbm-manager-delete-client",
								url: ajaxurl,
								type: "POST",
								beforeSend: function(xhr) {
									$('.wlbm .alert-dismissible').remove();
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(subHeader);
										toastr.success(
											response.data.message,
											'',
											{
												timeOut: 600,
												fadeOut: 600,
												closeButton: true,
												progressBar: true,
												onHidden: function() {
													window.location.reload();
												}
											}
										);
									} else {
										var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.data + '</strong></div>';
										$(errorSpan).insertBefore(subHeader);
									}
								},
								error: function(response) {
									var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.status + '</strong>: ' + response.statusText + '</div>';
									$(errorSpan).insertBefore(subHeader);
								}
							});
						}
					},
					cancel: {
						text: cancel,
						action: function () {
							$('.wlbm .alert-dismissible').remove();
						}
					}
				}
			});
		});

		// Save supplier.
		var saveSupplierFormId = '#wlbm-save-supplier-form';
		var saveSupplierForm = $(saveSupplierFormId);
		var saveSupplierBtn = $('#wlbm-save-supplier-btn');
		saveSupplierForm.ajaxForm({
			beforeSubmit: function(arr, $form, options) {
				$('div.text-danger').remove();
				$(".is-invalid").removeClass("is-invalid");
				$('.wlbm .alert-dismissible').remove();
				saveSupplierBtn.prop('disabled', true);
				loaderContainer.insertAfter(saveSupplierBtn);
				loader.appendTo(loaderContainer);
				return true;
			},
			success: function(response) {
				if(response.success) {
					var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
					$(alertBox).insertBefore(saveSupplierFormId);
					toastr.success(response.data.message);
					if(response.data.hasOwnProperty('reload') && response.data.reload) {
						window.location.reload();
					} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
						saveSupplierForm[0].reset();
						$('.selectpicker').selectpicker('refresh');
					}
				} else {
					if(response.data && $.isPlainObject(response.data)) {
						$(saveSupplierFormId + ' :input').each(function() {
							var input = this;
							$(input).removeClass('is-invalid');
							if(response.data[input.name]) {
								var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
								$(input).addClass('is-invalid');
								$(errorSpan).insertAfter(input);
							}
						});
					} else {
						var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
						$(errorSpan).insertBefore(saveSupplierFormId);
						toastr.error(response.data);
					}
				}
			},
			error: function(response) {
				saveSupplierBtn.prop('disabled', false);
				var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
				$(errorSpan).insertBefore(saveSupplierFormId);
				toastr.error(response.data);
			},
			complete: function(event, xhr, settings) {
				saveSupplierBtn.prop('disabled', false);
				loaderContainer.remove();
			}
		});

		// Delete supplier.
		var subHeader = '.wlbm-sub-header-left';
		$(document).on("click", '.delete-supplier', function(event) {
			event.preventDefault();
			$('.wlbm .alert-dismissible').remove();
			var supplierId = $(this).data('supplier');
			var nonce = $(this).data('nonce');
			var title = $(this).data('message-title');
			var content = $(this).data('message-content');
			var deleteUserMessage = $(this).data('delete-user-message');
			var cancel = $(this).data('cancel');
			var submit = $(this).data('submit');
			var action = $(this).data('action');
			$.confirm({
				title: title,
				content: '' +
				'<div class="form-check wlbm-confirm-deletion">' +
			    '<input type="checkbox" class="form-check-input wlbm-delete-user-checkbox" id="wlbm-delete-user-checkbox" value="1" />&nbsp;' +
			    '<label class="form-check-label" for="wlbm-delete-user-checkbox">' + deleteUserMessage + '</label>' +
			    '</div>',
				type: 'red',
				useBootstrap: false,
				buttons: {
					formSubmit: {
						text: submit,
           				btnClass: 'btn-red',
						action: function () {
							var deleteUser = this.$content.find('.wlbm-delete-user-checkbox').is(':checked');
							var delete_user = '';
							if(deleteUser) {
								delete_user = 'delete_user=1&';
							}
							$.ajax({
								data: "supplier_id=" + supplierId + "&" + delete_user + "delete-supplier-" + supplierId + "=" + nonce + "&action=wlbm-manager-delete-supplier",
								url: ajaxurl,
								type: "POST",
								beforeSend: function(xhr) {
									$('.wlbm .alert-dismissible').remove();
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(subHeader);
										toastr.success(
											response.data.message,
											'',
											{
												timeOut: 600,
												fadeOut: 600,
												closeButton: true,
												progressBar: true,
												onHidden: function() {
													window.location.reload();
												}
											}
										);
									} else {
										var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.data + '</strong></div>';
										$(errorSpan).insertBefore(subHeader);
									}
								},
								error: function(response) {
									var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.status + '</strong>: ' + response.statusText + '</div>';
									$(errorSpan).insertBefore(subHeader);
								}
							});
						}
					},
					cancel: {
						text: cancel,
						action: function () {
							$('.wlbm .alert-dismissible').remove();
						}
					}
				}
			});
		});

		// Forward complaint.
		var forwardComplaintFormId = '#wlbm-forward-complaint-form';
		var forwardComplaintForm = $(forwardComplaintFormId);
		var forwardComplaintBtn = $('#wlbm-forward-complaint-btn');
		$(document).on('click', '#wlbm-forward-complaint-btn', function(e) {
			e.preventDefault();
			$.confirm({
				title: forwardComplaintBtn.data('message-title'),
				content: forwardComplaintBtn.data('message-content'),
				type: 'red',
				useBootstrap: false,
				buttons: {
					confirm: {
						text: forwardComplaintBtn.data('submit'),
	       				btnClass: 'btn-red',
						action: function () {
							forwardComplaintForm.ajaxSubmit({
								beforeSubmit: function(arr, $form, options) {
									$('div.text-danger').remove();
									$(".is-invalid").removeClass("is-invalid");
									$('.wlbm .alert-dismissible').remove();
									forwardComplaintBtn.prop('disabled', true);
									loaderContainer.insertAfter(forwardComplaintBtn);
									loader.appendTo(loaderContainer);
									return true;
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(forwardComplaintFormId);
										toastr.success(response.data.message);
										if(response.data.hasOwnProperty('reload') && response.data.reload) {
											window.location.reload();
										} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
											forwardComplaintForm[0].reset();
										}
									} else {
										if(response.data && $.isPlainObject(response.data)) {
											$(forwardComplaintFormId + ' :input').each(function() {
												var input = this;
												$(input).removeClass('is-invalid');
												if(response.data[input.name]) {
													var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
													$(input).addClass('is-invalid');
													$(errorSpan).insertAfter(input);
												}
											});
										} else {
											var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
											$(errorSpan).insertBefore(forwardComplaintFormId);
											toastr.error(response.data);
										}
									}
								},
								error: function(response) {
									forwardComplaintBtn.prop('disabled', false);
									var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
									$(errorSpan).insertBefore(forwardComplaintFormId);
									toastr.error(response.data);
								},
								complete: function(event, xhr, settings) {
									forwardComplaintBtn.prop('disabled', false);
									loaderContainer.remove();
								}
							});
						}
					},
					cancel: {
						text: forwardComplaintBtn.data('cancel'),
						action: function () {
							return;
						}
					}
				}
			});
		});

		// Delete complaint.
		var subHeader = '.wlbm-sub-header-left';
		$(document).on("click", '.delete-complaint', function(event) {
			event.preventDefault();
			$('.wlbm .alert-dismissible').remove();
			var complaintId = $(this).data('complaint');
			var nonce = $(this).data('nonce');
			var title = $(this).data('message-title');
			var content = $(this).data('message-content');
			var cancel = $(this).data('cancel');
			var submit = $(this).data('submit');
			var action = $(this).data('action');
			$.confirm({
				title: title,
				content: content,
				type: 'red',
				useBootstrap: false,
				buttons: {
					confirm: {
						text: submit,
           				btnClass: 'btn-red',
						action: function () {
							$.ajax({
								data: "complaint_id=" + complaintId + "&" + "delete-complaint-" + complaintId + "=" + nonce + "&action=wlbm-manager-delete-complaint",
								url: ajaxurl,
								type: "POST",
								beforeSend: function(xhr) {
									$('.wlbm .alert-dismissible').remove();
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(subHeader);
										toastr.success(
											response.data.message,
											'',
											{
												timeOut: 600,
												fadeOut: 600,
												closeButton: true,
												progressBar: true,
												onHidden: function() {
													window.location.reload();
												}
											}
										);
									} else {
										var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.data + '</strong></div>';
										$(errorSpan).insertBefore(subHeader);
									}
								},
								error: function(response) {
									var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.status + '</strong>: ' + response.statusText + '</div>';
									$(errorSpan).insertBefore(subHeader);
								}
							});
						}
					},
					cancel: {
						text: cancel,
						action: function () {
							$('.wlbm .alert-dismissible').remove();
						}
					}
				}
			});
		});

		// Resolve complaint.
		var resolveComplaintFormId = '#wlbm-resolve-complaint-form';
		var resolveComplaintForm = $(resolveComplaintFormId);
		var resolveComplaintBtn = $('#wlbm-resolve-complaint-btn');
		$(document).on('click', '#wlbm-resolve-complaint-btn', function(e) {
			e.preventDefault();
			$.confirm({
				title: resolveComplaintBtn.data('message-title'),
				content: resolveComplaintBtn.data('message-content'),
				type: 'green',
				useBootstrap: false,
				buttons: {
					confirm: {
						text: resolveComplaintBtn.data('submit'),
	       				btnClass: 'btn-green',
						action: function () {
							resolveComplaintForm.ajaxSubmit({
								beforeSubmit: function(arr, $form, options) {
									$('div.text-danger').remove();
									$(".is-invalid").removeClass("is-invalid");
									$('.wlbm .alert-dismissible').remove();
									resolveComplaintBtn.prop('disabled', true);
									loaderContainer.insertAfter(resolveComplaintBtn);
									loader.appendTo(loaderContainer);
									return true;
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(resolveComplaintFormId);
										toastr.success(response.data.message);
										if(response.data.hasOwnProperty('reload') && response.data.reload) {
											window.location.reload();
										} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
											resolveComplaintForm[0].reset();
										}
									} else {
										if(response.data && $.isPlainObject(response.data)) {
											$(resolveComplaintFormId + ' :input').each(function() {
												var input = this;
												$(input).removeClass('is-invalid');
												if(response.data[input.name]) {
													var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
													$(input).addClass('is-invalid');
													$(errorSpan).insertAfter(input);
												}
											});
										} else {
											var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
											$(errorSpan).insertBefore(resolveComplaintFormId);
											toastr.error(response.data);
										}
									}
								},
								error: function(response) {
									resolveComplaintBtn.prop('disabled', false);
									var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
									$(errorSpan).insertBefore(resolveComplaintFormId);
									toastr.error(response.data);
								},
								complete: function(event, xhr, settings) {
									resolveComplaintBtn.prop('disabled', false);
									loaderContainer.remove();
								}
							});
						}
					},
					cancel: {
						text: resolveComplaintBtn.data('cancel'),
						action: function () {
							return;
						}
					}
				}
			});
		});

		// Save booking request.
		var saveBookingFormId = '#wlbm-save-booking-form';
		var saveBookingForm = $(saveBookingFormId);
		var saveBookingBtn = $('#wlbm-save-booking-btn');
		saveBookingForm.ajaxForm({
			beforeSubmit: function(arr, $form, options) {
				$('div.text-danger').remove();
				$(".is-invalid").removeClass("is-invalid");
				$('.wlbm .alert-dismissible').remove();
				saveBookingBtn.prop('disabled', true);
				loaderContainer.insertAfter(saveBookingBtn);
				loader.appendTo(loaderContainer);
				return true;
			},
			success: function(response) {
				if(response.success) {
					var alertBox = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
					$(alertBox).insertBefore(saveBookingFormId);
					toastr.success(response.data.message);
					if(response.data.hasOwnProperty('reload') && response.data.reload) {
						window.location.reload();
					} else if(response.data.hasOwnProperty('reset') && response.data.reset) {
						saveBookingForm[0].reset();
					}
				} else {
					if(response.data && $.isPlainObject(response.data)) {
						$(saveBookingFormId + ' :input').each(function() {
							var input = this;
							$(input).removeClass('is-invalid');
							if(response.data[input.name]) {
								var errorSpan = '<div class="text-danger mt-1">' + response.data[input.name] + '</div>';
								$(input).addClass('is-invalid');
								$(errorSpan).insertAfter(input);
							}
						});
					} else {
						var errorSpan = '<div class="text-danger mt-1">' + response.data + '<hr></div>';
						$(errorSpan).insertBefore(saveBookingFormId);
						toastr.error(response.data);
					}
				}
			},
			error: function(response) {
				saveBookingBtn.prop('disabled', false);
				var errorSpan = '<div class="text-danger mt-1"><strong>' + response.status + '</strong>: ' + response.statusText + '<hr></div>';
				$(errorSpan).insertBefore(saveBookingFormId);
				toastr.error(response.data);
			},
			complete: function(event, xhr, settings) {
				saveBookingBtn.prop('disabled', false);
				loaderContainer.remove();
			}
		});

		// Delete booking request.
		var subHeader = '.wlbm-sub-header-left';
		$(document).on("click", '.delete-booking', function(event) {
			event.preventDefault();
			$('.wlbm .alert-dismissible').remove();
			var bookingId = $(this).data('booking');
			var nonce = $(this).data('nonce');
			var title = $(this).data('message-title');
			var content = $(this).data('message-content');
			var cancel = $(this).data('cancel');
			var submit = $(this).data('submit');
			var action = $(this).data('action');
			$.confirm({
				title: title,
				content: content,
				type: 'red',
				useBootstrap: false,
				buttons: {
					formSubmit: {
						text: submit,
           				btnClass: 'btn-red',
						action: function () {
							$.ajax({
								data: "booking_id=" + bookingId + "&" + "delete-booking-" + bookingId + "=" + nonce + "&action=wlbm-manager-delete-booking",
								url: ajaxurl,
								type: "POST",
								beforeSend: function(xhr) {
									$('.wlbm .alert-dismissible').remove();
								},
								success: function(response) {
									if(response.success) {
										var alertBox = '<div class="alert alert-success alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong><i class="fa fa-check"></i> &nbsp;' + response.data.message + '</strong></div>';
										$(alertBox).insertBefore(subHeader);
										toastr.success(
											response.data.message,
											'',
											{
												timeOut: 600,
												fadeOut: 600,
												closeButton: true,
												progressBar: true,
												onHidden: function() {
													window.location.reload();
												}
											}
										);
									} else {
										var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.data + '</strong></div>';
										$(errorSpan).insertBefore(subHeader);
									}
								},
								error: function(response) {
									var errorSpan = '<div class="alert alert-danger alert-dismissible clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + response.status + '</strong>: ' + response.statusText + '</div>';
									$(errorSpan).insertBefore(subHeader);
								}
							});
						}
					},
					cancel: {
						text: cancel,
						action: function () {
							$('.wlbm .alert-dismissible').remove();
						}
					}
				}
			});
		});

	});
})(jQuery);
