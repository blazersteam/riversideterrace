<?php
defined( 'ABSPATH' ) || die();

$page_url = admin_url() . 'admin.php?page=' . WLBM_MENU_SUPPLIER_DASHBOARD;
?>
<div class="text-center">
	<h4><?php esc_html_e( 'Redirecting to Supplier Area...', 'WL-BM' ); ?></h4>
</div>
<?php
$script = 'window.location.href = "' . $page_url . '";';
wp_add_inline_script( 'wlbm-public', $script );
