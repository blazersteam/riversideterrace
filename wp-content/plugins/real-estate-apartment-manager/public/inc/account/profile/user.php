<?php
defined( 'ABSPATH' ) || die();
global $wp;
?>
<div class="text-center">
	<h4><?php esc_html_e( 'You are logged in.', 'WL-BM' ); ?></h4>
	<a href="<?php echo esc_url( wp_logout_url( home_url( $wp->request ) ) ); ?>" class="font-weight-bold">
		<?php esc_html_e( 'Logout','WL-BM' ); ?>
	</a>
</div>
