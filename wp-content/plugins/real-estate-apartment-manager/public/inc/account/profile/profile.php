<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$user = wp_get_current_user();

$client = $wpdb->get_row( $wpdb->prepare( "SELECT c.name, c.phone, f.flat_number, f.floor_number, b.name as building_name FROM {$wpdb->prefix}wlbm_clients as c, {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON b.ID = f.building_id WHERE c.user_id = %d AND f.ID = c.flat_id", $user->ID ) );

$supplier = $wpdb->get_row( $wpdb->prepare( "SELECT ID, name, phone FROM {$wpdb->prefix}wlbm_suppliers WHERE user_id = %d", $user->ID ) );

if ( $client ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/account/profile/client.php';
} elseif ( $supplier ) {
	require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/account/profile/supplier.php';
} else {
	require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/account/profile/user.php';
}
