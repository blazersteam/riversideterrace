<?php
defined( 'ABSPATH' ) || die();
?>
<div class="row">
	<div class="col-md-6">
		<h2 class="wlbm-form-registration-title"><?php esc_html_e( 'Registration Form', 'WL-BM' ); ?></h2>
		<h2 class="wlbm-form-login-title"><?php esc_html_e( 'Login Form', 'WL-BM' ); ?></h2>
	</div>
	<div class="col-md-6">
		<div class="float-right">
			<div class="wlbm-show-login-form">
				<span class="text-secondary"><?php esc_html_e( 'Login if you already have an account.', 'WL-BM' ); ?></span>
				<br>
				<button id="wlbm-show-login-form-btn" class="btn btn-sm btn-outline-primary mt-1 float-right">
					<i class="fas fa-sign-in-alt"></i>&nbsp;
					<?php esc_html_e( 'Login', 'WL-BM' ); ?>
				</button>
			</div>
			<div class="wlbm-show-registration-form">
				<span class="text-secondary"><?php esc_html_e( 'Register if you do not have an account.', 'WL-BM' ); ?></span>
				<br>
				<button id="wlbm-show-registration-form-btn" class="btn btn-sm btn-outline-primary mt-1 float-right">
					<i class="fas fa-user-plus"></i>&nbsp;
					<?php esc_html_e( 'Register', 'WL-BM' ); ?>
				</button>
			</div>
		</div>
	</div>
</div>
<p><?php esc_html_e( 'Fill all the required fields (*).', 'WL-BM' ); ?></p>
