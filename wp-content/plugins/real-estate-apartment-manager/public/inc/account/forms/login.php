<?php
defined( 'ABSPATH' ) || die();
?>
<form method="post" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" id="wlbm-login-form" class="wlbm-login-form">
	<?php $nonce = wp_create_nonce( 'login' ); ?>
	<input type="hidden" name="login" value="<?php echo esc_attr( $nonce ); ?>">
	<input type="hidden" name="action" value="wlbm-login">
	
	<div class="row justify-content-md-center">
		<div class="col-md-8">
			<h5><?php esc_html_e( 'Login Detail', 'WL-BM' ); ?></h5>
			<hr>

			<div class="form-group">
				<label for="wlbm-login-username">* <?php esc_html_e( 'Email Address or Username', 'WL-BM' ); ?>:</label><br>
				<input type="text" name="username" id="wlbm-login-username" class="form-control">
			</div>

			<div class="form-group">
				<label for="wlbm-login-password">* <?php esc_html_e( 'Password', 'WL-BM' ); ?>:</label>
				<input type="password" name="password" id="wlbm-login-password" class="form-control">
			</div>

			<hr>

			<div class="clearfix">
				<span class="float-right">
					<button type="submit" class="btn btn-primary" id="wlbm-login-btn">
						<i class="fas fa-sign-in-alt"></i>&nbsp;
						<?php esc_html_e( 'Login', 'WL-BM' ); ?>
					</button>
				</span>
			</div>
		</div>
	</div>
</form>
