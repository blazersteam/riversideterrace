<?php
defined( 'ABSPATH' ) || die();
?>
<form method="post" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" id="wlbm-registration-form" class="wlbm-registration-form">
	<?php $nonce = wp_create_nonce( 'register' ); ?>
	<input type="hidden" name="register" value="<?php echo esc_attr( $nonce ); ?>">
	<input type="hidden" name="action" value="wlbm-register">

	<div class="row">
		<div class="form-group col-md-12">
			<br>
			<label for="wlbm-registration-as">* <?php esc_html_e( 'Signup As', 'WL-BM' ); ?>:</label><br>
			<select name="register_as" id="wlbm-registration-as" class="form-control">
				<option value="resident"><?php esc_html_e( 'Resident', 'WL-BM' ); ?></option>
				<option value="supplier"><?php esc_html_e( 'Supplier', 'WL-BM' ); ?></option>
			</select>
			<br>
		</div>

		<div class="col-md-12">
			<h5><?php esc_html_e( 'Login Detail', 'WL-BM' ); ?></h5>
			<hr>
		</div>

		<div class="form-group col-md-6">
			<label for="wlbm-registration-email"><?php esc_html_e( 'Email Address', 'WL-BM' ); ?>:</label><br>
			<input type="email" name="email" id="wlbm-registration-email" class="form-control">
		</div>

		<div class="form-group col-md-6">
			<label for="wlbm-registration-username">* <?php esc_html_e( 'Username', 'WL-BM' ); ?>:</label>
			<input type="text" name="username" id="wlbm-registration-username" class="form-control">
		</div>

		<div class="form-group col-md-6">
			<label for="wlbm-registration-password">* <?php esc_html_e( 'Password', 'WL-BM' ); ?>:</label>
			<input type="password" name="password" id="wlbm-registration-password" class="form-control">
		</div>

		<div class="form-group col-md-6">
			<label for="wlbm-registration-password-confirm">* <?php esc_html_e( 'Confirm Password', 'WL-BM' ); ?>:</label>
			<input type="password" name="password_confirm" id="wlbm-registration-password-confirm" class="form-control">
		</div>

		<div class="col-md-12">
			<hr>
		</div>
	</div>

	<div class="wlbm-client">
		<div class="row">
			<div class="col-md-12">
				<h5><?php esc_html_e( 'Personal Detail', 'WL-BM' ); ?></h5>
				<hr>
			</div>

			<div class="form-group col-md-12">
				<label for="wlbm_client_flat">* <?php esc_html_e( 'Flat', 'WL-BM' ); ?>:</label>
				<select name="client_flat" class="form-control selectpicker" data-live-search="true" id="wlbm_client_flat">
					<option value="">-------- <?php esc_html_e( 'Select Flat', 'WL-BM' ); ?> --------</option>
					<?php foreach ( $flats as $flat ) {
						$option_text = esc_html__( 'Flat Number', 'WL-BM' ) . ': ' . $flat->flat_number;
					?>
					 <option data-icon="far fa-building" value="<?php echo esc_attr( $flat->ID ); ?>">
					 	&nbsp;<?php echo esc_html( $option_text ); ?>&nbsp;(<?php echo esc_html( $flat->building_name ); ?>) <?php esc_html_e( 'Floor', 'WL-BM' ); ?> <?php echo esc_attr( $flat->floor_number ); ?>
					 </option>
					<?php } ?>
				</select>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="wlbm_client_name">* <?php esc_html_e( 'Full Name', 'WL-BM' ); ?>:</label>
					<input type="text" name="client_name" class="form-control" id="wlbm_client_name">
				</div>

				<div class="form-group">
					<label for="wlbm_client_phone">* <?php esc_html_e( 'Phone', 'WL-BM' ); ?>:</label>
					<input type="text" name="client_phone" class="form-control" id="wlbm_client_phone">
				</div>
			</div>

			<div class="form-group col-md-6">
				<label for="wlbm_client_address"><?php esc_html_e( 'Address', 'WL-BM' ); ?>:</label>
				<textarea name="client_address" class="form-control" id="wlbm_client_address" rows="4"></textarea>
			</div>

			<div class="col-md-12">
				<hr>
			</div>
		</div>
	</div>

	<div class="wlbm-supplier">
		<div class="row">
			<div class="col-md-12">
				<h5><?php esc_html_e( 'Personal Detail', 'WL-BM' ); ?></h5>
				<hr>
			</div>

			<?php if ( count( $buildings ) ) { ?>
			<div class="form-group col-md-12">
				<label for="wlbm_supplier_building"><?php esc_html_e( 'Buildings', 'WL-BM' ); ?>:</label>
				<select name="supplier_building[]" class="form-control selectpicker" data-actions-box="true" data-live-search="true" id="wlbm_supplier_building" multiple title="-------- <?php esc_attr_e( 'Select Buildings', 'WL-BM' ); ?> --------">
					<?php foreach ( $buildings as $building ) {
						$option_text = $building->name;
					?>
					 <option value="<?php echo esc_attr( $building->ID ); ?>">
					 	<?php echo esc_html( $option_text ); ?>
				 	</option>
					<?php } ?>
				</select>
			</div>
			<?php } ?>

			<div class="col-md-6">
				<div class="form-group">
					<label for="wlbm_supplier_name">* <?php esc_html_e( 'Full Name', 'WL-BM' ); ?>:</label>
					<input type="text" name="supplier_name" class="form-control" id="wlbm_supplier_name">
				</div>

				<div class="form-group">
					<label for="wlbm_supplier_phone">* <?php esc_html_e( 'Phone', 'WL-BM' ); ?>:</label>
					<input type="text" name="supplier_phone" class="form-control" id="wlbm_supplier_phone">
				</div>
			</div>

			<div class="form-group col-md-6">
				<label for="wlbm_supplier_address"><?php esc_html_e( 'Address', 'WL-BM' ); ?>:</label>
				<textarea name="supplier_address" class="form-control" id="wlbm_supplier_address" rows="4"></textarea>
			</div>

			<div class="form-group col-md-6">
				<label for="wlbm_supplier_expertise"><?php esc_html_e( 'Expertise', 'WL-BM' ); ?>:</label>
				<input type="text" name="supplier_expertise" id="wlbm_supplier_expertise" class="form-control">
			</div>

			<div class="col-md-12">
				<hr>
			</div>
		</div>
	</div>

	<div class="clearfix">
		<span class="float-right">
			<button type="submit" class="btn btn-primary" id="wlbm-registration-btn" data-message-title="<?php esc_attr_e( 'Confirm Registration', 'WL-BM' ); ?>" data-message-content="<?php esc_attr_e( 'Please confirm the registration.', 'WL-BM' ); ?>" data-cancel="<?php esc_attr_e( 'Cancel', 'WL-BM' ); ?>" data-submit="<?php esc_attr_e( 'Confirm', 'WL-BM' ); ?>">
				<i class="fas fa-user-plus"></i>&nbsp;
				<?php esc_html_e( 'Register', 'WL-BM' ); ?>
			</button>
		</span>
	</div>
</form>
