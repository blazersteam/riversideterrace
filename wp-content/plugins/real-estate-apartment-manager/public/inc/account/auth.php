<?php
defined( 'ABSPATH' ) || die();

$logged_in = is_user_logged_in();

if ( ! $logged_in ) {
	global $wpdb;

	$flats     = $wpdb->get_results( "SELECT f.ID, f.flat_number, f.floor_number, b.name as building_name FROM {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON f.building_id = b.ID ORDER BY b.name ASC, f.flat_number ASC" );
	$buildings = $wpdb->get_results( "SELECT ID, name FROM {$wpdb->prefix}wlbm_buildings ORDER BY name ASC" );
}
?>
<div class="wlbm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
			<?php
			if ( ! $logged_in ) {
				require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/account/forms/heading.php';
				require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/account/forms/register.php';
				require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/account/forms/login.php';
			} else {
				require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/account/profile/profile.php';
			}
			?>
			</div>
		</div>
	</div>
</div>
