<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Language {
	public static function load_translation() {
		load_plugin_textdomain( 'WL-BM', false, basename( WL_BM_PLUGIN_DIR_PATH ) . '/languages' );
	}
}
