<?php
defined( 'ABSPATH' ) || die();

$action = '';
if ( isset( $_GET['action'] ) && ! empty( $_GET['action'] ) ) {
	$action = sanitize_text_field( $_GET['action'] );
}

if ( 'book_flat' === $action ) {
	require_once( WL_BM_PLUGIN_DIR_PATH . 'public/inc/booking/flats/flat.php' );
} else {
	require_once( WL_BM_PLUGIN_DIR_PATH . 'public/inc/booking/flats/index.php' );
}
