<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

$building_id = '';
if ( isset( $_GET['building_id'] ) && ! empty( $_GET['building_id'] ) ) {
	$building_id = sanitize_text_field( $_GET['building_id'] );
}

$flats_building = '';
if ( $building_id ) {
	$building = $wpdb->get_row( $wpdb->prepare( "SELECT b.ID, b.name, b.address, b.excerpt, b.phone, b.email, b.images FROM {$wpdb->prefix}wlbm_buildings as b LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON b.ID = f.building_id WHERE b.show_in_listing = '1' AND b.ID = %d", $building_id ) );

	if ( ! $building ) {
		die();
	}

	$building_id      = $building->ID;
	$building_name    = $building->name;
	$building_excerpt = $building->excerpt;
	$building_images  = $building->images ? unserialize( $building->images ) : array();

	$flats_building = ' AND f.building_id = ' . $building_id . ' ';

} else {
	$buildings = $wpdb->get_results( "SELECT b.ID, b.name, b.address, b.excerpt, b.phone, b.email, b.featured_image FROM {$wpdb->prefix}wlbm_buildings as b LEFT OUTER JOIN {$wpdb->prefix}wlbm_flats as f ON b.ID = f.building_id WHERE b.show_in_listing = '1' GROUP BY b.ID ORDER BY b.name ASC" );

	$buildings_count = count( $buildings );
}

$query = "SELECT f.ID, f.flat_number, f.floor_number, f.images, f.featured_image, f.area, f.price, f.rental_price, f.rental_price_in_heading, f.title, f.excerpt, ft.type, b.ID as building_id, b.name as building_name, (SELECT COUNT(c.ID) FROM {$wpdb->prefix}wlbm_clients as c WHERE c.flat_id = f.ID AND c.is_active = '1') as active_clients_count FROM {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON f.building_id = b.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_flat_types as ft ON ft.ID = f.flat_type_id WHERE f.show_in_listing = '1'{$flats_building}GROUP BY f.ID ORDER BY b.name ASC, f.flat_number ASC";

$per_page    = 15;
$total_query = "SELECT COUNT(1) FROM ({$query}) AS combined_table";
$total       = $wpdb->get_var( $total_query );

$page   = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
$offset = ( $page * $per_page ) - $per_page;

$flats = $wpdb->get_results( $wpdb->prepare( "$query LIMIT %d, %d", $offset, $per_page ) );
?>
<div class="wlbm">
	<div class="container-fluid">

		<?php
		if ( $building_id ) {
			require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/booking/buildings/building.php';
		} else {
			require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/booking/buildings/index.php';
		}
		?>

		<div class="row">
			<div class="col-md-12">

				<?php if ( $total ) { ?>
				<div class="list-group wlbm-flat-list">

					<?php
					foreach( $flats as $flat ) {
						$id = $flat->ID;

						$title   = $flat->title;
						$excerpt = $flat->excerpt;

						if ( $flat->rental_price_in_heading ) {
							$price_in_heading = number_format_i18n( $flat->rental_price );
							$monthly          = true;
						} else {
							$price_in_heading = number_format_i18n( $flat->price );
							$monthly          = false;
						}

						if ( $monthly ) {
							$price_to_display = $price_in_heading . ' <small>' . esc_html__( 'monthly', 'WL-BM' ) . '</small>'; 
						} else {
							$price_to_display = $price_in_heading;
						}

						$featured_image = $flat->featured_image;

						$building_name = $flat->building_name;
						$type          = $flat->type;
						$flat_number   = $flat->flat_number;
						$floor_number  = $flat->floor_number;
					?>
					<div href="#" class="list-group-item list-group-item-action mb-3">
						<article>
							<div class="row">
								<?php if ( $featured_image ) { ?>
								<div class="col-sm-3 col-md-2 mb-3">
									<div class="wlbm-flat-thumbnail">
										<img src="<?php echo esc_url( wp_get_attachment_image_url( $featured_image, 'thumbnail' ) ); ?>" class="img-responsive" alt="<?php echo esc_attr( $title ); ?>">
									</div>
								</div>
								<?php } ?>
								<div class="<?php echo esc_html( $featured_image ? 'col-sm-9 col-md-10' : 'col-sm-12 col-md-12' ); ?>  mb-3">
									<header class="d-flex w-100 justify-content-between wlbm-flat-header">
										<h2 class="mb-1 h5 wlbm-flat-heading">
											<a class="text-dark" href="<?php echo esc_url( add_query_arg( array( 'action' => 'book_flat', 'id' => $id ) ) ); ?>">
												<?php echo esc_html( $title ); ?>
											</a>
										</h2>
										<?php if ( $price_in_heading > 0 ) { ?>
										<span class="wlbm-flat-price">
											<?php echo wp_kses( $price_to_display, array( 'small' => array() ) ); ?>
										</span>
										<?php } ?>
									</header>
									<p class="mb-1 wlbm-flat-excerpt"><?php echo esc_html( stripslashes( $excerpt ) ); ?></p>
									<footer class="wlbm-flat-footer">
										<small class="wlbm-flat-meta float-left">
											<ul class="list-group ml-3">
												<li>
													<span class="wlbm-flat-meta-key"><?php esc_html_e( 'Building' ); ?>:</span>
													<span class="wlbm-flat-meta-value"><?php echo esc_html( $building_name ); ?></span>
												</li>
												<?php if ( $type ) { ?>
												<li>
													<span class="wlbm-flat-meta-key"><?php esc_html_e( 'Flat Type' ); ?>:</span>
													<span class="wlbm-flat-meta-value"><?php echo esc_html( $type ); ?></span>
												</li>
												<?php } ?>
												<li>
													<span class="wlbm-flat-meta-key"><?php esc_html_e( 'Flat Number' ); ?>:</span>
													<span class="wlbm-flat-meta-value"><?php echo esc_html( $flat_number ); ?></span>
												</li>
												<li>
													<span class="wlbm-flat-meta-key"><?php esc_html_e( 'Floor Number' ); ?>:</span>
													<span class="wlbm-flat-meta-value"><?php echo esc_html( $floor_number ); ?></span>
												</li>
											</ul>
										</small>
										<span class="float-right">
											<a href="<?php echo esc_url( add_query_arg( array( 'action' => 'book_flat', 'id' => $id ) ) ); ?>">
												<?php esc_html_e( 'More Detail' ); ?>
											</a>
										</span>
									</footer>
								</div>
							</div>
						</article>
					</div>
					<?php
					} ?>

				</div>

				<nav class="navigation pagination float-right" role="navigation">
					<h2 class="screen-reader-text"><?php esc_html_e( 'Posts navigation', 'WL-BM' ); ?></h2>
					<div class="nav-links">
					<?php
					echo paginate_links(
							array(
								'base'      => add_query_arg( 'cpage', '%#%' ),
								'format'    => '',
								'prev_text' => '&laquo;',
								'next_text' => '&raquo;',
								'total'     => ceil( $total / $per_page ),
								'current'   => $page
							)
						);
					?>
					</div>
				</nav>

				<?php } else { ?>
				<div class="alert alert-info text-center">
					<?php esc_html_e( 'There is no flat to be listed.', 'WL-BM' ); ?>
				</div>
				<?php } ?>

			</div>
		</div>
	</div>
</div>
