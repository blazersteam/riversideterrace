<?php
defined( 'ABSPATH' ) || die();

global $wpdb;

if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
	$id   = absint( $_GET['id'] );
	$flat = $wpdb->get_row( $wpdb->prepare( "SELECT f.ID, f.flat_number, f.floor_number, f.area, f.price, f.rental_price, f.rental_price_in_heading, f.title, f.description, f.images, f.featured_image, ft.type, b.name as building_name, (SELECT COUNT(c.ID) FROM {$wpdb->prefix}wlbm_clients as c WHERE c.flat_id = f.ID AND c.is_active = '1') as active_clients_count FROM {$wpdb->prefix}wlbm_flats as f LEFT OUTER JOIN {$wpdb->prefix}wlbm_buildings as b ON f.building_id = b.ID LEFT OUTER JOIN {$wpdb->prefix}wlbm_flat_types as ft ON ft.ID = f.flat_type_id WHERE f.ID = %d AND f.show_in_listing = '1'", $id ) );
}
if ( ! $flat ) {
	die();
}
$id = $flat->ID;

$title       = $flat->title;
$description = $flat->description;
$images      = $flat->images ? unserialize( $flat->images ) : array();

$price        = number_format_i18n( $flat->price );
$rental_price = number_format_i18n( $flat->rental_price );

if ( $flat->rental_price_in_heading ) {
	$price_in_heading = $rental_price;
	$monthly = true;
} else {
	$price_in_heading = $price;
	$monthly = false;
}

if ( $monthly ) {
	$price_to_display = $price_in_heading . ' <small>' . esc_html__( 'monthly', 'WL-BM' ) . '</small>'; 
} else {
	$price_to_display = $price;
}

$featured_image = $flat->featured_image;

$building_name = $flat->building_name;
$type          = $flat->type;
$flat_number   = $flat->flat_number;
$floor_number  = $flat->floor_number;
?>
<div class="wlbm">
	<div class="container-fluid">

		<div class="row">
			<div class="col-md-12">

				<div class="mb-3">
					<article class="wlbm-flat-single">
						<?php if ( $images && is_array( $images ) && count( $images ) ) { ?>
						<div class="row">
							<div class="col-md-12">
								<div class="wlbm-bxslider">
									<?php
										$priority = array();
										foreach ( $images as $key => $image ) {
											$priority[ $key ] = $image['priority'];
										}
										array_multisort( $priority, SORT_ASC, $images );
									?>
									<?php foreach( $images as $key => $image ) { ?>
									<div>
										<img src="<?php echo esc_url( wp_get_attachment_image_url( $image['id'], 'full' ) ); ?>" title="<?php echo esc_attr( $image['title'] ); ?>">
									</div>
									<?php } ?>
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="row">
							<div class="col-md-12 mb-3">
								<header class="ml-2">
									<div class="d-flex w-100 justify-content-between wlbm-flat-header pr-1">
										<h1 class="mb-2 wlbm-flat-heading">
											<span><?php echo esc_html( $title ); ?></span>
										</h1>
										<?php if ( $price_in_heading > 0 ) { ?>
										<span class="wlbm-flat-price">
											<?php echo wp_kses( $price_to_display, array( 'small' => array() ) ); ?>
										</span>
										<?php } ?>
									</div>
									<div class="wlbm-flat-footer mr-1">
										<small class="wlbm-flat-meta float-left">
											<ul class="list-group ml-3">
												<li>
													<span class="wlbm-flat-meta-key"><?php esc_html_e( 'Building' ); ?>:</span>
													<span class="wlbm-flat-meta-value"><?php echo esc_html( $building_name ); ?></span>
												</li>
												<li>
													<span class="wlbm-flat-meta-key"><?php esc_html_e( 'Flat Type' ); ?>:</span>
													<span class="wlbm-flat-meta-value"><?php echo esc_html( $type ); ?></span>
												</li>
												<li>
													<span class="wlbm-flat-meta-key"><?php esc_html_e( 'Flat Number' ); ?>:</span>
													<span class="wlbm-flat-meta-value"><?php echo esc_html( $flat_number ); ?></span>
												</li>
												<li>
													<span class="wlbm-flat-meta-key"><?php esc_html_e( 'Floor Number' ); ?>:</span>
													<span class="wlbm-flat-meta-value"><?php echo esc_html( $floor_number ); ?></span>
												</li>
												<?php if ( $price > 0 ) { ?>
												<li>
													<span class="wlbm-flat-meta-key"><?php esc_html_e( 'Sell Price' ); ?>:</span>
													<span class="wlbm-flat-meta-value"><?php echo esc_html( $price ); ?></span>
												</li>
												<?php } ?>
												<?php if ( $rental_price > 0 ) { ?>
												<li>
													<span class="wlbm-flat-meta-key"><?php esc_html_e( 'Rental Price' ); ?>:</span>
													<span class="wlbm-flat-meta-value">
														<?php echo esc_html( $rental_price ); ?>
														<small><?php esc_html_e( 'monthly' ); ?></small>
													</span>
												</li>
												<?php } ?>
											</ul>
										</small>
										<span class="float-right">
											<a href="#wlbm-request-booking-form">
												<?php esc_html_e( 'More Detail' ); ?>
											</a>
										</span>
									</div>
								</header>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="mb-1 wlbm-flat-excerpt">
									<?php echo wp_kses_post( $description ); ?>
								</div>
							</div>
						</div>
					</article>
				</div>

				<div class="mt-3">
					<hr>
					<p><?php esc_html_e( 'Fill all the required fields (*).', 'WL-BM' ); ?></p>
					<form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" id="wlbm-request-booking-form">
						<?php
						$nonce_action = 'request-booking';
						$nonce = wp_create_nonce( $nonce_action );
						?>
						<input type="hidden" name="<?php echo esc_attr( $nonce_action ); ?>" value="<?php echo esc_attr( $nonce ); ?>">

						<input type="hidden" name="action" value="wlbm-request-booking">

						<input type="hidden" name="flat" value="2">

						<div class="row">

							<div class="form-group col-md-6">
								<label for="wlbm_name" class="font-weight-bold">* <?php esc_html_e( 'Your Name', 'WL-BM' ); ?>:</label>
								<input type="text" name="name" class="form-control" id="wlbm_name" placeholder="<?php esc_attr_e( 'Your Name', 'WL-BM' ); ?>">
							</div>

							<div class="form-group col-md-6">
								<label for="wlbm_phone" class="font-weight-bold">* <?php esc_html_e( 'Phone', 'WL-BM' ); ?>:</label>
								<input type="text" name="phone" class="form-control" id="wlbm_phone" placeholder="<?php esc_attr_e( 'Phone Number', 'WL-BM' ); ?>">
							</div>

							<div class="form-group col-md-6">
								<label for="wlbm_address" class="font-weight-bold"><?php esc_html_e( 'Address', 'WL-BM' ); ?>:</label>
								<textarea name="address" class="form-control" id="wlbm_address" rows="3" placeholder="<?php esc_attr_e( 'Address', 'WL-BM' ); ?>"></textarea>
							</div>

							<div class="form-group col-md-6">
								<label for="wlbm_email" class="font-weight-bold"><?php esc_html_e( 'Email', 'WL-BM' ); ?>:</label>
								<input type="email" name="email" class="form-control" id="wlbm_email" placeholder="<?php esc_attr_e( 'Email Address', 'WL-BM' ); ?>">
							</div>

						</div>

						<div class="row">
							<div class="col-md-12">
								<span class="float-right">
									<button type="submit" class="btn btn-success" id="wlbm-request-booking-btn">
										<i class="fas fa-forward"></i>&nbsp;
										<?php esc_html_e( 'Request for Booking', 'WL-BM' ); ?>
									</button>
								</span>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
</div>
