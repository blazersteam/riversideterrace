<?php
defined( 'ABSPATH' ) || die();

if ( $building_images && is_array( $building_images ) && count( $building_images ) ) {
?>
<div class="row justify-content-md-center">
	<?php
		$priority = array();
		foreach ( $building_images as $key => $image ) {
			$priority[ $key ] = $image['priority'];
		}
		array_multisort( $priority, SORT_ASC, $building_images );
	?>
	<?php foreach( $building_images as $key => $image ) { ?>
	<div class="col-sm-12 col-md-6">
		<div class="wlbm-box blue">
			<img src="<?php echo esc_url( wp_get_attachment_image_url( $image['id'], 'full' ) ); ?>" alt="<?php echo esc_attr( $image['title'] ); ?>">
			<div class="box-content">
				<i class="fas fa-building circle-icon"></i>
				<h3 class="title"><?php echo esc_html( $image['title'] ); ?></h3>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<div class="row">
	<div class="col-md-12">
		<p class="text-center text-secondary">
			<?php
			/* translators: %s: building name */
			printf( esc_html__( 'Showing flats in "%s"', 'WL-BM' ), $building_name );
			?>
		</p>
	</div>
</div>
<?php
}
