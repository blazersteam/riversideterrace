<?php
defined( 'ABSPATH' ) || die();

if ( $buildings_count ) {
?>
<div class="row">
	<?php
	foreach( $buildings as $building ) {
		$featured_image = $building->featured_image;
		if ( $featured_image ) {
	?>
	<div class="col-sm-12 col-md-6">
		<div class="wlbm-box blue">
			<img src="<?php echo esc_url( wp_get_attachment_image_url( $featured_image, 'full' ) ); ?>" alt="<?php echo esc_attr( $building->name ); ?>">
			<div class="box-content">
				<i class="fas fa-building circle-icon"></i>
				<h3 class="title"><?php echo esc_html( $building->name ); ?></h3>
				<p><?php echo esc_html( $building->excerpt ); ?></p>
				<p><a class="button" href="<?php echo esc_url( add_query_arg( array( 'building_id' => $building->ID ) ) ); ?>"><?php esc_html_e( 'More Detail', 'WL-BM' ); ?></a></p>
			</div>
		</div>
	</div>
	<?php
		}
	}
	?>
</div>
<?php
}

