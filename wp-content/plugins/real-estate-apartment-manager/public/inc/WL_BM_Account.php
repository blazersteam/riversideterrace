<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Account {
	public static function register() {
		try {
			ob_start();
			global $wpdb;

			$error_message = esc_html__( 'Error occurred while registration. Please notify the administrator.', 'WL-BM' );

			if ( ! wp_verify_nonce( $_POST['register'], 'register' ) ) {
				die();
			}

			$register_as  = isset( $_POST['register_as'] ) ? sanitize_text_field( $_POST['register_as'] ) : '';

			// User data.
			$username         = isset( $_POST['username'] ) ? sanitize_text_field( $_POST['username'] ) : '';
			$login_email      = isset( $_POST['email'] ) ? sanitize_text_field( $_POST['email'] ) : '';
			$password         = isset( $_POST['password'] ) ? $_POST['password'] : '';
			$password_confirm = isset( $_POST['password_confirm'] ) ? $_POST['password_confirm'] : '';

			if ( 'resident' === $register_as ) {
				// Client data.
				$client_flat_id = isset( $_POST['client_flat'] ) ? absint( $_POST['client_flat'] ) : NULL;
				$client_name    = isset( $_POST['client_name'] ) ? sanitize_text_field( $_POST['client_name'] ) : '';
				$client_address = isset( $_POST['client_address'] ) ? sanitize_text_field( $_POST['client_address'] ) : '';
				$client_phone   = isset( $_POST['client_phone'] ) ? sanitize_text_field( $_POST['client_phone'] ) : '';
				$client_email   = isset( $_POST['email'] ) ? sanitize_text_field( $_POST['email'] ) : '';
			} elseif ( 'supplier' === $register_as ) {
				// Supplier data.
				$supplier_buildings = ( isset( $_POST['supplier_building'] ) && is_array( $_POST['supplier_building'] ) ) ? $_POST['supplier_building'] : array();
				$supplier_name      = isset( $_POST['supplier_name'] ) ? sanitize_text_field( $_POST['supplier_name'] ) : '';
				$supplier_address   = isset( $_POST['supplier_address'] ) ? sanitize_text_field( $_POST['supplier_address'] ) : '';
				$supplier_phone     = isset( $_POST['supplier_phone'] ) ? sanitize_text_field( $_POST['supplier_phone'] ) : '';
				$supplier_email     = isset( $_POST['email'] ) ? sanitize_text_field( $_POST['email'] ) : '';
				$supplier_expertise = isset( $_POST['supplier_expertise'] ) ? sanitize_text_field( $_POST['supplier_expertise'] ) : '';
			}

			// Start validation.
			$errors = array();

			// User validation.
			if ( empty( $username ) ) {
				$errors['username'] = esc_html__( 'Please provide username.', 'WL-BM' );
			}

			if ( strlen( $username ) > 60 ) {
				$errors['username'] = esc_html__( 'Maximum length cannot exceed 60 characters.', 'WL-BM' );
			}

			if ( empty( $password ) ) {
				$errors['password'] = esc_html__( 'Please provide password.', 'WL-BM' );
			}

			if ( empty( $password_confirm ) ) {
				$errors['password_confirm'] = esc_html__( 'Please confirm password.', 'WL-BM' );
			}

			if ( $password !== $password_confirm ) {
				$errors['password'] = esc_html__( 'Passwords do not match.', 'WL-BM' );
			}

			if ( ! empty( $email ) && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				$errors['email'] = esc_html__( 'Please provide a valid email.', 'WL-BM' );
			}

			if ( 'resident' === $register_as ) {
				// Client validation.
				if ( empty( $client_name ) ) {
					$errors['client_name'] = esc_html__( 'Please provide your full name.', 'WL-BM' );
				}

				if ( strlen( $client_name ) > 255 ) {
					$errors['client_name'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
				}

				if ( empty( $client_phone ) ) {
					$errors['client_phone'] = esc_html__( 'Please provide your phone number.', 'WL-BM' );
				}

				if ( strlen( $client_phone ) > 255 ) {
					$errors['client_phone'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
				}

				if ( ! empty( $client_flat_id ) ) {
					$client_flat = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_flats WHERE ID = %d", $client_flat_id ) );
					if ( ! $client_flat ) {
						$errors['client_flat'] = esc_html__( 'Please select a valid flat.', 'WL-BM' );
					}
				} else {
					$errors['client_flat'] = esc_html__( 'Please select a valid flat.', 'WL-BM' );
				}

			} elseif ( 'supplier' === $register_as ) {
				// Supplier validation.
				if ( empty( $supplier_name ) ) {
					$errors['supplier_name'] = esc_html__( 'Please provide your full name.', 'WL-BM' );
				}

				if ( strlen( $supplier_name ) > 255 ) {
					$errors['supplier_name'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
				}

				if ( empty( $supplier_phone ) ) {
					$errors['supplier_phone'] = esc_html__( 'Please provide your phone number.', 'WL-BM' );
				}

				if ( strlen( $supplier_phone ) > 255 ) {
					$errors['supplier_phone'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
				}

				if ( ! empty( $supplier_expertise ) && strlen( $supplier_expertise ) > 255 ) {
					$errors['supplier_expertise'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
				}

				if ( ! is_array( $supplier_buildings ) || ! count( $supplier_buildings ) ) {
					$errors['supplier_building[]'] = esc_html__( 'Please select building(s).', 'WL-BM' );
				}

			} else {
				$errors['register_as'] = esc_html__( 'Please select an option.', 'WL-BM' );
			}
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $error_message;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				if ( 'resident' === $register_as ) {
					$data = array(
						'name'      => $client_name,
						'phone'     => $client_phone,
						'email'     => $client_email,
						'address'   => $client_address,
						'is_active' => '0',
						'flat_id'   => $client_flat_id
					);

					$user_data = array(
						'user_login' => $username,
						'user_email' => $login_email,
						'user_pass'  => $password,
					);

					$user_id = wp_insert_user( $user_data );
					if ( is_wp_error( $user_id ) ) {
						throw new Exception( esc_html( $user_id->get_error_message(), 'WL-BM' ) );
					}

					$user = new WP_User( $user_id );
					$user->add_cap( WLBM_CLIENT );

					$data['user_id']    = $user_id;
					$data['user_login'] = $user->user_login;
					$data['user_pass']  = $user->user_pass;

					$success = $wpdb->insert( "{$wpdb->prefix}wlbm_clients", $data );
					$message = esc_html__( 'You have been successfully registered.', 'WL-BM' );

				} elseif ( 'supplier' === $register_as ) {
					$data = array(
						'name'      => $supplier_name,
						'phone'     => $supplier_phone,
						'email'     => $supplier_email,
						'address'   => $supplier_address,
						'expertise' => $supplier_expertise,
						'is_active' => '0'
					);

					$user_data = array(
						'user_login' => $username,
						'user_email' => $login_email,
						'user_pass'  => $password,
					);

					$user_id = wp_insert_user( $user_data );
					if ( is_wp_error( $user_id ) ) {
						throw new Exception( esc_html( $user_id->get_error_message(), 'WL-BM' ) );
					}

					$user = new WP_User( $user_id );
					$user->add_cap( WLBM_SUPPLIER );

					$data['user_id']    = $user_id;
					$data['user_login'] = $user->user_login;
					$data['user_pass']  = $user->user_pass;

					$success = $wpdb->insert( "{$wpdb->prefix}wlbm_suppliers", $data );
					$message = esc_html__( 'You have been successfully registered.', 'WL-BM' );

					$supplier_id = $wpdb->insert_id;

					if ( count( $supplier_buildings ) > 0 ) {
						$values                  = array();
						$place_holders           = array();
						$place_holders_buildings = array();
						foreach ( $supplier_buildings as $building_id ) {
							array_push( $values, $building_id, $supplier_id );
							array_push( $place_holders, '(%d, %d)' );
							array_push( $place_holders_buildings, '%d' );
						}

						// Insert building_supplier records.
						$sql     = "INSERT IGNORE INTO {$wpdb->prefix}wlbm_building_supplier (building_id, supplier_id) VALUES ";
						$sql     .= implode( ', ', $place_holders );
						$success = $wpdb->query( $wpdb->prepare( "$sql ", $values ) );
					}

				} else {
					throw new Exception( $error_message );
				}

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $error_message );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_set_current_user( $user_id, $username );
				wp_set_auth_cookie( $user_id );
				do_action( 'wp_login', $username );

				wp_send_json_success( array( 'message' => $message, 'reload' => true ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}

	public static function login() {
		if ( ! wp_verify_nonce( $_POST['login'], 'login' ) ) {
			die();
		}

		$username = isset( $_POST['username'] ) ? $_POST['username'] : NULL;
		$password = isset( $_POST['password'] ) ? $_POST['password'] : NULL;

		$user = wp_authenticate( $username, $password );
		if ( is_wp_error( $user ) ) {
			wp_send_json_error( $user->get_error_message() );
		}

        wp_set_current_user( $user->ID, $user->user_login );
        wp_set_auth_cookie( $user->ID );
        do_action( 'wp_login', $user->user_login );

		wp_send_json_success( array( 'message' => esc_html__( "You are logged in successfully.", 'WL-BM' ), 'reload' => true ) );
	}
}
