<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Shortcode {
	public static function account( $attr ) {
		ob_start();
		require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/account/auth.php';
		return ob_get_clean();
	}

	public static function booking( $attr ) {
		ob_start();
		require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/booking/flats/route.php';
		return ob_get_clean();
	}

	public static function shortcode_assets() {
		global $post;
		if ( is_a( $post, 'WP_Post' ) ) {
			if ( has_shortcode( $post->post_content, 'ream_account' ) || has_shortcode( $post->post_content, 'ream_booking' ) ) {
				/* Enqueue styles */
				wp_enqueue_style( 'bootstrap4', WL_BM_PLUGIN_URL . 'assets/css/bootstrap.min.css' );
				wp_enqueue_style( 'font-awesome-free', WL_BM_PLUGIN_URL . 'assets/css/all.min.css' );
				wp_enqueue_style( 'jquery-confirm', WL_BM_PLUGIN_URL . 'assets/css/jquery-confirm.min.css' );
				wp_enqueue_style( 'toastr', WL_BM_PLUGIN_URL . 'assets/css/toastr.min.css' );
				wp_enqueue_style( 'bootstrap-select', WL_BM_PLUGIN_URL . 'assets/css/bootstrap-select.min.css' );
				wp_enqueue_style( 'bootstrap-datetimepicker', WL_BM_PLUGIN_URL . 'assets/css/bootstrap-datetimepicker.min.css' );
				wp_enqueue_style( 'wlbm-public', WL_BM_PLUGIN_URL . 'assets/css/wlbm-public.css' );

				/* Enqueue scripts */
				wp_enqueue_script( 'popper', WL_BM_PLUGIN_URL . 'assets/js/popper.min.js', array( 'jquery' ), true, true );
				wp_enqueue_script( 'bootstrap', WL_BM_PLUGIN_URL . 'assets/js/bootstrap.min.js', array( 'popper' ), true, true );
				wp_enqueue_script( 'jquery-confirm', WL_BM_PLUGIN_URL . 'assets/js/jquery-confirm.min.js', array( 'jquery' ), true, true );
				wp_enqueue_script( 'toastr', WL_BM_PLUGIN_URL . 'assets/js/toastr.min.js', array( 'jquery' ), true, true );
				wp_enqueue_script( 'bootstrap-select', WL_BM_PLUGIN_URL . 'assets/js/bootstrap-select.min.js', array( 'bootstrap' ), true, true );
				wp_enqueue_script( 'moment', WL_BM_PLUGIN_URL . 'assets/js/moment.min.js', array(), true, true );
				wp_enqueue_script( 'bootstrap-datetimepicker', WL_BM_PLUGIN_URL . 'assets/js/bootstrap-datetimepicker.min.js', array( 'bootstrap', 'moment' ), true, true );
				wp_enqueue_script( 'jQuery-print', WL_BM_PLUGIN_URL . 'assets/js/jQuery.print.js', array( 'jquery' ), true, true );

				wp_enqueue_script( 'wlbm-public', WL_BM_PLUGIN_URL . 'assets/js/wlbm-public.js', array( 'jquery' ), '1.0.0', true );
				wp_enqueue_script( 'wlbm-public-ajax', WL_BM_PLUGIN_URL . 'assets/js/wlbm-public-ajax.js', array( 'jquery', 'jquery-form' ), '1.0.0', true );
				wp_localize_script( 'wlbm-public-ajax', 'wlbmajaxurl', esc_url( admin_url( 'admin-post.php' ) ) );
				wp_localize_script( 'wlbm-public-ajax', 'wlbmadminurl', admin_url() );
			}

			if ( has_shortcode( $post->post_content, 'ream_booking' ) ) {
				wp_enqueue_style( 'jquery-bxslider', WL_BM_PLUGIN_URL . 'assets/css/jquery.bxslider.css' );
				wp_enqueue_script( 'jquery-bxslider', WL_BM_PLUGIN_URL . 'assets/js/jquery.bxslider.min.js', array( 'jquery' ), true, true );
				wp_enqueue_script( 'wlbm-slider', WL_BM_PLUGIN_URL . 'assets/js/slider.js', array( 'jquery', 'jquery-bxslider' ), '1.0.0', true );
			}
		}
	}
}
