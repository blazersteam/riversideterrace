<?php
defined( 'ABSPATH' ) || die();

class WL_BM_Flat_Booking {
	public static function book() {
		try {
			ob_start();
			global $wpdb;

			$error_message = esc_html__( 'Error occurred while request for booking. Please notify the administrator.', 'WL-BM' );

			if ( ! wp_verify_nonce( $_POST['request-booking'], 'request-booking' ) ) {
				die();
			}

			$flat_id = isset( $_POST['flat'] ) ? absint( $_POST['flat'] ) : NULL;
			$name    = isset( $_POST['name'] ) ? sanitize_text_field( $_POST['name'] ) : '';
			$phone   = isset( $_POST['phone'] ) ? sanitize_text_field( $_POST['phone'] ) : '';
			$address = isset( $_POST['address'] ) ? sanitize_text_field( $_POST['address'] ) : '';
			$email   = isset( $_POST['email'] ) ? sanitize_text_field( $_POST['email'] ) : '';

			// Start validation.
			$errors = array();

			if ( empty( $name ) ) {
				$errors['name'] = esc_html__( 'Please provide your name.', 'WL-BM' );
			}

			if ( strlen( $name ) > 255 ) {
				$errors['name'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( empty( $phone ) ) {
				$errors['phone'] = esc_html__( 'Please provide your phone number.', 'WL-BM' );
			}

			if ( strlen( $phone ) > 255 ) {
				$errors['phone'] = esc_html__( 'Maximum length cannot exceed 255 characters.', 'WL-BM' );
			}

			if ( ! empty( $email ) && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				$errors['email'] = esc_html__( 'Please provide a valid email.', 'WL-BM' );
			}

			if ( ! empty( $flat_id ) ) {
				$flat = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}wlbm_flats WHERE ID = %d AND show_in_listing = '1'", $flat_id ) );
				if ( ! $flat ) {
					die();
				}
			} else {
				$flat_id = NULL;
			}
			// End validation.

		} catch ( Exception $exception ) {
			$buffer = ob_get_clean();
			if ( ! empty( $buffer ) ) {
				$response = $error_message;
			} else {
				$response = $exception->getMessage();
			}
			wp_send_json_error( $response );
		}

		if ( count( $errors ) < 1 ) {
			try {
				$wpdb->query( 'BEGIN;' );

				$data = array(
					'name'    => $name,
					'phone'   => $phone,
					'email'   => $email,
					'address' => $address,
					'flat_id' => $flat_id
				);

				$success = $wpdb->insert( "{$wpdb->prefix}wlbm_bookings", $data );
				$message = esc_html__( 'Your booking request has been submitted. Thank You.', 'WL-BM' );

				$buffer = ob_get_clean();
				if ( ! empty( $buffer ) ) {
					throw new Exception( $buffer );
				}

				if ( $success === false ) {
					throw new Exception( $wpdb->last_error );
				}

				$wpdb->query( 'COMMIT;' );

				wp_send_json_success( array( 'message' => $message, 'reset' => true ) );
			} catch ( Exception $exception ) {
				$wpdb->query( 'ROLLBACK;' );
				wp_send_json_error( $exception->getMessage() );
			}
		}
		wp_send_json_error( $errors );
	}
}
