<?php
defined( 'ABSPATH' ) || die();

require_once WL_BM_PLUGIN_DIR_PATH . 'includes/constants.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/WL_BM_Language.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/WL_BM_Shortcode.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/WL_BM_Account.php';
require_once WL_BM_PLUGIN_DIR_PATH . 'public/inc/WL_BM_Flat_Booking.php';

/* Load translation */
add_action( 'plugins_loaded', array( 'WL_BM_Language', 'load_translation' ) );

/* Shortcodes */
add_shortcode( 'ream_account', array( 'WL_BM_Shortcode', 'account' ) );
add_shortcode( 'ream_booking', array( 'WL_BM_Shortcode', 'booking' ) );

/* Shortcode Assets */
add_action( 'wp_enqueue_scripts', array( 'WL_BM_Shortcode', 'shortcode_assets' ) );

/* Action for registration */
add_action( 'admin_post_nopriv_wlbm-register', array( 'WL_BM_Account', 'register' ) );

/* Action for login */
add_action( 'admin_post_nopriv_wlbm-login', array( 'WL_BM_Account', 'login' ) );

/* Action for booking request */
add_action( 'admin_post_wlbm-request-booking', array( 'WL_BM_Flat_Booking', 'book' ) );
add_action( 'admin_post_nopriv_wlbm-request-booking', array( 'WL_BM_Flat_Booking', 'book' ) );
