<?php
defined( 'ABSPATH' ) || die();

require_once WL_BM_PLUGIN_DIR_PATH . 'admin/inc/WL_BM_LM.php';

class WL_BM_Helper {
	public static function get_allowed_images() {
		return array( 'image/jpg', 'image/jpeg', 'image/png' );
	}

	public static function wl_bm_lm_valid() {
		$wl_bm_lm = WL_BM_LM::get_instance();
		$wl_bm_lm_val = $wl_bm_lm->is_valid();
		if ( isset( $wl_bm_lm_val ) && $wl_bm_lm_val ) {
			return true;
		}
		return false;
	}
}
