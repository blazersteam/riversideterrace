<?php
defined( 'ABSPATH' ) || die();

/* Menu slugs for manager */
define( 'WLBM_MENU_FBC_MANAGER', 'flat_booking_complaints' );
define( 'WLBM_MENU_MANAGER_BUILDINGS', 'ream_buildings' );
define( 'WLBM_MENU_MANAGER_FLAT_TYPES', 'ream_flat_types' );
define( 'WLBM_MENU_MANAGER_FLATS', 'ream_flats' );
define( 'WLBM_MENU_MANAGER_SUPPLIERS', 'ream_suppliers' );
define( 'WLBM_MENU_MANAGER_CLIENTS', 'ream_clients' );
define( 'WLBM_MENU_MANAGER_BOOKINGS', 'ream_bookings' );
define( 'WLBM_MENU_MANAGER_COMPLAINTS', 'ream_complaints' );

/* Menu slugs for Client */
define( 'WLBM_MENU_CLIENT_DASHBOARD', 'client_area' );
define( 'WLBM_MENU_CLIENT_COMPLAINTS', 'client_area_complaints' );

/* Menu slugs for Supplier */
define( 'WLBM_MENU_SUPPLIER_DASHBOARD', 'supplier_area' );
define( 'WLBM_MENU_SUPPLIER_COMPLAINTS', 'supplier_area_complaints' );

/* Custom capabilities */
define( 'WLBM_CLIENT', 'wlbm-client' );
define( 'WLBM_SUPPLIER', 'wlbm-supplier' );
