<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'terracer_livedb' );
define( 'WP_MEMORY_LIMIT', '256M' );
/** MySQL database username */
define( 'DB_USER', 'terracer_liveuse' );

/** MySQL database password */
define( 'DB_PASSWORD', 'zlEQ?kcw3gyl' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'a1GUZV2;i[sKIhcTJgEYo 5SE9dii(ls5y)I1?#23DLlaX$ {-,1REHM,RYL9foc' );
define( 'SECURE_AUTH_KEY',  ',?2RUUcyaesd~Oez@vR:d=`No$Q7:~gJ?v?{JV{Kvhlb?zu!^[)eWOF}y):f}r9i' );
define( 'LOGGED_IN_KEY',    'qRzt<N$Z^}X QWn&Mds%;?#%gNLxaj-5=7},o4x6rIznANe?H3=Lo@tD},Xt-~Mv' );
define( 'NONCE_KEY',        'F.w=`1swU8O1F:)}$<5@rKxe^(V:O (fRoIH%E?_P%mFB?7%`qjTF`LEKF5P2tt5' );
define( 'AUTH_SALT',        '8CMaZ0!2[z ?o~E7s0.3s[+:4%h^GwE`)3B>EV>]aoU:z]F4n1LGC{KU3[!m.8^x' );
define( 'SECURE_AUTH_SALT', 'S#E/;2 u:}) &;?tRgCk46:n:eH9*2vQ7_]3^TEnB-5?49*:9m7~lrx_p$1=i>?*' );
define( 'LOGGED_IN_SALT',   '(Ts6gh0x.W|Q|d~fiudse;G}#YfmmK&j0:vRt|[=a3-&TT>glG.CQOe]Tyh2B-8t' );
define( 'NONCE_SALT',       'niBo]>6b,ZC5L>pv-Jp7;|zP|c1vH>#{;z$jB>F*%tm.>:it?&LETQT6zVBEB[}g' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'rt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define('WP_DEBUG_DISPLAY', false);
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
